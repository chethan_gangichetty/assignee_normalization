import psycopg2
from datetime import datetime
import csv

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select ucid, normasg from dolceradata.normalizedcompany where ucid like 'US%'"

cur.execute(query)
writer = writeCSV("dup_reassignment_US_greaterthan20170101.csv")

reass_rec = []
reference = datetime(2017, 1, 1, 0, 0, 0, 0)

counter = 0
for idx,rec in enumerate(cur):
    ucid, normasg = rec
    counter = counter + 1
    if counter%100000 == 0:
        print counter
    try:
        if normasg.get(u'source',"") == u'usptoreasg':
            for i in normasg[u'reasgrec']:
                try:
                    if parser.parse(str(i.get(u'recorddate',""))) >= reference:
                        assignor = i.get(u'assignor',"")
                        assigneesuggest = i.get(u'assigneesuggest',"")
                        assignorisinv = i.get(u'assignorisinv',"")
                        assignorsuggest = i.get(u'assignorsuggest',"")
                        assignorimmediate = i.get(u'assignorimmediate',"")
                        assigneeparent = i.get(u'assigneeparent',"")
                        assignee = i.get(u'assignee',"")
                        conveyance = i.get(u'conveyance',"")
                        assigneeimmediate = i.get(u'assigneeimmediate',"")
                        assignorparent = i.get(u'assignorparent',"")
                        date = i.get(u'date',"")
                        recorddate = i.get(u'recorddate',"")
                        convtype = i.get(u'convtype',"")
                        reel = i.get(u'reel',"")
                        assigneeisinv = i.get(u'assigneeisinv',"")
#                         reass_rec.append([ucid,assignor, assigneesuggest, assignorisinv, 
#                                           assignorsuggest, assignorimmediate, assigneeparent, 
#                                           assignee, conveyance, assigneeimmediate, 
#                                           assignorparent, date, recorddate, 
#                                           convtype, reel, assigneeisinv])
                        writer.writerow([ucid,assignor, assigneesuggest, assignorisinv, assignorsuggest, assignorimmediate, assigneeparent, assignee, conveyance, assigneeimmediate, assignorparent, date, recorddate,convtype, reel, assigneeisinv])
                except:
                    continue
    except:
        continue

# for i in reass_rec:
#     writer.writerow(i)
