import psycopg2
from datetime import datetime
import csv
from collections import defaultdict

def is_ascii(self,s):
    return all(ord(c) < 128 for c in s)

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select publication_id, normasg from dolceradata.normalizedcompany"

cur.execute(query)
writer = writeCSV("pubids_echo.csv")
pubids_echo = []

counter = 0
for idx,rec in enumerate(cur):
    pubid, normasg = rec
    counter = counter + 1
    if counter%100000 == 0:
        print counter
    try:
        for i in normasg[u'currentassigneesnormdata']:
            try:
                if i[u'source'] == 'echo':
                    pubids_echo.append(pubid)
                    writer.writerow([pubid])
            except:
                continue
    except:
        continue
