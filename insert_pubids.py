import psycopg2
from datetime import datetime
import csv

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

reader = readCSV("pubids_echo.csv")
conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor()
query = "insert into dolceradata.reindex values(%s)"
counter = 0
to_add = set()
for row in reader:
    counter = counter+1
    if counter%1000 == 0:
        dataText = ','.join([cur.mogrify('(%s)', row) for row in to_add])
        cur.execute('INSERT INTO  dolceradata.reindex(publication_id) VALUES '+dataText)
        print counter
        to_add = set()
        conn.commit()
    to_add.add(tuple([row[0]]))
    
    cur.execute(query,tuple([row[0]]))

if len(to_add)>0:
    dataText = ','.join([cur.mogrify('(%s)', row) for row in to_add])
    cur.execute('INSERT INTO  dolceradata.reindex(publication_id) VALUES '+dataText)
    conn.commit()
