import json
import csv
import psycopg2
from collections import defaultdict
import pandas as pd
import os
import cPickle as pkl

def standardize_array_logarithmic(array,desc=True,logbase=100.0):
    omin=min(array)
    omax=max(array)
    #print array
    if omax!=omin and omax!=0:
        for i in range(len(array)):
#        array[i]=math.log10(float(remap(array[i] * 1.0 ,omin,omax,1.0,10.0)))
            try:
                array[i]=remap(array[i] * 1.0 ,omin,omax,1.0001,logbase)
                array[i]=math.log(array[i],logbase)
            except:
                print "standardize_array_logarithmic() error"
                quit()
    #else:
        #print array
    #print array
    return array
def get_patent_status_lookup_table(filename):
    if filename=='':
        return { 'APA' : 'g', 'APA0' : 'p', 'ARA1' : 'p', 'ARA2' : 'g', 'ARA3' : 'g', 'ATA' : 'p', 'ATB' : 'g', 'ATT' : 'g', 'ATU1' : 'p', 'ATA5' : 'p', 'ATB8' : 'g', 'ATB9' : 'g', 'ATE' : 'g', 'ATU' : 'g', 'ATU2' : 'g', 'ATU3' : 'g', 'ATXE' : 'g', 'AUA' : 'P', 'AUA1' : 'p', 'AUA4' : 'g', 'AUA8' : 'g', 'AUB2' : 'g', 'AUB8' : 'g', 'AUB9' : 'g', 'AUC' : 'g', 'AUC1' : 'g', 'AUD0' : 'p', 'AUA0' : 'p', 'AUA5' : 'p', 'AUB1' : 'g', 'AUB3' : 'g', 'BAA' : 'p', 'BAB' : 'g', 'BAB1' : 'g', 'BEA1' : 'p', 'BEA2' : 'p', 'BEA4' : 'g', 'BEA5' : 'g', 'BE99' : 'p', 'BEA' : 'g', 'BEA3' : 'g', 'BEA6' : 'g', 'BEA7' : 'g', 'BEA8' : 'g', 'BEA9' : 'g', 'BEAA' : 'p', 'BEAB' : 'p', 'BEAC' : 'g', 'BEAD' : 'g', 'BEAE' : 'g', 'BEAF' : 'g', 'BEAG' : 'p', 'BEAH' : 'g', 'BEBF' : 'g', 'BET1' : 'g', 'BET2' : 'g', 'BEX' : 'g', 'BGA' : 'p', 'BGB1' : 'g', 'BGA1' : 'g', 'BGA2' : 'g', 'BGA3' : 'g', 'BGA4' : 'g', 'BGB2' : 'g', 'BGD' : 'g', 'BGS' : 'g', 'BGT' : 'g', 'BGU' : 'g', 'BGU1' : 'p', 'BGY1' : 'g', 'BGY2' : 'g', 'BNA' : 'p', 'BOA' : 'p', 'BRA' : 'p', 'BRA2' : 'p', 'BRB1' : 'g', 'BRD0' : 'p', 'BRE2' : 'p', 'BRA0' : 'p', 'BRA3' : 'p', 'BRC1' : 'g', 'BRC2' : 'g', 'BRC3' : 'g', 'BRC4' : 'g', 'BRU' : 'p', 'BRU8' : 'p', 'CAA1' : 'p', 'CAA2' : 'p', 'CAC' : 'g', 'CAA' : 'g', 'CAAA' : 'p', 'CAB' : 'g', 'CHA' : 'p', 'CHA5' : 'g', 'CHA9' : 'g', 'CHB' : 'g', 'CHA3' : 'p', 'CHA4' : 'p', 'CHA8' : 'p', 'CHB9' : 'g', 'CHC1' : 'p', 'CHC2' : 'p', 'CHH1' : 'p', 'CHH2' : 'p', 'CLA1' : 'p', 'CLA2' : 'p', 'CLA3' : 'p', 'CNA' : 'p', 'CNB' : 'g', 'CNC' : 'g', 'CNU' : 'p', 'CNY' : 'g', 'CNT' : 'p', 'COA1' : 'p', 'COA2' : 'p', 'CRA' : 'g', 'CSA1' : 'p', 'CSA2' : 'p', 'CSA3' : 'p', 'CSB' : 'g', 'CSB1' : 'g', 'CSB2' : 'g', 'CSB3' : 'g', 'CSB4' : 'g', 'CSB6' : 'g', 'CSL' : 'p', 'CSM' : 'p', 'CSP' : 'g', 'CUA1' : 'p', 'CUA' : 'p', 'CUB' : 'g', 'CUO' : 'p', 'CUP' : 'g', 'CYA' : 'g', 'CZA3' : 'p', 'CZB6' : 'g', 'CZU1' : 'g', 'DDA1' : 'p', 'DDA2' : 'p', 'DDA3' : 'g', 'DDA4' : 'g', 'DDA5' : 'g', 'DDA6' : 'g', 'DDA7' : 'g', 'DDA8' : 'g', 'DDA9' : 'p', 'DDAA' : 'p', 'DDAB' : 'g', 'DDAC' : 'g', 'DDAH' : 'p', 'DDB1' : 'g', 'DDB3' : 'g', 'DDB5' : 'g', 'DDC' : 'g', 'DDC2' : 'g', 'DDC4' : 'g', 'DDC5' : 'g', 'DDD' : 'g', 'DDS' : 'g', 'DDT' : 'g', 'DDT9' : 'g', 'DDU' : 'g', 'DDW' : 'g', 'DDY' : 'g', 'DDZ' : 'g', 'DEA1' : 'p', 'DEB1' : 'g', 'DEB2' : 'g', 'DEB3' : 'g', 'DEB4' : 'g', '41975' : 'g', '41976' : 'g', 'DED1' : 'g', 'DEI1' : 'p', 'DEI2' : 'g', 'DET' : 'p', 'DET0' : 'p', 'DET1' : 'p', 'DET2' : 'g', 'DET3' : 'g', 'DET4' : 'g', 'DET5' : 'p', 'DET8' : 'g', 'DEU' : 'p', 'DEU1' : 'p', 'DEA8' : 'p', 'DEA8' : 'p', 'DEA' : 'g', 'DEA0' : 'p', 'DEB' : 'g', '36861' : 'g', '41974' : 'g', 'DKA' : 'p', 'DKB' : 'p', 'DKB1' : 'g', 'DKC' : 'g', 'DKD0' : 'p', 'DKT3' : 'g', 'DKT4' : 'g', 'DKA0' : 'p', 'DKA4' : 'p', 'DKA5' : 'p', 'DKB2' : 'g', 'DKB3' : 'g', 'DKB4' : 'g', 'DKT1' : 'p', 'DKT2' : 'p', 'DKT5' : 'g', 'DKU1' : 'p', 'DKU3' : 'g', 'DKU4' : 'g', 'DKX0' : 'p', 'DKY3' : 'g', 'DKY4' : 'g', 'DKY5' : 'g', 'DKY6' : 'g', 'DOA' : 'g', 'EAA1' : 'p', 'EAA2' : 'p', 'EAA3' : 'p', 'EAB1' : 'g', 'ECA' : 'p', 'EEA' : 'p', 'EEB1' : 'g', 'EEU1' : 'p', 'EGA' : 'p', 'EPA1' : 'p', 'EPA2' : 'p', 'EPA3' : 'p', 'EPA4' : 'p', 'EPA8' : 'p', 'EPA9' : 'p', 'EPB1' : 'g', 'EPB2' : 'g', 'EPB3' : 'g', 'EPB8' : 'g', 'EPB8' : 'g', 'EPB9' : 'g', 'EPTD' : 'p', 'ESA1' : 'p', 'ESA6' : 'p', 'ESB1' : 'g', 'ESD0' : 'p', 'EST3' : 'g', 'ESA2' : 'g', 'ESA3' : 'g', 'ESA4' : 'g', 'ESA5' : 'g', 'ESA7' : 'g', 'ESA8' : 'g', 'ESAA' : 'p', 'ESAB' : 'p', 'ESAC' : 'p', 'ESAF' : 'g', 'ESBA' : 'g', 'ESBB' : 'g', 'EST1' : 'p', 'EST2' : 'p', 'EST4' : 'g', 'EST5' : 'g', 'ESU' : 'p', 'ESU4' : 'p', 'ESUA' : 'p', 'ESY' : 'p', 'ESY1' : 'p', 'ESY4' : 'p', 'ESY8' : 'p', 'ESYA' : 'g', 'FIA' : 'p', 'FIB' : 'g', 'FIB1' : 'g', 'FIC' : 'g', 'FID0' : 'p', 'FIA0' : 'p', 'FIU0' : 'p', 'FIU1' : 'g', 'FIX0' : 'p', 'FRA' : 'p', 'FRA1' : 'p', 'FRA2' : 'p', 'FRA5' : 'p', 'FRB1' : 'g', 'FRB2' : 'g', 'FRA3' : 'p', 'FRA4' : 'p', 'FRA6' : 'g', 'FRA7' : 'g', 'FRA8' : 'g', 'FRB3' : 'g', 'FRB4' : 'g', 'FRE' : 'g', 'FRM' : 'g', 'GBA' : 'g', 'GBA9' : 'p', 'GBB' : 'g', 'GBD0' : 'p', 'GBB8' : 'g', 'GBA8' : 'p', 'GBA0' : 'p', 'GBA1' : 'p', 'GBB2' : 'g', 'GBC' : 'g', 'GRA1' : 'p', 'GRT3' : 'g', 'GRA' : 'p', 'GRB' : 'g', 'GRB1' : 'g', 'GRB2' : 'g', 'GRT1' : 'p', 'GRU' : 'p', 'GRY' : 'g', 'GTA' : 'p', 'GTU' : 'p', 'HKA' : 'p', 'HKA1' : 'g', 'HKA2' : 'g', 'HKA3' : 'p', 'HNA' : 'g', 'HRA2' : 'p', 'HRB1' : 'g', 'HRA1' : 'p', 'HUA2' : 'p', 'HUA3' : 'p', 'HUB' : 'g', 'HUB1' : 'g', 'HUB3' : 'g', 'HUD0' : 'p', 'HUA0' : 'p', 'HUA1' : 'p', 'HUAA' : 'p', 'HUAB' : 'p', 'HUAC' : 'p', 'HUP' : 'g', 'HUU' : 'g', 'HUU0' : 'p', 'HUX' : 'p', 'HUX1' : 'p', 'HUX2' : 'p', 'IDA' : 'p', 'IEA1' : 'p', 'IEB1' : 'g', 'IEL' : 'p', 'IEA2' : 'p', 'IEB' : 'g', 'IEB3' : 'g', 'ILA' : 'p', 'ILD0' : 'p', 'ILA0' : 'p', 'ILA1' : 'p', 'INA' : 'p', 'ISA' : 'p', 'ITA1' : 'p', 'ITB' : 'g', 'ITB1' : 'g', 'ITD0' : 'p', 'ITA' : 'g', 'ITA0' : 'p', 'ITA2' : 'p', 'ITA3' : 'p', 'ITA4' : 'p', 'ITT1' : 'p', 'ITT2' : 'g', 'ITU' : 'g', 'ITU0' : 'p', 'ITU1' : 'p', 'ITU2' : 'p', 'ITU3' : 'p', 'ITU4' : 'p', 'ITY1' : 'g', 'JPA' : 'p', 'JPB' : 'g', 'JPB1' : 'g', 'JPB2' : 'g', 'JPC' : 'g', 'JPU' : 'p', 'JPY2' : 'p', 'JPT1' : 'p', 'JPA2' : 'p', 'JPB4' : 'g', 'JPT2' : 'p', 'JPU1' : 'p', 'JPU2' : 'p', 'KEA' : 'g', 'KEA1' : 'g', 'KRA' : 'p', 'KRB1' : 'g', 'KRU' : 'p', 'KRY1' : 'p', 'KRB2' : 'g', 'KRXA' : 'g', 'KRXU' : 'p', 'KRY2' : 'g', 'LTA' : 'p', 'LTB' : 'p', 'LTR3' : 'p', 'LTA3' : 'p', 'LUA1' : 'p', 'LUA9' : 'p', 'LUA' : 'p', 'LUA2' : 'p', 'LUA4' : 'p', 'LUA7' : 'p', 'LVA' : 'p', 'LVB' : 'p', 'LVA3' : 'g', 'LVA4' : 'p', 'LVB4' : 'g', 'MCA' : 'g', 'MCA1' : 'g', 'MDA' : 'p', 'MDB1' : 'p', 'MDB2' : 'p', 'MDC1' : 'g', 'MDC2' : 'g', 'MDF1' : 'g', 'MDF2' : 'g', 'MDU' : 'p', 'MDW1' : 'g', 'MDW2' : 'g', 'MDY1' : 'p', 'MDY2' : 'p', 'MNA1' : 'g', 'MNT' : 'g', 'MTA' : 'g', 'MWA' : 'g', 'MXA' : 'p', 'MXB' : 'g', 'MXA1' : 'p', 'MXA2' : 'p', 'MXA3' : 'p', 'MXU' : 'p', 'MXXA' : 'g', 'MXXB' : 'g', 'MXXU' : 'g', 'MYA' : 'p', 'MYA1' : 'g', 'NIA' : 'p', 'NLA' : 'p', 'NLA1' : 'p', 'NLB' : 'p', 'NLC' : 'g', 'NLC2' : 'g', 'NLI1' : 'p', 'NLI2' : 'g', 'NLC1' : 'g', 'NOA' : 'p', 'NOB' : 'p', 'NOB1' : 'g', 'NOC' : 'g', 'NOD0' : 'p', 'NOI2' : 'g', 'NOA0' : 'p', 'NOA1' : 'p', 'NOB2' : 'g', 'NOB3' : 'g', 'NZA' : 'p', 'OAA' : 'g', 'PAA' : 'p', 'PEA1' : 'p', 'PHA' : 'p', 'PHS' : 'g', 'PHU' : 'g', 'PHZ' : 'g', 'PLA1' : 'p', 'PLB1' : 'g', 'PLA2' : 'p', 'PLA3' : 'p', 'PLA4' : 'p', 'PLB' : 'g', 'PLB2' : 'g', 'PLB3' : 'g', 'PLB4' : 'g', 'PLL' : 'g', 'PLM' : 'g', 'PLO' : 'p', 'PLP' : 'g', 'PLU1' : 'p', 'PLU3' : 'p', 'PLY1' : 'g', 'PLY3' : 'g', 'PTA' : 'p', 'PTB' : 'g', 'PTE' : 'g', 'PTA1' : 'p', 'PTA4' : 'p', 'PTB1' : 'g', 'PTB4' : 'g', 'PTT' : 'g', 'PTU' : 'p', 'PTX' : 'p', 'PTY' : 'g', 'PYA1' : 'g', 'PYA3' : 'g', 'ROB' : 'g', 'ROA' : 'p', 'ROA3' : 'g', 'ROB0' : 'g', 'ROB1' : 'g', 'ROB2' : 'g', 'ROB3' : 'g', 'ROB4' : 'g', 'ROC' : 'g', 'ROC1' : 'g', 'ROL' : 'g', 'ROM' : 'g', 'ROP' : 'g', 'RUA' : 'p', 'RUC1' : 'g', 'RUC2' : 'g', 'RUC9' : 'g', 'RUA1' : 'p', 'RUA2' : 'p', 'RUA3' : 'g', 'RUA4' : 'g', 'RUD' : 'g', 'RUS' : 'g', 'RUT' : 'g', 'RUU' : 'g', 'SEA' : 'p', 'SEB' : 'p', 'SEC' : 'g', 'SED0' : 'p', 'SEL' : 'p', 'SEA0' : 'p', 'SEC2' : 'g', 'SEC5' : 'g', 'SEC9' : 'g', 'SEX0' : 'p', 'SGA' : 'p', 'SGA1' : 'p', 'SGA2' : 'p', 'SIA' : 'g', 'SIB' : 'g', 'SIT1' : 'g', 'SIA1' : 'g', 'SIA2' : 'g', 'SIA8' : 'g', 'SIC' : 'g', 'SIC1' : 'g', 'SIC2' : 'g', 'SIC8' : 'g', 'SIT2' : 'g', 'SKA3' : 'p', 'SKB6' : 'g', 'SKA5' : 'p', 'SMA' : 'p', 'SUA3' : 'g', 'SUA1' : 'p', 'SUA2' : 'p', 'SUA4' : 'g', 'SUC' : 'g', 'SUD' : 'g', 'SUS' : 'g', 'SUT' : 'p', 'SUU' : 'p', 'SVA' : 'p', 'THA' : 'g', 'THS' : 'g', 'THZ' : 'g', 'TJA' : 'p', 'TJA3' : 'p', 'TJB' : 'g', 'TJU' : 'g', 'TRT1' : 'p', 'TRT2' : 'p', 'TRA' : 'g', 'TRA1' : 'p', 'TRA2' : 'p', 'TRA3' : 'p', 'TRB' : 'g', 'TRU' : 'p', 'TRY' : 'g', 'TWB' : 'g', 'TWU' : 'g', 'TWA' : 'p', 'TWY' : 'g', 'UAC2' : 'g', 'USA' : 'g', 'USA1' : 'p', 'USA2' : 'p', 'USA9' : 'p', 'USB1' : 'g', 'USB2' : 'g', 'USE1' : 'g', 'USP' : 'g', 'USS1' : 'g', 'USA4' : 'p', 'USAA' : 'p', 'USAB' : 'p', 'USB3' : 'g', 'USB8' : 'g', 'USB9' : 'g', 'USBA' : 'g', 'USBB' : 'g', 'USC1' : 'g', 'USC2' : 'g', 'USC3' : 'g', 'USE' : 'g', 'USF1' : 'g', 'USH' : 'p', 'USH1' : 'p', 'USP1' : 'p', 'USP2' : 'g', 'USP3' : 'g', 'USP4' : 'p', 'USP9' : 'p', 'USX' : 'g', 'USXB' : 'g', 'USXH' : 'p', 'USXT' : 'p', 'UYA' : 'p', 'UYA1' : 'g', 'UYA2' : 'g', 'UYA3' : 'g', 'VEA1' : 'g', 'VNA1' : 'g', 'VNA3' : 'g', 'VNU' : 'p', 'WOA1' : 'p', 'WOA2' : 'p', 'WOA3' : 'p', 'WOA4' : 'p', 'WOA8' : 'p', 'WOA9' : 'p', 'WOB1' : 'p', 'WOB8' : 'p', 'WOB9' : 'p', 'WOC1' : 'p', 'WOC2' : 'p', 'YUA' : 'p', 'YUA5' : 'p', 'YUB' : 'g', 'ZAA' : 'p', 'ZAA1' : 'g', 'ZMA' : 'g', 'ZMA1' : 'g', 'ZMA5' : 'g', 'ZWA' : 'p', 'ZWA5' : 'p', 'INA1' : 'g', 'ATA2' : 'p', 'AUA2' : 'p', 'CHB5' : 'g', 'DEA5' : 'p', 'DEB8' : 'g', 'DEB9' : 'g', 'DET9' : 'p', 'ESB' : 'g', 'ESB2' : 'g', 'HKA0' : 'p', 'HUT' : 'g', 'INB' : 'g', 'INI1' : 'p', 'INI2' : 'p', 'INI4' : 'p', 'INP1' : 'p', 'INP2' : 'p', 'INP3' : 'p', 'INP4' : 'p', 'JPX' : 'p', 'KRB' : 'g', 'NLC6' : 'g', 'PHB1' : 'g', 'SUA' : 'g', 'VNA' : 'p', 'PHA' : 'p', 'PHB' : 'g', 'PHZ' : 'g', 'VNB' : 'g' }
    else:
        with open(filename) as f:
            d = dict(filter(None, csv.reader(f)))
        return d

def lookup_patent_status(l_table,l_value):
    if l_value in l_table:
        return l_table[l_value]
    else:
        return 'p'
    
status_table = get_patent_status_lookup_table("")

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def getLitigatedPatents():
    lit_patents = set()
    for filename in os.listdir("Litigation"):
        if ".csv" not in filename:
            continue
        reader = readCSV("Litigation/"+filename)
        for row in reader:
            if len(row)>0:
                lit_patents.add(row[0])
    return lit_patents

lit_patents = getLitigatedPatents()

import gc
gc.disable()
pubid_ucid_dict = pkl.load(open("pubid_ucid.pkl","rb"))
gc.enable()
gc.collect()

con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
cursor = con.cursor('iter')
query = """select * from dolceratesting.patentranking_321_claim_analytics"""
cursor.execute(query)
claim_stats = {}
for rindex,row in enumerate(cursor):
    if rindex%100000==0:
        print rindex,"Reading claims stats mapping"
    if row[1] not in claim_stats:
        claim_stats[pubid_ucid_dict.get(row[1],row[1])] = [pubid_ucid_dict.get(row[1],row[1])]+[r for r in row[2:]]
    else:
        if row[3] > claim_stats[pubid_ucid_dict.get(row[1],row[1])][2]:
            claim_stats[pubid_ucid_dict.get(row[1],row[1])] = [pubid_ucid_dict.get(row[1],row[1])]+[r for r in row[2:]]
        elif row[3] == claim_stats[pubid_ucid_dict.get(row[1],row[1])][2] and row[4] > claim_stats[pubid_ucid_dict.get(row[1],row[1])][3]:
            claim_stats[pubid_ucid_dict.get(row[1],row[1])] = [pubid_ucid_dict.get(row[1],row[1])]+[r for r in row[2:]]
        else:
            pass

cursor.close()
con.close()

import pandas as pd
fam_dict = {}
fam_pointer = {}
breakValue = 100000
assignee_dict = {}
forward_cit  = {}
lit_patents_final = set()

def chunck_generator(filename, header=False,chunk_size = 10 ** 5):
    for chunk in pd.read_csv(filename,delimiter=',',header=None, iterator=True, chunksize=chunk_size, parse_dates=[1] ): 
        yield (chunk)

def generator( filename, header=False,chunk_size = 10 ** 5):
    chunk = chunck_generator(filename, header=False,chunk_size = 10 ** 5)
    for row in chunk:
        yield row
   
filename = r'/home/premrajn/Assignee_Normalization_Scripts/INVDATA1.csv'
generator = generator(filename=filename)
count = 0

# while True:
#     print(count)
#     count+=1
#     reader = next(generator)

while True:
    print(count)
    count+=1
    try:
        reader = next(generator)
    except:
        break
    for index, row in reader.iterrows():
        if row[0].replace("-","") in lit_patents:
            lit_patents_final.add(row[0])
        elif row[0].rsplit("-",1)[0].replace("-","") in lit_patents:
            lit_patents_final.add(row[0])
        else:
            pass
        backward_citation = str(row[9]).split("|")
        assignee_dict[row[0]]= set(str(row[3]).split("|")+str(row[6]).split("|"))
        for cit in backward_citation:
            if cit not in forward_cit:
                forward_cit[cit] = []
            forward_cit[cit].append(row[0])
        if row[11] != "-1":
            if row[11] not in fam_dict:
                fam_dict[row[11]] = []
            fam_dict[row[11]].append(row[0])
            fam_pointer[row[0]] = row[11]
            
forward_cit_processed = defaultdict(list)
for pn in forward_cit:
    assignees = set(assignee_dict.get(pn,set([])))
    for cit in forward_cit[pn]:
        cit_assignees = assignee_dict.get(cit,[])
        if len(assignees.intersection(cit_assignees)) > 0:
            continue
        forward_cit_processed[pn].append(cit)
        

# import cPickle as pkl
# with open("step3.pkl","wb") as pk:
#     pkl.dump((fam_pointer,fam_dict,assignee_dict,forward_cit,lit_patents_final),pk)
#     
gc.collect()


con2 = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
inv_data_path = "/home/premrajn/Assignee_Normalization_Scripts/INVDATA1.csv"
cursor2 = con2.cursor()
reader = readCSV(inv_data_path)
to_add = set()

import multiprocessing
import time
from multiprocessing import Queue

def vworker(x,vqueue):
    print('vworker',x)
    n1 = time.time()
    con2 = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
    cursor2=con2.cursor()
    while True:
        rec=vqueue.get()
        if rec is None:
            if len(to_add) > 0:
                dataText = ','.join([cursor2.mogrify('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)', row) for row in to_add])
                cursor2.execute('INSERT INTO dolceratesting.patentranking_321_ranking_data(pn,family,cpc,ipc,len_fwd_cit,total_count,ind_count,ind_claim_length,dep_count,patent_status,len_jurisdictions,family_size,kudelski_family_size,litigation_flag) VALUES '+dataText)
                con2.commit()
                to_add = set()
            print('returning vworker',x)
            print(time.time()-n1)
            break 
        else:
            rindex,row=rec
            if rindex %10000 ==0:
                print('doing',rindex)
            cpc = row[7].split("||")[0]
            ipc = row[8].split("||")[0]
            if row[0] in fam_pointer:
                fam = fam_dict[fam_pointer[row[0]]]
            else:
                fam = [row[0]]
            fwd_citations = []
            for f in fam:
                temp = forward_cit_processed.get(f,[])
                for citation in temp:
                    fwd_citations.append(temp)

            total_count,ind_count,ind_claim_length,dep_count=0 ,0 ,0,0
            row[0] = row[0].split()[0]
            if row[0] in claim_stats:
                pn,total_count,ind_count,ind_claim_length,dep_count = claim_stats[row[0]]
            
            ##Patent Status
            patent_parts = row[0].split("-")
            country = patent_parts[0]
            kindcode = patent_parts[-1]
            patent_status = lookup_patent_status(status_table,country+kindcode)
            if patent_status == 'g':
                patent_status = "1"
            else:
                patent_status = "0"
            #Filed Jurisdictions
            jurisdictions = set()
            for f in fam:
                jurisdictions.add(f[:2])
            
            family_size = len(fam)
            kudelski_family_size = set()
            for f in fam:
                if "US" in f or "EP" in f:
                    patent_parts = f.split("-")
                    country = patent_parts[0]
                    kindcode = patent_parts[-1]
                    status = lookup_patent_status(status_table,country+kindcode)
                    if status == "g":
                        kudelski_family_size.add(f)
            litigation_flag = 0
            if row[0] in lit_patents_final:
                litigation_flag = 1
            kudelski_family_size = len(kudelski_family_size)
            to_add.add((row[0],fam_pointer.get(row[0],"-1"),cpc,ipc,len(fwd_citations),total_count
                          ,ind_count,ind_claim_length,dep_count,patent_status,len(jurisdictions),
                          family_size,kudelski_family_size,litigation_flag))
            if len(to_add) ==  10000:
                dataText = ','.join([cursor2.mogrify('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)', row) for row in to_add])
                cursor2.execute('INSERT INTO dolceratesting.patentranking_321_ranking_data(pn,family,cpc,ipc,len_fwd_cit,total_count,ind_count,ind_claim_length,dep_count,patent_status,len_jurisdictions,family_size,kudelski_family_size,litigation_flag) VALUES '+dataText)
                to_add = set()
                con2.commit()
                print('dump',rindex,x)
            

import psycopg2

num_workers = 30
p1 = [multiprocessing.Process(target=vworker, args=(x,vqueue)) for x in range(num_workers)]

for f in p1:
    f.start()

for rindex,row in enumerate(reader):
    vqueue.put((rindex,row))
    
for x in range(num_workers):
    vqueue.put(None)
for f in p1:
    f.join()  
    

    

