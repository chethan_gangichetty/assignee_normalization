import psycopg2
from collections import defaultdict
import csv

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolceradata.normalizedcompany"

cur.execute(query)

clusters = {"ARMOUR PHARMA","ALLIED CHEM","YANG JING","THERMOS CORP","NISSEI LTD",
            "FURUKAWA KOGYO CORP","AKER SUBSEA LTD",
            "ASS ELECT IND","CADENCE DESIGN SYS INC",
            "CAMCO INC","CROUZET SA","DANFOSS AS",
            "DECODE GENETICS EHF","EMS TECH INC",
            "EVEREADY BATTERY CO INC","FLUOR TECH CORP",
            "GIFU PREFECTURE","GTE SYLVANIA INC","HOESCH WERKE AG"
            ,"HOOVER BALL AND BEARING CO","HUACHEN GROUP AUTO HLDGS CO LTD","IRWIN IND TOOL CO","JEOL LTD",
            "KEYES FIBRE CO","KIENZLE APP GMBH","KONINK MINOLTA PHOTO IMAGING INC",
            "KUNSHAN GOVISIONOX OPTOELECTRONICS CO LTD","LEXTAR ELECTRONICS CORP",
            "LG HOUSEHOLD AND HEALTH CARE LTD","MARCONI CO LTD","MC GRAW EDISON CO",
            "MC TECHNOLOGIES GMBH","MERIAL INC","MERIAL LTD",
            "OPENWAVE SYS INC","OSRAM SYLVANIA INC","PFAFF AG G M","REPUBLIC STEEL CORP",
            "RICHARDSON MERRELL INC","RICHARDSON VICKS INC","ROGERS CORP","SHOWA CORP",
            "TELECOM SA","TELETYPE CORP","TOKAI KOGYO CO LTD",
            "TOYOTA IND CORP","UNION OIL CO","UNITED TECH CORP","VIA TECH INC",
            "INTELLECTUAL PROPERTY LTD"}

new_clusters = {
    'AMERICAN STANDARD INC': 'Ingersoll-Rand Plc',
    'CARL ZEISS': 'Jenoptik AG',
    'DIAMOND SHAMROCK CORP': 'Maxus Energy Corporation',
    'FUJI ELECTRONICS IND CO LTD':'Fuji Electronics Industries co.,LTD',
    'GEN INSTR CORP''NIKKO CO LTD': 'Vishay Intertechnology, Inc.',
    'SINGER CO': 'Retail Holdings N.V.',
    'TELEDYNE IND': 'Microchip Technology Incorporated',
    'VICKERS LTD': 'Lallemand Inc.',
    'VOEST ALPINE AG': 'Dr. Helmut Rothenberger-Privatstiftung'
}

norm_ucid = defaultdict(list)
ucid_pubid = {}
counter = 0
uid = set()
for pubid, ucid, normasg,_,_ in cur:
    counter += 1
    if counter%100000 == 0:
        print counter
    if not normasg:
        uid.add(ucid)
        continue
    if normasg[u'currentassigneesnormdata'] != []:
        cleaned = normasg[u'currentassigneesnormdata'][0].get(u'cleaned','')
        name = normasg[u'currentassigneesnormdata'][0].get(u'name','')
        parent = normasg[u'currentassigneesnormdata'][0].get(u'parent','')
        if (cleaned != '' or name != ''):
            if cleaned.upper() in clusters:
                norm_ucid[cleaned.upper()].append(ucid)
                ucid_pubid[ucid] = pubid
            if name.upper() in clusters:
                norm_ucid[name.upper()].append(ucid)
                ucid_pubid[ucid] = pubid
            if cleaned.upper() in new_clusters:
                if str(parent) == new_clusters[cleaned.upper()]:
                    norm_ucid[cleaned.upper()].append(ucid)
                    ucid_pubid[ucid] = pubid
            if name.upper() in new_clusters:
                if str(parent) == new_clusters[name.upper()]:
                    norm_ucid[name.upper()].append(ucid)
                    ucid_pubid[ucid] = pubid
                
writer = writeCSV("norm_ucid.csv")
for i in norm_ucid:
    writer.writerow([i,norm_ucid[i]])
writer2 = writeCSV("ucid_pubid.csv")
for i in ucid_pubid:
    writer2.writerow([i,ucid_pubid[i]])
writer=writeCSV("ucids_normasg_null.csv")
for i in uid:
    writer.writerow(i)