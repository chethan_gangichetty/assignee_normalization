import csv
import psycopg2
from collections import defaultdict

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')                                 
    return fileOutput

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select publication_id, normasg from dolceradata.normalizedcompany where ucid ilike 'US%'"
cur.execute(query)
writer = writeCSV("pubids_reasgrec.csv")

counter = 0
for idx,rec in enumerate(cur):
    pubid, normasg = rec
    counter = counter + 1
    if counter%10000 == 0:
        print counter
    try:
        if 'reasgrec' in normasg and len(normasg['reasgrec'])>0:
            writer.writerow(pubid)
    except:
        continue
