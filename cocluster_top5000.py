import requests
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con7 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur7 = con7.cursor('iter-7')
cur7.itersize = 10000
# query7 = "select * from dolcera.an_cocluster_merges where similarity_distance != 0 order by intersection desc limit 5000"
query7 = "select * from dolcera.an_cocluster_merges order by intersection desc limit 5000"
cur7.execute(query7)

xxx = writeCSV("cocluster_top5000.csv")

counter = 0
for a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14 in cur7:
    counter = counter + 1
    if counter%1000 == 0:
        print counter
    xxx.writerow([a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14])

