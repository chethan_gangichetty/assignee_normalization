import requests
from collections import defaultdict
import regex
import psycopg2


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


all_ids = set()
session = requests.session()
asian_lookup = defaultdict(list)
id_cache = {}


def getAllParents(idd):
    loop = 1
    idd = idd.split("-")[0]
    all_parents_aliases = set()
    counter = 0
    while loop == 1:
        # print loop
        counter = counter + 1
        if counter > 20:
            break
        if idd not in id_cache:
            url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
            url = url.replace("{{idd}}", idd)
            data = session.get(url).json()
            id_cache[idd] = data
        else:
            data = id_cache[idd]

        main_names = []
        for docindex, doc in enumerate(data["response"]["docs"]):
            doc_id = doc["id"]
            main_names.append(doc_id)
            try:
                ulti = doc["immParentID"][0]
            except:
                continue
            if docindex == 0:
                if ulti == idd:
                    loop = 0
                else:
                    idd = ulti
        all_parents_aliases.update(main_names)

    return all_parents_aliases


def pop_token(candidate):
    words = candidate.split(" ")
    if len(words[0]) > 3:
        pop = words[0]
    elif len(words) > 1:
        pop = words[0] + " " + words[1]
    else:
        pop = candidate
    return pop


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


if __name__ == "__main__":
    conn = psycopg2.connect(
        "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
    cur = conn.cursor()
    reader = readCSV("CLEANMATCH.csv")

    id_name = {}
    par_name = {}
    for rindex, row in enumerate(reader):
        if len(row) < 8:
            continue
        id_name[row[5]] = row[6].replace("^", ",")
        all_ids.add(row[5])
        start_letter = row[1][0]
        if not is_ascii(start_letter):
            asian_lookup[row[5]].append(row[1])

    print "length of all ids", len(all_ids)
    for index, idd in enumerate(all_ids):
        pars = getAllParents(idd)
        for p in pars:
            cur.execute('insert into dolceradata.corporatetree values(%s,%s)', (id_name[idd], p))
        if index % 1000 == 0:
            conn.commit()
            print index
    conn.commit()

