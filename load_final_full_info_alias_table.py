import csv
import psycopg2

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "insert into dolcera.rerun_final_full_info_alias_table values (%s, %s, %s, %s, %s,%s, %s, %s, %s, %s)"

reader = readCSV("final_full_alias_table.csv")
counter = 0
for u,o,c,cty,f,fl,std,strip,cstd,cstrip in reader:
    c = eval(c)
    cty = eval(cty)
    cur2.execute(query2,(u,o,c,cty,f,fl,std,strip,cstd,cstrip))
    counter+=1
    if counter%10000 == 0:
        print counter
        con2.commit()
con2.commit()