con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.an_final_alias_table"
cur.execute(query)

assgn_freq_dict = {}
assgn_countries_dict = {}
assgn_city_dict = {}

count = 0

for ulti, orig, countries, cities, freq in cur:
    count = count + 1
    if count%1000000 == 0:
        print count, "freq, country, city"
    assgn_freq_dict[strip_accents(orig)] = freq
    assgn_countries_dict[orig] = countries
    assgn_city_dict[orig] = cities

reader = readCSV("CLEANMATCH_OLD.csv")
old_ultiid = {}
old_immid = {}
old_dict = defaultdict(list)
for row in reader:
#     print row
#     break
# for ulti_std,orig_std,orig_Name,Cluster_rep,Std_Name,Capiq_ID, Capiq_Orig_Name,Ulti_ID,Ulti_Flag,Ulti_Parent,Immediate_Parent,Industry,Sub_Count,City,Website,Country,Score,CPC_Grade,Ind_Score,Flag,Ulti_geo, source in reader:
    if len(row)>5 and row[1] not in old_ultiid:
        old_ultiid[row[1]] = row[7]
        old_immid[row[1]] = row[5].split('-')[0]
        old_dict[row[1]] = row

reader = readCSV("CLEANMATCH.csv")
new_ultiid = {}
new_immid = {}
new_dict = defaultdict(list)
for row in reader:
#     print row
#     break
# for ulti_std,orig_std,orig_Name,Cluster_rep,Std_Name,Capiq_ID, Capiq_Orig_Name,Ulti_ID,Ulti_Flag,Ulti_Parent,Immediate_Parent,Industry,Sub_Count,City,Website,Country,Score,CPC_Grade,Ind_Score,Flag,Ulti_geo, source in reader:
    if len(row)>5 and row[1] not in new_ultiid:
        new_ultiid[row[1]] = row[7]
        new_immid[row[1]] = row[5].split('-')[0]
        new_dict[row[1]] = row

diff_cleanmatch = writeCSV("diff_cleanmatch_subset.csv")
for i in new_ultiid:
    if i in old_ultiid:
        if new_ultiid[i] != old_ultiid.get(i,"") and old_immid.get(i,"") != new_immid[i]:
            array = []
            array.extend(old_dict[i][:-1])
            if len(old_dict[i]) == 18:
                array.extend(['NA','NA','NA','NA','NA'])
            else:
                array.extend(['NA','NA','NA','NA','NA','NA'])
            array.append(assgn_freq_dict[strip_accents(i)])
            array.append(old_dict[i][-1])
            array.append("Before")
            diff_cleanmatch.writerow(array)
            array = []
            array.extend(new_dict[i])
            array.append("After")
            diff_cleanmatch.writerow(array)
