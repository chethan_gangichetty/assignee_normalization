import requests
from collections import defaultdict
import regex
import psycopg2
import tldextract
import cleanco
from unidecode import unidecode_expect_nonascii
import unidecode
from unidecode import unidecode
import unicodedata
import regex as re

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

def get_foundation_website(id_):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
    url = url.replace("{{idd}}", id_)
    data = session.get(url).json()
    if len(data['response']['docs'])>0:
        for i in data['response']['docs']:
            foundation = i.get('foundation','')
            website = i.get('website','')    
    else:
        foundation = ''
        website = ''
    return foundation, website

def get_ultiid_ultiname(id_):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
    url = url.replace("{{idd}}", id_)
    data = session.get(url).json()
    if len(data['response']['docs'])>0:
        for i in data['response']['docs']:
            ultiid = i.get('ultimateParentID','')[0]
            ultiname = i.get('ultiParent','')    
    else:
        ultiid = ''
        ultiname = ''
    return ultiid, ultiname

cst = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/CompanyStatusType.txt","rb").read()
cst = cst.split("#@#@#")
cst_ = {}
for i in cst:
    j = i.split("'~'")
    try:
        cst_[j[0]] = j[1]
    except:
        print j
        
ct = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/CompanyType.txt","rb").read()
ct = ct.split("#@#@#")
ct_ = {}
for i in ct:
    j = i.split("'~'")
    try:
        ct_[j[0]] = j[1]
    except:
        print j
        
ind = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/SimpleIndustry.txt","rb").read()
ind = ind.split("#@#@#")
ind_ = {}
for i in ind:
    j = i.split("'~'")
    try:
        ind_[j[0]] = j[1]
    except:
        print j
        

cg = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/CountryGeo.txt","rb").read()
cg = cg.split("#@#@#")
cg_ = {}
for i in cg:
    j = i.split("'~'")
    try:
        cg_[j[0]] = j[1]
    except:
        print j
        
sct = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/SearchCompanyNameType.txt","rb").read()
sct = sct.split("#@#@#")
sct_ = {}
for i in sct:
    j = i.split("'~'")
    try:
        sct_[j[0]] = j[1]
    except:
        print j
        

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor()
query = "insert into dolcera.an_foundation_company values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s)"

found_info = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/FoundationCompany.txt","rb").read()
found_info = found_info.split("#@#@#")

counter = 0
for line in found_info:
    if counter%10000 == 0:
        print counter, "Foundation file"
        con.commit()
    counter = counter+1
    line = line.split("'~'")
    if len(line) > 1:
        dummy = []
        dummy.append(line[0])
        dummy.append(unidecode_expect_nonascii(strip_accents(line[1])))
        dummy.append(unidecode_expect_nonascii(strip_accents(line[2])))
        dummy.append(cst_.get(line[3],""))
        dummy.append(ct_.get(line[4],""))
        dummy.extend(line[5:8])
        dummy.append(ind_.get(line[8],""))
        dummy.extend(line[9:18])
        dummy.append(sct_.get(line[18],""))
        dummy.append(cg_.get(line[19],""))
        dummy.extend(line[20:])
        cur.execute(query,tuple(dummy))
con.commit()

# ID - 0
# Name - 1
# City - 2
# Companystatustype.txt - 3
# Company Type - 4
# Fax - 5
# Contact Number - 6
# Contact Number - 7
# Industry - 8
# Mailing Address - 9
# Street - 10
# District - 11#
# Address - 12#
# foundation - 13
# f14
# f15
# Zipcode - 16
# webiste - 17
# SearchCompanyType-18
# Country - 19
# f20
# f21
# f22