#!/usr/bin/env python
import solr
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
import unicodedata
from collections import defaultdict
import re
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
csv.field_size_limit(sys.maxsize)
ADDRESS="localhost"
CAPIQ_CSV_FILEPATH="FCformat.txt"
CAPIQ_FILEPATH=""
ASN_PATH=""
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter='^', quotechar="'")
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("+"," ")
    g= g.replace("?"," ")
    g= g.replace("&","and")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
class indexCapIq(object):
    def __init__(self):
        self.s = solr.SolrConnection('http://'+ADDRESS+':8983/solr/companycore')
        self.relations={}
        self.articles = [" THE "," AND "]
        self.relationsCount = {}
        self.altNames=defaultdict(list)
        self.processCountry()
        self.processIndustry()
        self.processRelations()
        self.processAltNames()
        self.processVariations()
        self.processVariations1()
        print 'Here'
        self.iterateCapitalIQFile()
        
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def getIndustry(self,country):
        return self.industryCode.get(country,"")
    def processCountry(self):
        data = open(CAPIQ_FILEPATH+"CountryGeo.txt","rb").read()
        data = data.split("#@#@#")
        self.countryCode={}
        for row in data:
            row = row.split("'~'")
            if len(row)<2:
                continue
            print row
            self.countryCode[row[0]]=row[1]
    def processIndustry(self):
        data = open(CAPIQ_FILEPATH+"SimpleIndustry.txt","rb").read()
        data = data.split("#@#@#")
        self.industryCode={}
        for row in data:
            row = row.split("'~'")
            if len(row)!=2:
                continue
            print row
            self.industryCode[row[0]]=row[1]
    def processRelations(self):
        data = open(CAPIQ_FILEPATH+"CompanyRel.txt","rb").read()
        reader = data.split("#@#@#")
        self.relations={}
        self.relationsCount = {}
        for rindex,row in enumerate(reader):
            if rindex%100000==0:
                print rindex,"Reading company relations"
            row = row.split("'~'")
            if row[0] == "D":
                continue
            if len(row)<5:
                continue
            if row[4]=="5" or row[4]=="7" or row[4]=="9":
                #print row
                self.relations[row[3]]=row[2]
        for row in self.relations:
            sub = row
            par = self.relations.get(sub,sub)
            counter=0 
            while par!=sub:
                counter=counter+1
                if counter>20:
                    break
                self.relationsCount[par] = self.relationsCount.get(par,0)+1
                par = self.relations[sub]
    def processAltNames(self):
        data = open(CAPIQ_FILEPATH+"AlternateCompanyNames.txt","rb").read()
        reader = data.split("#@#@#")
        self.altNames=defaultdict(list)
        for row in reader:
            if len(row)<4:
                continue
            row = row.split("'~'")
            self.altNames[row[2]].append(row[3])
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"../AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            print row
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"../Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def strip_accents(self,s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," ")) ##Prem
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
    def iterateCapitalIQFile(self):
        data = open(CAPIQ_FILEPATH+"FoundationCompany.txt","rb").read()
        reader = data.split("#@#@#")
        zzz=writeCSV("Errors.csv")
        allowedCompanyTypes=set(["4","5","6","7","17","20"])
#         3 4 6 7 9 17 18 20 21
        self.names = {}
        for rindex,row in enumerate(reader):
            if rindex%10000==0:
                print rindex,"Getting names"
            row = row.split("'~'")
            if len(row)<2:
                continue
            name = row[1]
            idd = row[0]
            self.names[idd] = name
        for rindex,row in enumerate(reader):
            #print rindex
            row = row.split("'~'")
            if rindex%100000==0:
                print rindex
                self.s.commit()
            
            try:
                if row[4] not in allowedCompanyTypes:
                    continue
                subsidiary_count=0
                country=""
                city=""
                country=unidecode_expect_nonascii(strip_accents(self.getCountry(row[19])))
                industry = unidecode_expect_nonascii(strip_accents(self.getIndustry(row[8])))
                city=unidecode_expect_nonascii(strip_accents(row[2]))
                name=unidecode_expect_nonascii(strip_accents(row[1]))
                idd=unidecode_expect_nonascii(strip_accents(row[0]))
                foundation = row[13]
                immId=self.relations.get(idd,idd)
                immParent = unidecode_expect_nonascii(strip_accents(self.names.get(immId,"None")))
                try:
                    website = tldextract.extract(row[17]).registered_domain
                except Exception,e:
                    website = ""
                ultiId=idd
                ultiFlag=0
                loopcounter=0
                while ultiId in self.relations and self.relations[ultiId]!=ultiId and loopcounter<100:
                    ultiId=self.relations.get(ultiId,ultiId)
                    loopcounter=loopcounter+1
                ultiParent = unidecode_expect_nonascii(strip_accents(self.names.get(ultiId,"None")))
                if idd==ultiId:
                    ultiFlag=1
                subsidiary_count = self.relationsCount.get(idd,0)
                ori_name=unidecode_expect_nonascii(strip_accents(name))
                std_name=self.getStandardizedName(name)
                std_name=removeSpaces(std_name)
                stripped_name=self.getStrippedName(std_name)
                self.s.add(id=idd,original_name=ori_name,standardized_name=std_name,stripped_name=stripped_name,standardized_name_keywords=std_name,stripped_name_ws=stripped_name.replace(" ",""),stripped_name_keywords=stripped_name,country=country,city=city,ultimateFlag=str(ultiFlag),immParentID=immId,ultimateParentID=ultiId,
                               industry=industry,immParent=immParent,ultiParent=ultiParent,subsidiary_count = subsidiary_count, website = website, foundation=foundation)
                for entryindex,entry in enumerate(self.altNames[idd]):
#                     name=unidecode_expect_nonascii(entry.replace(".","").replace(",",""))
                    name=unidecode_expect_nonascii(strip_accents(entry)).replace(".","").replace(",","")
#                     name=unidecode_expect_nonascii(entry.replace(".","").replace(",",""))
#                     name=strip_accents(entry.replace(".","").replace(",","")) ##Prem
#                     ori_name=unidecode_expect_nonascii(name)
#                     ori_name=strip_accents(unicode(name,encoding= 'utf-8'))
                    ori_name=name
                    std_name=self.getStandardizedName(name)
                    std_name=removeSpaces(std_name)
                    stripped_name=self.getStrippedName(std_name)
                    self.s.add(id=idd+"-"+str(entryindex),original_name=ori_name,standardized_name=std_name,stripped_name=stripped_name,standardized_name_keywords=std_name,stripped_name_ws=stripped_name.replace(" ",""),stripped_name_keywords=stripped_name,country=country,city=city,ultimateFlag=str(ultiFlag),immParentID=immId,ultimateParentID=ultiId,
                               industry=industry,immParent=immParent,ultiParent=ultiParent,subsidiary_count = subsidiary_count, website = website,foundation=foundation)
            except Exception,e:
                print e
                print std_name,stripped_name,country,city
                zzz.writerow([idd])
        self.s.commit()
if __name__=="__main__":
    g=indexCapIq()
