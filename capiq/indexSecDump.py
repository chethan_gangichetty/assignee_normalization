from mysolr import Solr
x = Solr('http://localhost:8984/solr/companycore',version=4)
x.delete_by_query('*:*',commit=True)

import solr
import psycopg2
from unidecode import unidecode_expect_nonascii
import unicodedata
ADDRESS="localhost"
s = solr.SolrConnection('http://'+ADDRESS+':8984/solr/companycore')

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "select * from dolcera.sec_dump2"

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

cur2.execute(query2)
counter = 0
for row in cur2:
#     if row[0] == '6c28a4f3ecbdc2aa91a1df59fd096a21':
    counter+=1
    if counter%1000 == 0:
        print counter
    ori_name=unidecode_expect_nonascii(strip_accents(row[1])).replace(".","").replace(",","")
    try:
        s.add(id=row[0],original_name=ori_name,standardized_name=row[2],stripped_name=row[3],standardized_name_keywords=row[4],stripped_name_ws=row[5],stripped_name_keywords=row[6],country=row[7],city=row[8],ultimateFlag=row[9],immParentID=row[10],ultimateParentID=row[11],industry=row[12],immParent=row[13],ultiParent=row[14],subsidiary_count=row[15], website=row[16],foundation=row[17])
    except:
        print row[0]
s.commit()
