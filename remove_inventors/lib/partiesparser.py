import xml.etree.ElementTree as ET
import psycopg2
import pprint
#import networkx as nx
#from patentnumberparser import pnparser


class partiesparser:
    def __init__(self, pxmltext):
        self.tbl = {ord(c): u' ' for c in u'.-,;'}
        self.data = {'inv': None, 'appl': None, 'asg': None, 'agent': None, 'examiner': None, 'reassignments': None}
        self.reassignments = None
        self.pxml = ET.fromstring(pxmltext)
        self.xpaths = {'appl': './applicants/applicant', 'inv': './inventors/inventor', 'asg': './assignees/assignee',
                       'agent': './agents/agent', 'examiner': './examiners/examiner',
                       'reasg': './assignee-history/reassignments/reassignment'}

    def getpartiesdata(self, fields):
        for field in fields:
            self.data[field] = self.parsepartydata(field)

    def getreasgdata(self):
        datafinal = []
        for reassignment in self.pxml.findall(self.xpaths['reasg']):
            dates = []
            mindate = None
            assignors = []
            assignees = []
            for x in reassignment.findall('./assignors/assignor'):
                if 'execution-date' in x.attrib:
                    dates.append(int(x.attrib['execution-date']))
                assignor = self.parseaddressbook(x[0])
                assignors.append(assignor)
            if dates:
                mindate = min(dates)
            try:
                reelframe = reassignment.attrib['reel-frame'].strip()
            except:
                reelframe = None
                pass
            try:
                recorddate=reassignment.attrib['recorded-date']
            except:
                recorddate = None
                pass
            conv = reassignment.findall('.//conveyance')
            for x in reassignment.findall('./assignees/assignee'):
                assignee = self.parseaddressbook(x[0])
                assignees.append(assignee)
            datafinal.append([mindate, reelframe, conv[0].text, assignees, assignors, ET.tostring(reassignment),recorddate])
        self.data['reassignments'] = [x for x in sorted(datafinal, key=lambda x: (x[0], x[1]), reverse=True)]

    def __str__(self):
        pstr = pprint.pformat(self.data)
        return pstr

    def parsepartydata(self, partyname):
        pxmllist = self.pxml.findall(self.xpaths[partyname])
        recs = []
        recdata = []
        atlist = ['load-source', 'format']
        for pxml in pxmllist:
            if pxml is not None or len(pxml) > 0:
                names = []
                inventor = pxml
                tup = {}

                attributes = inventor.attrib
                if 'sequence' not in attributes:
                    continue
                tup['sequence'] = attributes['sequence']
                for at in atlist:
                    # print at

                    if at in attributes:
                        # print at+' in '+ str(attributes)

                        # print str(attributes[at])+' in '+str(sourcepref)
                        tup[at] = attributes[at]
                        # print 'appended: '+str(sourcepref[attributes[at]])

                    else:
                        tup[at] = ''
                tup.update(self.parseaddressbook(inventor[0]))

                recs.append(tup)

        finalrec = {}
        slots = ['fname', 'lname', 'name', 'city', 'state', 'country', 'address-1', 'address-2', 'address-3',
                 'postcode']
        for rec in recs:
            # print inventor
            try:
                recdata.append(rec)
            except:
                print 'error getting:' + str(rec)
        # print recdata

        return recdata

    def parseaddressbook(self, addressbookxml):
        fname = ''
        lname = ''
        name = ''
        city = ''
        state = ''
        country = ''
        postcode = ''
        res = {}
        for child in addressbookxml:

            if child.tag == 'first-name':
                fname += child.text if child.text else ''
            if child.tag == 'last-name':
                lname += child.text if child.text else ''
            if child.tag == 'name':
                name = child.text if child.text else ''
            if child.tag.find('address') <> -1:
                for childadr in child:
                    if childadr.tag == 'city':
                        city = childadr.text if childadr.text else ''
                    if childadr.tag == 'state':
                        state = childadr.text if childadr.text else ''
                    if childadr.tag == 'country':
                        country = childadr.text if childadr.text else ''
                    if childadr.tag.find('address') <> -1:
                        res[childadr.tag] = childadr.text if childadr.text else ''
                    if childadr.tag == 'postcode':
                        postcode = childadr.text if childadr.text else ''
        if not name:
            name = lname + ' ' + fname
        if name.strip():
            res['name'] = name.strip()
        if fname:
            res['fname'] = fname.strip()
        if lname and lname.strip() <> name.strip():
            res['lname'] = lname.strip()
        if state.strip():
            res['state'] = state.strip()
        if city.strip():
            res['city'] = city.strip()
        if country.strip():
            res['country'] = country.strip()
        if postcode.strip():
            res['postcode'] = postcode.strip()
        return res


import csv

if __name__ == '__main__':
    assignees = set()
    #numparser = pnparser()
    pnlist = []
    # with open('data/curasgtestset.csv','rU') as inf:
    #     inr=csv.reader(inf)
    #     for pn, in inr:
    #         pnlist.append(numparser.getstandardizedpn(pn.strip()))
    pnlistlist = []
    # for x in xrange(0,len(pnlist),50):
    #     pnlistlist.append(pnlist[x:x+50])
    # print pnlistlist[-1]
    pnlistlist = [['US-20020116601-A1']]
    con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    cur = con.cursor()
    for pns in pnlistlist:
        inq = '(\'' + '\',\''.join(pns) + '\')'
        cur.execute(
            "select b.publication_id,content  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in" + inq)
        for id, partyxmltext in cur:
            parser = partiesparser(partyxmltext)
            parser.getpartiesdata(['inv', 'asg', 'appl', 'agent', 'examiner'])
            parser.getreasgdata()
            print parser
        #     print parser
        #     for key in parser.data:
        #         if key in ['inv', 'asg', 'appl']:
        #             for rec in parser.data[key]:
        #                 assignees.add(rec['name'])
        #                 # pass
        #                 print key, rec['name'], rec['sequence']
        #         elif key == 'reassignments':
        #             for rec in parser.data[key]:
        #                 for adb in rec[3]:
        #                     assignees.add(adb['name'])
        #                 for adb in rec[4]:
        #                     assignees.add(adb['name'])
        # break
    cur.close()
    con.close()

    # for name in assignees:
    #     print name
