
from unidecode import unidecode_expect_nonascii
import re
from cleanco import cleanco
import csv
import sys
import unicodedata
import os
csv.field_size_limit(sys.maxsize)
ADDRESS=""
CAPIQ_CSV_FILEPATH=os.path.dirname(os.path.abspath(__file__))
CAPIQ_FILEPATH = os.path.dirname(os.path.abspath(__file__))
ASN_PATH=os.path.dirname(os.path.abspath(__file__))
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    import csv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace("+"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"/AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"/Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)
    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin')) if unicodedata.category(c) != 'Mn')
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
# def readCSV(path):
#     import csv
#     reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
#     return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
# def writeCSV(path,mode="w"):
#     import unicodecsv
#     myfile=open(path,mode)
#     fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
#     return fileOutput
if __name__=="__main__":
    i = indexCapIq()
    std_name = i.getStandardizedName("Infosys technologies ltd")
    print std_name
    stripped_name = i.getStrippedName(std_name)
    print stripped_name
