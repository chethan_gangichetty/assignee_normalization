from collections import defaultdict
# import regex
from remove_business_entities import indexCapIq
from fuzzywuzzy import fuzz

capiq = indexCapIq()

def getnameidnew(data):
    names = set()
    pref = {'epo': 0, 'intermediate': 1, 'original': 2}

    nameid = {}
    normdata = {}

    for key in data:
        if key in ['inv', 'appl', 'asg']:
            seqnamedict = defaultdict(list)

            for dd in data[key]:
                if 'name' in dd:
                    seqnamedict[dd['sequence']].append([dd['name'], pref.get(dd['format'], 3)])
            for seq in seqnamedict:
                prefnames = [n for n in sorted(seqnamedict[seq], key=lambda x: x[1])][0]
                nameid[prefnames[0]] = prefnames[0]
            names.update([x['name'] for x in data[key]])

        if key == 'reassignments':
            names.update([r['name'] for x in data[key] for r in x[3] + x[4]])

    for name1 in sorted(names, key=lambda x: len(x)):
        matched = None

        for name2 in nameid:

            if matchnames(name1, name2):
                matched = name2
                break
        if matched:
            nameid[name1] = nameid[name2]
        else:
            nameid[name1] = name1
    # pprint(nameid)
    idname = defaultdict(set)
    for key in nameid:
        idname[nameid[key]].add(key)

    return nameid, idname

def matchnames( name1, name2):
    procname1 = name1.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()

    procname2 = name2.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()
    name1tw = ' '.join(procname1[:2])
    name2tw = ' '.join(procname2[:2])
    name1full = ' '.join(procname1)
    name2full = ' '.join(procname2)
    name1str = capiq.getStrippedName(name1)
    name2str = capiq.getStrippedName(name2)
    if name1full.find(name2tw) <> -1 or name2full.find(name1tw) <> -1 or fuzz.token_set_ratio(name1full,
                                                                                              name2full) >= 90 or \
            name1str.find(name2str) <> -1 or name2str.find(name1str) <> -1:
        # print name1full
        # print name2full
        # print fuzz.token_set_ratio(name1full,name2full)
        # print name1,name2,"true"
        return True
    else:
        # print name1full
        # print name2full
        # print fuzz.token_set_ratio(name1full,name2full)
        # print name1,name2,"false"
        return False