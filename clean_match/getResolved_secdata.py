import requests
import unicodedata
import json
# !/usr/bin/env python
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2


def removeAllPunctuations(g):
    g = g.replace(".", "")
    g = g.replace(",", "")
    g = g.replace("'", "")
    g = g.replace("-", " ")
    g = g.replace("/", "")
    g = g.replace(":", "")
    g = g.replace(";", "")
    g = g.replace("+", " ")
    g = g.replace('"', "")
    g = g.replace("*", "")
    g = g.replace("?", " ")
    g = g.replace("[", " ")
    g = g.replace("]", " ")
    g = g.replace("(", " ")
    g = g.replace(")", " ")
    g = g.replace("<", " ")
    g = g.replace(">", " ")
    g = g.replace("=", " ")
    g = g.replace(",", " ")
    g = g.replace("&", "AND")
    g = re.sub('\s+', ' ', g).strip()
    return g


ASN_PATH = "../"
CAPIQ_FILEPATH = "../"


def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'latin-1')) if unicodedata.category(c) != 'Mn')
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def removeSpaces(name):
    while "  " in name:
        name = name.replace("  ", " ")
    return name.strip()


class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE ", " AND "]

    def getCountry(self, country):
        return self.countryCode.get(country, "")

    def processVariations(self):
        reader = readCSVori(CAPIQ_FILEPATH + "AN_variations_modified.csv")
        self.AN_variations = {}
        for row in reader:
            self.AN_variations[row[0].strip()] = row[1].strip()

    def processVariations1(self):
        reader = readCSVori(CAPIQ_FILEPATH + "Bussiness.csv")
        self.replacers = []
        for row in reader:
            self.replacers.append(" " + row[0].upper() + " ")
        self.replacers.append(" " + "CO" + " ")

    def is_ascii(self, s):
        return all(ord(c) < 128 for c in s)

    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'latin')) if unicodedata.category(c) != 'Mn')
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

    def getStandardizedName(self, name):
        if name == "":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name = self.strip_accents(name)
        name = unidecode_expect_nonascii(name.replace(".", "").replace(",", " "))
        name = removeSpaces(name)
        name = removeAllPunctuations(name)
        name = " " + name.upper() + " "
        for v in self.AN_variations:
            newv = " " + v + " "
            name = name.replace(newv, " " + self.AN_variations[v] + " ")
        name = removeSpaces(name)
        return name.strip()

    def getStrippedName(self, name):
        if name == "":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name = unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name = cleanco(name).clean_name()
        stripped_name = " " + stripped_name + " "
        for r in self.replacers:
            stripped_name = stripped_name.replace(r, " ")
        for r in self.articles:
            stripped_name = stripped_name.replace(r, " ")
        stripped_name = removeSpaces(stripped_name)
        return stripped_name.strip()


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def readCSVori(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


session = requests.session()
icq = indexCapIq()


def capiqMapping(website, search, cities):
    cities = ['"' + unidecode_expect_nonascii(ct.replace(" AND ", " ")).replace("&", "") + '"' for ct in cities]
    cities = "(" + " OR ".join(cities) + ")"
    search = unidecode_expect_nonascii(search.split("(")[0])
    search1 = icq.getStandardizedName(search)
    search2 = icq.getStrippedName(search)
    search1 = search1.replace('"', "")
    search2 = search2.replace('"', "")

    url_search1 = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search1 = url_search1.replace("{search1}", search1.replace("&", "%26"))
    url_search1 = url_search1.replace(" ", "%20")
    try:
        data_search1 = session.get(url_search1).json()
    except IndexError, e:
        data_search1 = ""

    url_search2 = "http://work1:8983/solr/companycore/select?indent=on&q=(stripped_name_keywords:%22{search2}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search2 = url_search2.replace("{search2}", search2.replace("&", "%26"))
    url_search2 = url_search2.replace(" ", "%20")
    try:
        data_search2 = session.get(url_search2).json()
    except IndexError, e:
        data_search2 = ""

    return data_search1, data_search2

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'latin')) if unicodedata.category(c) != 'Mn')
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def capiqMap_SubCnt(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}", id)
    url = url.replace(" ", "%20")
    data = session.get(url).text
    try:
        data = session.get(url).text.replace("\n", "||")
    except IndexError, e:
        return "Error!!"
    data = data.replace(
        "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||",
        "")
    data = unidecode_expect_nonascii(strip_accents(data))
    if data == "":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit) > 0:
        return msplit[0]


def capiqMap_geography(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}", id)
    url = url.replace(" ", "%20")
    data = session.get(url).text
    try:
        data = session.get(url).text.replace("\n", "||")
    except IndexError, e:
        return "Error!!"
    data = data.replace(
        "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||",
        "")
    data = unidecode_expect_nonascii(strip_accents(data))
    if data == "":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit) > 0:
        return msplit[0]

def matchSolr_secdata(search1,search2):
    url_search1 = "http://work1:8984/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,standardized_name,stripped_name,standardized_name_keywords,stripped_name_ws,stripped_name_keywords,country,city,ultimateflag,immParent,ultimateParentID,industry,immParentID,ultiParent,subsidiary_count,website,foundation"
    url_search1 = url_search1.replace("{search1}", search1.replace("&", "%26"))
    url_search1 = url_search1.replace(" ", "%20")
    try:
        data_search1 = session.get(url_search1).json()
    except IndexError, e:
        data_search1 = ""

    url_search2 = "http://work1:8984/solr/companycore/select?indent=on&q=(stripped_name_keywords:%22{search2}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,standardized_name,stripped_name,standardized_name_keywords,stripped_name_ws,stripped_name_keywords,country,city,ultimateflag,immParent,ultimateParentID,industry,immParentID,ultiParent,subsidiary_count,website,foundation"
    url_search2 = url_search2.replace("{search2}", search2.replace("&", "%26"))
    url_search2 = url_search2.replace(" ", "%20")
    try:
        data_search2 = session.get(url_search2).json()
    except IndexError, e:
        data_search2 = ""

    return data_search1, data_search2

reader = readCSV("../geoLookup.csv")
transformation_dict = {}
transformation_dict_not = {}

# Create a dictionary with patent country codes as keys and expanded countries as values, transformation_dict = {"US": "United States"}

for row in reader:
    if row[0] == 'HK':
        transformation_dict[row[0]] = 'Hong Kong'
    elif row[0] == 'MO':
        transformation_dict[row[0]] = 'Macau'
    elif row[0] == 'CD':
        transformation_dict[row[0]] = 'Democratic Republic of the Congo'
    elif row[0] == 'IR':
        transformation_dict[row[0]] = 'Iran'
    elif row[0] == 'MK':
        transformation_dict[row[0]] = 'Macedonia'
    #         if row[1] == 'Micronesia, Federated States of':
    #             transformation_dict[row[0]] = 'Macau'
    elif row[0] == 'PS':
        transformation_dict[row[0]] = 'Palestinian Authority'
    elif row[0] == 'TW':
        transformation_dict[row[0]] = 'Taiwan'
    elif row[0] == 'TZ':
        transformation_dict[row[0]] = 'Tanzania'
    #         if row[1] == 'Virgin Islands, US':
    #             transformation_dict[row[0]] = 'Macau'
    else:
        transformation_dict[row[0]] = row[1]

# Create a dictionary with CPC as keys and Industries as values, code_transformation = {"A01": [Chemicals, Machinery, Pharmaceuticals, Food Products]"}

reader = readCSV("../industryCPC.csv")
code_transformation = {}
for row in reader:
    code_transformation[row[0]] = row[1].split("|") + ["Industrial Conglomerates"]

# Create a dictionary with Industries as keys and CPC codes as values, industry_cpc_dict = {"Diversified Telecommunication Services" : [H04L,H04W,H04M,H04B,G06F,H04Q,H04N,G02B,G06Q]}

reader = readCSV("../new_CPCIndustry.csv")
industry_cpc_dict = {}
for row in reader:
    industry_cpc_dict[row[0]] = row[1].split("|")

con_alias = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_alias = con_alias.cursor('iter_alias')
cur_alias.itersize = 10000
query_alias = "select * from dolcera.an_final_alias_table2"
cur_alias.execute(query_alias)

# Iterate through the an_final_alias_table and create a dictionary orig_freq_dict with original_names as keys and frequency as values, orig_freq_dict = {"orig_name1": freq1}

orig_freq_dict = {}
# counter = 0
# for ulti, orig, countries, cities, freq in cur_alias:
#     if counter % 10000 == 0:
#         print counter, "orig_name freq dict"
#     orig_freq_dict[strip_accents(orig)] = freq
#     counter += 1

con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.an_ipcdict_gencan"
cur.execute(query)

con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "insert into dolcera.an_resolvedstuff_final values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

con4 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur4 = con4.cursor('iter-1')
cur4.itersize = 10000
query4 = "select * from dolcera.an_ipcdict_assgn_gencan"
cur4.execute(query4)

# Iterate through the dolcera.an_ipcdict_gencan and create dictionaries cpc_dict {cluster: [A63, Y10]}, cpc_dict_l4 {cluster: [A63F, Y10H]}.
# cpc_dict has the top-5 3-char class codes mapped at cluster representative level in the descending order of frequency.
# cpc_dict_l4 has top 80% 4-char class codes mapped at cluster representative level in the descending order of frequency.

cpc_dict = defaultdict(list)
cpc_dict_l4 = defaultdict(list)
counter = 0
for cluster, cpc, counts in cur:
    cpc_count_dict = {}
    for idx, i in enumerate(cpc):
        cpc_count_dict[i] = counts[idx]
    sumValue = 0
    for c in counts:
        sumValue = sumValue + c
    sorted_x = sorted(cpc_count_dict.items(), key=operator.itemgetter(1), reverse=True)
    sum = 0
    for x in sorted_x:
        if sum <= 0.8 * sumValue:
            cpc_dict_l4[cluster].append(x[0])
            sum += x[1]

    code = cpc
    if len(code) > 5:
        code = code[:5]
    code = [c[:3] for c in code if len(c) == 4]
    covered = set()
    new_code = []
    for c in code:
        if c not in covered:
            new_code.append(c)
            covered.add(c)
    cpc_dict[cluster] = new_code

    counter = counter + 1
    if counter % 10000 == 0:
        print counter, "Reading CPC codes"

# Iterate through the dolcera.an_ipcdict_assgn_gencan and create dictionaries cpc_dict_assgn {assgn: [A63, Y10]}, cpc_dict_assgn_l4 {assgn: [A63F, Y10H]}, code_ind_dict {assgn: [A63, Y10]}.
# cpc_dict has the top-5 3-char class codes mapped at assignee level in the descending order of frequency.
# cpc_dict_assgn_l4 has top 80% 4-char class codes mapped at assignee level in the descending order of frequency.
# code_ind_dict has 3-char class codes mapped at assignee level  in the descending order of frequency. Each class code should have a min-frequency of 10 and should account for atleast 3% of cumulative frequencies of all the class codes.

cpc_dict_assgn = defaultdict(list)
cpc_dict_assgn_l4 = defaultdict(list)
counter = 0
code_ind_dict = {}
for assgn, cpc, counts in cur4:
    counter = counter + 1
    if counter % 10000 == 0:
        print counter, "Reading CPC codes_assgn"
    code_assgn = cpc

    cpc_count_dict = {}
    for idx, i in enumerate(cpc):
        cpc_count_dict[i] = counts[idx]
    sumValue = 0
    for c in counts:
        sumValue = sumValue + c
    sorted_x = sorted(cpc_count_dict.items(), key=operator.itemgetter(1), reverse=True)
    sum = 0
    for x in sorted_x:
        if sum <= 0.8 * sumValue:
            cpc_dict_assgn_l4[assgn].append(x[0])
            sum += x[1]

    sum_counts = 0
    for c in counts:
        sum_counts = sum_counts + c
    code_ind = {}
    code_ind2 = {}
    code_trun = [c[:3] for c in code_assgn if len(c) == 4]
    for idx, cd in enumerate(code_trun):
        code_ind[cd] = code_ind.get(cd, 0) + counts[idx]
    code_ind2 = {i: code_ind[i] for i in code_ind if (code_ind[i] >= int(0.03 * sum_counts) and code_ind[i] > 10)}
    sorted_codeind = [i[0] for i in sorted(code_ind2.items(), key=operator.itemgetter(1), reverse=True)]
    code_ind_dict[assgn] = sorted_codeind

    if len(code_assgn) > 5:
        code_assgn = code_assgn[:5]
    code_assgn = [c[:3] for c in code_assgn if len(c) == 4]
    covered = set()
    new_code = []
    for c in code_assgn:
        if c not in covered:
            new_code.append(c)
            covered.add(c)
    cpc_dict_assgn[assgn] = new_code

con_alias = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_alias = con_alias.cursor('iter_alias')
cur_alias.itersize = 10000
query_alias = "select std_name, countries, cities, frequency from dolcera.rerun_final_full_info_alias_table order by frequency desc"
cur_alias.execute(query_alias)
orig_ctry = {}
orig_cty = {}
orig_freq_dict = {}
counter = 0

for std,ctry,cty,freq in cur_alias:
    counter+=1
    if counter%10000 == 0:
        print counter
    if std not in orig_freq_dict:
        orig_freq_dict[std] = freq
    if std not in orig_ctry:
        orig_ctry[std] = ctry
    if std not in orig_cty:
        orig_cty[std] = cty

reader = readCSV("../NEW_CLEANMATCH3.csv")
echoed_names = set()
indirect_names = set()
counter = 0
criteria = set()
clst_dict = {}
for row in reader:
    clst_dict[row[1]] = row[0]
    counter+= 1
    if counter%100000 == 0:
        print counter
    if len(row)>5:
        if row[-1] in ['Edit distance','Inventor 4','Inventor 3','Inventor 2','Inventor 1','Cocluster']:
            indirect_names.add(row[1])
        continue
    echoed_names.add(row[1])

writer = writeCSV("sec_matches.csv")

lines = []
dicta = {}
resolved = set()
normalized_data = []
cluster_dict = defaultdict(list)
cluster_dict1 = defaultdict(list)
cluster_dict2 = defaultdict(list)
covered = set()
array = []
clean_cluster_matches = defaultdict(list)
country_dict = defaultdict(set)
ind_ignore = ['Industrial Conglomerates', 'Diversified Consumer Services', 'Trading Companies and Distributors',
              'Construction and Engineering', 'Diversified Financial Services', 'Commercial Services and Supplies',
              'Multiline Retail', 'Distributors', 'Real Estate Management and Development', 'Specialty Retail',
              'Professional Services']

# Iterate through the DataNew.csv ( ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,data1,data2,data3,city,ori_name_ori )
# data1=solrMatch("standardized_name_keywords",stdName)
# data2=solrMatch("stripped_name_ws",strippedName.replace(" ",""))
# data3=solrMatch("standardized_name_keywords",commasplitname)
icq = indexCapIq()

counter = 0
brk = 0
for name in list(echoed_names)+list(indirect_names):
#     if brk == 1:
#         break
    try:
        counter +=1
        if counter%10000 == 0:
            print counter
        ori_name = name
#         if ori_name == 'IDPA HLDGS INC':
#         brk = 1
        std_name = name
        stripped_name = icq.getStrippedName(ori_name)
        clusterValues = [clst_dict[name]]
        std_data,strip_data = matchSolr_secdata(std_name,stripped_name)


        if ori_name in orig_ctry and len(orig_ctry[ori_name])>0:
            countries_all = orig_ctry[ori_name]
        else:
            countries_all = ["NoCountry"]

        matches_exact = []
        matches = []

        for dct in std_data[u'response'][u'docs']:
            array = []
            array.append(str(dct.get('id','')))
            array.append(str(dct.get('original_name',[''])[0]).strip().replace("  "," "))
            array.append(str(dct.get('standardized_name',[''])[0]))
            array.append(str(dct.get('stripped_name',[''])[0]))
            array.append(str(dct.get('standardized_name_keywords','')))
            array.append(str(dct.get('stripped_name_ws','')))
            array.append(str(dct.get('stripped_name_keywords','')))
            array.append(str(dct.get('country','')))
            array.append(str(dct.get('city','')))
            array.append(str(dct.get('ultimateflag','')))
            array.append(str(dct.get('immParentID','')[0]))
            array.append(str(dct.get('ultimateParentID','')[0]))
            array.append(str(dct.get('industry','')))
            array.append(str(dct.get('immParent','')))
            array.append(str(dct.get('ultiParent','')))
            array.append(str(dct.get('subsidiary_count','')))
            array.append(str(dct.get('website','')))
            array.append(str(dct.get('foundation','')))
            array.append(array.append(dct.get('haskey','')))
            matches_exact.append(array)

        for dct in strip_data[u'response'][u'docs']:
            array = []
            array.append(str(dct.get('id','')))
            array.append(str(dct.get('original_name',[''])[0]).strip().replace("  "," "))
            array.append(str(dct.get('standardized_name',[''])[0]))
            array.append(str(dct.get('stripped_name',[''])[0]))
            array.append(str(dct.get('standardized_name_keywords','')))
            array.append(str(dct.get('stripped_name_ws','')))
            array.append(str(dct.get('stripped_name_keywords','')))
            array.append(str(dct.get('country','')))
            array.append(str(dct.get('city','')))
            array.append(str(dct.get('ultimateflag','')))
            array.append(str(dct.get('immParentID','')[0]))
            array.append(str(dct.get('ultimateParentID','')[0]))
            array.append(str(dct.get('industry','')))
            array.append(str(dct.get('immParent','')))
            array.append(str(dct.get('ultiParent','')))
            array.append(str(dct.get('subsidiary_count','')))
            array.append(str(dct.get('website','')))
            array.append(str(dct.get('foundation','')))
            array.append(array.append(dct.get('haskey','')))
            matches.append(array)

        bonus = 0
        if len(clusterValues) > 1:
            choices = clusterValues
            new_cluster = process.extractOne(std_name, choices)[0]
            temp_cluster = new_cluster
        else:
            temp_cluster = list(clusterValues)[0]
        cpc_cluster = icq.getStandardizedName(temp_cluster)

        # Retrieve the 3-char class codes at assignee level and at cluster representative level for the incoming record from cpc_dict_assgn and cpc_dict. Save them in code_assgn and code respectively.
        code_assgn = cpc_dict_assgn.get(std_name, [])
        code = cpc_dict.get(cpc_cluster, [])
        industries = set()
        industries_assgn = set()

        # Retrieve the corresponding industries for code_assgn and code from code_transformation. Save them in industries_assgn and industries respectively.
        for i in code_assgn:
            industries_assgn.update(code_transformation.get(i, []))
        for i in code:
            industries.update(code_transformation.get(i, []))
        cluster = "|".join(clusterValues)

        # Create a dictionary, country_dict = {"Cluster": [country1, country2]}, countries taken from Alias Table
        country_dict[cluster] = [c for c in countries_all]
        matchFlag = 0
        if ori_name in orig_cty and len(orig_cty[ori_name])>0:
            city = orig_cty[ori_name][0]
        else:
            city = ""
        flag = 0
        if clst_dict[name] + cluster in covered:
            continue
        else:
            covered.add(clst_dict[name] + cluster)
        chosen_msplit = []

        covered_codes = set()
        industry_score_dict = {}

        # Iterate over the code variable and create a dictionary, industry_score_dict = {"Industry": 32, "Industry2: 30}. For a given value in code, retrieve the corresponding industries from code_transformation and score all the Industries with the same value. The starting value is 32 and the subsequent values will have 2 less in magnitude (30, 28 etc.)
        for cindex, c in enumerate(code):
            for industry in code_transformation.get(c, []):
                if industry not in covered_codes:
                    industry_score_dict[industry] = 32 - 2 * cindex
                    covered_codes.add(industry)

        covered_codes_assgn = set()
        industry_score_dict_assgn = {}

        # Iterate over the code_assgn variable and create a dictionary, industry_score_dict_assgn = {"Industry": 32, "Industry2: 30}. For a given value in code_assgn, retrieve the corresponding industries from code_transformation and score all the Industries with the same value. The starting value is 32 and the subsequent values will have 2 less in magnitude (30, 28 etc.)
        for cindex, c in enumerate(code_assgn):
            for industry_assgn in code_transformation.get(c, []):
                if industry_assgn not in covered_codes_assgn:
                    industry_score_dict_assgn[industry_assgn] = 32 - 2 * cindex
                    covered_codes_assgn.add(industry_assgn)

        # For the give orig_name, retrieve the 3-char class codes from code_ind_dict variable and create a dictionary, code_ind_score_dict = {"Industry": 40, "Industry2: 39}. Industries belonging to the same class code shouldbe scored with the same value.
        code_ind_assgn = code_ind_dict.get(std_name, [])
        industries_code_ind_assgn = set()
        for i in code_ind_assgn:
            industries_code_ind_assgn.update(code_transformation.get(i, []))

        covered_codes_ind = set()
        code_ind_score_dict = {}

        for cindex, c in enumerate(code_ind_assgn):
            for industry in code_transformation.get(c, []):
                if industry not in covered_codes_ind:
                    code_ind_score_dict[industry] = 40 - cindex
                    covered_codes_ind.add(industry)
        new_candidates = []

        for countryindex, country in enumerate(countries_all):
            if flag == 1:
                break
            countries = set([transformation_dict.get(c.strip(), c.strip()) for c in [country] if c.strip() != ""])


            countries_incoming = set()
            candidates = []
            if std_name != stripped_name:

                # If standard_name and stripped_name are not same, iterate through data1 (Metadata: "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||")
                for m in matches_exact:
                    industry_match = 0
                    country_match = 0
                    city_match = 0
                    industry_score = 0
                    website = ''
                    code_ind_score = 0
                    reverse_flag = 0
                    website = m[16]
                    city_incoming = m[8]
                    if city_incoming == "":
                        city_incoming = "City not found"
                    country_incoming = transformation_dict.get(m[7],m[7])
                    countries_incoming.add(country_incoming)
                    industry = m[12]

                    # Search for the incoming industry in industry_score_dict_assgn, industry_score_dict and code_ind_score_dict successively to check if there is an industry match. Retrive the industry_score(cpc_grade in the table) from industry_score_dict_assgn/industry_score_dict and code_ind_score from code_ind_score_dict(ind_score in the table)
                    if industry in industry_score_dict_assgn and industry != "":
                        industry_match = 1
                        industry_score = industry_score_dict_assgn[industry]
                    else:
                        if industry in industry_score_dict and industry != "":
                            industry_match = 1
                            industry_score = industry_score_dict[industry]

                    if industry in code_ind_score_dict and industry != "":
                        industry_match = 1
                        code_ind_score = code_ind_score_dict[industry]

                    # For the incoming industry, save the class codes from industry_cpc_dict dictionary into capiq_ind_codes
                    if industry != "" and industry_match == 0:
                        capiq_ind_codes = industry_cpc_dict.get(industry, "")

                        if industry == 'Diversified Telecommunication Services' or industry == 'Communications Equipment' or industry == 'Wireless Telecommunication Services':
                            capiq_ind_codes = industry_cpc_dict.get("Diversified Telecommunication Services",
                                                                    "") + industry_cpc_dict.get(
                                "Communications Equipment", "") + industry_cpc_dict.get(
                                "Wireless Telecommunication Services", "")

                        if industry == 'Electric Utilities' or industry == 'Independent Power and Renewable Electricity Producers' or industry == 'Electrical Equipment' or industry == 'Energy Equipment and Services':
                            capiq_ind_codes = industry_cpc_dict.get("Electric Utilities", "") + industry_cpc_dict.get(
                                "Independent Power and Renewable Electricity Producers", "") + industry_cpc_dict.get(
                                "Electrical Equipment", "") + industry_cpc_dict.get("Energy Equipment and Services", "")

                        if industry == 'Air Freight and Logistics' or industry == 'Containers and Packaging' or industry == 'Airlines':
                            capiq_ind_codes = industry_cpc_dict.get("Air Freight and Logistics",
                                                                    "") + industry_cpc_dict.get(
                                "Containers and Packaging", "") + industry_cpc_dict.get("Airlines", "")

                        if industry == 'Life Sciences Tools and Services' or industry == 'Health Care Technology' or industry == 'Healthcare Providers and Services' or industry == 'Healthcare Equipment and Supplies':
                            capiq_ind_codes = industry_cpc_dict.get("Life Sciences Tools and Services",
                                                                    "") + industry_cpc_dict.get(
                                "Health Care Technology", "") + industry_cpc_dict.get(
                                "Healthcare Providers and Services", "") + industry_cpc_dict.get(
                                "Healthcare Equipment and Supplies", "")

                        if industry == 'Gas Utilities' or industry == 'Oil, Gas and Consumable Fuels':
                            capiq_ind_codes = industry_cpc_dict.get("Gas Utilities", "") + industry_cpc_dict.get(
                                "Oil, Gas and Consumable Fuels", "")

                        if industry == 'Biotechnology' or industry == 'Pharmaceuticals':
                            capiq_ind_codes = industry_cpc_dict.get("Pharmaceuticals", "") + industry_cpc_dict.get(
                                "Biotechnology", "")

                        if industry == 'Software' or industry == 'IT Services':
                            capiq_ind_codes = industry_cpc_dict.get("Software", "") + industry_cpc_dict.get(
                                "IT Services", "")

                        if industry == 'Construction Materials' or industry == 'Building Products':
                            capiq_ind_codes = industry_cpc_dict.get("Construction Materials",
                                                                    "") + industry_cpc_dict.get("Building Products", "")

                        if industry == 'Consumer Finance' or industry == 'Banks' or industry == 'Equity Real Estate Investment Trusts (REITs)' or industry == 'Capital Markets' or industry == 'Thrifts and Mortgage Finance' or industry == 'Diversified Financial Services' or industry == 'Insurance':
                            capiq_ind_codes = industry_cpc_dict.get("Consumer Finance", "") + industry_cpc_dict.get(
                                "Banks", "") + industry_cpc_dict.get("Equity Real Estate Investment Trusts (REITs)",
                                                                     "") + industry_cpc_dict.get("Capital Markets",
                                                                                                 "") + industry_cpc_dict.get(
                                "Thrifts and Mortgage Finance", "") + industry_cpc_dict.get(
                                "Diversified Financial Services", "") + industry_cpc_dict.get("Insurance", "")

                        if industry == 'Road and Rail' or industry == 'Transportation Infrastructure':
                            capiq_ind_codes = industry_cpc_dict.get("Road and Rail", "") + industry_cpc_dict.get(
                                "Transportation Infrastructure", "")

                        if industry == 'Automobiles' or industry == 'Auto Components':
                            capiq_ind_codes = industry_cpc_dict.get("Automobiles", "") + industry_cpc_dict.get(
                                "Auto Components", "")

                        if industry == 'Specialty Retail' or industry == 'Multiline Retail':
                            capiq_ind_codes = industry_cpc_dict.get("Specialty Retail", "") + industry_cpc_dict.get(
                                "Multiline Retail", "")

                        if industry == 'Trading Companies and Distributors' or industry == 'Distributors':
                            capiq_ind_codes = industry_cpc_dict.get("Trading Companies and Distributors",
                                                                    "") + industry_cpc_dict.get("Distributors", "")

                        # Retrive the 4-char class codes from cpc_dict_l4 and cpc_dict_assgn_l4 into cpc_cluster_codes_l4 and cpc_assgn_codes_l4. Determine the common class codes between capiq_ind_codes and cpc_cluster_codes_l4/cpc_assgn_codes_l4. Ignore Y10 class codes for intersection.
                        cpc_cluster_codes_l4 = cpc_dict_l4.get(cpc_cluster, "")
                        cpc_assgn_codes_l4 = cpc_dict_assgn_l4.get(std_name, "")
                        assgn_intersection = list(set(capiq_ind_codes) & set(cpc_assgn_codes_l4))
                        cluster_intersection = list(set(capiq_ind_codes) & set(cpc_cluster_codes_l4))
                        if len(assgn_intersection) >= 1 or len(cluster_intersection) >= 1:
                            if (len([i for i in assgn_intersection if i[0:3] != 'Y10']) == 0) and (
                                    len([i for i in cluster_intersection if i[0:3] != 'Y10']) == 0):
                                industry_match = 0
                            else:
                                industry_match = 1
                                reverse_flag = 1

                    if industry != "" and industry_match == 0 and industry not in ind_ignore:
                        continue

                    if city_incoming.upper() in city:
                        city_match = 1
                    try:
                        itg = 0
                    except:
                        itg = 0

                    if country_incoming in countries or "NoCountry" in countries:
                        country_match = 1
                    else:
                        country_match = 0
                        city_match = 0
                        new_candidates.append(
                            [m, country_match * 16 + city_match * 32 + industry_match * 8 + 4 + bonus, itg,
                             industry_score, website, code_ind_score, 'sec-std', industry_match, reverse_flag])
                        continue
                    candidates.append(
                        [m, country_match * 16 + city_match * 32 + industry_match * 8 + 4 + bonus, itg,
                         industry_score, website, code_ind_score, 'sec-std', reverse_flag])

            ulti_dict = {}
            new_can = []

            # Iterate over the stripped_matches (data2) and use the same scoring mechanism similar to standardized matches for industry, city and country matches.
            for m in matches:
                industry_match = 0
                country_match = 0
                city_match = 0
                industry_score = 0
                website = ''
                code_ind_score = 0
                reverse_flag = 0
                website = m[16]
                city_incoming = m[8]
                if city_incoming == "":
                    city_incoming = "City not found"
                country_incoming = transformation_dict.get(m[7],m[7])
                countries_incoming.add(country_incoming)
                industry = m[12]
                ulti_dict[m[11]] = ulti_dict.get(m[11], 0) + 1

                if industry in industry_score_dict_assgn and industry != "":
                    industry_match = 1
                    industry_score = industry_score_dict_assgn[industry]
                else:
                    if industry in industry_score_dict and industry != "":
                        industry_match = 1
                        industry_score = industry_score_dict[industry]

                if industry in code_ind_score_dict and industry != "":
                    industry_match = 1
                    code_ind_score = code_ind_score_dict[industry]
                try:
                    itg = 0
                except:
                    itg = 0

                if industry != "" and industry_match == 0:
                    capiq_ind_codes = industry_cpc_dict.get(industry, "")

                    if industry == 'Diversified Telecommunication Services' or industry == 'Communications Equipment' or industry == 'Wireless Telecommunication Services':
                        capiq_ind_codes = industry_cpc_dict.get("Diversified Telecommunication Services",
                                                                "") + industry_cpc_dict.get("Communications Equipment",
                                                                                            "") + industry_cpc_dict.get(
                            "Wireless Telecommunication Services", "")

                    if industry == 'Electric Utilities' or industry == 'Independent Power and Renewable Electricity Producers' or industry == 'Electrical Equipment' or industry == 'Energy Equipment and Services':
                        capiq_ind_codes = industry_cpc_dict.get("Electric Utilities", "") + industry_cpc_dict.get(
                            "Independent Power and Renewable Electricity Producers", "") + industry_cpc_dict.get(
                            "Electrical Equipment", "") + industry_cpc_dict.get("Energy Equipment and Services", "")

                    if industry == 'Air Freight and Logistics' or industry == 'Containers and Packaging' or industry == 'Airlines':
                        capiq_ind_codes = industry_cpc_dict.get("Air Freight and Logistics",
                                                                "") + industry_cpc_dict.get("Containers and Packaging",
                                                                                            "") + industry_cpc_dict.get(
                            "Airlines", "")

                    if industry == 'Life Sciences Tools and Services' or industry == 'Health Care Technology' or industry == 'Healthcare Providers and Services' or industry == 'Healthcare Equipment and Supplies':
                        capiq_ind_codes = industry_cpc_dict.get("Life Sciences Tools and Services",
                                                                "") + industry_cpc_dict.get("Health Care Technology",
                                                                                            "") + industry_cpc_dict.get(
                            "Healthcare Providers and Services", "") + industry_cpc_dict.get(
                            "Healthcare Equipment and Supplies", "")

                    if industry == 'Gas Utilities' or industry == 'Oil, Gas and Consumable Fuels':
                        capiq_ind_codes = industry_cpc_dict.get("Gas Utilities", "") + industry_cpc_dict.get(
                            "Oil, Gas and Consumable Fuels", "")

                    if industry == 'Biotechnology' or industry == 'Pharmaceuticals':
                        capiq_ind_codes = industry_cpc_dict.get("Pharmaceuticals", "") + industry_cpc_dict.get(
                            "Biotechnology", "")

                    if industry == 'Software' or industry == 'IT Services':
                        capiq_ind_codes = industry_cpc_dict.get("Software", "") + industry_cpc_dict.get("IT Services",
                                                                                                        "")

                    if industry == 'Construction Materials' or industry == 'Building Products':
                        capiq_ind_codes = industry_cpc_dict.get("Construction Materials", "") + industry_cpc_dict.get(
                            "Building Products", "")

                    if industry == 'Consumer Finance' or industry == 'Banks' or industry == 'Equity Real Estate Investment Trusts (REITs)' or industry == 'Capital Markets' or industry == 'Thrifts and Mortgage Finance' or industry == 'Diversified Financial Services' or industry == 'Insurance':
                        capiq_ind_codes = industry_cpc_dict.get("Consumer Finance", "") + industry_cpc_dict.get("Banks",
                                                                                                                "") + industry_cpc_dict.get(
                            "Equity Real Estate Investment Trusts (REITs)", "") + industry_cpc_dict.get(
                            "Capital Markets", "") + industry_cpc_dict.get("Thrifts and Mortgage Finance",
                                                                           "") + industry_cpc_dict.get(
                            "Diversified Financial Services", "") + industry_cpc_dict.get("Insurance", "")

                    if industry == 'Road and Rail' or industry == 'Transportation Infrastructure':
                        capiq_ind_codes = industry_cpc_dict.get("Road and Rail", "") + industry_cpc_dict.get(
                            "Transportation Infrastructure", "")

                    if industry == 'Automobiles' or industry == 'Auto Components':
                        capiq_ind_codes = industry_cpc_dict.get("Automobiles", "") + industry_cpc_dict.get(
                            "Auto Components", "")

                    if industry == 'Specialty Retail' or industry == 'Multiline Retail':
                        capiq_ind_codes = industry_cpc_dict.get("Specialty Retail", "") + industry_cpc_dict.get(
                            "Multiline Retail", "")

                    if industry == 'Trading Companies and Distributors' or industry == 'Distributors':
                        capiq_ind_codes = industry_cpc_dict.get("Trading Companies and Distributors",
                                                                "") + industry_cpc_dict.get("Distributors", "")

                    cpc_cluster_codes_l4 = cpc_dict_l4.get(cpc_cluster, "")
                    cpc_assgn_codes_l4 = cpc_dict_assgn_l4.get(std_name, "")
                    assgn_intersection = list(set(capiq_ind_codes) & set(cpc_assgn_codes_l4))
                    cluster_intersection = list(set(capiq_ind_codes) & set(cpc_cluster_codes_l4))
                    if len(assgn_intersection) >= 1 or len(cluster_intersection) >= 1:
                        if (len([i for i in assgn_intersection if i[0:3] != 'Y10']) == 0) and (
                                len([i for i in cluster_intersection if i[0:3] != 'Y10']) == 0):
                            industry_match = 0
                        else:
                            industry_match = 1
                            reverse_flag = 1

                if industry != "" and industry_match == 0 and industry not in ind_ignore:
                    continue

                if city_incoming.upper() in city:
                    city_match = 1

                if country_incoming in countries or "NoCountry" in countries:
                    country_match = 1
                else:
                    country_match = 0
                    city_match = 0
                    new_candidates.append([m, country_match * 16 + city_match * 32 + industry_match * 8, itg, industry_score,website, code_ind_score, 'sec-strip', industry_match, reverse_flag])
                    continue

                candidates.append(
                    [m, country_match * 16 + city_match * 32 + industry_match * 8, itg, industry_score, website,code_ind_score, 'sec-strip', reverse_flag])
            sorted_x = sorted(ulti_dict.items(), key=operator.itemgetter(1))
            sorted_x.reverse()

            if len(candidates) > 0:
                candidates = sorted(candidates, key=lambda x: (x[1], x[-5], x[-3], x[-6], x[-4]), reverse=True)
                conflict = -1
                max_score = candidates[0][1]
                ulti_parent = candidates[0][0][11]
                ulti_parents = set()
                score_grade_ind = str(candidates[0][1]) + "|" + str(candidates[0][3]) + "|" + str(candidates[0][5])

                for c in candidates:
                    if c[1] == max_score and c[0][11] not in list(ulti_parents) and score_grade_ind == str(
                            c[1]) + "|" + str(c[3]) + "|" + str(c[5]):
                        conflict = conflict + 1
                        ulti_parents.add(c[0][11])

                # Iterate over candidates in the descending order of score, cpc_grade, ind_score and search for conflicts(entries with same highest max_score, cpc_grade and ind_score) but different ultimate parents.
                if conflict > 0:
                    website_set = set()
                    for c in candidates:
                        if c[1] == max_score and c[-4] != "":
                            website_set.add(c[-4])
                    # In case of conflict only 1 candidate with max_score has website and score>=24, consider that as the relevant capiq match.
                    if len(list(website_set)) == 1 and flag == 0:
                        for c in candidates:
                            if c[-4] == list(website_set)[0]:
                                if c[1] >= 16:
                                    array = []
                                    array.extend([ori_name, cpc_cluster, std_name])
                                    array.extend(c[0])
                                    array.append(c[1])
                                    array.append(c[-5])
                                    array.append(c[-3])
                                    array.append(c[-2])
                                    array.append(0)
                                    array.append(c[-1])
                                    array.append(orig_freq_dict.get(strip_accents(ori_name)))
                                    writer.writerow(array)
                                    flag = 1
                                    break
                    else:
                        # In case of conflict multiple candidates has websites with max_score, retrive the candidate whose ultimate parent as highest subsidiary count and if score>=24 consider that as the relevant capiq match.
                        if flag == 0:
                            ulti_subsidiary_counts = []
                            for c in candidates:
                                if c[1] == max_score:
                                    sub_count = capiqMap_SubCnt(c[0][11])
                                    if sub_count != "":
                                        if sub_count[7] != "":
                                            ulti_subsidiary_counts.append(int(sub_count[7]))
                                        else:
                                            ulti_subsidiary_counts.append(0)
                                else:
                                    ulti_subsidiary_counts.append(0)
                            index = ulti_subsidiary_counts.index(max(ulti_subsidiary_counts))
                            c = candidates[index]
                            if c[1] >= 16:
                                array = []
                                array.extend([ori_name, cpc_cluster, std_name])
                                array.extend(c[0])
                                array.append(c[1])
                                array.append(c[-5])
                                array.append(c[-3])
                                array.append(c[-2])
                                array.append(0)
                                array.append(c[-1])
                                array.append(orig_freq_dict.get(strip_accents(ori_name)))
                                writer.writerow(array)
                                flag = 1
                                break
                else:
                    # Incase of no conflicts, coonsider the capiq match with score>=24 as the relevant capiq match
                    if flag == 0:
                        for c in candidates:
                            if c[1] >= 16:
                                array = []
                                array.extend([ori_name, cpc_cluster, std_name])
                                array.extend(c[0])
                                array.append(c[1])
                                array.append(c[-5])
                                array.append(c[-3])
                                array.append(c[-2])
                                array.append(0)
                                array.append(c[-1])
                                array.append(orig_freq_dict.get(strip_accents(ori_name)))
                                writer.writerow(array)
                                flag = 1
                                break
    except:
        continue