import requests
import unicodedata
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2
from collections import defaultdict
from pprint import pprint

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

def readCSVori(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "select orig_name, ulti_name from dolcera.an_final_alias_table"
old_orig = set()
cur2.execute(query2)

for orig,ulti in cur2:
    old_orig.add(orig)

con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "select orig_name, ulti_name from dolcera.rerun_final_full_info_alias_table"
new_orig = set()
cur2.execute(query2)

for orig,ulti in cur2:
    new_orig.add(orig)
    
diff = old_orig.difference(new_orig)

reader = readCSV("../Bussiness.csv")
busent = set()
for row in reader:
    if len(row[0].split(" "))==1:
        busent.add(row[0].upper())

con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "select orig_name, ulti_name from dolcera.an_final_alias_table"
cur2.execute(query2)
writer = writeCSV("../removed_companies_as_inventors.csv")
# orig_ulti_dict = {}
counter = 0
for orig,ulti in cur2:
    counter+=1
    if counter%100000 == 0:
        print counter
    if orig in diff:
        flag = 0
        flag2 = 0
        for j in busent:
            if j == orig.split(" ")[-1]:
                flag = 1
                break
        for j in busent:
            if j == ulti.split(" ")[-1]:
                flag2 = 1
                break
        if flag ==1 and flag2 == 1:
            writer.writerow([orig,ulti])
