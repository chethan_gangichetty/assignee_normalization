from collections import defaultdict
# import statistics
# import numpy
import random
from unidecode import unidecode
from cleanco import cleanco
import re
import ast
import psycopg2
import operator

import requests
import unicodedata
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2

ASN_PATH = "../"
CAPIQ_FILEPATH = "../"

negative_popular_token = set(["ltd","co","corp","inc","tech","and","electric","ag","kk","ind","gmbh","corporation","univ","a","tech","company","s","university","of","electronics","stock","inst","research","industry","l","h","the","m","limited","sa","chemical","motor","institute","v","p","res","group","technologies","llc","china","r","ab","j","e","international","mfg","b","int","c","kaisha","de","kabushiki","d","shanghai","nv","systems","institut","chem","kg","n","industries","gmb","beijing","li","spa","w","gen","kogyo","eng","f","shenzhen","aktiengesellschaft","james","david","wang","as","zhejiang","chen","zhang","an","wei","hong","tianjin","academy","plc","suzhou","kim","yang","japan","ya","??","du","amp","jun","joseph","tokyo","lin","??",""])
negative_popular_token.update(["industry","electric","shanghai","beijing","shenzhen","zhejiang","wang","jiangsu","the","tianjin","zhang","suzhou","yu","academy","energy","wei","chinese","college","an","shandong","chen","hong","hua","jin","anhui","chongqing","yang","guangdong","xi","xin","yi","sheng","jun","ming","nanjing","jian","xian","zhi","guo","ningbo","qingdao","guangzhou","wuxi","national","ag","chengdu","hai","hangzhou","hui","yong","de","xiao","prec","rui","fu","wu","wen","wuhan","sichuan","s","cheng","qi","qing","long","yan","shi","zhong","jia","dongguan","xiang","a","yuan","ying","bo","sci","zhou","henan","dong","zhao","xing","huang","fujian","changzhou","ji","harbin","he","yun","korea","lee","park","young","coop","sung","jin","found","nat","jung","dong","jong","yong","sang","jae","won","taiwan","lin","entpr","huang","li","cheng","ming","yi","yu","wen","yang","wu","au","chang","hong","chi","sheng","wei","chung","fu","mei","hua","guo","feng","ching","hung","kogyo","seiko","eng"])

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace("+"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)   
    
    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'latin')) if unicodedata.category(c) != 'Mn')
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
        
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
    
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()

def pop_token(candidate):
    words = candidate.split(" ")
    if len(words[0]) >= 3:
            pop = words[0]
    elif len(words)>1:
            pop = words[0] + " " + words[1]
    else:
            pop = candidate
    return pop

name_level_mapping = {}
cluster_level_mapping = {}
source = {}

ciq = indexCapIq()

con_d = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_d = con_d.cursor('iter_d')
cur_d.itersize = 10000
query_d = "select * from dolcera.rerun_resolvedstuff_final_new_ind_bkp order by frequency desc"
cur_d.execute(query_d)

counter = 0
clst_resolved_data = defaultdict(list)

for a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21 in cur_d:
    if a2 not in clst_resolved_data:
        clst_resolved_data[a2] = [a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21]
        source[a2] = "Clean Cluster"
    if a2 not in cluster_level_mapping:
        cluster_level_mapping[a2] = [a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21]
    counter += 1
    if counter%10000 == 0:
        print "Cluster rep - resolved clusters data", counter

# reader = readCSV("../new_CPCIndustry.csv")
# ind_ipc_probs = defaultdict(list)

# for row in reader:
#     ind = row[0]
#     class_codes = row[1].split("|")
#     prob = row[2].split("|")
    
#     cls_prob_dict = {}
#     for idx, i in enumerate(class_codes):
#         cls_prob_dict[i] = prob[idx]
#     ind_ipc_probs[ind] = cls_prob_dict
    
con4 = psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur4 = con4.cursor('iter-4')
cur4.itersize = 10000
query4 = "select * from dolcera.rerun_ipcdict_assgn_gencan"
cur4.execute(query4)

cpc_dict_assgn = defaultdict(dict)
counter = 0
for assgn, cpc, counts in cur4:
    counter = counter + 1
    if counter%10000==0:
        print counter,"Reading CPC codes_assgn"
    code_assgn = cpc
    sum_counts = sum(counts)

    code_counts_dict = {}
    for idx, cd in enumerate(code_assgn):
        code_counts_dict[cd] = code_counts_dict.get(cd,0)+counts[idx]
        
    sorted_x = sorted(code_counts_dict.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    
    codes = []
    code_counts = []
    sumValue = 0
    for x in sorted_x:
        if sumValue <= 0.8*sum_counts:
            codes.append(x[0])
            code_counts.append(x[1])
            sumValue += x[1]  
    for rindex,c in enumerate(codes):
        cpc_dict_assgn[assgn][c] = code_counts[rindex]
        
con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.rerun_resolvedstuff_final_new_ind_bkp order by frequency desc"
cur.execute(query)

counter = 0
for a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21 in cur:
    std_name = ciq.getStandardizedName(a3)
    if a20 == 1 and (a2 in clst_resolved_data):
        if clst_resolved_data[a2][-1] == 0:
            name_level_mapping[std_name] = [a1,a2]
            name_level_mapping[std_name].extend(clst_resolved_data[a2][2:])
    else:
        name_level_mapping[std_name] = [a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21]
    if counter%100000 == 0:
        print "Resolved Clusters", counter
    counter = counter + 1

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor('iter-2')
cur2.itersize = 10000
query2 = "select * from dolcera.rerun_invpair_merges order by intersection desc, similarity_distance desc"
cur2.execute(query2)

counter = 0
for c1, c2, invc1, invc2, intersection, cntry_c1, cntry_c2, cpc_c1, cpc_c2, comm_ctry, comm_cpc, aliases, inv_ratio, edit_distance in cur2:
    counter += 1
    if counter%10000 == 0:
        print counter, "Inv pair"
    if (c1 in cluster_level_mapping and c2 not in cluster_level_mapping) or (c2 in cluster_level_mapping and c1 not in cluster_level_mapping):
        common_country = list(set(cntry_c1) & set(cntry_c2))
        common_cpc = list(set(cpc_c1) & set(cpc_c2))
        
        if len(cntry_c1) > 0 and len(cntry_c2) > 0:
            len_common_country = len(common_country)
        else:
            len_common_country = 1
            
        if len(cpc_c1) > 0 and len(cpc_c2) > 0:
            len_common_cpc = len(common_cpc)
        else:
            len_common_cpc = 1
            
        if len_common_country > 0 or len_common_cpc > 0: 
            flag = 0
            inv_flag = ""
            if edit_distance >= 0.85:
                flag = 1
                inv_flag = "Inventor 1"
            if edit_distance >= 0.8 and inv_ratio >= 0.1:
                flag = 1
                inv_flag = "Inventor 2"
            if edit_distance >= 0.7 and inv_ratio >= 0.3:
                flag = 1        
                inv_flag = "Inventor 3"
            if c1 in cluster_level_mapping:
                rc = c1
                uc = c2
            else:
                rc = c2
                uc = c1  
            pair = [rc,uc]            
            if pop_token(rc) in uc and pop_token(rc).lower() not in negative_popular_token:
                flag = 1
                inv_flag = "Inventor 4"
            if flag == 1:
                cluster_level_mapping[uc] = [en for en in cluster_level_mapping[rc]]
                source[uc] = inv_flag

con3 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur3 = con3.cursor('iter-3')
cur3.itersize = 10000
query3 = "select * from dolcera.rerun_cocluster_merges order by intersection desc, similarity_distance desc"
cur3.execute(query3)

counter = 0
for c1, c2, freqc1, freqc2, intersection, cntry_c1, cntry_c2, cpc_c1, cpc_c2, comm_ctry, comm_cpc, aliases, cooc_ratio, edit_distance in cur3:
    counter += 1
    if counter%10000 == 0:
        print counter, "Cocluster pair"
    if (c1 in cluster_level_mapping and c2 not in cluster_level_mapping) or (c2 in cluster_level_mapping and c1 not in cluster_level_mapping):
        common_country = list(set(cntry_c1) & set(cntry_c2))
        common_cpc = list(set(cpc_c1) & set(cpc_c2))
        
        if len(cntry_c1) > 0 and len(cntry_c2) > 0:
            len_common_country = len(common_country)
        else:
            len_common_country = 1
            
        if len(cpc_c1) > 0 and len(cpc_c2) > 0:
            len_common_cpc = len(common_cpc)
        else:
            len_common_cpc = 1
            
        if len_common_country > 0 or len_common_cpc > 0: 
            flag = 0
            if edit_distance >= 0.8 and cooc_ratio >= 0.1:
                flag = 1
            elif edit_distance >= 0.7 and cooc_ratio >= 0.3:
                flag = 1        
            if c1 in cluster_level_mapping:
                rc = c1
                uc = c2
            else:
                rc = c2
                uc = c1  
            pair = [rc,uc]            
            if pop_token(rc) in uc and pop_token(rc).lower() not in negative_popular_token: # and cooc_ratio >= 0.3:
                flag = 1
                
            if flag == 1:
                cluster_level_mapping[uc] = cluster_level_mapping[rc]
                source[uc] = "Cocluster"

con4 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur4 = con4.cursor('iter-4')
cur4.itersize = 10000
query4 = "select * from dolcera.rerun_editdistance_merges order by inv_counts desc, cooc_counts desc, similarity_distance desc"
cur4.execute(query4)

counter = 0
for c1, c2, cntry_c1, cntry_c2, cpc_c1, cpc_c2, comm_ctry, comm_cpc, aliases, edit_distance, inv_counts, cooc_counts in cur4:
    counter += 1
    if counter%10000 == 0:
        print counter, "Edit distance pair"
    if (c1 in cluster_level_mapping and c2 not in cluster_level_mapping) or (c2 in cluster_level_mapping and c1 not in cluster_level_mapping):
        if c1 in cluster_level_mapping:
            rc = c1
            uc = c2
        else:
            rc = c2
            uc = c1
        if edit_distance >= 0.85:
            cluster_level_mapping[uc] = cluster_level_mapping[rc]
            source[uc] = "Edit distance"
        elif pop_token(rc) in uc and pop_token(rc).lower() not in negative_popular_token: # and cooc_ratio >= 0.3:
            cluster_level_mapping[uc] = cluster_level_mapping[rc]
            source[uc] = "Edit distance"

## Write final sheet
# CLEANMATCH.csv - Metadata: (Ulti_Std,Orig_std,Orig_Name,Cluster_rep,Orig_Std_Name,Capiq_ID, Capiq_Orig_Name,Ulti_ID,Ulti_Flag,Immediate_parent,Ulti_parent,Industry,Sub_Count,City,Website,Country,Score,CPC_Grade,Ind_Score,Flag,Ulti_geo,source)

reader = readCSV("../formattedAlias_New.csv")
zzz = writeCSV("../JAN_2019_CLEANMATCH.csv")
for row in reader:
    array =[]
    array.extend(row)
    if row[1] in name_level_mapping:
        array.extend(name_level_mapping[row[1]])
        array.append("Main Name")
    elif row[0] in cluster_level_mapping:
        array.extend(cluster_level_mapping[row[0]])
        array.append(source[row[0]])
    zzz.writerow(array)