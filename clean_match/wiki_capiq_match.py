import requests
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("?"," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
ASN_PATH="../"
CAPIQ_FILEPATH = "../"
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
                self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def getStandardizedName(self,name):
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
                stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
                stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
session =requests.session()
icq = indexCapIq()
def capiqMapping(website,search,cities):
    cities = ['"'+unidecode_expect_nonascii(ct.replace(" AND "," ")).replace("&","")+'"' for ct in cities]
    cities = "("+" OR ".join(cities) + ")"
    search = unidecode_expect_nonascii(search.split("(")[0])
    search1 = icq.getStandardizedName(search)
    search2 = icq.getStrippedName(search)
    search1 =search1.replace('"',"")
    search2 =search2.replace('"',"")

    url_search1 = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search1 = url_search1.replace("{search1}",search1.replace("&","%26"))
    url_search1 = url_search1.replace(" ","%20")
    try:
        data_search1=session.get(url_search1).json()
    except IndexError,e:
        data_search1=""
        
    url_search2 = "http://work1:8983/solr/companycore/select?indent=on&q=(stripped_name_keywords:%22{search2}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search2 = url_search2.replace("{search2}",search2.replace("&","%26"))
    url_search2 = url_search2.replace(" ","%20")
    try:
        data_search2=session.get(url_search2).json()
    except IndexError,e:
        data_search2=""   

    return data_search1, data_search2

reader = readCSV("../geoLookup.csv")
transformation_dict = {}
transformation_dict_not ={}
for row in reader:
    transformation_dict[row[0]]=row[1]
reader = readCSV("../industryCPC.csv")
code_transformation = {}
for row in reader:
    code_transformation[row[0]] = row[1].split("|")+["Industrial Conglomerates"]

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.an_cpcdict"
cur.execute(query)

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "insert into dolcera.an_wiki_capiq_match values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

# con3 = psycopg2.connect(
# "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
# cur3 = con3.cursor()
# query3 = "insert into dolcera.an_wiki_capiq_dump2 values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

alias_lookup = {}
reader = readCSV("../wiki/parse_wiki_data_stage2.csv")
for rindex, row in enumerate(reader):
    if row[0]!= "" and row[0] not in alias_lookup:
        alias_lookup[row[0]] = []
        alias_lookup[row[0]].append(row[0])
        alias_lookup[row[0]].extend(row[1].split("|"))
    alias_lookup[row[0]].extend(row[1].split("|"))
        
reader = readCSV("../KB_matches.csv")

# Iterate through the KB matches (Metadata: orig_name, ultimate_name, matched_entry["main"], matched_entry["website"], matched_entry["place"],city, matched_entry["parent"], website_lookup.get(matched_entry["parent"], ""))

cnt = 0

for row in reader:
    cnt = cnt+1
    if cnt%1000 == 0:
        print cnt
        con2.commit()
    counter = 0
    match = 0
    cities= set()
    if row[4]!="":
        ct=ast.literal_eval(row[4].upper())
        ct = [c.replace('"',"") for c in ct]
        cities.update(ct) 
    if row[5]!="":
        ct=ast.literal_eval(row[5].upper())
        if len(ct)>10:
            ct=ct[:10]
        cities.update(ct)

# If matched_entry["main"] is not an empty string, retrive the capiq entry by matching matched_entry[main] with standardized_name_keywords

    if row[2]!="":
        candidates = []
        for name in alias_lookup[row[2]]:
            if name!= "":
                website = row[3]
                search = name
                search1 = icq.getStandardizedName(search)
                search2 = icq.getStrippedName(search)
                data_std, data_strip =  capiqMapping(website,search,list(cities))

        # Process the standardized matches from capiq
                if search1.upper() != search2.upper():
                    rows_std = len(data_std['response']['docs'])
                    data_std = data_std['response']['docs']

                    if rows_std > 0:
                        for m in data_std:
                            bonus = 8
                            website_match = 0
                            city_match = 0      

                            website_incoming = m.get("website","")
                            website_incoming = str(tldextract.extract(website_incoming).registered_domain)
                            city_incoming = m.get("city","")

                            if website_incoming!= "" and website_incoming == website:
                                website_match = 1
                            if city_incoming!= "" and city_incoming.upper() in list(cities):
                                city_match = 1    

                            solr_row = []
                            solr_row.append(m.get("id",""))
                            solr_row.extend(m.get("original_name",""))
                            solr_row.extend(m.get("ultimateParentID",""))
                            solr_row.extend(m.get("ultimateFlag",""))
                            solr_row.append(m.get("immParent",""))
                            solr_row.append(m.get("ultiParent",""))
                            solr_row.append(m.get("industry",""))
                            solr_row.append(m.get("subsidiary_count",""))
                            solr_row.append(m.get("city",""))
                            solr_row.append(m.get("website",""))
                            solr_row.append(m.get("country",""))               
                            array = []
    #                         array.append(unicode(row[0], "utf-8"))
    #                         array.append(unicode(row[1], "utf-8"))
    #                         array.append(unicode(search, "utf-8"))
    #                         array.append(unicode(search1, "utf-8"))
    #                         array.append(unicode(search2, "utf-8"))
                            array.append(row[0])
                            array.append(row[1])
                            array.append(search)
                            array.append(list(cities))
                            array.extend(solr_row)
                            array[-1] = array[-1].replace("||","")
                            array.extend([bonus+website_match*32+city_match*16,1])
                            candidates.append(array)

                rows_strip = len(data_strip['response']['docs'])
                data_strip = data_strip['response']['docs']

                if rows_strip > 0:
                    for m in data_strip:
                        bonus = 0
                        website_match = 0
                        city_match = 0      

                        website_incoming = m.get("website","")
                        website_incoming = str(tldextract.extract(website_incoming).registered_domain)
                        city_incoming = m.get("city","")

                        if website_incoming!= "" and website_incoming == website:
                            website_match = 1
                        if city_incoming!= "" and city_incoming.upper() in list(cities):
                            city_match = 1    

                        solr_row = []
                        solr_row.append(m.get("id",""))
                        solr_row.extend(m.get("original_name",""))
                        solr_row.extend(m.get("ultimateParentID",""))
                        solr_row.extend(m.get("ultimateFlag",""))
                        solr_row.append(m.get("immParent",""))
                        solr_row.append(m.get("ultiParent",""))
                        solr_row.append(m.get("industry",""))
                        solr_row.append(m.get("subsidiary_count",""))
                        solr_row.append(m.get("city",""))
                        solr_row.append(m.get("website",""))
                        solr_row.append(m.get("country",""))               
                        array = []
#                         array.append(unicode(row[0], "utf-8"))
#                         array.append(unicode(row[1], "utf-8"))
#                         array.append(unicode(search, "utf-8"))
#                         array.append(unicode(search1, "utf-8"))
#                         array.append(unicode(search2, "utf-8"))
                        array.append(row[0])
                        array.append(row[1])
                        array.append(search)
#                         array.append(search1)
#                         array.append(search2)
#                         print search2
                        array.append(list(cities))
                        array.extend(solr_row)
                        array[-1] = array[-1].replace("||","")
                        array.extend([bonus+website_match*32+city_match*16,2])
                        candidates.append(array)

        if len(candidates) > 0:
            candidates = sorted(candidates, key = lambda x: (x[-2],x[11]), reverse=True)

            conflict = -1
            max_score = candidates[0][-2]
            ulti_parent = candidates[0][6]
            ulti_parents = set()
            
            for c in candidates:
                if c[-2] == max_score and c[6] not in list(ulti_parents):
                    conflict = conflict+1 
                    ulti_parents.add(c[6])

            for c in candidates:
                c.extend([conflict])
                if c[-3] >= 24:
                    try:
                        cur2.execute(query2,tuple(c))
                        break
                    except Exception, e:
                        print e
                        print "Resolved Capiq entry",array
                    
                    
con2.commit()

