import requests
import json
#!/usr/bin/env python
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace("+"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
ASN_PATH="../"
CAPIQ_FILEPATH = "../"
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
session =requests.session()
icq = indexCapIq()
def capiqMapping(website,search,cities):
    cities = ['"'+unidecode_expect_nonascii(ct.replace(" AND "," ")).replace("&","")+'"' for ct in cities]
    cities = "("+" OR ".join(cities) + ")"
    search = unidecode_expect_nonascii(search.split("(")[0])
    search1 = icq.getStandardizedName(search)
    search2 = icq.getStrippedName(search)
    search1 =search1.replace('"',"")
    search2 =search2.replace('"',"")

    url_search1 = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search1 = url_search1.replace("{search1}",search1.replace("&","%26"))
    url_search1 = url_search1.replace(" ","%20")
    try:
        data_search1=session.get(url_search1).json()
    except IndexError,e:
        data_search1=""
        
    url_search2 = "http://work1:8983/solr/companycore/select?indent=on&q=(stripped_name_keywords:%22{search2}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search2 = url_search2.replace("{search2}",search2.replace("&","%26"))
    url_search2 = url_search2.replace(" ","%20")
    try:
        data_search2=session.get(url_search2).json()
    except IndexError,e:
        data_search2=""   

    return data_search1, data_search2

# def capiqMapping(website,search,cities):
#     cities = ['"'+unidecode_expect_nonascii(ct.replace(" AND "," ")).replace("&","")+'"' for ct in cities]
#     cities = "("+" OR ".join(cities) + ")"
#     search = unidecode_expect_nonascii(search.split("(")[0]) # Check with Prem what are we doing here
#     search1 = icq.getStandardizedName(search)
#     search2 = icq.getStrippedName(search)
#     url = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22 OR stripped_name_keywords:%22{search2}%22) AND city:{cities} &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
#     url = url.replace("{website}",website)
#     url = url.replace("{search1}",search1.replace("&","%26"))
#     url = url.replace("{search2}",search2.replace("&","%26"))
#     url = url.replace("{cities}",cities)
#     url = url.replace(" ","%20")
# #     print url
#     data = session.get(url).text
#     try:
#         data=session.get(url).text.replace("\n","||")
#         #data=urllib2.urlopen(url).read().replace("\n","||")
#     except IndexError,e:
#         return "Error!!"
#     data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||","")
#     if data=="":
#         return None
#     msplit = list(csv.reader([data]))
#     if len(msplit)>0:
        
#         return msplit[0]

def capiqMap_SubCnt(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}",id)
    url = url.replace(" ","%20")
    data = session.get(url).text
    try:
        data=session.get(url).text.replace("\n","||")
    except IndexError,e:
        return "Error!!"
    data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||","")
    if data=="":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit)>0:     
        return msplit[0]
    
def capiqMap_geography(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}",id)
    url = url.replace(" ","%20")
    data = session.get(url).text
    try:
        data=session.get(url).text.replace("\n","||")
    except IndexError,e:
        return "Error!!"
    data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||","")
    if data=="":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit)>0:     
        return msplit[0]
    
def getResolvedClusters():
    reader = readCSV("../geoLookup.csv")
    transformation_dict = {}
    transformation_dict_not ={}
    for row in reader:
        if row[0] == 'HK': 
            transformation_dict[row[0]] = 'Hong Kong'
        elif row[0] == 'MO':
            transformation_dict[row[0]] = 'Macau'
        elif row[0] == 'CD':
            transformation_dict[row[0]] = 'Democratic Republic of the Congo'            
        elif row[0] == 'IR':
            transformation_dict[row[0]] = 'Iran'
        elif row[0] == 'MK':
            transformation_dict[row[0]] = 'Macedonia'
#         if row[1] == 'Micronesia, Federated States of':
#             transformation_dict[row[0]] = 'Macau'
        elif row[0] == 'PS':
            transformation_dict[row[0]] = 'Palestinian Authority'
        elif row[0] == 'TW':
            transformation_dict[row[0]] = 'Taiwan'
        elif row[0] == 'TZ':
            transformation_dict[row[0]] = 'Tanzania'
#         if row[1] == 'Virgin Islands, US':
#             transformation_dict[row[0]] = 'Macau'
        else:
            transformation_dict[row[0]]=row[1]

    reader = readCSV("../industryCPC.csv")
    code_transformation = {}
    for row in reader:
        code_transformation[row[0]] = row[1].split("|")+["Industrial Conglomerates"]

    reader = readCSV("../CPCindustry.csv")
    industry_cpc_dict = {}
    for row in reader:
        industry_cpc_dict[row[0]]= row[1].split("|")

        
    con_alias = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_alias = con.cursor('iter_alias')
    cur_alias.itersize = 10000
    query_alias = "select * from dolcera.an_final_alias_table"
    cur_alias.execute(query_alias)
    
    orig_freq_dict = {}
    counter = 0
    for ulti, orig, countries, cities, freq in cur_alias:
        if counter%10000 == 0:
            print counter, "orig_name freq dict'
        orig_freq_dict[orig] = freq
        
    con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor('iter-1')
    cur.itersize = 10000
    query = "select * from dolcera.an_cpcdict"
    cur.execute(query)

    con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur2 = con2.cursor()
    query2 = "insert into dolcera.an_resolvedstuff_research2 values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    
    con4 = psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur4 = con4.cursor('iter-1')
    cur4.itersize = 10000
    query4 = "select * from dolcera.an_cpcdict_assgn"
    cur4.execute(query4)
    
# Iterate through the dolcera.an_cpcdict and create a dictionary {cluster: [cpc1, cpc2]}
    cpc_dict = defaultdict(list)
    cpc_dict_l4 = defaultdict(list)
    counter = 0    
    for cluster, cpc, counts in cur:
        cpc_count_dict = {}
        for idx, i in enumerate(cpc):
            cpc_count_dict[i] = counts[idx]
        sumValue = 0
        for c in counts:
            sumValue = sumValue + c
        sorted_x = sorted(cpc_count_dict.items(),key = operator.itemgetter(1), reverse = True)
        sum = 0
        for x in sorted_x:
            if sum <= 0.8*sumValue:
                cpc_dict_l4[cluster].append(x[0])
                sum += x[1]

        code = cpc
        if len(code)>5:
            code = code[:5]
        code = [c[:3] for c in code if len(c)==4]
        covered=set()
        new_code = []
        for c in code:
            if c not in covered:
                new_code.append(c)
                covered.add(c)
        cpc_dict[cluster] = new_code

        counter = counter + 1
        if counter%10000==0:
            print counter,"Reading CPC codes"
        
    # Iterate through the dolcera.an_cpcdict_assgn and create a dictionary {assgn: [cpc1, cpc2]}        

    cpc_dict_assgn = defaultdict(list)  
    cpc_dict_assgn_l4 = defaultdict(list)
    counter = 0
    code_ind_dict = {}
    for assgn, cpc, counts in cur4:
        counter = counter + 1
        if counter%10000==0:
            print counter,"Reading CPC codes_assgn"
        code_assgn = cpc
            
        cpc_count_dict = {}
        for idx, i in enumerate(cpc):
            cpc_count_dict[i] = counts[idx]
        sumValue = 0
        for c in counts:
            sumValue = sumValue + c
        sorted_x = sorted(cpc_count_dict.items(),key = operator.itemgetter(1), reverse = True)
        sum = 0
        for x in sorted_x:
            if sum <= 0.8*sumValue:
                cpc_dict_assgn_l4[assgn].append(x[0])
                sum += x[1]
            
        sum_counts = 0
        for c in counts:
            sum_counts = sum_counts + c
        code_ind = {}
        code_ind2 = {}
        code_trun = [c[:3] for c in code_assgn if len(c)==4]
        for idx, cd in enumerate(code_trun):
            code_ind[cd] = code_ind.get(cd,0)+counts[idx]
        code_ind2 = {i:code_ind[i] for i in code_ind if (code_ind[i] >= int(0.03*sum_counts) and code_ind[i]>10) }
        sorted_codeind = [i[0] for i in sorted(code_ind2.items(), key=operator.itemgetter(1),reverse=True)]
        code_ind_dict[assgn] = sorted_codeind   
    
        if len(code_assgn)>5:
            code_assgn = code_assgn[:5]
        code_assgn = [c[:3] for c in code_assgn if len(c)==4]
        covered=set()
        new_code = []
        for c in code_assgn:
            if c not in covered:
                new_code.append(c)
                covered.add(c)
        cpc_dict_assgn[assgn] = new_code
        
    reader=readCSV("../DataNew/DataNew.csv")
    
    lines=[]
    dicta={}
    resolved=set()
    normalized_data = []
    cluster_dict = defaultdict(list)
    cluster_dict1 = defaultdict(list)
    cluster_dict2 = defaultdict(list)
    covered = set()
    array = []
    clean_cluster_matches = defaultdict(list)
    country_dict = defaultdict(set)
    ind_ignore = ['Industrial Conglomerates','Diversified Consumer Services','Trading Companies and Distributors', 'Construction and Engineering', 'Diversified Financial Services', 'Commercial Services and Supplies', 'Multiline Retail', 'Distributors', 'Real Estate Management and Development', 'Specialty Retail', 'Professional Services']
#     brk = 0
    
# Iterate through the DataNew.csv ( ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,data1,data2,data3,city,ori_name_ori )
    # data1=solrMatch("standardized_name_keywords",stdName)
    # data2=solrMatch("stripped_name_ws",strippedName.replace(" ",""))
    # data3=solrMatch("standardized_name_keywords",commasplitname)    
    
    for rindex,row in enumerate(reader):
        if rindex%10000==0:
            print rindex
            con2.commit()
        ori_name = row[0]
        std_name = row[1]
        stripped_name = row[2]
        clusterValues = [row[-1]]
        bonus = 0
#         if len(stripped_name)>17:
#             bonus = 20  
        if len(clusterValues)>1:
            choices = clusterValues
            new_cluster = process.extractOne(std_name, choices)[0]
            temp_cluster = new_cluster
        else:
            temp_cluster = list(clusterValues)[0]
        cpc_cluster = icq.getStandardizedName(temp_cluster)
        code_assgn = cpc_dict_assgn.get(std_name,[])
        code = cpc_dict.get(cpc_cluster,[])
        industries = set()
        industries_assgn = set()
        for i in code_assgn:
            industries_assgn.update(code_transformation.get(i,[]))
        for i in code:
            industries.update(code_transformation.get(i,[]))
        cluster = "|".join(clusterValues)
        stripped_name = row[2]
        if row[5]=="['']":
            countries_all=["NoCountry"]
        else:
            countries_all = row[5].replace("[","").replace("]","").replace("'","").split(",")
        country_dict[cluster] = [c for c in countries_all]
        matchFlag = 0
        city = row[-2].upper()
        flag = 0
        if row[1]+cluster in covered:
                continue
        else:
            covered.add(row[1]+cluster)
        chosen_msplit=[]

        covered_codes = set()
        industry_score_dict = {}

        for cindex,c in enumerate(code):
            for industry in code_transformation.get(c,[]):
                if industry not in covered_codes:
                    industry_score_dict[industry] = 32-2*cindex
                    covered_codes.add(industry)

        covered_codes_assgn = set()
        industry_score_dict_assgn = {}

        for cindex,c in enumerate(code_assgn):
            for industry_assgn in code_transformation.get(c,[]):
                if industry_assgn not in covered_codes_assgn:
                    industry_score_dict_assgn[industry_assgn] = 32-2*cindex
                    covered_codes_assgn.add(industry_assgn)

        code_ind_assgn = code_ind_dict.get(std_name,[])
        industries_code_ind_assgn = set()
        for i in code_ind_assgn:
            industries_code_ind_assgn.update(code_transformation.get(i,[]))

        covered_codes_ind = set()
        code_ind_score_dict = {}

        for cindex,c in enumerate(code_ind_assgn):
            for industry in code_transformation.get(c,[]):
                if industry not in covered_codes_ind:
                    code_ind_score_dict[industry] = 40-cindex
                    covered_codes_ind.add(industry)

        for countryindex,country in enumerate(countries_all):
            if flag == 1:
                break
            countries = set([transformation_dict.get(c.strip(),c.strip()) for c in [country] if c.strip()!=""])
            matches_exact = row[6].split("||")[:-1]
            matches = row[7].split("||")[:-1] + row[8].split("||")[:-1]
            countries_incoming = set()
            candidates = []
            new_candidates = []
            if std_name!=stripped_name:

# Iterate through data1 (Metadata: "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||")

                for m in matches_exact:
                    industry_match =0
                    country_match = 0
                    city_match = 0
                    industry_score = 0
                    website = ''
                    code_ind_score = 0
                    m = m.replace("\,","^")
                    msplit = list(csv.reader([m]))
                    msplit = msplit[0]
                    if len(msplit)<7:
                        continue
                    website = msplit[-2]
                    city_incoming = msplit[-3].upper()
                    if city_incoming =="":
                        city_incoming ="City not found"
                    country_incoming = msplit[-1]
                    countries_incoming.add(country_incoming)
                    industry = msplit[6]
                    if industry=="" and msplit[7]=="0" and msplit[0]==msplit[2] and city_incoming=="City not found" and msplit[-2]=="" and "univ" not in std_name.lower() and "acad" not in std_name.lower() and "inst" not in std_name.lower():
                        continue

                    if industry in industry_score_dict_assgn and industry!="":
                        industry_match=1
                        industry_score = industry_score_dict_assgn[industry]
                    else:
                        if industry in industry_score_dict and industry!="":
                            industry_match=1
                            industry_score = industry_score_dict[industry]

                    if industry in code_ind_score_dict and industry!="":
                        industry_match=1
                        code_ind_score = code_ind_score_dict[industry]

                    if industry!= "" and industry_match == 0: #come back
                        capiq_ind_codes = industry_cpc_dict.get(industry,"")
                        
                        if industry == 'Diversified Telecommunication Services' or industry == 'Communications Equipment' or industry == 'Wireless Telecommunication Services':
                            capiq_ind_codes = industry_cpc_dict.get("Diversified Telecommunication Services","") + industry_cpc_dict.get("Communications Equipment","") + industry_cpc_dict.get("Wireless Telecommunication Services","")
                            
                        if industry == 'Electric Utilities' or industry == 'Independent Power and Renewable Electricity Producers' or industry == 'Electrical Equipment' or industry == 'Energy Equipment and Services':
                            capiq_ind_codes = industry_cpc_dict.get("Electric Utilities","") + industry_cpc_dict.get("Independent Power and Renewable Electricity Producers","") + industry_cpc_dict.get("Electrical Equipment","") + industry_cpc_dict.get("Energy Equipment and Services","")
                            
                        if industry == 'Air Freight and Logistics' or industry == 'Containers and Packaging' or industry == 'Airlines':
                            capiq_ind_codes = industry_cpc_dict.get("Air Freight and Logistics","") + industry_cpc_dict.get("Containers and Packaging","") + industry_cpc_dict.get("Airlines","")
                            
                        if industry == 'Life Sciences Tools and Services' or industry == 'Health Care Technology' or industry == 'Healthcare Providers and Services' or industry == 'Healthcare Equipment and Supplies':
                            capiq_ind_codes = industry_cpc_dict.get("Life Sciences Tools and Services","") + industry_cpc_dict.get("Health Care Technology","") + industry_cpc_dict.get("Healthcare Providers and Services","") + industry_cpc_dict.get("Healthcare Equipment and Supplies","")
                            
                        if industry == 'Gas Utilities' or industry == 'Oil, Gas and Consumable Fuels':
                            capiq_ind_codes = industry_cpc_dict.get("Gas Utilities","") + industry_cpc_dict.get("Oil, Gas and Consumable Fuels","")
                            
                        if industry == 'Biotechnology' or industry == 'Pharmaceuticals':
                            capiq_ind_codes = industry_cpc_dict.get("Pharmaceuticals","") + industry_cpc_dict.get("Biotechnology","")

                        if industry == 'Software' or industry == 'IT Services':
                            capiq_ind_codes = industry_cpc_dict.get("Software","") + industry_cpc_dict.get("IT Services","")
                            
                        if industry == 'Construction Materials' or industry == 'Building Products':
                            capiq_ind_codes = industry_cpc_dict.get("Construction Materials","") + industry_cpc_dict.get("Building Products","")
                            
                        if industry == 'Consumer Finance' or industry == 'Banks' or industry == 'Equity Real Estate Investment Trusts (REITs)' or industry == 'Capital Markets' or industry == 'Thrifts and Mortgage Finance' or industry == 'Diversified Financial Services' or industry == 'Insurance':
                            capiq_ind_codes = industry_cpc_dict.get("Consumer Finance","") + industry_cpc_dict.get("Banks","") + industry_cpc_dict.get("Equity Real Estate Investment Trusts (REITs)","") + industry_cpc_dict.get("Capital Markets","") + industry_cpc_dict.get("Thrifts and Mortgage Finance","") + industry_cpc_dict.get("Diversified Financial Services","") + industry_cpc_dict.get("Insurance","")
                            
                        if industry == 'Road and Rail' or industry == 'Transportation Infrastructure':
                            capiq_ind_codes = industry_cpc_dict.get("Road and Rail","") + industry_cpc_dict.get("Transportation Infrastructure","")
                            
                        if industry == 'Automobiles' or industry == 'Auto Components':
                            capiq_ind_codes = industry_cpc_dict.get("Automobiles","") + industry_cpc_dict.get("Auto Components","")
                            
                        if industry == 'Specialty Retail' or industry == 'Multiline Retail':
                            capiq_ind_codes = industry_cpc_dict.get("Specialty Retail","") + industry_cpc_dict.get("Multiline Retail","")
                            
                        if industry == 'Trading Companies and Distributors' or industry == 'Distributors':
                            capiq_ind_codes = industry_cpc_dict.get("Trading Companies and Distributors","") + industry_cpc_dict.get("Distributors","")
                            
                        cpc_cluster_codes_l4 = cpc_dict_l4.get(cpc_cluster,"")
                        cpc_assgn_codes_l4 = cpc_dict_assgn_l4.get(std_name,"")
                        assgn_intersection = list(set(capiq_ind_codes) & set(cpc_assgn_codes_l4))
                        cluster_intersection = list(set(capiq_ind_codes) & set(cpc_cluster_codes_l4))
                        if len(assgn_intersection)>=1 or len(cluster_intersection) >= 1:
                            if (len([i for i in assgn_intersection if i[0:3] != 'Y10']) == 0) and (len([i for i in cluster_intersection if i[0:3] != 'Y10']) == 0):
                                industry_match = 0
                            else:
                                industry_match = 1

#                     if industry!= "" and industry_match == 0 and industry not in ind_ignore:
#                         continue
                        
                    if city_incoming.upper() in city:
                        city_match =1
                    try:
                        itg = int(msplit[7]) 
                    except:
                        itg= 0 

                    if  country_incoming in countries or "NoCountry" in countries:
                        country_match=1
                    else:
                        new_candidates.append([msplit,country_match*16+city_match*32+industry_match*8+4+bonus,itg,industry_score,website, code_ind_score,'capiq-std',industry_match])
#                         continue
                    candidates.append([msplit,country_match*16+city_match*32+industry_match*8+4+bonus,itg,industry_score,website, code_ind_score,'capiq-std'])
            ulti_dict = {}
            new_can = []
            for m in matches:
                industry_match =0
                country_match = 0
                city_match = 0
                industry_score = 0
                website = ''
                code_ind_score = 0
                m = m.replace("\,","^")
                msplit = list(csv.reader([m]))
                msplit = msplit[0]
                if len(msplit)<7:
                        continue
                website = msplit[-2]
                city_incoming = msplit[-3].upper()
                if city_incoming =="":
                    city_incoming ="City not found"
                country_incoming = msplit[-1]
                countries_incoming.add(country_incoming)

                industry = msplit[6]
                if industry=="" and msplit[7]=="0" and msplit[0]==msplit[2] and city_incoming=="City not found" and msplit[-2]=="" and "univ" not in std_name.lower() and "acad" not in std_name.lower() and "inst" not in std_name.lower():
                    continue
                ulti_dict[msplit[2]] = ulti_dict.get(msplit[2],0) + 1 

                if industry in industry_score_dict_assgn and industry!="":
                    industry_match=1
                    industry_score = industry_score_dict_assgn[industry]
                else:
                    if industry in industry_score_dict and industry!="":
                        industry_match=1
                        industry_score = industry_score_dict[industry]

                if industry in code_ind_score_dict and industry!="":
                    industry_match=1
                    code_ind_score = code_ind_score_dict[industry]
                try:
                    itg = int(msplit[7])
                except:
                    itg= 0
#                 new_can.append([msplit,country_match*16+city_match*32+industry_match*8,itg,industry_score, website,code_ind_score])

                if industry!= "" and industry_match == 0:
                    capiq_ind_codes = industry_cpc_dict.get(industry,"")

                    if industry == 'Diversified Telecommunication Services' or industry == 'Communications Equipment' or industry == 'Wireless Telecommunication Services':
                        capiq_ind_codes = industry_cpc_dict.get("Diversified Telecommunication Services","") + industry_cpc_dict.get("Communications Equipment","") + industry_cpc_dict.get("Wireless Telecommunication Services","")

                    if industry == 'Electric Utilities' or industry == 'Independent Power and Renewable Electricity Producers' or industry == 'Electrical Equipment' or industry == 'Energy Equipment and Services':
                        capiq_ind_codes = industry_cpc_dict.get("Electric Utilities","") + industry_cpc_dict.get("Independent Power and Renewable Electricity Producers","") + industry_cpc_dict.get("Electrical Equipment","") + industry_cpc_dict.get("Energy Equipment and Services","")

                    if industry == 'Air Freight and Logistics' or industry == 'Containers and Packaging' or industry == 'Airlines':
                        capiq_ind_codes = industry_cpc_dict.get("Air Freight and Logistics","") + industry_cpc_dict.get("Containers and Packaging","") + industry_cpc_dict.get("Airlines","")

                    if industry == 'Life Sciences Tools and Services' or industry == 'Health Care Technology' or industry == 'Healthcare Providers and Services' or industry == 'Healthcare Equipment and Supplies':
                        capiq_ind_codes = industry_cpc_dict.get("Life Sciences Tools and Services","") + industry_cpc_dict.get("Health Care Technology","") + industry_cpc_dict.get("Healthcare Providers and Services","") + industry_cpc_dict.get("Healthcare Equipment and Supplies","")

                    if industry == 'Gas Utilities' or industry == 'Oil, Gas and Consumable Fuels':
                        capiq_ind_codes = industry_cpc_dict.get("Gas Utilities","") + industry_cpc_dict.get("Oil, Gas and Consumable Fuels","")

                    if industry == 'Biotechnology' or industry == 'Pharmaceuticals':
                        capiq_ind_codes = industry_cpc_dict.get("Pharmaceuticals","") + industry_cpc_dict.get("Biotechnology","")

                    if industry == 'Software' or industry == 'IT Services':
                        capiq_ind_codes = industry_cpc_dict.get("Software","") + industry_cpc_dict.get("IT Services","")

                    if industry == 'Construction Materials' or industry == 'Building Products':
                        capiq_ind_codes = industry_cpc_dict.get("Construction Materials","") + industry_cpc_dict.get("Building Products","")

                    if industry == 'Consumer Finance' or industry == 'Banks' or industry == 'Equity Real Estate Investment Trusts (REITs)' or industry == 'Capital Markets' or industry == 'Thrifts and Mortgage Finance' or industry == 'Diversified Financial Services' or industry == 'Insurance':
                        capiq_ind_codes = industry_cpc_dict.get("Consumer Finance","") + industry_cpc_dict.get("Banks","") + industry_cpc_dict.get("Equity Real Estate Investment Trusts (REITs)","") + industry_cpc_dict.get("Capital Markets","") + industry_cpc_dict.get("Thrifts and Mortgage Finance","") + industry_cpc_dict.get("Diversified Financial Services","") + industry_cpc_dict.get("Insurance","")

                    if industry == 'Road and Rail' or industry == 'Transportation Infrastructure':
                        capiq_ind_codes = industry_cpc_dict.get("Road and Rail","") + industry_cpc_dict.get("Transportation Infrastructure","")

                    if industry == 'Automobiles' or industry == 'Auto Components':
                        capiq_ind_codes = industry_cpc_dict.get("Automobiles","") + industry_cpc_dict.get("Auto Components","")

                    if industry == 'Specialty Retail' or industry == 'Multiline Retail':
                        capiq_ind_codes = industry_cpc_dict.get("Specialty Retail","") + industry_cpc_dict.get("Multiline Retail","")

                    if industry == 'Trading Companies and Distributors' or industry == 'Distributors':
                        capiq_ind_codes = industry_cpc_dict.get("Trading Companies and Distributors","") + industry_cpc_dict.get("Distributors","")
                    
                    cpc_cluster_codes_l4 = cpc_dict_l4.get(cpc_cluster,"")
                    cpc_assgn_codes_l4 = cpc_dict_assgn_l4.get(std_name,"")
                    assgn_intersection = list(set(capiq_ind_codes) & set(cpc_assgn_codes_l4))
                    cluster_intersection = list(set(capiq_ind_codes) & set(cpc_cluster_codes_l4))
                    if len(assgn_intersection)>=1 or len(cluster_intersection) >= 1:
                        if (len([i for i in assgn_intersection if i[0:3] != 'Y10']) == 0) and (len([i for i in cluster_intersection if i[0:3] != 'Y10']) == 0):
                            industry_match = 0
                        else:
                            industry_match = 1

#                 if industry!= "" and industry_match == 0 and industry not in ind_ignore:
#                     continue

                if city_incoming.upper() in city:
                    city_match =1
                    
                if  country_incoming in countries or "NoCountry" in countries:
                    country_match=1
                else:
                    new_candidates.append([msplit,country_match*16+city_match*32+industry_match*8,itg,industry_score,website, code_ind_score,'capiq-strip',industry_match])
#                     continue

                candidates.append([msplit,country_match*16+city_match*32+industry_match*8,itg,industry_score,website,code_ind_score,'capiq-strip'])
            sorted_x = sorted(ulti_dict.items(), key=operator.itemgetter(1))
            sorted_x.reverse()

            if len(candidates)>0:
                candidates = sorted(candidates, key = lambda x: (x[1], x[-4], x[-2], x[-5], x[-3]), reverse=True)
                c = candidates[0]
                flag = 1
                array = []
                array.extend([ori_name,cpc_cluster,std_name])
                array.extend(c[0])
                array.append(c[1])
                array.append(c[-5])
                array.append(c[-3])
                array.append(c[-2])
                array.append(c[-1])
                cur2.execute(query2, tuple(array))
                
    con2.commit()    

if __name__ == "__main__":
    getResolvedClusters()
