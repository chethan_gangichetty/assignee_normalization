import requests
import json
#!/usr/bin/env python
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
ASN_PATH="../"
CAPIQ_FILEPATH = "../"
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
#             print row
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
                self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def getStandardizedName(self,name):
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "
        
        for v in self.AN_variations:
            #print v,name,name.replace(" "+v,self.AN_variations[v])
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
            #print name,v
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
                stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
                stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
session =requests.session()
icq = indexCapIq()
def capiqMapping(website,search,cities):
    cities = ['"'+unidecode_expect_nonascii(ct.replace(" AND "," ")).replace("&","")+'"' for ct in cities]
    cities = "("+" OR ".join(cities) + ")"
    print cities,"Cities"
    search = unidecode_expect_nonascii(search.split("(")[0]) # Check with Prem what are we doing here
    search1 = icq.getStandardizedName(search)
    search2 = icq.getStrippedName(search)
    search1 =search1.replace('"',"")
    search2 =search2.replace('"',"")
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22 OR stripped_name_keywords:%22{search2}%22) AND city:{cities} &wt=json&sort=subsidiary_count%20desc&rows=100&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{website}",website)
    url = url.replace("{search1}",search1.replace("&","%26"))
    url = url.replace("{search2}",search2.replace("&","%26"))
    url = url.replace("{cities}",cities)
    url = url.replace(" ","%20")
    data = session.get(url).text
    try:
        data=session.get(url).json()#text.replace("\n","||")
        #data=urllib2.urlopen(url).read().replace("\n","||")
    except IndexError,e:
        return "Error!!"
    #data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country\n","")
    if data=="":
        return None
    #data = data.replace("||","\n")
    return data
#     msplit = list(csv.reader([data]))
#     if len(msplit)>0:
        
#         return msplit[0]

reader = readCSV("../geoLookup.csv")
transformation_dict = {}
transformation_dict_not ={}
for row in reader:
    transformation_dict[row[0]]=row[1]
reader = readCSV("../industryCPC.csv")
code_transformation = {}
for row in reader:
    code_transformation[row[0]] = row[1].split("|")+["Industrial Conglomerates"]
#     reader=readCSV("../CPCDICT.csv")

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.an_cpcdict"
cur.execute(query)

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "insert into dolcera.an_resolvedstuff_dump_wiki values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

cpc_dict = {}
counter = 0

# Iterate through the dolcera.an_cpcdict and create a dictionary {cluster: [cpc1, cpc2]}

for cluster, cpc, counts in cur:
    counter = counter + 1
    code = row[1]
    if len(code)>5:
        code = code[:5]
    code = set([c[:3] for c in code if len(c)==4])
    cpc_dict[row[0]] = code
    
reader = readCSV("../KB_matches.csv")

# Iterate through the KB matches (Metadata: orig_name, ultimate_name, matched_entry["main"], matched_entry["website"], matched_entry["place"],city, matched_entry["parent"], website_lookup.get(matched_entry["parent"], ""))

cnt = 0

for row in reader:
    cnt = cnt+1
    if cnt%1000 == 0:
        print cnt
    counter = 0
    # break
    match = 0
    cities= set()
    if row[4]!="":
        ct=ast.literal_eval(row[4].upper())
        ct = [c.replace('"',"") for c in ct]
#         print 1, cities
        cities.update(ct) 
#         print 2, cities
    if row[5]!="":
        print row[5],"row5 variable 1"
        ct=ast.literal_eval(row[5].upper())
#         print ct,"ct variable 1"
        if len(ct)>10:
            ct=ct[:10] 
#         print ct,"ct variable 2"
        cities.update(ct)
#         print 3, cities
    if len(cities)==0:
        continue

# If matched_entry["main"] is not an empty string, retrive the capiq entry by matching matched_entry[main] with 

    if row[2]!="":
        website = row[3]
        search = row[2]
        data =  capiqMapping(website,search,list(cities))
        rows = len(data['response']['docs'])
        data = data['response']['docs']
        if rows > 0:
#             msplit = list(csv.reader([data],lineterminator='||'))
#             msplit = list(read_file(data))
            match = 1
            for m in data:
                solr_row = []
                solr_row.append(m.get("id",""))
                solr_row.extend(m.get("original_name",""))
                solr_row.extend(m.get("ultimateParentID",""))
                solr_row.append(m.get("ultimateFlag",""))
                solr_row.append(m.get("immParent",""))
                solr_row.append(m.get("ultiParent",""))
                solr_row.append(m.get("industry",""))
                solr_row.append(m.get("subsidiary_count",""))
                solr_row.append(m.get("city",""))
                solr_row.append(m.get("website",""))
                solr_row.append(m.get("country",""))               
                array = []
                array.append(row[0])
                array.append(row[1])
                array.append("")
                array.extend(solr_row)
                array[-1] = array[-1].replace("||","")
                array.extend([-1])
                cur2.execute(query2, tuple(array))
# Look for parent match
    if len(row)>=7 and row[6]!="" and match==0:
        website = row[7]
        search = row[6]

        data =  capiqMapping(website,search,list(cities))
        rows = len(data['response']['docs'])
        data = data['response']['docs']
        if rows > 0:
            match = 1
            for m in data:
                solr_row = []
                solr_row.append(m.get("id",""))
                solr_row.extend(m.get("original_name",""))
                solr_row.extend(m.get("ultimateParentID",""))
                solr_row.append(m.get("ultimateFlag",""))
                solr_row.append(m.get("immParent",""))
                solr_row.append(m.get("ultiParent",""))
                solr_row.append(m.get("industry",""))
                solr_row.append(m.get("subsidiary_count",""))
                solr_row.append(m.get("city",""))
                solr_row.append(m.get("website",""))
                solr_row.append(m.get("country",""))               
                array = []
                array.append(row[0])
                array.append(row[1])
                array.append("")
                array.extend(solr_row)
                array[-1] = array[-1].replace("||","")
                array.extend([-1])
                cur2.execute(query2, tuple(array))
                    
con2.commit()

