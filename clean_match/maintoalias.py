import psycopg2
import urllib2
from cleanco import cleanco
from unidecode import unidecode
from unidecode import unidecode_expect_nonascii
import requests
import re
import csv
import sys
from collections import defaultdict
from itertools import combinations
import operator
import unicodedata

csv.field_size_limit(sys.maxsize)
ADDRESS="localhost"
CAPIQ_CSV_FILEPATH="FCformat.txt"
CAPIQ_FILEPATH=""
ASN_PATH="../"
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

def removeAllPunctuations(g):
    g= g.replace("."," ")
    g= g.replace(","," ")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/"," ")
    g= g.replace(":"," ")
    g= g.replace(";"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("&","and")
    g= g.replace("+"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g

def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    
class indexCapIq(object):
    def __init__(self):
        self.articles = [" THE "," AND "]
        self.processVariations()
        self.processVariations1()
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVNormal(ASN_PATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            #print row
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVNormal(ASN_PATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)  
    def strip_accents(self,s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()

ciq = indexCapIq()

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select ulti_name, orig_name, countries, cities, frequency, std_name, strip_name, clst_std_name, clst_strip_name from dolcera.rerun_final_full_info_alias_table order by frequency desc"
cur.execute(query)

alias_dict = defaultdict(dict)
alias_dict1 = defaultdict(list)
standardized_dict = {}
cluster_freq_dict = {}
cluster_country_dict = {}
aliasToMain={}
mainToAlias=defaultdict(set)
zzz = writeCSV("../formattedAlias_New.csv")

counter = 0

# Iterate through the final alias table (Metadata: ulti_name, orig_name, countries, cities, frequency)

for ulti_name, orig_name, countries, cities, freq, std_name, strip_name, clst_std_name, clst_strip_name in cur:
    counter += 1
    if counter%10000 == 0:
        print counter
    if counter==5000000000:
        break

    clustername = ulti_name
    companyname = orig_name

# Retrieve the standardized names for ultimate_name and original_name for every entry in the alias table and create a standardizedname dictionary as {orig_name1: stdname1, ulti_name1: stdname2}

    if ulti_name not in standardized_dict:
#         stdname = ciq.getStandardizedName(ulti_name)
        standardized_dict[ulti_name] = clst_std_name    

#     if stdname not in cluster_country_dict:
#         cluster_country_dict[stdname] = countries

    if orig_name not in standardized_dict:
#         stdname = ciq.getStandardizedName(orig_name)
        standardized_dict[orig_name] = std_name

    std1 = standardized_dict[ulti_name]
    std2 = standardized_dict[orig_name]
#     print std2, ciq.getStrippedName(std2)
    aliasToMain[std2]=std1
    mainToAlias[std1].add(std2)
    mainToAlias[std1].add(ciq.getStrippedName(std2))
    if "&" in std2:
        name = std2.replace("&","AND")
        mainToAlias[std1].add(name)
        mainToAlias[std1].add(ciq.getStrippedName(name))

#     zzz.writerow([std1,std2])

# Create dictionaries alias_dict {stdname1_orig:{stdname2_ulti: NNN, stdname3_ulti: NNN}} and alias_dict1 {stdname2_ulti: [stdname1_orig, stdname2_orig...]}

# #         alias_dict[std2][std1] = alias_dict[std2].get(std1,0)+1
#     alias_dict[std2][std1] = cluster_freq_dict.get(std1,0)
#     alias_dict1[std1].append(std2)

con9 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur9 = con9.cursor()
query9 = "insert into dolcera.rerun_maintoalias_new values (%s, %s)"

counter = 0
for i in mainToAlias:
    counter = counter + 1
    alias = [j for j in list(mainToAlias[i])]
    alias_new = []
    for a in alias:
        try:
            b = unicode(a, 'latin-1').decode('latin-1').encode('utf-8')
        except:
            try:
                b = unicode(a, 'utf-8','ignore')
            except:
                b = unicode(a, 'latin-1')
        alias_new.append(b)
    values = (i, alias_new)
    cur9.execute(query9,(values))
    if counter%10000 == 0:
        con9.commit()
        print counter, "miantoalias"
con9.commit()
print "Done writing an_maintoalias_gencan table"
