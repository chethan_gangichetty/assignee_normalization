
import psycopg2
import urllib2
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
import requests
import re
import csv
import sys
import psycopg2

csv.field_size_limit(sys.maxsize)
ADDRESS="localhost"
CAPIQ_CSV_FILEPATH="FCformat.txt"
CAPIQ_FILEPATH=""
ASN_PATH="../"
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter='^', quotechar="'")
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def removeAllPunctuations(g):
    g= g.replace("."," ")
    g= g.replace(","," ")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/"," ")
    g= g.replace(":"," ")
    g= g.replace(";"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("&","and")
    g= g.replace("+"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
s = requests.Session()

def solrMatch(field,query,til=-1):
    if query.strip()=="":
        return ""
    url="http://localhost:8983/solr/companycore/select?fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country&indent=on&q={{field}}:{{name}}&wt=csv&rows=40&sort=subsidiary_count%20desc"
    url=url.replace("{{field}}",field)

    query="%22"+query.replace("&","%26")+"%22"
    if til!=-1:
        query=query.replace("%22","")+"~"+str(til)
    url=url.replace("{{name}}",query)
    url=url.replace(" ","+")
    #print url
    try:
        data=s.get(url).text.replace("\n","||")
        #data=urllib2.urlopen(url).read().replace("\n","||")
    except IndexError,e:
        return "Error!!"
    data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||","")
    return data
start=int(sys.argv[1])
end = start+800000
zzz=writeCSV("../DataNew/DataNew"+sys.argv[1]+".csv")
def matchSolr(ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,city,ori_name_ori):
    data1=solrMatch("standardized_name_keywords",stdName)
    strippedQuery = strippedName.replace(" ","")
    if len(strippedQuery)<5:
        data2=""
    else:
        data2=solrMatch("stripped_name_ws",strippedName.replace(" ",""))
    data3=""
    if data1=="" and data2=="":
        data3=solrMatch("standardized_name_keywords",commasplitname)
    #data4=solrMatch("stripped_name_keywords",commasplitnamestripped)
    zzz.writerow([ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,data1,data2,data3,city,ori_name_ori])
    return True
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()


class indexCapIq(object):
    def __init__(self):
        self.articles = [" THE "," AND "]
        self.processVariations()
        self.processVariations1()
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVNormal(ASN_PATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVNormal(ASN_PATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()

if __name__=="__main__":
    reader=readCSVNormal("../geoLookup.csv")
    geoLookup={}
    capiq=indexCapIq
    for row in reader:
        print row
        geoLookup[row[0].strip()]=row[1].strip()
    f=indexCapIq()

    con = psycopg2.connect(
        "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor('iter-1')
    cur.itersize = 1000
    query = 'select * from dolcera.an_final_alias_table order by frequency desc'

    cur.execute(query)

    counter = 0
    # reader=readCSVNormal("../FinalSampleAlias.csv")

# Iterate through the final_alias_table 
    
    for ulti_name, orig_name, countries, cities, freq in cur:
        #print rindex
        # print counter
        if counter<start:
            counter = counter + 1
            continue
        if counter>=end:
            break
        counter = counter + 1
        if counter%1000==0:
            print counter,"Entries processed"
        n= orig_name
        countries= countries
        cities = cities
        
# Get standardized name and stripped name of original_name using indexCapiq object

        stdName=f.getStandardizedName(n)
        strippedName=f.getStrippedName(stdName)
        commasplitname=""
        commasplitnamestripped=""
        if ";" in n:
            n=n.split(";")[0]
        if "," in n:
                commasplitname=n.split(",")[0]
                if len(commasplitname.split())==1:
                    commasplitname=""
                cstdName=f.getStandardizedName(commasplitname)
                #print cstdName
                commasplitnamestripped=""
        ori_name=ulti_name
        
# Retrive the top 40 matches sorted through subsidiaries count from company solr hosted in 8983 by quering standardized name of original name as standardized_name_keywords

        matchFlag=matchSolr(n,stdName,strippedName,commasplitname,commasplitnamestripped,countries,cities,ori_name)
