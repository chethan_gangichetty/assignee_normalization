import unicodedata
import requests
import json
# !/usr/bin/env python
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import wikipedia
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
# import spacy

# nlp = spacy.load('en')

csv.field_size_limit(sys.maxsize)
ADDRESS = "localhost"
CAPIQ_CSV_FILEPATH = "FCformat.txt"
CAPIQ_FILEPATH = "../"
ASN_PATH = ""


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def readCSVori(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


def removeAllPunctuations(g):
    g = g.replace(".", "")
    g = g.replace(",", "")
    g = g.replace("'", "")
    g = g.replace("-", " ")
    g = g.replace("/", "")
    g = g.replace(":", "")
    g = g.replace(";", "")
    g = g.replace(".", "")
    g = g.replace('"', "")
    g = g.replace("*", "")
    g = g.replace("[", " ")
    g = g.replace("]", " ")
    g = g.replace("(", " ")
    g = g.replace(")", " ")
    g = g.replace("<", " ")
    g = g.replace(">", " ")
    g = g.replace("=", " ")
    g = g.replace(",", " ")
    g = g.replace("+", " ")
    g = g.replace("?", " ")
    g = g.replace("&", "and")
    g = re.sub('\s+', ' ', g).strip()
    return g


def removeSpaces(name):
    while "  " in name:
        name = name.replace("  ", " ")
    return name.strip()

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

class indexCapIq(object):
    def __init__(self):
        # self.s = solr.SolrConnection('http://'+ADDRESS+':8983/solr/companycore')
        self.relations = {}
        self.articles = [" THE ", " AND "]
        self.relationsCount = {}
        self.altNames = defaultdict(list)
        # self.processCountry()
        # self.processIndustry()
        # self.processRelations()
        # self.processAltNames()
        self.processVariations()
        self.processVariations1()
        # self.iterateCapitalIQFile()

    def getCountry(self, country):
        return self.countryCode.get(country, "")

    def getIndustry(self, country):
        return self.industryCode.get(country, "")

    def processCountry(self):
        data = open(CAPIQ_FILEPATH + "CountryGeo.txt", "rb").read()
        data = data.split("#@#@#")
        self.countryCode = {}
        for row in data:
            row = row.split("'~'")
            if len(row) < 2:
                continue
#             print row
            self.countryCode[row[0]] = row[1]

    def processIndustry(self):
        data = open(CAPIQ_FILEPATH + "SimpleIndustry.txt", "rb").read()
        data = data.split("#@#@#")
        self.industryCode = {}
        for row in data:
            row = row.split("'~'")
            if len(row) != 2:
                continue
#             print row
            self.industryCode[row[0]] = row[1]

    def processRelations(self):
        data = open(CAPIQ_FILEPATH + "CompanyRel.txt", "rb").read()
        reader = data.split("#@#@#")
        self.relations = {}
        self.relationsCount = {}
        for rindex, row in enumerate(reader):
            if rindex % 100000 == 0:
                print rindex, "Reading company relations"
            row = row.split("'~'")
            if len(row) < 5:
                continue
            if row[4] == "5" or row[4] == "7" or row[4] == "9":
                # print row
                self.relations[row[3]] = row[2]
        for row in self.relations:
            sub = row
            par = self.relations.get(sub, sub)
            counter = 0
            while par != sub:
                counter = counter + 1
                if counter > 20:
                    break
                self.relationsCount[par] = self.relationsCount.get(par, 0) + 1
                par = self.relations[sub]

    def processAltNames(self):
        data = open(CAPIQ_FILEPATH + "AlternateCompanyNames.txt", "rb").read()
        reader = data.split("#@#@#")
        self.altNames = defaultdict(list)

        for row in reader:
            if len(row) < 4:
                continue
            row = row.split("'~'")
            self.altNames[row[2]].append(row[3])

    def processVariations(self):
        reader = readCSVori(CAPIQ_FILEPATH + "AN_variations_modified.csv")
        self.AN_variations = {}
        for row in reader:
#             print row
            self.AN_variations[row[0].strip()] = row[1].strip()

    def processVariations1(self):
        reader = readCSVori(CAPIQ_FILEPATH + "Bussiness.csv")
        self.replacers = []
        for row in reader:
            self.replacers.append(" " + row[0].upper() + " ")
        self.replacers.append(" " + "CO" + " ")
    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()

    def getStrippedName(self, name):
        if name=="":
            return ""
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


if __name__ == "__main__":
    reader = readCSV("../geoLookup.csv")
    transformation_dict = {}
    transformation_dict_not = {}

    # Create a dictionary transformation_dict by reading geoLookup.csv

    for row in reader:
        transformation_dict[row[0]] = row[1]
    meta_entry = {}
    lookup = defaultdict(list)
#     strip_meta_entry = {}
    strip_lookup = defaultdict(list)
    cq = indexCapIq()
    reader = readCSV("../wiki/parse_wiki_data_stage2.csv")
    website_lookup = {}

    # Iterate through the parse_wiki_data_stage2.csv (Metadata: Name, Alias, Parent, Website, Country, City, Industry)

    for rindex, row in enumerate(reader):

        # Ignore the records having no City or (Website and Parent)
        if row[5] == "" or (row[3] == "" and row[2] == ""):
            continue
        if rindex % 1000 == 0:
            print rindex
        main = row[0]

        # Ignore the records with Industry less than 6 characters
        if len(row) < 6:
            continue
        alias = row[1].split("|")
        website = tldextract.extract(row[3].split("|")[0]).registered_domain
        website_lookup[main] = website
        web = website.replace(".com", "")
        country = row[4].split("|")
        place = unidecode_expect_nonascii(strip_accents(row[5].upper())).split("|")

        # Create a dictionary for every record from wiki data as {"main":name,"website":website,"country":country,"place":place,"parent":parent,"parent_website":""}
        entry = {"main": row[0], "website": website, "country": country, "place": place, "parent": row[2].split("|")[0],
                 "parent_website": ""}

        # Iterate through all the alias_entries for every record in parse_wiki_data_stage2.csv (name = [main_name]+alias+[web])
#         for name in [main] + alias + [web]:
        for name in [main] + alias:
            name = unidecode_expect_nonascii(strip_accents(name)).upper()
            if name.strip() == "":
                continue

            # Create a lookup dictionary having name ([main_name]+alias+[web]), standardizedname and strippedname as keys and its corresponding counter values.

            lookup[name].append(rindex)
            std_name = cq.getStandardizedName(name)
            lookup[std_name].append(rindex)
            stripped_name = cq.getStrippedName(name)
            strip_lookup[stripped_name.replace(" ", "")].append(rindex)
            
            # Create a dictionary meta_entry with counter as key and {"main":name,"website":website,"country":country,"place":place,"parent":parent,"parent_website":""} as values

            meta_entry[rindex] = entry
            
        for name in [web]:
            name = unidecode_expect_nonascii(strip_accents(name)).upper()
            if name.strip() == "":
                continue

            # Create a lookup dictionary having name ([main_name]+alias+[web]), standardizedname and strippedname as keys and its corresponding counter values.

            strip_lookup[name].append(rindex)
            std_name = cq.getStandardizedName(name)
            strip_lookup[std_name].append(rindex)
            stripped_name = cq.getStrippedName(name)
            strip_lookup[stripped_name.replace(" ", "")].append(rindex)        

            meta_entry[rindex] = entry
            
    reader = readCSV("../DataNew/DataNew.csv")
    zzz = writeCSV("../KB_matches_new.csv")
    yyy = writeCSV("Enhanced_KB_Matches.csv")
    flagged_entries = writeCSV("../Multiple_KB_matches.csv")

    # Iterate through the DataNew.csv ( ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,data1,data2,data3,city,ori_name_ori )
    # data1=solrMatch("standardized_name_keywords",stdName)
    # data2=solrMatch("stripped_name_ws",strippedName.replace(" ",""))
    # data3=solrMatch("standardized_name_keywords",commasplitname)

    for rindex, row in enumerate(reader):
        flag = 0
        name = row[1]
        stripped = row[2].replace(" ", "")
        city = row[-2].upper()

        # Ignore the records for which city = "" and neither stdName nor strippedName is present in lookup
        if (name in lookup or stripped in strip_lookup) and city != "":
            ori_country = eval(row[5])
            country = [transformation_dict.get(cn, cn) for cn in ori_country]
        else:
            continue
        matched_entry = {"website": "", "place": "", "main": "", "parent": "NA", "match": "NA"}

        # Search if there is any stdname matches in the lookup and get the first matched entry which has the same city

        if name in lookup:
            for entry in set(lookup[name]):
                if flag == 1:
                    break
                if meta_entry[entry]["website"] == "" and website_lookup.get(meta_entry[entry]["parent"], "") == "":
                    continue
                for ct in meta_entry[entry]["place"]:
                    if ct in city:
                        matched_entry = meta_entry[entry]
                        matched_entry['match'] = 'standard match'
                        flag = 1
                        break

        if stripped in strip_lookup and flag == 0:
            for entry in set(strip_lookup[stripped]):
                if flag == 1:
                    break
                if meta_entry[entry]["website"] == "" and website_lookup.get(meta_entry[entry]["parent"], "") == "":
                    continue
                for ct in meta_entry[entry]["place"]:
                    if ct in city:
                        matched_entry = meta_entry[entry]
                        matched_entry['match'] = 'stripped match'
                        flag = 1
                        break

        if flag == 1:
            zzz.writerow([row[0], row[-1], matched_entry["main"], matched_entry["website"], matched_entry["place"],row[-2].upper(), matched_entry["parent"], website_lookup.get(matched_entry["parent"], ""), matched_entry["match"]])




