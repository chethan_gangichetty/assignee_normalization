import psycopg2
import urllib2
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
import requests
import re
import csv
import sys
import psycopg2
import unicodedata
from collections import defaultdict
csv.field_size_limit(sys.maxsize)
from multiprocessing import Pool

ADDRESS="localhost"
CAPIQ_CSV_FILEPATH="FCformat.txt"
CAPIQ_FILEPATH=""
ASN_PATH="../"
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter='^', quotechar="'")
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def removeAllPunctuations(g):
    g= g.replace("."," ")
    g= g.replace(","," ")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/"," ")
    g= g.replace(":"," ")
    g= g.replace(";"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("&","and")
    g= g.replace("+"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
s = requests.Session()

def solrMatch(field,query,til=-1):
    if query.strip()=="":
        return ""
    url="http://localhost:8983/solr/companycore/select?fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country&indent=on&q={{field}}:{{name}}&wt=csv&rows=40&sort=subsidiary_count%20desc"
    url=url.replace("{{field}}",field)

    query="%22"+query.replace("&","%26")+"%22"
    if til!=-1:
        query=query.replace("%22","")+"~"+str(til)
    url=url.replace("{{name}}",query)
    url=url.replace(" ","+")
    #print url
    try:
        data=s.get(url).text.replace("\n","||")
        #data=urllib2.urlopen(url).read().replace("\n","||")
    except IndexError,e:
        return "Error!!"
    data=data.replace("id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||","")
    return data
def matchSolr(ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,city,ori_name_ori,zzz):
    data1=solrMatch("standardized_name_keywords",stdName)
    strippedQuery = strippedName.replace(" ","")
    if len(strippedQuery)<5:
        data2=""
    else:
        data2=solrMatch("stripped_name_ws",strippedName.replace(" ",""))
    data3=""
    if data1=="" and data2=="":
        data3=solrMatch("standardized_name_keywords",commasplitname)
    #data4=solrMatch("stripped_name_keywords",commasplitnamestripped)
    zzz.writerow([ori_name,stdName,strippedName,commasplitname,commasplitnamestripped,countries,data1,data2,data3,city,ori_name_ori])
    return True
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()


def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

class indexCapIq(object):
    def __init__(self):
        self.relations={}
        self.articles = [" THE "," AND "]
        self.relationsCount = {}
        self.altNames=defaultdict(list)
        self.processVariations()
        self.processVariations1()
        
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"../AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            print row
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"../Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)  
    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," ")) ##Prem
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()


def process(map):
    start = map[0]
    end = map[1]
    zzz = writeCSV("../DataNew/DataNew" + str(start) + ".csv")
#     reader = readCSVNormal("../geoLookup.csv")
#     geoLookup = {}
#     for row in reader:
#         geoLookup[row[0].strip()] = row[1].strip()

    capiq = indexCapIq
    f = indexCapIq()
    con = psycopg2.connect(
        "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor('iter-1')
    cur.itersize = 1000
    query = 'select * from dolcera.rerun_final_alias_table order by frequency desc'

    cur.execute(query)

    counter = 0

    # Iterate through the final_alias_table
    for ulti_name, orig_name, countries, cities, freq in cur:
        # print rindex
        # print counter
        if counter < start:
            counter = counter + 1
            continue
        if counter >= end:
            break
        counter = counter + 1
        if counter % 10000 == 0:
            print counter, "Entries processed"
        n = orig_name
        n = strip_accents(n)
        countries = countries
        cities = cities

        # Get standardized name and stripped name of original_name using indexCapiq object

        stdName = f.getStandardizedName(n)
        strippedName = f.getStrippedName(stdName)
        commasplitname = ""
        commasplitnamestripped = ""
        if ";" in n:
            n = n.split(";")[0]
        if "," in n:
            commasplitname = n.split(",")[0]
            if len(commasplitname.split()) == 1:
                commasplitname = ""
            cstdName = f.getStandardizedName(commasplitname)
            # print cstdName
            commasplitnamestripped = ""
        ori_name = ulti_name

        # Retrive the top 40 matches sorted through subsidiaries count from company solr hosted in 8983 by quering standardized name of original name as standardized_name_keywords

        matchFlag = matchSolr(n, stdName, strippedName, commasplitname, commasplitnamestripped, countries, cities, ori_name,zzz)

    return True

if __name__=="__main__":

    start = 0
    jobs = []

    con_count = psycopg2.connect(
        "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_count = con_count.cursor()
    query_count = 'select count(*) from dolcera.rerun_final_alias_table'

    cur_count.execute(query_count)
    count = 0

    for i, in cur_count:
        count = i

    for x in xrange(start,count, 800000):
        jobs.append((x,x+800000))

    p = Pool(15)
    print p.map(process,jobs)
