import psycopg2
import urllib2
from cleanco import cleanco
from unidecode import unidecode
from unidecode import unidecode_expect_nonascii
import requests
import re
import csv
import sys
from collections import defaultdict
from itertools import combinations
import operator
import unicodedata

csv.field_size_limit(sys.maxsize)
ADDRESS="localhost"
CAPIQ_CSV_FILEPATH="FCformat.txt"
CAPIQ_FILEPATH=""
ASN_PATH="../"
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

def removeAllPunctuations(g):
    g= g.replace("."," ")
    g= g.replace(","," ")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/"," ")
    g= g.replace(":"," ")
    g= g.replace(";"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("&","and")
    g= g.replace("+"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g

def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
class indexCapIq(object):
    def __init__(self):
        self.articles = [" THE "," AND "]
        self.processVariations()
        self.processVariations1()
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVNormal(ASN_PATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            #print row
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVNormal(ASN_PATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def strip_accents(self,s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()

if __name__=="__main__":
    ciq = indexCapIq()

    con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor('iter-1')
    cur.itersize = 10000
    query = "select * from dolcera.rerun_final_full_info_alias_table order by frequency desc"
    cur.execute(query)

    alias_dict = defaultdict(dict)
    alias_dict1 = defaultdict(list)
    standardized_dict = {}
    cluster_freq_dict = {}
    cluster_country_dict = {}
#     aliasToMain={}
#     mainToAlias=defaultdict(set)
    zzz = writeCSV("../formattedAlias_New.csv")


# Iterate through the final alias table (Metadata: ulti_name, orig_name, countries, cities, frequency) and create a cluster_freq_dict {cluster_std: freq}
    counter = 0
    for ulti_name, orig_name, countries, cities, freq, flag, std_name, strip_name, clst_std_name, clst_strip_name in cur:
        counter += 1
        if counter%10000 == 0:
            print counter, "Cluster frequency"
#         std1 = ciq.getStandardizedName(ulti_name)
        std1 = clst_std_name
        if std1 not in cluster_freq_dict:
            cluster_freq_dict[std1] = freq
        if freq>cluster_freq_dict[std1]:
            cluster_freq_dict[std1] = freq
    
    
    con_d = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_d = con_d.cursor('iter-1_d')
    cur_d.itersize = 10000
#     query_d = "select * from dolcera.an_final_alias_table order by frequency desc"
    query_d = "select * from dolcera.rerun_final_full_info_alias_table order by frequency desc" 
    cur_d.execute(query_d)
    
# Iterate through the final alias table (Metadata: ulti_name, orig_name, countries, cities, frequency) 

    counter = 0
    for ulti_name, orig_name, countries, cities, freq, flag, std_name, strip_name, clst_std_name, clst_strip_name in cur_d:
        counter += 1
        if counter%10000 == 0:
            print counter, "Alias dict generation"
        if counter==5000000000:
            break

        clustername = ulti_name
        companyname = orig_name

# Retrieve the standardized names for ultimate_name and original_name for every entry in the alias table and create a standardizedname dictionary as {orig_name1: stdname1, ulti_name1: stdname2}

        if ulti_name not in standardized_dict:
#             stdname = ciq.getStandardizedName(ulti_name)
            stdname = clst_std_name
            standardized_dict[ulti_name] = stdname    

        if stdname not in cluster_country_dict:
            cluster_country_dict[stdname] = countries

        if orig_name not in standardized_dict:
#             stdname = ciq.getStandardizedName(orig_name)
            stdname = std_name
            standardized_dict[orig_name] = stdname

        std1 = standardized_dict[ulti_name]
        std2 = standardized_dict[orig_name]

        zzz.writerow([std1,std2])

# Create dictionaries alias_dict {stdname1_orig:{stdname2_ulti: NNN, stdname3_ulti: NNN}} and alias_dict1 {stdname2_ulti: [stdname1_orig, stdname2_orig...]} where NNN is the frequency taken from cluster_freq_dict (from an_final_alias_table)

    #         alias_dict[std2][std1] = alias_dict[std2].get(std1,0)+1
        alias_dict[std2][std1] = cluster_freq_dict.get(std1,0)
        alias_dict1[std1].append(std2)
        
    reader = readCSV("../INVDATA1_New.csv")
    cluster_to_cpc = defaultdict(dict)
    cluster_to_ipc = defaultdict(dict)
    cluster_to_inv_pair = defaultdict(set)
    inv_pair_cluster = defaultdict(set)
    assignee_to_cpc = defaultdict(dict)
    assignee_to_ipc = defaultdict(dict)
    cluster_to_cluster = {}
    assignee_counts = {}
    cluster_counts = {}

# Iterate through INVDATA1.csv (Metadata: pn, f_epoinv, cpcl3, pa, ad, icl, normpa, cpc, ipc, pcitpn, f_epopa, fam, primary_cpc )

    for rindex,row in enumerate(reader):
        if rindex%10000 == 0:
            print rindex

# Iterate through the patent assignees and extend the standardized names of patent assignees to standardizedname dictionary

        pa = row[3].split("|")
        if rindex==1000000000:
            break
        for entry in pa:
            if entry not in standardized_dict:
                stdname = ciq.getStandardizedName(entry)
                standardized_dict[entry] = stdname
        pa = set([standardized_dict.get(entry,entry) for entry in pa if entry!=""])
        
# Create dictionaries, assignee_counts {assgn_stdname: NNN}, assignee_to_cpc = {assgn_stdname: {cpc1: NNN, cpc2: NNN}}, assignee_to_ipc = {assgn_stdname: {ipc1: NNN, ipc2: NNN}}

        for entry in pa:
            assignee_counts[entry] = assignee_counts.get(entry,0) + 1

        row[3]="|".join(pa)

        pa_assignees = pa
        for entry in pa_assignees:
            if entry=="":
                continue
            cpc = row[2].split("|")
#             cpc = [row[-1]]
            for c in cpc:
                if c!="":
                    assignee_to_cpc[entry][c]=assignee_to_cpc[entry].get(c,0)+1

            ipc_f = [i.replace(" ","") for i in row[8].split("||")]
            ipc_t = [i[0:4] for i in ipc_f if len(i)>4]     
#             ipc_t = [row[-1]]
            for i in ipc_t:
                if i!= "":
                    assignee_to_ipc[entry][i]=assignee_to_ipc[entry].get(i,0)+1

        panew = set()

# Retrieve the cluster representative of the standardized assignees from the alias_dict with the highest frequency of occurence

        for entry in pa:
            entries = alias_dict.get(entry,{entry:1})
            cl = max(entries.iteritems(), key=operator.itemgetter(1))[0]
            cluster_counts[cl] = cluster_counts.get(cl,0) + 1
            panew.add(cl)
        pa = panew
        cpc = row[2].split("|")
#         cpc = [row[-1]]
        ipc_f = [i.replace(" ","") for i in row[8].split("||")]
        ipc_t = [i[0:4] for i in ipc_f if len(i)>4]  
#         ipc_t = [row[-1]]
        inv_pair = combinations(row[1].split("|"),2)

# Iterate through the standardized assignees for every patent entry in INVDATA1.csv, create cluster_to_cpc {std_name1:{cpc1: NNN, cpc2: NNN}}, cluster_to_cpc {std_name1:{ipc1: NNN, ipc2: NNN}}, cluster_to_invpair {std_name1: [(inv1,inv2), (inv3,inv4)]} and cluster_to_cluster {std_name1||std_name2: NNN}

        for entry in pa:
            if entry=="":
                continue
            for c in cpc:
                if c!="":
                    cluster_to_cpc[entry][c]=cluster_to_cpc[entry].get(c,0)+1
                    
            for i in ipc_t:
                if i!= "":
                    cluster_to_ipc[entry][i]=cluster_to_ipc[entry].get(i,0)+1
                    
            for p in inv_pair:
                p = list(p)
                p.sort()
                p = "|".join(p)
                if len(entry)<3:
                    continue
                cluster_to_inv_pair[entry].add(p)
                inv_pair_cluster[p].add(entry)
        pa_pair = combinations(pa,2)
        for entry in pa_pair:
            entry = list(entry)
            entry.sort()
            cluster_to_cluster["||".join(entry)] = cluster_to_cluster.get("||".join(entry),0)+1

    print "Done formatting"    
    
    con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur2 = con2.cursor()
#     query = 'insert into dolcera.an_cpcdict_gencan values(%s, %s, %s)'
    query = 'insert into dolcera.rerun_cpcdict_gencan2 values(%s, %s, %s)'

# Record the contents of the cluster_to_cpc in the reverse order of the frequencies for every cluster entry in an_cpcdict_gencan table
    
    counter = 0
    for cluster in cluster_to_cpc:
        counter = counter+1
        data = cluster_to_cpc[cluster]
        sorted_x = sorted(data.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        cpc = [i[0] for i in sorted_x]
        counts = [i[1] for i in sorted_x]
        cur2.execute(query, ((cluster,cpc, counts)))
        if counter%10000 == 0:
            con2.commit()
            print counter, "cpc dict entries"
    con2.commit()
    
    print "Done writing CPC file"   
    
    con6 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur6 = con6.cursor()
#     query = 'insert into dolcera.an_cpcdict_assgn_gencan values(%s, %s, %s)'
    query = 'insert into dolcera.rerun_cpcdict_assgn_gencan2 values(%s, %s, %s)'

# Record the contents of the assignee_to_cpc in the reverse order of the frequencies for every cluster entry in an_cpcdict_assgn_gencan table
    
    counter = 0
    for assignee in assignee_to_cpc:
        counter = counter+1
        data = assignee_to_cpc[assignee]
        sorted_x = sorted(data.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        cpc = [i[0] for i in sorted_x]
        counts = [i[1] for i in sorted_x]
        cur6.execute(query, ((assignee,cpc, counts)))
        if counter%10000 == 0:
            con6.commit()
            print counter, "assignee_to_cpc entries"
    con6.commit()
    
    print "Done writing CPC_Assignee file"   
    
    con_ipc = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_ipc = con_ipc.cursor()
#     query_ipc = 'insert into dolcera.an_ipcdict_assgn_gencan values(%s, %s, %s)'
    query_ipc = 'insert into dolcera.rerun_ipcdict_assgn_gencan2 values(%s, %s, %s)'

# Record the contents of the assignee_to_ipc in the reverse order of the frequencies for every cluster entry in an_ipcdict_assgn_gencan table
    
    counter = 0
    for assignee in assignee_to_ipc:
        counter = counter+1
        data = assignee_to_ipc[assignee]
        sorted_x = sorted(data.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        cpc = [i[0] for i in sorted_x]
        counts = [i[1] for i in sorted_x]
        cur_ipc.execute(query_ipc, ((assignee,cpc, counts)))
        if counter%10000 == 0:
            con_ipc.commit()
            print counter, "assignee_to_ipc entries"
    con_ipc.commit()
    
    print "Done writing IPC_Assignee file"
    
    con_ipc_c = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_ipc_c = con_ipc_c.cursor()
#     query_ipc_c = 'insert into dolcera.an_ipcdict_gencan values(%s, %s, %s)'
    query_ipc_c = 'insert into dolcera.rerun_ipcdict_gencan2 values(%s, %s, %s)'

# Record the contents of the cluster_to_ipc in the reverse order of the frequencies for every cluster entry in an_cpcdict_assgn table
    
    counter = 0
    for cluster in cluster_to_ipc:
        counter = counter+1
        data = cluster_to_ipc[cluster]
        sorted_x = sorted(data.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        cpc = [i[0] for i in sorted_x]
        counts = [i[1] for i in sorted_x]
        cur_ipc_c.execute(query_ipc_c, ((cluster,cpc, counts)))
        if counter%10000 == 0:
            con_ipc_c.commit()
            print counter, "cluster_to_ipc entries"
    con_ipc_c.commit()
    
    print "Done writing IPC_Cluster file"
    
    con3 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur3 = con3.cursor()
#     query = "insert into dolcera.an_invpair_gencan values (%s, %s)"   
    query = "insert into dolcera.rerun_invpair_gencan2 values (%s, %s)"   

    counter = 0

# Record the contents of the cluster_to_invpair in an_invpair
    
    for cluster in cluster_to_inv_pair:
        counter = counter+1
        data = cluster_to_inv_pair[cluster]
        data =  [i.replace('"',"") for i in data]
        cur3.execute(query,(cluster,data))
        if counter%10000 == 0:
            con3.commit()
            print counter, "invpair entries"
    con3.commit()
    
    print "Done writing inv pair file"
    
    con4 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur4 = con4.cursor()
#     query = "insert into dolcera.an_cocluster_gencan values (%s, %s, %s)"   
    query = "insert into dolcera.rerun_cocluster_gencan2 values (%s, %s, %s)"   

    sorted_x = sorted(cluster_to_cluster.items(), key=operator.itemgetter(1))
    sorted_x.reverse()

    counter = 0

# Record the contents of the cluster_to_cluster in an_cocluster_gencan table
    
    for x in sorted_x:
        counter = counter + 1
        row = (x[0].split("||"))
        row.append(x[1])
        try:
            cur4.execute(query,tuple(row))
        except:
            print row
            pass
        if counter%10000 == 0:
            con4.commit()
            print counter, "cocluster entries"
    con4.commit()
    
    print "Done writing coassignee file"
    
    top_combinations = {}
    for inv in inv_pair_cluster:
        entries = inv_pair_cluster[inv]
        combs = combinations(entries,2)
        for comb in combs:
            comb = sorted(list(comb))
            top_combinations[tuple(comb)] = top_combinations.get(tuple(comb),0) + 1

    print "Top combinations populated"

# Record the contents of dictionary {(Cluster1, CLuster2): NNN}, NNN being the number of common inventor pairs in an_candidates_gencan table in the reverse order of number of common inventor pairs
    
    con5 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur5 = con5.cursor()
#     query = "insert into dolcera.an_candidates_gencan values (%s, %s, %s)"
    query = "insert into dolcera.rerun_candidates_gencan2 values (%s, %s, %s)"

    sorted_x = sorted(top_combinations.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    counter = 0
    for x in sorted_x:
        counter =counter+1
        try:
            cur5.execute(query,(x[0][0], x[0][1], x[1]))
        except:
            print x
            pass
        if counter%10000 == 0:
            con5.commit()
            print counter, "candidates"
    con5.commit()

    print "Top combinations written"

# Record the contents of cluster_counts and assignee_counts into an_cluster_counts and an_assignee_counts respectively
    
    con7 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur7 = con7.cursor()
#     query7 = "insert into dolcera.an_cluster_counts values (%s, %s)"
    query7 = "insert into dolcera.rerun_cluster_counts2 values (%s, %s)"
    
    counter = 0
    for x in cluster_counts:
        counter = counter+1
        cur7.execute(query7,(x,cluster_counts[x]))
        if counter%10000 == 0:
            con7.commit()
            print counter, "cluster counts"
    con7.commit()
    print "Done writing an_cluster_counts table" 
    
    con8 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur8 = con8.cursor()
#     query8 = "insert into dolcera.an_assignee_counts values (%s, %s)"
    query8 = "insert into dolcera.rerun_assignee_counts2 values (%s, %s)"
    
    counter = 0
    for x in assignee_counts:
        counter = counter+1
        cur8.execute(query8,(x,assignee_counts[x]))
        if counter%10000 == 0:
            con8.commit()
            print counter, "assignee counts"
    con8.commit()
    print "Done writing an_assignee_counts table" 
    
    con10 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur10 = con10.cursor()
#     query10 = "insert into dolcera.an_cluster_country_gencan values (%s, %s)"
    query10 = "insert into dolcera.rerun_cluster_country_gencan2 values (%s, %s)"
    
# Record the contents of cluster_country_dict into an_clutser_country_gencan
    
    counter = 0
    for x in cluster_country_dict:
        counter = counter+1
        cur10.execute(query10,(x,cluster_country_dict[x]))
        if counter%10000 == 0:
            con10.commit()
            print counter, "cluster country"
    con10.commit()
    print "Done writing an_cluster_country table"