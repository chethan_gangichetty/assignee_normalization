from __future__ import division
from collections import defaultdict
import Levenshtein
from itertools import combinations
import Levenshtein
from unidecode import unidecode_expect_nonascii
from fuzzywuzzy import fuzz
import urllib2
import json
from cleanco import cleanco
import ast
import re
import csv
import sys
import psycopg2
from collections import defaultdict

def is_ascii(s):
    return all(ord(c) < 128 for c in s)

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace("+"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g

ASN_PATH="../"
CAPIQ_FILEPATH = "../"
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    
class indexCapIq(object):
    def __init__(self):
        self.articles = [" THE "," AND "]
        self.processVariations()
        self.processVariations1()
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVNormal(ASN_PATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVNormal(ASN_PATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
                self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)
    def strip_accents(self,s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
    
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVNormal(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

if __name__=="__main__":
    idx=indexCapIq()
    aliasToMain={}
    mainToAlias=defaultdict(set)

    conx = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    curx = conx.cursor('iter-x')
    curx.itersize = 10000
    queryx = "select * from dolcera.rerun_maintoalias_new"
    curx.execute(queryx)
    
    counter = 0
    for cluster, aliases in curx:
        counter += 1
        if counter%10000 == 0:
            print counter
        mainToAlias[cluster] = aliases

    con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor('iter-1')
    cur.itersize = 10000
    query = "select * from dolcera.rerun_cluster_country_gencan"
    cur.execute(query)

    countryDict={}

    counter = 0
    for cluster, country in cur:
        counter = counter + 1
        if counter%10000 == 0:
            print "Cluster Country", counter
        if cluster not in countryDict and cluster!= '':
            countryDict[cluster] = country

    con_c = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur_c = con.cursor('iter-1_c')
    cur_c.itersize = 10000
    query_c = "select * from dolcera.rerun_cluster_counts"
    cur_c.execute(query_c)

    cluster_freq_dict = {}
    
    counter = 0
    for cluster, counts in cur_c:
        counter = counter + 1
        if counter%10000 == 0:
            print "Cluster Counts", counter            
        if cluster not in cluster_freq_dict and cluster!= '':
            cluster_freq_dict[cluster] = counts

    con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur2 = con2.cursor('iter-2')
    cur2.itersize = 10000
    query2 = "select * from dolcera.rerun_ipcdict_gencan"
    cur2.execute(query2)

    cpcDict = defaultdict(set)
    counter = 0
    for cluster, cpc, counts in cur2:
        for index, c in enumerate(cpc):
            if counts[index]>=2:
                cpcDict[cluster].add(c) 
        counter = counter + 1
        if counter%10000 == 0:
            print "CPC Dict Table", counter

    con4 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur4 = con4.cursor('iter-4')
    cur4.itersize = 10000
    query4 = "select * from dolcera.rerun_cocluster_gencan order by counts desc"
    cur4.execute(query4)

    con6 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur6 = con6.cursor()
    query6 = "insert into dolcera.rerun_cocluster_merges values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    
    xxx = writeCSV("coassigneemerges_errors.csv")
    counter = 0
    for c1, c2, counts in cur4:
        counter = counter + 1
        if counter%10000 == 0:
            print "Candidates", counter
            con6.commit()
        if counts < 5:
            continue
        
        UC = c1
        RC = c2
        
        c1_cpc = cpcDict[UC]
        c2_cpc = cpcDict[RC]

        country1=countryDict.get(UC,[])
        country2=countryDict.get(RC,[])

        count1=cluster_freq_dict.get(UC,0)
        count2=cluster_freq_dict.get(RC,0)

        editdistance=0
        set1=[c for c in mainToAlias[c1] if is_ascii(c)]
        set2=[c for c in mainToAlias[c2] if is_ascii(c)]
        closest_aliases = ""
        for var1 in set1:
            for var2 in set2:
#                 if len(var2.split(" "))<=5 or len(var1.split(" "))<=5:
                distance=Levenshtein.distance(var1,var2)
                if max(len(var1),len(var2)) != 0:
                    comparedistance=1-distance/max(len(var1),len(var2))
                else:
                    comparedistance = 0
                if comparedistance>editdistance:
                    editdistance=comparedistance
                    closest_aliases="||".join([var1,var2])  
        try:
            invratio=int(counts)/min(count1,count2)
        except:
            invratio=0

        common_country = list(set(country1) & set(country2))
        common_cpc = list(set(c1_cpc) & set(c2_cpc))
        try:
            cur6.execute(query6,(UC, RC, count1, count2, counts, country1, country2, list(c1_cpc), list(c2_cpc), common_country, common_cpc, closest_aliases, invratio, editdistance))
        except:
            xxx.writerow([UC, RC, count1, count2, counts, country1, country2, list(c1_cpc), list(c2_cpc), common_country, common_cpc, closest_aliases, invratio, editdistance])
        
    con6.commit()