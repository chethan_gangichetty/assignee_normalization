import requests
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 
from collections import defaultdict
from __future__ import division

con4 = psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur4 = con4.cursor('iter-1')
cur4.itersize = 10000
query4 = "select * from dolcera.an_ipcdict_assgn_gencan"
cur4.execute(query4)

cpc_dict_assgn = defaultdict(dict)
counter = 0
for assgn, cpc, counts in cur4:
    counter = counter + 1
    if counter%10000==0:
        print counter,"Reading CPC codes_assgn"

    code_assgn = cpc
    sum_counts = sum(counts)

#     code_trun = [c[:3] for c in code_assgn if len(c)==4]

    code_counts_dict = {}
    for idx, cd in enumerate(code_assgn):
        code_counts_dict[cd] = code_counts_dict.get(cd,0)+counts[idx]
        
    sorted_x = sorted(code_counts_dict.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    
    codes = []
    code_counts = []
    sumValue = 0
    for x in sorted_x:
        if sumValue <= 0.8*sum_counts:
            codes.append(x[0])
            code_counts.append(x[1])
            sumValue += x[1]  
    for rindex,c in enumerate(codes):
        cpc_dict_assgn[assgn][c] = code_counts[rindex]

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "select * from dolcera.an_resolvedstuff_final5"
cur2.execute(query2)

industry_to_assignee = defaultdict(list)

for Orig_name, Ulti_name ,Std_name ,Id ,original_name ,ultimateParentID ,ultimateFlag ,immParent ,ultiParent ,Industry ,subsidiary_count ,city ,website ,country ,Score ,Cpc_grade ,Ind_score ,Flag,Ulti_geo, reverse_flag in cur2:
    if int(Score) >= 52 and Industry != '':
        industry_to_assignee[Industry].append(Std_name) 

industry_to_cpc = defaultdict(dict)
null_cpc_dict = 0
counter = 0
for ind in industry_to_assignee:
    for assgn in industry_to_assignee[ind]:
        code_cnts_dict = cpc_dict_assgn[assgn]
        if code_cnts_dict == {}:
            null_cpc_dict += 1
        for c in code_cnts_dict:
            industry_to_cpc[ind][c] = industry_to_cpc[ind].get(c,0) + code_cnts_dict[c]

industry_to_cpc_final = defaultdict(list)

for i in industry_to_cpc:
#     if i == 'Containers and Packaging':
    sorted_x = sorted(industry_to_cpc[i].items(), key=operator.itemgetter(1), reverse = True)
    sum_counts = sum(row[1] for row in sorted_x)
    sumValue = 0
    codes_ind = []
    for x in sorted_x:
        if sumValue <= 0.8*sum_counts:
            codes_ind.append((x[0],x[1]/(0.8*sum_counts)))
            sumValue = sumValue + x[1]
    industry_to_cpc_final[i] = codes_ind

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

dummy = writeCSV('../newfile.csv')
for i in industry_to_cpc_final:
    codes = '|'.join([j[0] for j in industry_to_cpc_final[i]])
    dummy.writerow([i,codes])
