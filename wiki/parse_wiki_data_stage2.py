import requests
import json
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

if __name__=="__main__":
    reader = readCSV("parse_wiki_data_stage1.csv")
    all_entries = set()
    for rindex,row in enumerate(reader):
        if rindex ==20:
            pass
        entries = set(row[2].split("|"))
        entries.update(set(row[4].split("|")))
        entries.update(set(row[5].split("|")))
        entries.update(set(row[6].split("|")))
        for entry in entries:
            if len(entry)<3:
                continue
            if entry[0]!="Q":
                continue
            try:
                int(entry[1:])
            except:
                continue
            all_entries.add(entry)
    print len(all_entries)
    session = requests.session()
    lookup = {}
    for chunk in chunks(list(all_entries),50):
        query = "|".join(chunk)
        url = "https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels&ids="+query+"&languages=en&format=json"
        #print url
        text = session.get(url).text
        dicta = json.loads(text)
        counter = 0
        for entry in dicta["entities"]:
            #print entry
            try:
                lookup[entry] = dicta["entities"][entry]["labels"]["en"]["value"]
            except:
                continue
            counter = counter+1
        print counter
    print lookup
    if "Q1384140" in lookup:
        print "True"
    zzz = writeCSV("parse_wiki_data_stage2.csv")
    reader = readCSV("parse_wiki_data_stage1.csv")
    all_entries = set()
    for rindex,a in enumerate(reader):
        if rindex ==20:
            pass
        a[2] = "|".join([lookup.get(entry,entry) for entry in a[2].split("|")])
        a[4] = "|".join([lookup.get(entry,entry) for entry in a[4].split("|")])
        a[5] = "|".join([lookup.get(entry,entry) for entry in a[5].split("|")])
        a[6] = "|".join([lookup.get(entry,entry) for entry in a[6].split("|")])
        zzz.writerow(a)