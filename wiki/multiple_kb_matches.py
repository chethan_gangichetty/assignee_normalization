import requests
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import wikipedia
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
import spacy

csv.field_size_limit(sys.maxsize)
ADDRESS = "localhost"
CAPIQ_CSV_FILEPATH = "FCformat.txt"
CAPIQ_FILEPATH = "../"
ASN_PATH = ""

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def readCSVori(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


def removeAllPunctuations(g):
    g = g.replace(".", "")
    g = g.replace(",", "")
    g = g.replace("'", "")
    g = g.replace("-", " ")
    g = g.replace("/", "")
    g = g.replace(":", "")
    g = g.replace(";", "")
    g = g.replace(".", "")
    g = g.replace('"', "")
    g = g.replace("*", "")
    g = g.replace("[", " ")
    g = g.replace("]", " ")
    g = g.replace("(", " ")
    g = g.replace(")", " ")
    g = g.replace("<", " ")
    g = g.replace(">", " ")
    g = g.replace("=", " ")
    g = g.replace(",", " ")
    g = g.replace("+", " ")
    g = g.replace("?", " ")
    g = g.replace("&", "and")
    g = re.sub('\s+', ' ', g).strip()
    return g


def removeSpaces(name):
    while "  " in name:
        name = name.replace("  ", " ")
    return name.strip()

class indexCapIq(object):
    def __init__(self):
        # self.s = solr.SolrConnection('http://'+ADDRESS+':8983/solr/companycore')
        self.relations = {}
        self.articles = [" THE ", " AND "]
        self.relationsCount = {}
        self.altNames = defaultdict(list)
        # self.processCountry()
        # self.processIndustry()
        # self.processRelations()
        # self.processAltNames()
        self.processVariations()
        self.processVariations1()
        # self.iterateCapitalIQFile()

    def getCountry(self, country):
        return self.countryCode.get(country, "")

    def getIndustry(self, country):
        return self.industryCode.get(country, "")

    def processCountry(self):
        data = open(CAPIQ_FILEPATH + "CountryGeo.txt", "rb").read()
        data = data.split("#@#@#")
        self.countryCode = {}
        for row in data:
            row = row.split("'~'")
            if len(row) < 2:
                continue
            print row
            self.countryCode[row[0]] = row[1]

    def processIndustry(self):
        data = open(CAPIQ_FILEPATH + "SimpleIndustry.txt", "rb").read()
        data = data.split("#@#@#")
        self.industryCode = {}
        for row in data:
            row = row.split("'~'")
            if len(row) != 2:
                continue
            print row
            self.industryCode[row[0]] = row[1]

    def processRelations(self):
        data = open(CAPIQ_FILEPATH + "CompanyRel.txt", "rb").read()
        reader = data.split("#@#@#")
        self.relations = {}
        self.relationsCount = {}
        for rindex, row in enumerate(reader):
            if rindex % 100000 == 0:
                print rindex, "Reading company relations"
            row = row.split("'~'")
            if len(row) < 5:
                continue
            if row[4] == "5" or row[4] == "7" or row[4] == "9":
                # print row
                self.relations[row[3]] = row[2]
        for row in self.relations:
            sub = row
            par = self.relations.get(sub, sub)
            counter = 0
            while par != sub:
                counter = counter + 1
                if counter > 20:
                    break
                self.relationsCount[par] = self.relationsCount.get(par, 0) + 1
                par = self.relations[sub]

    def processAltNames(self):
        data = open(CAPIQ_FILEPATH + "AlternateCompanyNames.txt", "rb").read()
        reader = data.split("#@#@#")
        self.altNames = defaultdict(list)

        for row in reader:
            if len(row) < 4:
                continue
            row = row.split("'~'")
            self.altNames[row[2]].append(row[3])

    def processVariations(self):
        reader = readCSVori(CAPIQ_FILEPATH + "AN_variations_modified.csv")
        self.AN_variations = {}
        for row in reader:
            print row
            self.AN_variations[row[0].strip()] = row[1].strip()

    def processVariations1(self):
        reader = readCSVori(CAPIQ_FILEPATH + "Bussiness.csv")
        self.replacers = []
        for row in reader:
            self.replacers.append(" " + row[0].upper() + " ")
        self.replacers.append(" " + "CO" + " ")

    def getStandardizedName(self, name):
        name = unidecode_expect_nonascii(name.replace(".", "").replace(",", " "))
        name = removeSpaces(name)
        name = removeAllPunctuations(name)
        name = " " + name.upper() + " "

        for v in self.AN_variations:
            # print v,name,name.replace(" "+v,self.AN_variations[v])
            newv = " " + v + " "
            name = name.replace(newv, " " + self.AN_variations[v] + " ")
            # print name,v
        name = removeSpaces(name)
        return name.strip()

    def getStrippedName(self, name):
        stripped_name = cleanco(name).clean_name()
        stripped_name = " " + stripped_name + " "
        for r in self.replacers:
            stripped_name = stripped_name.replace(r, " ")
        for r in self.articles:
            stripped_name = stripped_name.replace(r, " ")
        stripped_name = removeSpaces(stripped_name)
        return stripped_name.strip()


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

reader = readCSV("../geoLookup.csv")

transformation_dict = {}
transformation_dict_not = {}

# Create a dictionary transformation_dict by reading geoLookup.csv

for row in reader:
    transformation_dict[row[0]] = row[1]
meta_entry = {}
lookup = defaultdict(list)
cq = indexCapIq()
reader = readCSV("parse_wiki_data_stage2.csv")
website_lookup = {}


for rindex, row in enumerate(reader):

    # Ignore the records having no City or (Website and Parent)
    if row[5] == "" or (row[3] == "" and row[2] == ""):
        continue

    if rindex % 1000 == 0:
        print rindex
    
    if rindex == 1000:
        break
    
    main = row[0]

    # Ignore the records with Industry less than 6 characters
    if len(row) < 6:
        continue

    alias = row[1].split("|")
    website = tldextract.extract(row[3].split("|")[0]).registered_domain
    website_lookup[main] = website
    web = website.replace(".com", "")
    country = row[4].split("|")
    place = unidecode_expect_nonascii(row[5].upper()).split("|")

    # Create a dictionary for every record from wiki data as {"main":name,"website":website,"country":country,"place":place,"parent":parent,"parent_website":""}
    entry = {"main": row[0], "website": website, "country": country, "place": place, "parent": row[2].split("|")[0],
             "parent_website": ""}

    # Iterate through all the alias_entries for every record in parse_wiki_data_stage2.csv (name = [main_name]+alias+[web])
    for name in [main] + alias + [web]:
        name = unidecode_expect_nonascii(name).upper()
        if name.strip() == "":
            continue

        # Create a lookup dictionary having name ([main_name]+alias+[web]), standardizedname and strippedname as keys and its corresponding counter values.
        lookup[name].append(rindex)

        std_name = cq.getStandardizedName(name)
        lookup[std_name].append(rindex)
        stripped_name = cq.getStrippedName(name)
        lookup[stripped_name.replace(" ", "")].append(rindex)

        # Create a dictionary meta_entry with counter as key and {"main":name,"website":website,"country":country,"place":place,"parent":parent,"parent_website":""} as values
        meta_entry[rindex] = entry

reader = readCSV("../DataNew/DataNew.csv")
flagged_entries = writeCSV("../Multiple_KB_matches.csv")

for rindex, row in enumerate(reader):
    flag = 0
    name = row[1]
    stripped = row[2].replace(" ", "")
    city = row[-2].upper()

    # Ignore the records for which city = "" and neither stdName nor strippedName is present in lookup
    if (name in lookup or stripped in lookup) and city != "":
        ori_country = eval(row[5])
        country = [transformation_dict.get(cn, cn) for cn in ori_country]
    else:
        continue
    matched_entry = {"website": "", "place": "", "main": "", "parent": "NA"}

    # Search if there is any stdname matches in the lookup and get the first matched entry which has the same city
    if name in lookup:
        print "LOOKUP"

###### MY CODE FOR FLAGGING MULTIPLE ENTRIES | MY CODE FOR FLAGGING MULTIPLE ENTRIES| MY CODE FOR FLAGGING MULTIPLE ENTRIES #####
        flg_matched_entry = set()
        dummy = 0
        cty = []
        row_0 = []
        row_minus1 = []
        row_minus2 = []
        flg_matched_entry = []
        for entry in set(lookup[name]): 
            if meta_entry[entry]["website"] == "" and website_lookup.get(meta_entry[entry]["parent"], "") == "":
                continue
            for ct in meta_entry[entry]["place"]:
                if ct in city:
                    flg_matched_entry.append(meta_entry[entry])
                    cty.append(ct)
                    row_0.append(row[0])
                    row_minus1.append(row[-1])
                    row_minus2.append(row[-2])
                    dummy = dummy + 1
        if dummy > 1:
            for index, dentry in enumerate(flg_matched_entry):
                flagged_entries.writerow( [1, name, row_0[index], row_minus1[index], dentry["main"], 
                                           dentry["website"], cty[index],dentry["place"], row_minus2[index].upper(), dentry["parent"], website_lookup.get(dentry["parent"], "")])

###### MY CODE FOR FLAGGING MULTIPLE ENTRIES | MY CODE FOR FLAGGING MULTIPLE ENTRIES| MY CODE FOR FLAGGING MULTIPLE ENTRIES #####

    if stripped in lookup and flag == 0:
###### MY CODE FOR FLAGGING MULTIPLE ENTRIES | MY CODE FOR FLAGGING MULTIPLE ENTRIES| MY CODE FOR FLAGGING MULTIPLE ENTRIES #####
        dummy = 0
        cty = []
        row_0 = []
        row_minus1 = []
        row_minus2 = []
        flg_matched_entry = []
        for entry in set(lookup[stripped]):
            if meta_entry[entry]["website"] == "" and website_lookup.get(meta_entry[entry]["parent"], "") == "":
                continue
            for ct in meta_entry[entry]["place"]:

                if ct in city:
                    flg_matched_entry.append(meta_entry[entry])
                    cty.append(ct)
                    row_0.append(row[0])
                    row_minus1.append(row[-1])
                    row_minus2.append(row[-2])
                    dummy = dummy + 1
        if dummy > 1:
            for index, dentry in enumerate(flg_matched_entry):
                flagged_entries.writerow( [2, stripped, row_0[index], row_minus1[index], dentry["main"], 
                                           dentry["website"], cty[index],dentry["place"], row_minus2[index].upper(), dentry["parent"], website_lookup.get(dentry["parent"], "")])


###### MY CODE FOR FLAGGING MULTIPLE ENTRIES | MY CODE FOR FLAGGING MULTIPLE ENTRIES| MY CODE FOR FLAGGING MULTIPLE ENTRIES #####
