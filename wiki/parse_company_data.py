#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division
import argparse
import json
import sys
from gzip import GzipFile
from bz2 import BZ2File
from collections import defaultdict
from unidecode import unidecode


def concat_claims(claims):
    for rel_id, rel_claims in claims.iteritems():
        for claim in rel_claims:
            yield claim

#call = 0
label_dict = {"Q159":"Automobile"}
def to_triplets(ent):
    
    triplets = []
    e1 = ent['id']
    # if e1!="Q37156":
    #     return triplets
    #call =  call + 1
    #print(call)
    label = ent["labels"].get("en",{}).get("value","")
    label_dict[e1] = label
    alias = ent["aliases"].get("en",[])
    alias =  [a.get("value","") for a in alias if a["value"]!=""]
    #print(label)
    #raw_input()
    if label == "":
        #print("returned")
        return None
    allowed_types = set(["Q134161","Q4830453","Q783794","Q778575","Q221096","Q290000","Q562166","Q613142","Q811930","Q891723","Q1071015","Q1430364","Q1757263","Q2005696","Q2518331","Q3590882","Q3751865","Q5154231","Q5418962","Q6500733","Q7257732","Q11570991","Q14942280","Q15081030","Q16274795","Q23700958","Q25086060","Q27555350","Q28823841","Q28826710","Q614084","Q622439","Q658255","Q765517","Q897403","Q1280716","Q1434636","Q1466031","Q1589009","Q1616075","Q1956113","Q2030042","Q2401725","Q3062125","Q3354019","Q3962942","Q7830259","Q11290050","Q13636181","Q14751296","Q15910273","Q15968577","Q17442766","Q19644607","Q19720069","Q23670565","Q23905463","Q27038993","Q11707","Q27686","Q129238","Q131734","Q216931","Q291240","Q489209","Q672070","Q740752","Q778274","Q921773","Q936518","Q1058914","Q1116826","Q1133938","Q1331793","Q1389311","Q1406985","Q1520069","Q2210455","Q2363097","Q2534781","Q2693570","Q4387609","Q5124708","Q5354754","Q5691177","Q7361709","Q12144057","Q16140622","Q18388277","Q21980538","Q27038995","Q27230968","Q29044175","Q29885033","Q133744","Q156362","Q161726","Q190928","Q206361","Q213441","Q331803","Q468755","Q507619","Q786820","Q814468","Q860572","Q1109680","Q1321781","Q1369723","Q1416636","Q1529128","Q1631129","Q1660104","Q1827281","Q1951366","Q2169973","Q2538889","Q2553558","Q2560355","Q2590801","Q2995256","Q3380824","Q5169293","Q5603499","Q8303885","Q11220497","Q14941854","Q16481737","Q17027266","Q17326725","Q19852230","Q20015305","Q20180983","Q23000150","Q23006196","Q26268922","Q28223113","Q29643579","Q112046","Q219577","Q270791","Q377688","Q721067","Q726870","Q730038","Q742421","Q946499","Q1057214","Q1194970","Q1252971","Q1341478","Q1395324","Q2089936","Q2401749","Q2689503","Q2865305","Q3300038","Q3402645","Q3487766","Q3661311","Q5172517","Q8073775","Q10422442","Q11753232","Q14636607","Q15634581","Q24649023","Q28039026","Q28827885"])
    allowed_properties = {"P373":"alias","P31":"type","P127":"parent","P749":"parent","P856":"website","P17":"country","P159":"hq","P452":"industry","P740":"hq"}
    record = defaultdict(list)
    record["label"].append(label)
    record["alias"].extend(alias)
    flag = 0
    main = 0
    try:
        if "P31" in ent["claims"]:
            #print(ent["claims"]["P31"])
            #print(len(ent["claims"]["P31"]))
            #print("^^^")
            for entry in ent["claims"]["P31"]:
                #print(entry)
                if entry["mainsnak"]['datavalue']['value']['id'] in allowed_types:
                    main = 1
                #print(main)
                #print(label)
    except Exception,e:
        print(ent["claims"])
        print(e)
        return None
    if main == 0:
        return None
    #raw_input()
    if main==1:
        return ent
    else:
        return None
    #flag = 1
    #claims = concat_claims(ent['claims'])
    #for claim in claims:
    #    #print(claim)
    #    mainsnak = claim['mainsnak']
    #    #print(mainsnak['property'])
    #    if mainsnak['property'] not in allowed_properties:
    #        continue
        # if mainsnak['property'] == "P31":
        #     if mainsnak['datavalue']['value']['id'] in allowed_types:
        #         flag = 1
        #     else:
        #         break
        #if mainsnak['snaktype'] != "value":
        #    continue
        if mainsnak['datatype'] == 'wikibase-item':
            rel = mainsnak['property']
            e2 = 'Q{}'.format(mainsnak['datavalue']['value']['numeric-id'])
            record[allowed_properties[mainsnak['property']]].append(e2)
            #triplets.append((label, rel, e2))
        if mainsnak['datatype'] == 'string' or mainsnak['datatype'] == 'url':
            rel = mainsnak['property']
            e2 = mainsnak['datavalue']['value']
            record[allowed_properties[mainsnak['property']]].append(e2)
            #triplets.append((label, rel, e2))
    #print(record,flag)
    #raw_input()
    if flag == 1:
        return record
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

if __name__ == '__main__':
    #call = 0
    array = []
    zzz = writeCSV("parse_wiki_data_stage1.csv")
    parser = argparse.ArgumentParser(
        description='Log-Bilinear model for relation extraction.')
    _arg = parser.add_argument
    _arg('--read-dump', type=str, action='store',
         metavar='PATH', help='Reads in a wikidata json dump.')
    args = parser.parse_args()
    
    train_set = None
    if args.read_dump:
        dump_in = BZ2File(args.read_dump, 'r')
        #dump_in = open("latest-all.json","rb")
        line = dump_in.readline();
        iter = 0
        counter =0
        zzz = open("companies_json.txt","wb")
        while line != '':
            
            iter += 1
            counter = counter + 1
            #print(counter)
            if counter%10000==0:
                print(counter),"triplets processed"
                #break
            line = dump_in.readline()
            flag= 1
            # if "Q891723" in line or "Q4830453" in line or "Q1589009" in line or "Q783794" in line or "Q778575" in line:
            #     flag =1
            # if flag == 0:
            #     continue
            #print(line)
            try:
                ent = json.loads(line.rstrip('\n,'))
                if not ent['id'].startswith('Q'):
                    print("Skipping item with id {}".format(ent['id']),file=sys.stderr)
                    continue
                record = to_triplets(ent)
                # print('\n'.join(
                #     ['{}\t{}\t{}'.format(*t) for t in to_triplets(ent)]),
                #       file=sys.stdout)
                #raw_input()
            except:
                pass
            #except (KeyError, ValueError) as e:
            #    print(e, file=sys.stderr)
            #zzz = open("companies_json.txt","wb")
            if record:
                #print("Appended")
                #array.append(["|".join(record.get("label",[])),"|".join(record.get("alias",[])),
                #                                                    "|".join(record.get("parent",[])),
                #                                                    "|".join(record.get("website",[])),
                #                                                    "|".join(record.get("country",[])),
                #                                                    "|".join(record.get("hq",[])),
                #"|".join(record.get("industry",[]))])
                zzz.write(json.dumps(record)+"\n")
            if iter % 1000 == 0:
                sys.stdout.flush()
        import cPickle as pickle
        pickle.dump( label_dict, open( "save.p", "wb" ) )
        #for a in array:
        #    a[2] = "|".join([label_dict.get(a[2],a[2]) for entry in a[2].split("|")])
        #    a[4] = "|".join([label_dict.get(a[4],a[4]) for entry in a[4].split("|")])
        #    a[5] = "|".join([label_dict.get(a[5],a[5]) for entry in a[5].split("|")])
        #    a[6] = "|".join([label_dict.get(a[6],a[6]) for entry in a[6].split("|")])
        #    zzz.writerow(a)
