import requests
from collections import defaultdict
import regex
import psycopg2


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


all_ids = set()
session = requests.session()
asian_lookup = defaultdict(list)
id_cache = {}


def getAllParents(idd):
    loop = 1
#     idd = idd.split("-")[0]
    all_parents_aliases = set()
    counter = 0
    while loop == 1:
        counter = counter + 1
        if counter > 20:
            break
        if idd not in id_cache:
#             url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}-*%20OR%20id:{{idd}}&wt=json"
            url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
            url = url.replace("{{idd}}", idd)
            data = session.get(url).json()
            id_cache[idd] = data
        else:
            data = id_cache[idd]
        ulti = []
        asian_array = asian_lookup[idd]
        allids = set()
#         all_parents_aliases.update(asian_array)
        main_names = []
        other_names = []
        for docindex, doc in enumerate(data["response"]["docs"]):
            try:
                original_name = doc["original_name"][0]
            except:
                continue
            ultimate_parent = doc.get("ultiParent", "")
            doc_id = doc["id"]
            allids.add(doc_id)
            if "-" in doc_id:
                other_names.append(original_name)
            else:
                main_names.append(original_name)
            try:
                ulti = doc["immParentID"][0]
            except:
                continue
            if docindex == 0:
                if ulti == idd:
                    loop = 0
                else:
                    idd = ulti
        all_parents_aliases.update(allids)
#         all_parents_aliases.update(main_names)
#         other_names = [oth for oth in other_names if oth.split()[0] != main_names[0].split()[0]]
#         all_parents_aliases.update(other_names)

    return all_parents_aliases


def pop_token(candidate):
    words = candidate.split(" ")
    if len(words[0]) > 3:
        pop = words[0]
    elif len(words) > 1:
        pop = words[0] + " " + words[1]
    else:
        pop = candidate
    return pop


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


if __name__ == "__main__":
    conn = psycopg2.connect(
        "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
    cur = conn.cursor()
    reader = readCSV("JAN_2019_CLEANMATCH.csv")

    id_name = {}
    par_name = {}
    for rindex, row in enumerate(reader):
        if len(row) < 8:
            continue
        id_name[row[5]] = row[6].replace("^", ",")
        all_ids.add(row[5])
        start_letter = row[1][0]
        if not is_ascii(start_letter):
            asian_lookup[row[5]].append(row[1])
    print "length of all ids", len(all_ids)

#     all_ids = {'312375': 'AB Volvo (publ)', '175265': 'Eaton Corporation plc', '228921482': 'OSRAM Licht AG', '184201050': 'SVP Holdings Ltd.', '408374': 'Rolls-Royce Holdings plc', '874137': 'Carl Zeiss Stiftung AG'}
    
    for index, idd in enumerate(all_ids):
        pars = getAllParents(idd)
#         pars_tokens = set([p.split()[0] for p in pars if len(p.split()[0]) > 0])
#         asian_equi = asian_lookup.get(idd, [])
#         asian_equi = set([p for p in asian_equi if p.split()[0] not in pars_tokens])
#         pars.update(asian_equi)
        for p in pars:
            cur.execute('insert into dolceradata.companyalias_new values(%s, %s,%s)', (idd, id_name[idd], p))
#             cur.execute('insert into dolceradata.companyalias2 values(%s, %s,%s)', (idd, all_ids[idd], p))
        if index % 1000 == 0:
            conn.commit()
            print index
    conn.commit()

