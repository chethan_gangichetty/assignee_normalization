import psycopg2
import time
import hashlib
import multiprocessing

from multiprocessing import Queue
import math


import psycopg2

def getclasscodes(con,cursor,rangej,con2,cursor2):
    cpcq = """
    with ucids as (
        select publication_id,ucid from xml.t_patent_document_values 
        where """+rangej+"""),
    cpcs as (
        select publication_id,(xpath('//classification-cpc//text()',content))::text[] primarycpc from xml.t_classifications_cpc 
        where """+rangej+"""),
    ipcs as (
        select publication_id,(xpath('//classification-ipcr//text()',content))::text[] ipc from xml.t_classifications_ipcr 
        where """+rangej+""")
    select * from 
    ucids left join cpcs on cpcs.publication_id = ucids.publication_id
    left join ipcs on ucids.publication_id = ipcs.publication_id
    
    """
    cursor.execute(cpcq)
    priclasslist=[]
    for pubid,ucid,_,cpcs,_,ipcs  in cursor:
        if cpcs:
            priclass_cpc = cpcs[0]
            if 'FI' not in priclass_cpc:
                for cpc in cpcs:
                    if 'FI' in cpc:
                        priclass_cpc = cpc
                        break
        else:
            priclass_cpc = ''
        
                    
        if ipcs:
            priclass_ipc = ipcs[0]
            if 'FI' not in priclass_ipc:
                for ipc in ipcs:
                    if 'FI' in ipc:
                        priclass_ipc = ipc
                        break
        else:
            priclass_ipc = ''
            
        
                    
        if 'FI' not in priclass_cpc and 'FI' in priclass_ipc:         
            matches = []
            if cpcs:
                for cpc in cpcs:
                    m = len(cpc)
                    for i in xrange(1, len(cpc)):
                        if cpc[:i] != priclass_ipc[:i]:
                            m = i - 1
                            break
                    matches.append((cpc, m))
                match, score = max(matches,key=lambda x: x[1])
                if score > 6:
                    priclass_cpc = match
          
        if priclass_cpc == '' and priclass_ipc!='':
            priclass_cpc = priclass_ipc
        priclasslist.append((pubid ,ucid, priclass_cpc[:19].replace(" ",""),priclass_cpc[:19].replace(" ","")[:4],ucid[:2]))
    
    if len(priclasslist) > 0:
        dataText = ','.join([cursor2.mogrify('(%s,%s,%s,%s,%s)', row) for row in priclasslist])
        cursor2.execute('INSERT INTO dolcera.primary_cpc_bala(publication_id,ucid,primarycpc,cpcl4,country) VALUES '+dataText)
        con2.commit()


def vworker(x,vqueue):
    print('vworker',x)
    n1 = time.time()
    con = psycopg2.connect("dbname='alexandria' user='alexandria'  password='pergola-uncross-linseed' host='db1-newnew.dolcera.net' port=5434")
    cursor = con.cursor()
    con2 = psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cursor2 = con2.cursor()
    
    while True:
        rec=vqueue.get()
        if rec is None:
            print('returning vworker',x)
            print(time.time()-n1)
            break 
        else:
            publication_id,idx = rec
            if idx%1000000 == 0:
                print(idx)
            rangej = """ publication_id>="""+str(publication_id)+ """ and publication_id<"""+str(publication_id+10000)
            getclasscodes(con,cursor,rangej,con2,cursor2)


vqueue=Queue(100000)


num_workers = 20
p1 = [multiprocessing.Process(target=vworker, args=(x,vqueue)) for x in range(num_workers)]

for f in p1:
    f.start()
    


for x in range(0,190000000,10000):
    vqueue.put((x,x))
    
for x in range(num_workers):
    vqueue.put(None)
for f in p1:
    f.join()
