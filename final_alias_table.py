import requests
import unicodedata
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    
con_read = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_read = con_read.cursor('iter_read2')
cur_read.itersize = 10000
query_read = "select * from dolcera.an_final_alias_table"
cur_read.execute(query_read)

con_write = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_write = con_write.cursor()
query_write = "insert into dolcera.an_final_alias_table2 values(%s, %s, %s, %s, %s)"

row = []
ctries = []
citys = []

count = 0
for ulti_name, orig_name, countries, cities, freq in cur_read:
    count = count+1
    ctries = []
    citys = []
    for ctry in countries:
        ctries.append(unidecode_expect_nonascii((strip_accents(ctry))))
    for cty in cities:
        citys.append(unidecode_expect_nonascii((strip_accents(cty))))
    row.append([ulti_name, unidecode_expect_nonascii(strip_accents(orig_name)), ctries,citys, freq])
    if count%10000 == 0:
        print count

count = 0
for i in row:
    count += 1
    cur_write.execute(query_write,(i[0],i[1],i[2],i[3],i[4]))
    if count%10000 == 0:
        print count
        con_write.commit()
con_write.commit()
