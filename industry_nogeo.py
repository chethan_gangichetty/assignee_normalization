import requests
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2 

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor('iter-1')
cur.itersize = 10000
query = "select * from dolcera.an_final_alias_table"
cur.execute(query)

assgn_freq_dict = {}
# cluster_freq_dict = {}
assgn_countries_dict = {}
assgn_city_dict = {}

count = 0

for ulti, orig, countries, cities, freq in cur:
    count = count + 1
    if count%10000 == 0:
        print count, "freq, country, city"
    assgn_freq_dict[orig] = freq
    assgn_countries_dict[orig] = countries
    assgn_city_dict[orig] = cities
#     if ulti not in cluster_freq_dict:
#         cluster_freq_dict[ulti] = freq
#     if freq>cluster_freq_dict[ulti]:
#         cluster_freq_dict[ulti] = freq

con_dict = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_dict = con_dict.cursor('iter-dict')
cur_dict.itersize = 10000
query_dict = "select * from dolcera.an_cpcdict_assgn"
cur_dict.execute(query_dict)

count = 0
assgn_cpc_dict = defaultdict(list)
for assgn, cpc, counts in cur_dict:
    count = count + 1
    if count%10000 == 0:
        print count, "cpc_dict"
    assgn_cpc_dict[assgn] = cpc

con_f = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_f = con_f.cursor('iter-f')
cur_f.itersize = 10000
query_f = "select orig_name, ulti_name from dolcera.an_resolvedstuff_final"
cur_f.execute(query_f)

resolvedClusters = set()
resolvedassignees = set()

counter = 0
for a1,a2 in cur_f:
    if counter%10000 == 0:
        print "Resolved Clusters", counter
    resolvedClusters.add(a2)
    resolvedassignees.add(a1)
#     resolvedClusters.add(ultiname)

con6 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur6 = con6.cursor('iter-6')
cur6.itersize = 10000
# query6 = "with resolved_cluster as (select distinct(ulti_name) from dolcera.an_resolvedstuff_final) select * from dolcera.an_resolvedstuff_research where Industry not in ('Industrial Conglomerates','Diversified Consumer Services','Trading Companies and Distributors',  'Construction and Engineering', 'Diversified Financial Services', 'Commercial Services and Supplies',  'Multiline Retail', 'Distributors', 'Real Estate Management and Development', 'Specialty Retail',  'Professional Services', '') and score in ('48','20','16', '52', '36', '32') and flag ilike 'capiq%' and ulti_name not in (select ulti_Name from resolved_cluster)"
# query6 = "with resolved_cluster as (select distinct(ulti_name) from dolcera.an_resolvedstuff_final) select * from dolcera.an_resolvedstuff_research where (score = '8' or score = '12') and flag ilike 'capiq%' and ulti_name not in (select ulti_Name from resolved_cluster)"
query6 = "select * from dolcera.an_resolvedstuff_research where (score = '8' or score = '12') and flag ilike 'capiq%'"
cur6.execute(query6)

xxx = writeCSV("resolvedresearch_dump_geo.csv")

resolvedClusters = set()
counter = 0
for a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19 in cur6:
    if counter%10000 == 0:
        print "Resolved Clusters", counter
    if a1 in resolvedassignees:
        continue
    if a2 in resolvedClusters:
        continue
    freq = assgn_freq_dict[a1]
    cpc = assgn_cpc_dict[a3]
    country = assgn_countries_dict[a1]
    city = assgn_city_dict[a1]
    xxx.writerow([a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,country, city, freq, cpc])
    counter = counter + 1
#     resolvedClusters.add(ultiname)

