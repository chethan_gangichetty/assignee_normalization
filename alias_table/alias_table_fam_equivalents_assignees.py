from __future__ import division
from collections import defaultdict
from itertools import combinations
import operator
from fuzzywuzzy import fuzz
import re
from collections import Counter
from string import digits
import ast
import psycopg2
# from capiq import indexCapIq

#idc = indexCapIq()
def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g 

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput


# reader = readCSV("ubtech_patents_app_num.csv")
# nums=set()
# for row in reader:
#     nums.add(row[0])
# 



# reader = readCSV("asgTable.csv")
# allowed_app_nums= set()
# for rindex,row in enumerate(reader):
#     if rindex%10000 == 0:
#         print rindex
#     # #print row
#     #if "LG" not in row[3].upper() and "SAMSUNG" not in row[3] and "ERICSSON" not in row[3].upper() and "DAEWOO" not in row[3].upper():
#     #     continue
#     # if "JJGC" not in row[3].upper():
#     #     continue
#     if len(row)<9:
#         continue
#     if "samsung" in row[3].lower():# or "avago" in row[3].lower():
#         allowed_app_nums.add(row[2])
#         continue
# reader = readCSV("asgTable.csv")
# ccc= writeCSV("testset_samsung.csv")
# for rindex,row in enumerate(reader):
#     if rindex%10000 == 0:
#         print rindex
#     if row[2] in allowed_app_nums:        
#         ccc.writerow(row)
# vvv= writeCSV("testset_samsung_invdata.csv")
# reader = readCSV("INVDATA1.csv")
# for row in reader:
#     if row[-1] in allowed_app_nums:
#         vvv.writerow(row)
#         
# exit()

con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")

cur = con.cursor('iter-1')
cur.itersize = 1000
cur_smt = con.cursor('iter-2')
cur_smt.itersize = 1000
cur_asian = con.cursor('iter-3')
cur_asian.itersize = 1000
cur_insert = con.cursor()

query = "select * from dolcera.rerun_alias_table_assignees"
query_smt = "select * from dolcera.rerun_an_smt_pairs_assignees"
query_asian = "select * from dolcera.rerun_asian_pairs_assignees"
inq = "insert into dolcera.rerun_family_equivalents_assignees values (%s, %s, %s, %s, %s)"

cur.execute(query)
cur_smt.execute(query_smt)
cur_asian.execute(query_asian)

asian_aliases = defaultdict(list)

#################################################################################################################################
# Create asian_aliases {asian_topmatch: [asianname1, asianname2]} and smt_aliases {smt_topmatch: [smtname1, smtname2]}
#################################################################################################################################

# Iterate through the asian_pairs and determine top_matches for asian_names with ratio of co-occured freq & indv. freq > 0.6

for asian_name, top_match, asian_freq, cooc_cnt, ratio in cur_asian:
    if asian_freq<5:
        continue
    if top_match != "" and ratio>0.6:
        asian_aliases[top_match].append(asian_name)

# Iterate through the asian_pairs and determine top_matches for asian_names with ratio of co-occured freq & indv. freq > 0.6     
        
smt_aliases = defaultdict(list)
for smt_name, top_match, smt_freq, cooc_cnt, ratio in cur_smt:
    if smt_freq<5:
        continue
    if top_match != "" and ratio>0.6:
        smt_aliases[top_match].append(smt_name)

# reader = readCSV("../asian_pairs.csv")
# asian_aliases = defaultdict(list)
# for row in reader:
#     array = ast.literal_eval(row[2])
#     if int(row[1])<5:
#         continue
#     if len(array)>0 and array[0][1]>0.6*int(row[1]):
#         asian_aliases[array[0][0]].append(row[0])
#
# reader = readCSV("../smt_pairs.csv")
# smt_aliases = defaultdict(list)
# for row in reader:
#     array = ast.literal_eval(row[2])
#     if int(row[1])<5:
#         continue
#     if len(array)>0 and array[0][1]>0.6*int(row[1]):
#         smt_aliases[array[0][0]].append(row[0])
        
# reader = readCSV("../SampleAlias.csv")

standardized_lookup = {}
covered_set = set()
main_aliases = defaultdict(list)
alias_lookup = {}
# for rindex,row in enumerate(reader):
#     if rindex%10000==0:
#         print rindex,"Reading Sample Alias"
#     main_aliases[row[0]].append(row[1])
#     alias_lookup[row[1]] = row[0]

#################################################################################################################################
# Iterate through the base alias table entries and create main_aliases {ultiname1: [origname1, origname2], ultiname2: []origname3, origname4]}, alias_lookup {origname1: ultiname1, origname2:ultiname2} 
#################################################################################################################################

counter = 0
for ulti_name,orig_name,countries,cities,freq in cur:
    counter += 1
    if counter%10000==0:
        print counter, "Reading Sample Alias"
    main_aliases[ulti_name].append(orig_name)
    alias_lookup[orig_name] = ulti_name

    # if row[0] not in covered_set:
    #     std_name = idc.getStandardizedName(row[0])
    #     covered_set.add(row[0])
    #     standardized_lookup[std_name] = row[0]

#################################################################################################################################
#                                 Extend the main_aliases to include top_matches from asian_aliases and smt_aliases 
#################################################################################################################################

for entry in asian_aliases:
    #entry = standardized_lookup[entry]
    main_aliases[entry].extend(asian_aliases[entry])
    for name in asian_aliases[entry]:
        alias_lookup[name] = entry
        
for entry in smt_aliases:
    #entry = standardized_lookup[entry]
    main_aliases[entry].extend(smt_aliases[entry])        
    for name in asian_aliases[entry]:
        alias_lookup[name] = entry
# print main_aliases["UNION BROTHER MACHINERY CO LTD"]
# raw_input()

# reader = readCSV("../pn_ignore_overall.csv")
ignore_fam = set()
# for rindex,row in enumerate(reader):
#     if rindex%10000==0:
#         print rindex,"Reading multi assignee families"
#     ignore_fam.add(row[0])
reader = readCSV("../asgTable_removeinventors.csv")
for row in reader:
    if row[4]!="1":
        ignore_fam.add(row[2])

reader = readCSV("../asgTable_removeinventors.csv")
comp_id_dict = {}
id_comp_dict = {}
counter = 0
first_aggregation = defaultdict(list)
first_aggreagtion_dict = defaultdict(dict)
frequency_distribution = defaultdict(set)
family_dict = defaultdict(list)
family_pointer = defaultdict(list)
country_dict = defaultdict(dict)
city_dict = defaultdict(dict)
negative_families= set()
app_num_dict = defaultdict(list)
negative_apps = set()

#################################################################################################################################
#                                                     Iterate through asgTable.csv 
# Metadata: (Application ID|Country|Family ID|Assignee|Sequence|Format(EPO/Original/Intd/SMT)|IFI|City|Applicant/Assignee)
#################################################################################################################################

for rindex,row in enumerate(reader):
    if rindex%10000 == 0:
        print rindex

###################################Ignore the Assignee records and consider only Applicant records###############################

    if row[-1]=="Applicant":
        continue
    #if "LG" not in row[3].upper() and "SAMSUNG" not in row[3] and "ERICSSON" not in row[3].upper() and "DAEWOO" not in row[3].upper():
    #     continue
    # if "JJGC" not in row[3].upper():
    #     continue
    if len(row)<9:
        continue
    
    # if row[8].replace("-","") not in nums:
    #     continue
    app_num = row[0]
    row[3] = removeAllPunctuations(row[3]).upper()
    
############################ Create Surrogate ID's and corresponding dictionaries for every Assignee ############################
    
    if row[3] not in comp_id_dict:
        comp_id_dict[row[3]] = counter
        id_comp_dict[counter] = row[3]
        counter = counter + 1
    # print row[3]
    # raw_input()
    idd = comp_id_dict.get(row[3],-1)
    # print row[3],idd
    
############################ Create dictionaries for counts of Country and City for every Assignee ##############################
########################################## {Asn_1 : {Cntry1: NNN, Cntry2: NNN } } ###############################################
########################################## {Asn_1 : {City1: NNN, City2: NNN } } #################################################
    
    if row[1]!="":
        country_dict[idd][row[1]] = country_dict[idd].get(row[1],0) + 1
    if row[7]!="":
        row[7]=row[7].translate(None, digits).strip()
        if len(row[7])>2:
            city_dict[idd][row[7]] = city_dict[idd].get(row[7],0) + 1
    # if idd==7752:
    #     try:
    #         if 4 in first_aggreagtion_dict[row[8]][row[4]]:
    #             print row
    #             raw_input()
    #     except Exception,e:
    #         continue
            
################################### Create first_aggregation_dict (Dictionary within Dictionary): ###############################
################################## {Pn_1 : {Sq1: ["Asgn1", "Asgn2"], Sq2: ["Asgn3", "Asgn4"]} } #################################
            
    # first_aggregation[row[8]+":"+row[4]].append(idd)
    if row[4] not in first_aggreagtion_dict[row[8]]:
        first_aggreagtion_dict[row[8]][row[4]]=[]
        first_aggreagtion_dict[row[8]][row[4]].append(idd)
    else:
        first_aggreagtion_dict[row[8]][row[4]].append(idd)
    group = alias_lookup.get(row[3],row[3])
    if row[2] in ignore_fam:
        continue
#     if "US-" not in row[8] and "EP-" not in row[8] and "JP-" not in row[8] and "CN-" not in row[8] and "KR-" in row[8]:
#         continue

############################ Create family id distribution for every Assignee across all the patents ############################

    frequency_distribution[group].add(row[2])

############################ Create a family dictionary with Family ID as the key and Assignees as values #######################
#################### Create a family pointer dictionary with Assigness as the keys and Family ID's as values ####################
############################# Create app_num dictionary with app_num as the key and Assignees as values #########################
    
    family_dict[row[2]].append(idd)
    app_num_dict[row[0]].append(idd)
    if row[4]!="1":
        negative_families.add(row[2])
        negative_apps.add(row[0])
    if row[2]!="-1":
        family_pointer[idd].append(row[2])

# Create Family ID's co-occurence matrix app_num_equivalents {CluRep1|CluRep2: NNN} and app_num_equivalents_nums {CluRep1|CluRep2: [Fmly1, Fmly2]}
        
app_num_equivalents_nums = defaultdict(list)
app_num_equivalents = {}
for entry in family_dict:
    if entry not in negative_families:
        id_names = family_dict[entry]
        names = set([alias_lookup.get(id_comp_dict[idd],"None") for idd in id_names])
        for comb in combinations(names,2):
            comb = list(comb)
            comb.sort()
            #print comb
            app_num_equivalents["|".join(comb)] = app_num_equivalents.get("|".join(comb),0) + 1
            app_num_equivalents_nums["|".join(comb)].append(entry)
            
#yyy = writeCSV("../equivalent_clusters_as_per_fam_num.csv")
#print app_num_equivalents
# sorted_x = sorted(app_num_equivalents.items(), key=operator.itemgetter(1))
# sorted_x.reverse()
# for entry in sorted_x:
#     entry = list(entry)
#     pair = entry[0].split("|")
#     count1 = len(frequency_distribution.get(pair[0],[]))
#     count2 = len(frequency_distribution.get(pair[1],[]))
#     #entry.append(app_num_equivalents_nums[entry[0]])
#     entry.append(count1)
#     entry.append(count2)
#     yyy.writerow(entry)

sorted_x = sorted(app_num_equivalents.items(), key=operator.itemgetter(1))
sorted_x.reverse()
counter = 0
for entry in sorted_x:
    counter = counter +1
    entry = list(entry)
    pair = entry[0].split("|")
    count1 = len(frequency_distribution.get(pair[0],[]))
    count2 = len(frequency_distribution.get(pair[1],[]))
    pair.append(entry[1])
    pair.append(count1)
    pair.append(count2)
    if len(pair) == 5:
        cur_insert.execute(inq,tuple(pair))
    else:
#         print(pair)
        pass
    if counter%10000 == 0:
        con.commit()
con.commit()
        