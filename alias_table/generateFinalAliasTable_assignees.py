from __future__ import division
import regex
from collections import defaultdict
import operator
from unidecode import unidecode
import ast
import psycopg2

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")

cur = con.cursor('iter-1')
cur.itersize = 1000
cur_smt = con.cursor('iter-2')
cur_smt.itersize = 1000
cur_asian = con.cursor('iter-3')
cur_asian.itersize = 1000
cur_fam_eqv = con.cursor('iter-4')
cur_fam_eqv.itersize = 1000
cur_insert = con.cursor()

query = "select * from dolcera.rerun_alias_table_assignees order by frequency desc"
query_smt = "select * from dolcera.rerun_an_smt_pairs_assignees"
query_asian = "select * from dolcera.rerun_asian_pairs_assignees"
query_fam_eqv = "select * from dolcera.rerun_family_equivalents_assignees"
inq = "insert into dolcera.rerun_final_alias_table_assignees values (%s, %s, %s, %s, %s)"

cur.execute(query)

row_dict = defaultdict(list)
alias_dict = {}

#################################################################################################################################
# Iterate through base alias table, asian_pairs table and smt_pairs table.
# Create alias_dict for mapping original names and cluster representatives for every assignee (inlcuding asian and smt_names)
#################################################################################################################################

for ulti_name,orig_name,countries,cities,freq in cur:
    alias_dict[orig_name] = ulti_name
    if ulti_name not in row_dict:
        row_dict[ulti_name] = [countries,cities]

cur_smt.execute(query_smt)
cur_asian.execute(query_asian)

for asian_name, top_match, asian_freq, cooc_cnt, ratio in cur_asian:
    if asian_freq<5:
        continue
    if top_match != "" and ratio>0.6:
        alias_dict[asian_name] = top_match

smt_names = set()
for smt_name, top_match, smt_freq, cooc_cnt, ratio in cur_smt:
    smt_names.add(smt_name.lower())
    if smt_freq<5:
        continue
    if top_match != "" and ratio>0.6:
        alias_dict[smt_name] = top_match

cur_fam_eqv.execute(query_fam_eqv)
counter = 0

# Iterate through family equivalent table and update alias_dict with assignee1 and assignee2 if co-occured families > 0.7*(cnt1, cnt2) and choose the assignee which is not in smt_names or which has highest number of families as CLuster representative 

for asn1, asn2, cooc, an1f, an2f in cur_fam_eqv:
    counter += 1
    pair = [asn1, asn2]
    count1 = int(an1f)
    count2 = int(an2f)
    inters = int(cooc)

    if inters >= 0.7 * min(count1, count2) and inters >= 5:
        maxVal = max(count1, count2)
        if pair[0].lower() in smt_names and pair[1].lower() not in smt_names:
            alias_dict[pair[0]] = pair[1]
        elif pair[1].lower() in smt_names and pair[0].lower() not in smt_names:
            alias_dict[pair[1]] = pair[0]
        else:
            if count1 == maxVal:
                alias_dict[pair[1]] = pair[0]
            else:
                alias_dict[pair[0]] = pair[1]

con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_insert = con.cursor()

con2 = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur3 = con2.cursor('iter-20')
cur3.itersize = 1000
cur3.execute(query)

cnt = 0
for ulti_name,orig_name,countries,cities,freq in cur3:
    cnt = cnt +1
    if cnt%1000 == 0:
        con.commit()
    try:
        ulti = alias_dict[orig_name]
    except Exception, e:
        print e
        continue
    counter = 0
    while alias_dict.get(ulti,ulti)!=ulti and counter<100:
        ulti = alias_dict.get(ulti,ulti)
        counter = counter +1
    if ulti not in row_dict:
        continue
    countries_final = row_dict[ulti][0]
    cities_final = row_dict[ulti][1]
    cur_insert.execute(inq,(ulti, orig_name, countries_final, cities_final, freq ))
con.commit()


# from __future__ import division
# import regex
# from collections import defaultdict
# import operator
# from unidecode import unidecode
# import ast


# def readCSV(path):
#     import csv
#     reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
#     return reader

# def writeCSV(path,mode="w"):
#     import unicodecsv
#     myfile=open(path,mode)
#     fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
#     return fileOutput


# reader = readCSV("../SampleAlias.csv")
# row_dict = {}
# alias_dict = {}
# for row in reader:
#     alias_dict[row[1]] = row[0]
#     if row[0] not in row_dict:
#         row_dict[row[0]] = [r for r in row[2:-1]]

# reader = readCSV("../asian_pairs.csv")
# for row in reader:
#     array = ast.literal_eval(row[2])
#     if int(row[1])<5:
#         continue
#     if len(array)>0 and array[0][1]>0.6*int(row[1]):
#         alias_dict[row[0]] = array[0][0]
        
# reader = readCSV("../smt_pairs.csv")
# smt_names = set()
# for row in reader:
#     array = ast.literal_eval(row[2])
#     smt_names.add(row[0].lower())
#     if int(row[1])<5:
#         continue
#     if len(array)>0 and array[0][1]>0.6*int(row[1]):
#         alias_dict[row[0]] = array[0][0]

# print "Done reading pairs"
# reader = readCSV("../equivalent_clusters_as_per_fam_num.csv")
# for rindex,row in enumerate(reader):
#     if rindex%100==0:
#         print rindex,"processing equivalents"
#     pair = row[0].split("|")
#     count1 = int(row[2])
#     count2 = int(row[3])
#     inters = int(row[1])
    
#     if inters>=0.7*min(count1,count2) and inters>=5:
#         maxVal = max(count1,count2)
#         if pair[0].lower() in smt_names and pair[1].lower() not in smt_names:
#             alias_dict[pair[0]] = pair[1]
#         elif pair[1].lower() in smt_names and pair[0].lower() not in smt_names:
#             alias_dict[pair[1]] = pair[0]
#         else:
#             if count1 == maxVal:
#                 alias_dict[pair[1]] = pair[0]
#             else:
#                 alias_dict[pair[0]] = pair[1]

# reader = readCSV("../SampleAlias.csv")
# zzz = writeCSV("../FinalSampleAlias.csv")
# for rindex,row in enumerate(reader):
#     if rindex%1000==0:
#         print rindex,"Printing final table"
#     if 1:
#         ulti = alias_dict[row[1]]
#         counter = 0
#         while alias_dict.get(ulti,ulti)!=ulti and counter<100:
#             ulti = alias_dict.get(ulti,ulti)
#             counter = counter +1 
#         if ulti not in row_dict:
#             continue
#         row[0] = ulti
#         row[2] = row_dict[ulti][0]
#         row[3] = row_dict[ulti][1]
#     zzz.writerow(row)
    
        