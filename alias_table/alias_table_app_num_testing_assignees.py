from __future__ import division
from collections import defaultdict
from itertools import combinations
import operator
from fuzzywuzzy import fuzz
import re
from collections import Counter
from string import digits
import psycopg2
def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g 

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

#################################################################################################################################
#                     Create a set IGNORE_FAM containing the list of family_id's to be ignored. 
#                     It contains:
#                     a) Family_ID's which have more than one sequence
#                     b) Family_ID's which have patents which have been reassigned to different assignees
#################################################################################################################################

reader = readCSV("../pn_ignore_overall.csv")
ignore_fam = set()

for row in reader:
    ignore_fam.add(row[0])
reader = readCSV("../asgTable_removeinventors.csv")
counter= 0
for row in reader:
    counter+=1
    if counter%100000 == 0:
        print "Iterating Ignore Family ID's loop: Processed "+str(counter)+" records"
    if row[4]!="1":
        ignore_fam.add(row[2])

reader = readCSV("../asgTable_removeinventors.csv")
comp_id_dict = {}
id_comp_dict = {}
counter = 0
first_aggregation = defaultdict(list)
first_aggreagtion_dict = defaultdict(dict)
frequency_distribution = {}
family_dict = defaultdict(list)
family_pointer = defaultdict(list)
country_dict = defaultdict(dict)
city_dict = defaultdict(dict)
negative_families= set()
app_num_dict = defaultdict(list)
negative_apps = set()

#################################################################################################################################
#                                                     Iterate through asgTable.csv 
# Metadata: (Application ID|Country|Family ID|Assignee|Sequence|Format(EPO/Original/Intd/SMT)|IFI|City|Applicant/Assignee)
#################################################################################################################################

for rindex,row in enumerate(reader):
    if rindex%100000 == 0:
        print str(rindex)+ " records iterated in asgTable_removeinventors.csv"

###################################Ignore the Assignee records and consider only Applicant records###############################

    if row[-1]=="Applicant" or row[-1] == "inventors":
        continue
    # #print row
    #if "LG" not in row[3].upper() and "SAMSUNG" not in row[3] and "ERICSSON" not in row[3].upper() and "DAEWOO" not in row[3].upper():
    #     continue
    # if "JAPAN" not in row[3].upper():
    #     continue
    if len(row)<9:
        continue
    
    # if row[8].replace("-","") not in nums:
    #     continue
    app_num = row[0]
    row[3] = removeAllPunctuations(row[3]).upper()
    
############################ Create Surrogate ID's and corresponding dictionaries for every Assignee ############################

    if row[3] not in comp_id_dict:
        comp_id_dict[row[3]] = counter
        id_comp_dict[counter] = row[3]
        counter = counter + 1
    # print row[3]
    # raw_input()
    idd = comp_id_dict.get(row[3],-1)
    # print row[3],idd
    
############################ Create dictionaries for counts of Country and City for every Assignee ##############################
########################################## {Asn_1 : {Cntry1: NNN, Cntry2: NNN } } ###############################################
########################################## {Asn_1 : {City1: NNN, City2: NNN } } #################################################

    if row[1]!="":
        country_dict[idd][row[1]] = country_dict[idd].get(row[1],0) + 1
    if row[7]!="":
        row[7]=row[7].translate(None, digits).strip()
        if len(row[7])>2:
            city_dict[idd][row[7]] = city_dict[idd].get(row[7],0) + 1
            
################################### Create first_aggregation_dict (Dictionary within Dictionary): ###############################
################################## {Pn_1 : {Sq1: ["Asgn1", "Asgn2"], Sq2: ["Asgn3", "Asgn4"]} } #################################

    if row[4] not in first_aggreagtion_dict[row[8]]:
        first_aggreagtion_dict[row[8]][row[4]]=[]
        first_aggreagtion_dict[row[8]][row[4]].append(idd)
    else:
        first_aggreagtion_dict[row[8]][row[4]].append(idd)
        
############################ Create individual frequency distribution of every Assignee across all the patents ##################

    frequency_distribution[idd] = frequency_distribution.get(idd,0) + 1

############################ Ignore patents with Family ID's which are present in ignore_fam set ################################

    if row[2] in ignore_fam:
        #print row[-1],row[2]
        continue
    if "US-" not in row[8] and "EP-" not in row[8] and "JP-" not in row[8] and "CN-" not in row[8] and "KR-" in row[8]:
        continue
        
############################ Create a family dictionary with Family ID as the key and Assignees as values #######################
#################### Create a family pointer dictionary with Assigness as the keys and Family ID's as values ####################
    family_dict[row[2]].append(idd)
    #app_num_dict[row[0]].append(idd)
    if row[4]!="1":
        negative_families.add(row[2])
        negative_apps.add(row[0])
    if row[2]!="-1":
        family_pointer[idd].append(row[2])
    #print row[1]+row[4]

    
query_country = 'insert into dolcera.rerun_countries_table_assignees values(%s,%s,%s)'
con_country = psycopg2.connect( "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_country = con_country.cursor()

for rindex,idd in enumerate(country_dict):
    if rindex%10000 == 0:
        con_country.commit()
    assignee = id_comp_dict.get(idd,"")
    if assignee != "":
        countries_dict = country_dict[idd]
        sorted_x = sorted(countries_dict.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        total = sum(row[1] for row in sorted_x)
        sumValue = 0
        counts = []
        countries = []
        for x in sorted_x:
            if sumValue <= 0.8*total:
                countries.append(x[0])
                counts.append(x[1])
                sumValue += x[1]
        cur_country.execute(query_country,(assignee,countries,counts))
        
con_country.commit()
                           
query_city = 'insert into dolcera.rerun_cities_table_assignees values(%s,%s,%s)'
con_city = psycopg2.connect( "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur_city = con_city.cursor()
                            
for rindex, idd in enumerate(city_dict):
    if rindex%100000 == 0:
        con_city.commit()
    assignee = id_comp_dict.get(idd,"")
    if assignee != "":
        cities_dict = city_dict[idd]
        sorted_x = sorted(cities_dict.items(), key=operator.itemgetter(1))
        sorted_x.reverse()
        total = sum(row[1] for row in sorted_x)
        sumValue = 0
        counts = []
        cities = []
        for x in sorted_x:
            if sumValue <= 0.8*total:
                cities.append(x[0])
                counts.append(x[1])
                sumValue += x[1]
        cur_city.execute(query_city,(assignee,cities,counts))
        
con_city.commit()
                         
second_aggregation = defaultdict(dict)

#################################################################################################################################
#                                     Create function for determining Popular token of any assignee
#################################################################################################################################

def pop_token(candidate):
    words = candidate.split(" ")
    if len(words[0]) >= 3:
            pop = words[0]
    elif len(words)>1:
            pop = words[0] + " " + words[1]
    else:
            pop = candidate
    return pop

#################################################################################################################################
#                                     Iterate through every patent entry in first aggregation dict
#################################################################################################################################

for entryindex,entry in enumerate(first_aggreagtion_dict):

##################### For every patent entry, create a dictionary having popular tokens for all the assigness ###################
############################ {Seq1: ("pop_asgn1", "pop_asgn2"), Seq2: ("pop_asgn3", "pop_asgn4")} ###############################
    
    if entryindex%100000==0:
        print entryindex,"Second stage"
    pop_tokens = defaultdict(set)
    for seq in first_aggreagtion_dict[entry]:
        for asg in first_aggreagtion_dict[entry][seq]:
            pop_tokens[seq].add(pop_token(id_comp_dict[asg]))
    flag = 0
    # print pop_tokens
    
##################### Ignore those patents which have common popular tokens spread across different sequences ###################

    for seq1 in first_aggreagtion_dict[entry]:
        for seq2 in first_aggreagtion_dict[entry]:
            if seq1!=seq2:
                common = pop_tokens[seq1].intersection(pop_tokens[seq2])
                if len(common) !=0:
                    flag=1
                    break
    if flag==1:
        continue
        
################# Create second_aggregation_dict (Dictionary within Dictionary) for saving CO_OCCURENCE MATRIX ##################
########################################## {Asgn1 : {Asgn2: NNN, Asgn3: NNN} } ##################################################
       
    for seq in first_aggreagtion_dict[entry]:
        names = first_aggreagtion_dict[entry][seq]
        if len(names)==0:
            continue
        combs = combinations(names,2)
        #print entry,names
        for comb in combs:
            second_aggregation[comb[0]][comb[1]] = second_aggregation[comb[0]].get(comb[1],0) + 1
            second_aggregation[comb[1]][comb[0]] = second_aggregation[comb[1]].get(comb[0],0) + 1


first_aggreagtion_dict = {}
sorted_x = sorted(frequency_distribution.items(), key=operator.itemgetter(1))
sorted_x.reverse()
zzz = writeCSV("../SampleAlias_Assignees.csv")
writeArray = []
ultimate_dict = {}
ultimate_country_dict = {}
ultimate_city_dict = {}

#################################################################################################################################
#                 Iterate through every patent entry in frequency distribution in the descending order of frequencies
#################################################################################################################################

for entry in sorted_x:
    name = id_comp_dict[entry[0]]
    entry_frequency = entry[1]
    if len(name)<3:
        continue
    print name,entry[0]
    
    ori_frequency = frequency_distribution.get(entry[0],0)

# Retrieve Co-occured Assignees, Co-occurence frequency, Individual freq of co-occured assigness for every Assignee in second aggregation dictionary

    candidates = [[id_comp_dict[cand],second_aggregation[entry[0]][cand],frequency_distribution.get(cand,0)] for cand in second_aggregation[entry[0]]]

# Retain Co-occured assignees whose individual frequency is greater than the individual frequency of assignee under question

    candidates = [[can[0],can[1]] for can in candidates if can[2]>ori_frequency and can[0]!=name]

# Sort the candidates in the reverse order of co-occurence frequencies

    candidates.sort(key=lambda x: x[1],reverse=True)
    print name,candidates,"Candidates",ori_frequency
    flag = 0
    last_entry = []

    for candidate in candidates:
        write_flag = 0
        if len(candidate[0]) < 3:#.strip()=="":
                continue

# Calculate scores ( SCORE1 - related to fuzzywuzzy distance, SCORE2 - ratio of co-occurence frequency and individual frequency )

        score1 = fuzz.ratio(candidate[0],name)
        scorenew = fuzz.token_sort_ratio(candidate[0],name)
        print name,candidate[0],score1,scorenew
        if scorenew > score1:
            score1 = scorenew
        score2 = candidate[1]/ori_frequency
        last_entry.append(candidate[0]+" "+str(score1)+" "+str(score2))
        words = candidate[0].split()

######################## Evaluate pop token of every candidate in all the filtered co-occured candidates ########################
        
        if len(words[0]) >= 3:
                pop = words[0]
        elif len(words)>1:
                pop = words[0] + " " + words[1]
        else:
                pop = candidate[0]
        print pop,"Popular",score1,score2

################## Evaluate multiple criteria based on pop token, score1 and score2 to decide the Cluster name ##################
        
        if candidate[0] in name:
            write_flag=1
        if score1 >=90 and score2>0.001:
            write_flag = 1
        if score1 >= 80 and pop in name and score2>0.005:
            write_flag = 1
        if score1 >= 70 and score2 > 0.01 and pop in name:
            write_flag = 1
        if pop in name and score2>0.5:
            write_flag = 1
        if write_flag == 1:
            print "flag is 1"
            #zzz.writerow([name,ori_frequency,candidate[0],candidate[1],candidate[2],score1,score2])
            ultimate_dict[name] = candidate[0]
            ulti = candidate[0]
            
############## Iterate through Cluster representative dictionary till the final Cluster representative is obtained ##############

            while ultimate_dict[ulti]!=ulti:
                ulti = ultimate_dict[ulti]
            ulti_id = comp_id_dict[ulti]

###################### Create dictionaries for counts of Country and City for every Cluster representative ######################
            
            country_dict_temp = country_dict.get(entry[0],{})
            city_dict_temp = city_dict.get(entry[0],{})
            for country in country_dict_temp:
                ultimate_country_dict[ulti_id][country] = ultimate_country_dict[ulti_id].get(country,0) + country_dict_temp[country]
            for city in city_dict_temp:
                ultimate_city_dict[ulti_id][city] = ultimate_city_dict[ulti_id].get(city,0) + city_dict_temp[city] 
            writeArray.append([ulti,name,entry_frequency])
            #zzz.writerow([name,ulti])
            flag = 1
            break
            
## Evaluate the family members if a particular assignee has no other co-occured assigness in the second aggregation dictionary ##

    if flag==0 and len(candidates)==0:
        print "unresolved"
        families = family_pointer.get(entry[0],[])
        #print name,entry[0],families,
        
        candidates = set()
        
###### Consolidate the family members of all the families a particular assignee belongs to, ignoring the negative families ######

        for family in families:
            #print family)duct
            if family not in negative_families:
                candidates.update(family_dict[family])
                
############ Retain family members whose individual frequencies are greater than that of the assignee under question ############ 
                
        candidates = [[id_comp_dict[candidate],frequency_distribution.get(candidate,0)] for candidate in candidates]
        candidates = [[can[0],can[1]] for can in candidates if can[1]>ori_frequency]
        print "Comparing",name,candidates

## Iterate through the candidates (family members), calculate pop token, score1(fuzzy distance reltd.) to obtain cluster repr. ##
###################### Create dictionaries for counts of Country and City for every Cluster representative ######################

        candidates.sort(key=lambda x: x[1],reverse=True)
        for candidate in candidates:
            if len(candidate[0]) < 3:#.strip()=="":
                continue
            words = candidate[0].split()
            if len(words[0]) > 3:
                    pop = words[0]
            elif len(words)>1:
                    pop = words[0] + " " + words[1]
            else:
                    pop = candidate[0]
            print pop,"Popular II"
            score1 = fuzz.ratio(candidate[0],name)
            scorenew = fuzz.token_sort_ratio(candidate[0],name)
            print name,candidate[0],score1,scorenew,"-->"
            if scorenew > score1 and pop in name:
                score1 = scorenew
            # if candidate[0] in name:
            #     write_flag=1
            #     flag = 1
            if score1 > 75 and pop in name:
                print "Solved at family level"
                ultimate_dict[name] = candidate[0]
                ulti = candidate[0]
                while ultimate_dict[ulti]!=ulti:
                    ulti = ultimate_dict[ulti]
                ulti_id = comp_id_dict[ulti]
                country_dict_temp = country_dict.get(entry[0],{})
                #print country_dict_temp,"country_dict_temp"
                for country in country_dict_temp:
                    ultimate_country_dict[ulti_id][country] = ultimate_country_dict[ulti_id].get(country,0) + country_dict_temp[country] 
                city_dict_temp = city_dict.get(entry[0],{})
                for city in city_dict_temp:
                    ultimate_city_dict[ulti_id][city] = ultimate_city_dict[ulti_id].get(city,0) + city_dict_temp[city] 
                writeArray.append([ulti,name,entry_frequency])
                #zzz.writerow([name,ulti])
                flag = 1
                break
                
#### Make the assignee as it's own cluster if there's no Cluster representative from Co-occured assignees or family members #####
                
    if flag == 0:
        ultimate_dict[name] = name
        ulti_id = comp_id_dict[name]
        print "ulti",name,ulti_id
        ultimate_country_dict[ulti_id] = country_dict.get(ulti_id,{})
        ultimate_city_dict[ulti_id] = city_dict.get(ulti_id,{})
        writeArray.append([name,name,entry_frequency])
        #zzz.writerow([name,name])

inq = 'insert into dolcera.rerun_alias_table_assignees values(%s,%s,%s,%s,%s)'
con = psycopg2.connect( "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor()
cnt = 0
for entry in writeArray:
    cnt = cnt+1
    countries = ultimate_country_dict[comp_id_dict[entry[0]]]
    #print entry[0]
    sorted_x = sorted(countries.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    total = sum(row[1] for row in sorted_x)
    sumValue = 0
    countries = []
    for x in sorted_x:
        if sumValue <= 0.8*total:
            countries.append(x[0])
            sumValue += x[1]
    cities = ultimate_city_dict[comp_id_dict[entry[0]]]
    #print entry[0]
    sorted_x = sorted(cities.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    total = sum(row[1] for row in sorted_x)
    sumValue = 0
    cities = []
    for x in sorted_x:
        if sumValue <= 0.8*total:
            cities.append(x[0])
            sumValue += x[1]
    cur.execute(inq,(entry[0],entry[1],countries,cities,entry[2]))
    if cnt%1000 == 0:
        con.commit()
        print(str(cnt) + " entries passed")
    # zzz.writerow([entry[0],entry[1],"|".join(countries),"|".join(cities),entry[2]])
con.commit()