import psycopg2
from collections import defaultdict
import operator

con = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur = con.cursor()
query = "select * from dolcera.rerun_final_alias_table"

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con.cursor()
query2 = "select * from dolcera.rerun_final_alias_table_assignees"

cur.execute(query)
orig_ulti_dict = {}
orig_full_row = {}
ulti_orig_dict = defaultdict(set)
counter = 0
for u,o,ctry,cty,f,_ in cur:
    counter+=1
    if counter%10000 == 0:
        print str(counter)+" : First Loop"
    orig_ulti_dict[o] = u
    ulti_orig_dict[u].add(o)
    orig_full_row[o] = [u,o,ctry,cty,f]
    
cur2.execute(query2)
orig_freq_assg_dict = {}
assg_orig_full_row = {}
assg_orig_ulti_dict = {}
ulti_orig_assg_dict = defaultdict(set)
counter = 0
for u,o,ctry,cty,f in cur2:
    counter+=1
    if counter%100000 == 0:
        print str(counter)+" : Second Loop"
    assg_orig_ulti_dict[o] = u
    ulti_orig_assg_dict[u].add(o)
    orig_freq_assg_dict[o] = f
    assg_orig_full_row[o] = [u,o,ctry,cty,f]
    
cur2.execute(query2)
assg_ulti_orig_freq_dict = defaultdict(list)
for u,o,_,_,f in cur2:
    assg_ulti_orig_freq_dict[u].append((o,f))
    
unresolved = set()
resolved =set()
for i in ulti_orig_assg_dict:
    if i not in orig_ulti_dict:
        unresolved.add(i)
    else:
        resolved.add(i)
        
final = []
for i in resolved:
    for j in ulti_orig_assg_dict[i]:
        if i!= j and j not in orig_full_row:
            final.append([orig_full_row[i][0],j,orig_full_row[i][2],orig_full_row[i][3],orig_freq_assg_dict[j],'assignees-1'])
            
final_unresolved = []
new_names = set()
flag2 = 0
for name in unresolved:
    flag = 0
    for i in sorted(assg_ulti_orig_freq_dict[name], key=operator.itemgetter(1), reverse = True):
        if i[0] in orig_full_row:
            rep = i[0]
            flag = 1
            break
    if flag == 1:
        for j in ulti_orig_assg_dict[name]:
            if j not in orig_full_row:
                final_unresolved.append([orig_full_row[rep][0],j,orig_full_row[rep][2],orig_full_row[rep][3],orig_freq_assg_dict[j],'assignees-2'])
    else:
        for j in ulti_orig_assg_dict[name]:
            if j not in orig_full_row:
                row = []
                row.extend(assg_orig_full_row[j])
                row.append('assignees-3')
                final_unresolved.append(row)
                
con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
query2 = "insert into dolcera.rerun_final_alias_table values (%s, %s, %s, %s, %s, %s)"

counter = 0
for i in final:
    counter+=1
    if counter%10000 == 0:
        con2.commit()
    cur2.execute(query2,tuple(i))
con2.commit()

counter = 0
for i in final_unresolved:
    counter+=1
    if counter%10000 == 0:
        print counter
        con2.commit()
    cur2.execute(query2,tuple(i))
    
con2.commit()