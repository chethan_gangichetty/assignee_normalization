from __future__ import division
from collections import defaultdict
from itertools import combinations
import operator
from fuzzywuzzy import fuzz
import re
from collections import Counter
from string import digits

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

fam = set()
reader = readCSV("../asgTable.csv")
for rindex,row in enumerate(reader):
    if rindex%10000 == 0:
        print rindex
    if row[-1]=="Assignee":
        continue
    # #print row
    #if "LG" not in row[3].upper() and "SAMSUNG" not in row[3] and "ERICSSON" not in row[3].upper() and "DAEWOO" not in row[3].upper():
    #     continue
    if "PROCTE" not in row[3].upper():
        continue
    if len(row)<9:
        continue
    fam.add(row[2])
    
reader = readCSV("../asgTable.csv")
zzz = writeCSV("Proctertestset.csv")
for rindex,row in enumerate(reader):
    if row[2] in fam:
        zzz.writerow(row)
