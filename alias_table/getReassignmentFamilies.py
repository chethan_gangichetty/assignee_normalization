import urllib2
import json
import requests

goodlist = '''EP RAP1
CN ASS
DE 8327
DE 8127
CN C56
EP RAP2
DE R081
EP NLS
EP RAP3
AU PC1
AU PC
NL NL80
EP NLT1
FI PC
FR TP
EP ITPR
NZ ASS
BR B25D
GB COOA
EP RAP4
CH PUE
HU HPC4
AU TC
FI GB
CH PFA
EP BECH
NO CHAD
BR B25G
HU GB9A
NL SNR
ES PC1K
ES PC2A
AU HB
FI HC
HU HC9A
HU DGB9
EP BECN
NL SD
FR TQ
NL TNT
MX GB
AT PC
FI TC
BR B25C
ES PC1A
SK PC4A
DD ASS
MX HC
FI PCU
BE CH
NL TD
HK AS
IL HC
PT PD3A
SI SP73
AT PD9K
BR PC
PT PC4A
SK TC4A
AT HC
BR B25B
BE CN
IL HP
BR B25L
BR B25E
BR B25K
LT PD9A
LT PC9A
BR B25M
PT PC3A
BR B25F
PT PD4A
AT TC9K
MD TC4A
CH PUEA
PT PC4K
CH PFUS
AU HC1
NL CD
BR B25I
NL CNR
MD PD4A
FI TCU
PT PC3K
MD HC9A
BR B25J
BR B25H
AU PCD
EE HC1A
MD GB9A
PT PD3K
SK SPCC
EE GB1A
PT PD4K
BR PCP
NL DNT
NL DD
MD PD4K
AT HC
AT PC
AT PD9K
AT TC9K
AU HB
AU HC1
AU PC
AU PC1
AU PCD
AU TC
BE CH
BE CN
BR B25A
BR B25B
BR B25C
BR B25D
BR B25E
BR B25F
BR B25G
BR B25H
BR B25I
BR B25J
BR B25K
BR B25L
BR B25M
BR PC
BR PCP
CH PFA
CH PFUS
CH PUE
CH PUEA
CN ASS
CN C56
DD ASS
DE 8127
DE 8327
DE R081
EE GB1A
EE HC1A
EP RAP1
EP RAP2
EP RAP3
EP RAP4
ES PC1A
ES PC1K
ES PC2A
FI GB
FI HC
FI PC
FI PCU
FI TC
FI TCU
FR TP
FR TQ
GB COOA
HK AS
HU DGB9
HU GB9A
HU HC9A
HU HPC4
IL HC
IL HP
LT PC9A
LT PD9A
MD GB9A
MD HC9A
MD PD4A
MD PD4K
MD TC4A
MX GB
MX HC
NL CD
NL CNR
NL DD
NL DNT
NL NL80
NL SD
NL SNR
NL TD
NL TNT
NO CHAD
NZ ASS
PT PC3A
PT PC3K
PT PC4A
PT PC4K
PT PD3A
PT PD3K
PT PD4A
PT PD4K
SI SP73
SK PC4A
SK SPCC
SK TC4A
US AS
'''.splitlines()
session = requests.session()

def writeCSV(path,mode="w"):
    import unicodecsv as csv
    myfile=open(path,mode)
    fileOutput = csv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput
def iteration(cursormark="*",fields=["pn,stdinv"]):
    url="http://srch11.dolcera.net:11080/solr/alexandria-standard/select?fl=ucid,fam,lsccd&indent=on&q=lsccd:*&rows=2000&wt=json&cursorMark={{cursorMark}}&sort=ucid+asc"
    url=url.replace("{{cursorMark}}",cursormark)
    print url
    error = 0
    while error == 0:
        try:
            data=session.get(url).text
            dicta=json.loads(data)
            newCursor=dicta["nextCursorMark"]
            error = 1
        except:
            print "Error"
            newcursor = cursormark
            error = 0
    return newCursor,dicta

if __name__=="__main__":
    cursormark="*"
    zzz=writeCSV("pn_ignore_overall.csv")
    counter=0
    while 1:
        newCursor,dicta=iteration(cursormark)
        if cursormark==newCursor:
            break
        cursormark=newCursor
        #as_counter = 0
        for d in dicta["response"]["docs"]:
            fam=d.get("fam","")
            ls = d.get("lsccd",[])
            pn = d.get("ucid","")
            #print ls,'ls'
            as_counter = 0
            for entry in ls:
                lookup = " ".join(entry.split("_")[:-1])
                #print lookup
                lookup = lookup.replace("_"," ")
                #print lookup
                if lookup in goodlist:
                    as_counter = as_counter+1
                    if ("US-" not in pn) or (lookup.startswith("US ") and as_counter >1):
                        print "Found it"
                        as_counter = as_counter + 1
                        zzz.writerow([pn,fam,lookup])
        counter=counter+1
        print counter,"batches done"
