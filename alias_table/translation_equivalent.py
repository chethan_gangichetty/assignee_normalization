from __future__ import division
import regex
from collections import defaultdict
import operator
# from capiq import indexCapIq
from unidecode import unidecode_expect_nonascii
import re
import psycopg2
# idc = indexCapIq()

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace(".","")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= re.sub( '\s+', ' ', g ).strip()
    return g 

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

if __name__=="__main__":
    # reader = readCSV("../SampleAlias.csv")
    con = psycopg2.connect(
        "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur = con.cursor()
    cur_smt = con.cursor()
    cur_asian = con.cursor()

    alias = {}
    query = "select * from dolcera.rerun_alias_table"
    cur.execute(query, tuple())
    for ulti, orig, countries, cities, freq in cur:
        alias[orig] = ulti

    # for row in reader:
    #     alias[row[1]] = row[0]
    # reader = readCSV("pn_ignore_overall.csv")
    fams = set()
    # for row in reader:
    #     fams.add(row[0])
    reader  =readCSV("../asgTable_removeinventors.csv")
    seq_data = defaultdict(list)
    mxw_names = set()
    regular_names = set()
    asian_cases = []
    asian_freq_dist = defaultdict(dict)
    smt_freq_dist = defaultdict(dict)
    asian_name_dist = {}
    smt_name_dist = {}
    
#################################################################################################################################
#                                                     Iterate through asgTable.csv 
# Metadata: (Application ID|Country|Family ID|Assignee|Sequence|Format(EPO/Original/Intd/SMT)|IFI|City|Applicant/Assignee)
#################################################################################################################################

    for rindex,row in enumerate(reader):
        if rindex%10000==0:
            print rindex
        # if rindex==400000000:
        #     break
        if row[2] in fams:
            continue
        number = row[8]+":"+row[4]

# Create a dictionary seq_data, PatentNumber_SequenceNumber as keys and Assignees as values
# { Pn1_Seq1: [Asgn1, Asgn2], Pn2_Seq2: [Asgn3, Asgn4]} 

        seq_data[number].append(row[3])
        
# Segregate the mxw-smt entries from the others including (original, intermediate and epo) 

        if row[6]=="mxw-smt":
            # print row[6],row[8]
            mxw_names.add(row[3])
        else:
            regular_names.add(row[3])

# Iterate through the sequence data
            
    for sindex,seq in enumerate(seq_data):
        if sindex%10000==0:
            print sindex,"Processing sequences"
        entries = seq_data[seq]
        asian = ""
        smt = ""
        entries = [removeAllPunctuations(entry) for entry in entries]

# Search for asian entry (non-latin entry) and mxw-smt entries for every record in Seq_data
        
        for entry in entries:
            flag1 = regex.search(ur'[^\p{Latin}\s\p{punct}\d]', entry)
            if flag1:
                #print entry
                asian = entry
                #print asian
                break
        for entry in entries:
            if entry in mxw_names and entry not in regular_names:
                smt = entry
                break
        if asian=="" and smt=="":
            continue
        others = [entry for entry in entries if entry!=asian and entry!=smt]
        if len(others)<2:
            continue
        #print others
        
# Retrieve the cluster representatives for every non-asian entry and non mxw-smt entry
        
        others = set([alias.get(oth,oth+"") for oth in others])
        
# Create frequeny distribution for all the non-latin (asian) and mxw-smt entries -> asian_name_dist and smt_name_dist

        if asian!="":
            asian_name_dist[asian] = asian_name_dist.get(asian,0) +1
        if smt!="":
            smt_name_dist[smt] = smt_name_dist.get(smt,0) +1

# Create a co-occurence dictionary for all the non-asian and mxw-smt entries -> smt_freq_dist and asian_freq_dist
            
        for oth in  others:
            if asian!="":
                asian_freq_dist[asian][oth] = asian_freq_dist[asian].get(oth,0) + 1
            if smt!="":
                smt_freq_dist[smt][oth] = smt_freq_dist[smt].get(oth,0) + 1
        #print asian_freq_dist
        #print smt_freq_dist
        #raw_input()
    # zzz = writeCSV("../smt_pairs_test.csv")
    # yyy = writeCSV("../asian_pairs_test.csv")

#################################################################################################################################
#         Iterate through the smt_name frequency distribution in the descending order of individual frequencies of smt_names
#################################################################################################################################
    
    sorted_x = sorted(smt_name_dist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()

    inq_smt = 'insert into dolcera.rerun_an_smt_pairs values (%s, %s, %s, %s, %s)'

    count_smt = 0
    for xindex, x in enumerate(sorted_x):
        count_smt = count_smt + 1
        array = []
        array.extend(x)
# Retrieve the assignee with the highest co-occured frequency from the smt_freq_dist
        sorted_y = sorted(smt_freq_dist[x[0]].items(), key=operator.itemgetter(1))
        sorted_y.reverse()
        array.append(sorted_y[0])
        cur_smt.execute(inq_smt, (
        array[0], list(array[2])[0], array[1], list(array[2])[1], int(list(array[2])[1]) / int(array[1])))
        if count_smt % 10000 == 0:
            con.commit()
    con.commit()

    # for xindex,x in enumerate(sorted_x):
    #     array = []
    #     array.extend(x)
    #     # print x
    #     # print smt_freq_dist[x[0]]
    #     # raw_input()
    #     sorted_y = sorted(smt_freq_dist[x[0]].items(), key=operator.itemgetter(1))
    #     sorted_y.reverse()
    #     if len(sorted_y)>15:
    #         sorted_y = sorted_y[:15]
    #     # print sorted_y
    #     # raw_input()
    #     array.append([x for x in sorted_y])
    #     #array
    #     zzz.writerow(array)
    
#################################################################################################################################
#    Iterate through the asian_name frequency distribution in the descending order of individual frequencies of smt_names
#################################################################################################################################

    sorted_x = sorted(asian_name_dist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()

    inq_asian = 'insert into dolcera.rerun_asian_pairs values (%s, %s, %s, %s, %s)'

    count_asian = 0
    for xindex, x in enumerate(sorted_x):
        count_asian += 1
        array = []
        array.extend(x)
        sorted_y = sorted(asian_freq_dist[x[0]].items(), key=operator.itemgetter(1))
        sorted_y.reverse()
# Retrieve the assignee with the highest co-occured frequency from the asian_freq_dist
        array.append(sorted_y[0])
        cur_asian.execute(inq_asian, (
        array[0], list(array[2])[0], array[1], list(array[2])[1], int(list(array[2])[1]) / int(array[1])))
        if count_asian % 10000 == 0:
            con.commit()
    con.commit()

    # for xindex,x in enumerate(sorted_x):
    #     array = []
    #     array.extend(x)
    #     sorted_y = sorted(asian_freq_dist[x[0]].items(), key=operator.itemgetter(1))
    #     sorted_y.reverse()
    #     if len(sorted_y)>15:
    #         sorted_y = sorted_y[:15]
    #     array.append([x for x in sorted_y])
    #     yyy.writerow(array)


        #flag1 = regex.search(ur'[^\p{Latin}\s\p{punct}\d]', row[3])
    
                    
        
        