import psycopg2
from datetime import datetime
import csv

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

reference = datetime(2018, 8, 29, 0, 0, 0, 0)
conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select publication_id, updatedon from dolceradata.normalizedcompany"

cur.execute(query)
writer = writeCSV("pubids.csv")
pubid_list =set()
counter = 0
for i in cur:
    counter = counter + 1
    if counter%1000000 == 0:
        print counter
    pubid = list(i)[0]
    time = list(i)[-1]
    if time<= reference:
        pubid_list.add(pubid)
        writer.writerow([pubid,time])

# for row in pubid_list:
#     writer.writerow(row)
