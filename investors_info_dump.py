import requests
from collections import defaultdict
import regex
import psycopg2
import tldextract
import cleanco
from unidecode import unidecode_expect_nonascii
from unidecode import unidecode
import unicodedata
import regex as re

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

def get_foundation_website(id_):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
    url = url.replace("{{idd}}", id_)
    data = session.get(url).json()
    if len(data['response']['docs'])>0:
        for i in data['response']['docs']:
            foundation = i.get('foundation','')
            website = i.get('website','')    
    else:
        foundation = ''
        website = ''
    return foundation, website

def get_ultiid_ultiname(id_):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=id:{{idd}}&wt=json"
    url = url.replace("{{idd}}", id_)
    data = session.get(url).json()
    if len(data['response']['docs'])>0:
        for i in data['response']['docs']:
            ultiid = i.get('ultimateParentID','')[0]
            ultiname = i.get('ultiParent','')    
    else:
        ultiid = ''
        ultiname = ''
    return ultiid, ultiname

reader = readCSV("CLEANMATCH.csv")
session = requests.session()

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor()
query = "insert into dolceradata.an_investors_info2 values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

com_rel = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/CompanyRel.txt","rb").read()
com_rel = com_rel.split("#@#@#")
inve_invr = defaultdict(list)
counter = 0
for line in com_rel:
    if counter%10000 == 0:
        print counter, "Company Rel"
    counter = counter+1
    line = line.split("'~'")
    if line[0]=="A" and line[4] == '1':
        inve_invr[line[3]].append(line[2])
        
found_info = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/FoundationCompany.txt","rb").read()
found_info = found_info.split("#@#@#")
id_name = {}
id_website = {}
counter = 0
for line in found_info:
    if counter%10000 == 0:
        print counter, "Foundation file"
    counter = counter+1
    line = line.split("'~'")
    if len(line) > 1:
        id = unidecode_expect_nonascii(strip_accents(line[0]))
        if id not in id_name:
            id_name[id] = unidecode_expect_nonascii(strip_accents(line[1]))
        if id not in id_website:
            try:
                website = tldextract.extract(row[17]).registered_domain
            except Exception,e:
                website = ""
            id_website[id] = website

data = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/CompanyRel.txt","rb").read()
reader = data.split("#@#@#")
id_relations={}
id_relationsCount = {}
for rindex,row in enumerate(reader):
    if rindex%100000==0:
        print rindex,"Reading company relations"
    row = row.split("'~'")
    if row[0] == "D":
        continue
    if len(row)<5:
        continue
    if row[4]=="5" or row[4]=="7" or row[4]=="9":
        id_relations[row[3]]=row[2]
for row in id_relations:
    sub = row
    par = id_relations.get(sub,sub)
    counter=0 
    while par!=sub:
        counter=counter+1
        if counter>20:
            break
        id_relationsCount[par] = id_relationsCount.get(par,0)+1
        par = id_relations[sub]
            
inv_desc = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/BusinessDescription.txt","rb").read()
inv_desc = inv_desc.split("#@#@#")
id_desc = {}
counter = 0
for line in inv_desc:
    if counter%10000 == 0:
        print counter, "Business Description"
    counter = counter+1
    line = line.split("'~'")
    if line[0] == 'A':
        id = line[1]
        if id not in id_desc:
#             id_desc[id] = unidecode_expect_nonascii(strip_accents(line[2]))
            id_desc[id] = unidecode_expect_nonascii(strip_accents(' '.join(re.split('(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s',line[2])[0:3])))

id_ultiid = {}
found_info = open("/home/premrajn/Assignee_Normalization_Scripts/capiq/FoundationCompany.txt","rb").read()
found_info = found_info.split("#@#@#")

counter = 0
for line in found_info:
    if counter%10000 == 0:
        print counter, "Foundation file"
    counter = counter+1
    line = line.split("'~'")
    if len(line) > 1:
        id = unidecode_expect_nonascii(strip_accents(line[0]))
    ultiId = id
    loopcounter = 0
    while ultiId in id_relations and id_relations[ultiId]!=ultiId and loopcounter<100:
        ultiId=id_relations.get(ultiId,ultiId)
        loopcounter=loopcounter+1
    id_ultiid[id] = ultiId
    
reader = readCSV("capiqid_companytype.csv")
counter = 0
id_type = {}
for row in reader:
    if counter%10000 == 0:
        print counter, "Capiqid Company Type"
    counter = counter+1
    id_type[row[0]] = row[1]
    
reader = readCSV("corporate_tree.csv")
counter = 0
corp_tree = {}
for row in reader:
    if counter%10000 == 0:
        print counter, "Corporate Tree"
    counter = counter+1
    corp_tree[row[0]] = eval(row[1])
    
counter = 0
# reader = readCSV("CLEANMATCH.csv")
# writer = writeCSV("foundation_investors.csv")
to_add = []
covered_ids = set()
for idd in id_ultiid:
#     if len(row)>5:
    if counter%10000 == 0:
        print counter, "Clean Match"
#             conn.commit()
    counter += 1
#     idd = row[7]
    if idd not in covered_ids:
        covered_ids.add(idd)
    else:
        continue
#     normpa = row[10]
    normpa = id_name.get(id_ultiid[idd],"")
    found, web = get_foundation_website(idd)
    desc = id_desc.get(idd,"")
    inv_ids = set()
    subs = corp_tree.get(idd,idd)
    for i in subs:
        invs = set(inve_invr.get(i,[]))
        for j in invs:
            inv_ids.add(j)
#         inv_ids = inve_invr.get(idd,[])
    inv_ids = list(inv_ids)
    inv_name = []
    inv_website = []
    inv_desc = []
    inv_ultiid = []
    inv_ultiname = []
    inv_type = []
    for inv in inv_ids:
        ultiid_inv = id_ultiid.get(inv,"")
        ultiid_name = id_name.get(ultiid_inv,"")
#             ultiid_inv,ultiid_name = get_ultiid_ultiname(inv)
        inv_ultiid.append(ultiid_inv)
        inv_ultiname.append(ultiid_name)
        inv_name.append(id_name.get(inv,""))
        inv_website.append(id_website.get(inv,""))
        inv_desc.append(id_desc.get(inv,""))
        inv_type.append(id_type.get(inv,""))
    array = []
    array.append(idd)
    array.append(normpa)
    array.append(found)
    array.append(web)
    array.append(inv_ids)
    array.append(inv_name)
    array.append(inv_website)
    array.append(inv_desc)
    array.append(inv_ultiid)
    array.append(inv_ultiname)
    array.append(desc)
    array.append(inv_type)
    to_add.append(tuple(array))
    if len(to_add) ==  10000:
        dataText = ','.join([cur.mogrify('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)', row) for row in to_add])
        cur.execute('INSERT INTO dolceratesting.an_investors_info(capiqid,normpa,foundation,website,investor,inv_name,inv_website,inv_desc,inv_ultiid,inv_ultiname,description,inv_type) VALUES '+dataText)
        to_add = []
        conn.commit()
dataText = ','.join([cur.mogrify('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)', row) for row in to_add])
cur.execute('INSERT INTO dolceratesting.an_investors_info(capiqid,normpa,foundation,website,investor,inv_name,inv_website,inv_desc,inv_ultiid,inv_ultiname,description,inv_type) VALUES '+dataText)
to_add = []
conn.commit()
conn.close()


#         cur.execute(query, tuple(array))
# conn.commit()
