import requests
import unicodedata
import json
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
import csv
import sys
import tldextract
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2
import multiprocessing
from multiprocessing import Queue
import time

def removeAllPunctuations(g):
    g= g.replace(".","")
    g= g.replace(",","")
    g= g.replace("'","")
    g= g.replace("-"," ")
    g= g.replace("/","")
    g= g.replace(":","")
    g= g.replace(";","")
    g= g.replace("+"," ")
    g= g.replace('"',"")
    g= g.replace("*","")
    g= g.replace("?"," ")
    g= g.replace("["," ")
    g= g.replace("]"," ")
    g= g.replace("("," ")
    g= g.replace(")"," ")
    g= g.replace("<"," ")
    g= g.replace(">"," ")
    g= g.replace("="," ")
    g= g.replace(","," ")
    g= g.replace("&","AND")
    g= re.sub( '\s+', ' ', g ).strip()
    return g
ASN_PATH=""
CAPIQ_FILEPATH = ""
def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
def removeSpaces(name):
    while "  " in name:
        name=name.replace("  "," ")
    return name.strip()
class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE "," AND "]
    def getCountry(self,country):
        return self.countryCode.get(country,"")
    def processVariations(self):
        reader=readCSVori(CAPIQ_FILEPATH+"AN_variations_modified.csv")
        self.AN_variations={}
        for row in reader:
            self.AN_variations[row[0].strip()]=row[1].strip()
    def processVariations1(self):
        reader=readCSVori(CAPIQ_FILEPATH+"Bussiness.csv")
        self.replacers=[]
        for row in reader:
            self.replacers.append(" "+row[0].upper()+" ")
        self.replacers.append(" "+"CO"+" ")
    def is_ascii(self,s):
        return all(ord(c) < 128 for c in s)    
    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin')) if unicodedata.category(c) != 'Mn')  
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    def getStandardizedName(self, name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=self.strip_accents(name)
        name=unidecode_expect_nonascii(name.replace(".","").replace(","," "))
        name=removeSpaces(name)
        name=removeAllPunctuations(name)
        name=" "+name.upper()+" "        
        for v in self.AN_variations:
            newv=" "+v+" "
            name=name.replace(newv," "+self.AN_variations[v]+" ")
        name=removeSpaces(name)
        return name.strip()
    def getStrippedName(self,name):
        if name=="":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name=unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name=cleanco(name).clean_name()
        stripped_name=" "+stripped_name+" "
        for r in self.replacers:
            stripped_name=stripped_name.replace(r," ")
        for r in self.articles:
            stripped_name=stripped_name.replace(r," ")
        stripped_name=removeSpaces(stripped_name)
        return stripped_name.strip()
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def readCSVori(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader
def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

# icq = indexCapIq()
# reader = readCSV("CLEANMATCH_OLD.csv")
# old_ultiid = {}
# old_immid = {}
# old_dict = defaultdict(list)
# for row in reader:
# # for ulti_std,orig_std,orig_Name,Cluster_rep,Std_Name,Capiq_ID, Capiq_Orig_Name,Ulti_ID,Ulti_Flag,Ulti_Parent,Immediate_Parent,Industry,Sub_Count,City,Website,Country,Score,CPC_Grade,Ind_Score,Flag,Ulti_geo, source in reader:
#     if len(row)>5 and row[1] not in old_ultiid:
#         old_ultiid[row[1]] = row[7]
#         old_immid[row[1]] = row[5].split('-')[0]
#         old_dict[row[1]] = row
        
# reader = readCSV("CLEANMATCH.csv")
# new_ultiid = {}
# new_immid = {}
# new_dict = defaultdict(list)
# for row in reader:
# # for ulti_std,orig_std,orig_Name,Cluster_rep,Std_Name,Capiq_ID, Capiq_Orig_Name,Ulti_ID,Ulti_Flag,Ulti_Parent,Immediate_Parent,Industry,Sub_Count,City,Website,Country,Score,CPC_Grade,Ind_Score,Flag,Ulti_geo, source in reader:
#     if len(row)>5 and row[1] not in new_ultiid:
#         new_ultiid[row[1]] = row[7]
#         new_immid[row[1]] = row[5].split('-')[0]
#         new_dict[row[1]] = row

def vworker(x,vqueue,outqueue):
    print('vworker',x)
    n1 = time.time()
    icq = indexCapIq()
    cluster_dict = {}
    assgn_dict = {}
    cluster_fd = {}
    assg_fd = {}
    counter = 0
    while True:
        rec=vqueue.get()
        if rec is None:
            outqueue.put((cluster_dict,cluster_fd, assg_fd, assgn_dict))
            print('returning vworker',x)
            print(time.time()-n1)
            break 
        else:
            ulti,orig,freq=rec
            counter += 1
            if counter%10000 == 0:
                print counter,x
            std_name = icq.getStandardizedName(ulti)
            std_name_assgn = icq.getStandardizedName(orig)
            if ulti not in cluster_dict:
                cluster_dict[ulti] = std_name
            if std_name not in cluster_fd:
                cluster_fd[std_name] = freq
            if freq > cluster_fd[std_name]:
                cluster_fd[std_name] = freq
            
            assgn_dict[orig] = std_name_assgn
            assg_fd[std_name_assgn] = freq
            
def allworker(x,outqueue):
    print('vworker',x)
    n1 = time.time()
    all_cluster_dict = {}
    all_cluster_fd = {}
    all_assg_fd = {}
    all_assgn_dict = {}
    counter = 0
    while True:
        rec=outqueue.get()
        if rec is None:
            freq_dist_assgn = writeCSV("freq_dist_assgn.csv")
            freq_dist_cluster = writeCSV("freq_dist_cluster.csv")
            print "in none"
            for i in all_assgn_dict:
#                 freq_dist_assgn.writerow([i, all_assgn_dict.get(i,"XXX NOT XXX"), all_assg_fd.get(all_assgn_dict[i],99998888)])
                freq_dist_assgn.writerow([i, all_assgn_dict.get(i,"XXX NOT XXX")])
            print "done file1"
            for i in all_cluster_dict:
                freq_dist_cluster.writerow([i, all_cluster_dict[i], all_cluster_fd[all_cluster_dict[i]]])
            print('returning vworker',x)
            print(time.time()-n1)
            break 
        else:
            cluster_dict,cluster_fd,assg_fd,assgn_dict=rec
            for zz in cluster_dict:
                all_cluster_dict[zz]=cluster_dict[zz]
            for zz in cluster_fd:
                all_cluster_fd[zz]=cluster_fd[zz]
            for zz in assg_fd:
                all_assg_fd[zz]=assg_fd[zz]
            for zz in assgn_dict:
                all_assgn_dict[zz] = assgn_dict[zz]
                
vqueue=Queue(100000)
outqueue=Queue(100000)

num_workers = 23
p1 = [multiprocessing.Process(target=vworker, args=(x,vqueue,outqueue)) for x in range(num_workers)]
p2 = [multiprocessing.Process(target=allworker, args=(x,outqueue)) for x in range(1)]

for f in p1:
    f.start()

for f in p2:
    f.start()

con5 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur5 = con5.cursor("iter-1")
query5 = "select * from dolcera.an_final_alias_table_accented_cities"
cur5.execute(query5)

counter = 0
for ulti, orig, countries, cities, freq in cur5:
    vqueue.put((ulti,orig,freq))
    
for x in range(num_workers):
    vqueue.put(None)
    
for f in p1:
    f.join()

for x in range(1):
    outqueue.put(None)
    
for f in p2:
    f.join()
