import urllib2
import json
import requests
import psycopg2

session = requests.session()


def writeCSV(path, mode="w"):
    import unicodecsv as csv
    myfile = open(path, mode)
    fileOutput = csv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


def iteration(cursormark="*", fields=["pn,stdinv"]):
    url = "http://srch12.dolcera.net:11080/solr/alexandria-standard/select?fl=pn,stdinv,cpcl3,pa,ad,icl3,normpa,pcitpn,fam,stdpa,cpcl,icl&indent=on&q=*:*&rows=2000&wt=json&cursorMark={{cursorMark}}&sort=ucid+asc"
    url = url.replace("{{cursorMark}}", cursormark)
    print url
    error = 0
    while error == 0:
        try:
            data = session.get(url).text
            dicta = json.loads(data)
            # print dicta
            newCursor = dicta["nextCursorMark"]
            error = 1
        except Exception, e:

            print "Error", e

            newcursor = cursormark
            error = 0
    return newCursor, dicta


if __name__ == "__main__":
    # cursormark = "AoExVVMtMjAwOTAwMDk3ODYtQTE="
    cursormark='*'
#     zzz = writeCSV("INVDATA1.csv", "w")
    counter = 0
    flag = 0
    conn=psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    cur1 = conn.cursor()
    inq = 'insert into dolcera.invdata1 values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
    while 1:
        newCursor, dicta = iteration(cursormark)
        if cursormark == newCursor:
            break
        cursormark = newCursor
        for d in dicta["response"]["docs"]:
            pn = d.get("pn", "")
            stdinv = d.get("stdinv", [])
            invComp = set(stdinv)
            cpcl3 = d.get("cpcl3", [])
            pa = d.get("pa", [])
            ad = d.get("ad", "")
            icl = d.get("icl3", "")
            fam = d.get("fam", "-1")
            normpa = d.get("normpa", [])
            cpc = d.get("cpcl", [])
            ipc = d.get("icl", [])
            pcitpn = d.get("pcitpn", [])
            stdpa = d.get("stdpa", [])
            stdpa = [spa for spa in stdpa if spa.upper() not in stdinv]
#             zzz.writerow([pn, "|".join(stdinv), "|".join(cpcl3), "|".join(pa), ad, "|".join(icl), "|".join(normpa),
#                           "||".join(cpc), "||".join(ipc), "||".join(pcitpn), "||".join(stdpa), fam])
            cur1.execute(inq, (pn, stdinv, cpcl3, pa, ad, icl, normpa, cpc, ipc, pcitpn, stdpa, fam))
            flag = flag+1
            if flag%100 == 0:
                conn.commit()
            
        counter = counter + 1
               
        print counter, "batches done"
