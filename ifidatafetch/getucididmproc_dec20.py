#!/usr/bin/env python


'''
getdataprem: script using Common table expression

with expression creates common tables:

        with pdv as  (select columns from patentdocvalue.....),
        partt as (select content from parties table),
        ...
        ...

we are fetching data from %(start)s to %(end)s publication_id and storing as Common table
after getting data in every commont table
 left join tables to get all required data in select part:

        select commontable.column, commontable2.columns from commontable join commontable1



parties parser takes parties xml in constructor

partiesparser(partyxml)

getparties data takes list of fields to parse ['inv','asg','appl']
getpartiesdata([list of fields])

to parse and get reassignment

getreasgdata()


sample output of this script


'''
import logging
import psycopg2
from multiprocessing import Queue, Process
import psycopg2.extensions
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
import time

import csv
analyticslibpath='/Volumes/Partition2/analyticslib'
import sys
from pprint import pprint
sys.path.insert(0,analyticslibpath)
getdataprem = '''
with pdv as (
    SELECT ucid publication_no,
    publication_id id,
    country country,
    family_id fam
    from xml.t_patent_document_values  where publication_id>=%(start)s and publication_id <= %(end)s ),
    antab as (select * from xml.t_application_reference  where publication_id>=%(start)s and publication_id <= %(end)s),
    partt as (select publication_id,content  parties
	from xml.t_parties where publication_id>=%(start)s and publication_id <= %(end)s)

    select xpath('//@ucid',antab.content)::text[] application_no,pdv.publication_no,pdv.id,pdv.country,pdv.fam,partt.parties from pdv left outer join
    partt on pdv.id=partt.publication_id left outer join antab on pdv.id = antab.publication_id
    '''
def outworker(id,writer,fileobj,outque):
    outcount=0

    # conn=psycopg2.connect("dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
    # cur1 = conn.cursor()
    # inq = 'insert into dolcera.asg values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
    
    while True:

        outrec=outque.get()
        #print outrec
        if not outrec:
            print 'output thread ',id,'completed. wrote records',outcount
            fileobj.close()
            return
        for asg in outrec["assignees"]:
            # pprint(asg)
            country = asg["country"]
            city = asg.get("record", {}).get("city", None)
            fam = outrec["fam"]
            try:
                ucid = outrec["ucid"][0]
            except Exception,e:
                print e
                continue
            name = asg["name"]
            format = asg["format"]
            source = asg["source"]
            sequence = asg["sequence"]
            field = asg["field"]
            #print asg
            app_num = outrec.get("application_no","None")
            outcount+=1
            writer.writerow([ucid, country, fam, name, sequence, format, source,city,app_num,field])
            # cur1.execute(inq,(ucid, country, fam, name, sequence, format, source,city,app_num, field))
            # if outcount%10 == 0:
            #     conn.commit()
    
def getdata(id,inque,outque):
    con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
    cur = con.cursor()
    count=0
    while True:
        stend=inque.get()
        if not stend:
            print 'thread id ',id,'completed total records done :',count
            cur.close()
            con.close()

            return
        start,end=stend



        slots=['ucid','application_no','pub_id','country','fam','parties']
        logging.info(slots)
        cur.execute(getdataprem%{'start': start, 'end': end})
        # cur.execute(getdataprem%{'start': 'xml.f_ucid2id(\'CN-206357249-U\')', 'end': 'xml.f_ucid2id(\'CN-206357249-U\')'})#test run
        for rec in cur.fetchall():
            record=dict(zip(slots,rec))
            #print rec
            pxml=record['parties']

            if pxml:
                # print pxml
                try:
                    parser=partiesparser(pxml)
                except Exception,e:
                    print e
                    continue
                parser.data={}
                parser.getpartiesdata(['asg','appl'])#['inv', 'asg', 'appl', 'agent', 'examiner']
                try:
                    parser.getreasgdata() #to get reassignment data
                except Exception,e:
                    print record["ucid"]
                    print e
                    continue
                #print "---",parser.data,
                record['parties']=parser.data
                newreasg=[]
                reassignments=parser.data['reassignments']
                for reasg in reassignments:
                    # print reasg
                    reasgrec = dict(zip(['executiondate','reel','conveyance','assignees','assignors','rawxml','recorddate'], reasg))
                    newreasg.append(reasgrec)
                parser.data['reassignments']=newreasg
            # pprint(record)

            assignees=[]
            asgseq={}
            applseq={}
            formatpref=['epo','intermediate','original']
            # print record['parties']
            if record['parties']:
                for format in formatpref:
                    if 'appl' in record['parties'] and record['parties']['appl']:

                        for rec in record['parties']['appl']:
                            # print rec
                            field='applicant'
                            fformat=rec['format']
                            if format==fformat:
                                seq=rec['sequence']
                                #if seq not in applseq:
                                try:
                                    applseq[seq+":"+rec['load-source']+fformat]={'name':rec['name'],'field':'applicant','format':format,'source':rec['load-source'],'country':rec.get('country'),'record':rec}
                                except Exception,e:
                                    print e

                    # print applseq
                    if 'asg' in record['parties'] and record['parties']['asg']:

                        for rec in record['parties']['asg']:
                            field = 'assignee'
                            fformat = rec['format']
                            if fformat==format:
                                seq = rec['sequence']
                                if seq not in asgseq:  # skip other formats of seq
                                    try:
                                        asgseq[seq+":"+rec['load-source']+fformat] = {'name': rec['name'], 'field': 'assignee', 'format': format,
                                                   'source': rec['load-source'],'country':rec.get('country'),'record':rec}
                                    except Exception,e:
                                        print e
                # if 'reassignments' in record['parties'] and record['parties']['reassignments']:
                #     reasg=record['parties']['reassignments']
                #     reasgset=set()
                #     asgneeset=set()
                #     for reasgrec in reasg:
                #         if 'assignors' in reasgrec and reasgrec['assignors']:
                #             for x in reasgrec['assignors']:
                #                 if 'name' in x and x['name']:
                #                     reasgset.add(x['name'])
                #         if 'assignees' in reasgrec and reasgrec['assignees']:
                #             for x in reasgrec['assignees']:
                #                 if 'name' in x and x['name']:
                #                     asgneeset.add(x['name'])
                #                     assignees.append(
                #                         {'name': x['name'], 'field': 'assignees', 'source': 'USPTO reassignment',
                #                          'format': 'original','country':x.get('country'),'record':rec})
                #
                #     for name in reasgset.difference(asgneeset):
                #         assignees.append({'sequence':-1,'country':None,'name':name,'field':'assignors','source':'USPTO reassignment','format':'original'})

                # print asgseq,"asg seq"
                # print applseq,"appl seq"
                # print "---"
                for x in asgseq:
                    asgseq[x].update({'sequence': x.split(":")[0], 'field':"Assignee"})
                    assignees.append(asgseq[x])
                    #print 'seq added',asgseq[x]
                for x in applseq:
                    applseq[x].update({'sequence': x.split(":")[0],'field':"Applicant"})
                    assignees.append(applseq[x])
            record['assignees']=assignees
            # pprint(record)

            premslots=['ucid','country','fam','assignees',"application_no"]
            premrec={}
            for slot in premslots:
                premrec[slot]=record[slot]
            count+=1
            #print premrec
            outque.put(premrec)
from alib.patentproc.partiesparser import partiesparser
def writeCSV(path,mode="w"):
    import unicodecsv as csv
    myfile=open(path,mode)
    fileOutput = csv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput,myfile

from multiprocessing import Process,Queue
if __name__ == "__main__":
    numproc=10
    processes=[]
    inque=Queue(10000)
    outque=Queue(10000)
    for x in xrange(0,numproc):
        processes.append(Process(target=getdata,args=(x,inque,outque)))
    for p in processes:
        p.start()
    zzz,fileobj = writeCSV("../../asgTable.csv")

    outproc=Process(target=outworker,args=(id,zzz,fileobj,outque))
    outproc.start()
    start_time = time.time()
    batches=0
    for x in xrange(0,199000000,10000):
        inque.put([x,x+10000])
        batches+=1
#         if batches>=3:
#             break
        #break
    for p in processes:
        inque.put(None)
    for p in processes:
        p.join()
    outque.put(None)
    outproc.join()

    print time.time() - start_time,'finished'

