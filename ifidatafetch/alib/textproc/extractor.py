#!/usr/bin/env python
import json
import csv
import logging
import psycopg2
from pprint import pprint
import urllib2
from rake_a_string_nltk import RakeKeywordExtractor
import string
import operator
from nltk.tokenize import sent_tokenize
from multiprocessing import Queue,Process
from lxml import etree
import os

def extractexample(desc):
    descxml=etree.fromstring(desc)
    # print descxml.tag
    root=etree.fromstring('<ddes></ddes>')
    toadd=False
    for child in descxml:

        if 'description' in  child.attrib:
            if child.attrib['description']=='Detailed Description' and child.attrib['end']=='lead':
                toadd=True
                continue
            if child.attrib['description']=='Detailed Description' and child.attrib['end']=='tail':
                toadd=False
                continue
        if toadd:
            root.append(child)
    return etree.tostring(root)



class getSentences(object):
    def __init__(self):
        self.table = string.maketrans("","")
        reader = csv.reader(open(os.path.join(os.path.dirname(__file__),'data/stopwords.csv'),'rU'), delimiter=',', quotechar='"')
        self.stopwords=set()
        for row in reader:
            self.stopwords.add(row[0].lower())
        reader = csv.reader(open(os.path.join(os.path.dirname(__file__),'./data/newstopwords.csv'),'rU'), delimiter=',', quotechar='"')
        self.newstopwords=set()
        for row in reader:
            self.newstopwords.add(row[0].lower())
        self.rakeobj=RakeKeywordExtractor()
    def removeNonAscii(self,s):
        return "".join(filter(lambda x: ord(x)<128, s))
    def test_trans(self,s):
        s= s.replace('-',' ')
        return s.translate(self.table, string.punctuation+string.digits)
    def getKeywords(self,line):
        all_grams=set()
        for d in self.rakeobj.processrakes(line):
                grams=self.generateAllGrams(d)
                all_grams.update(grams)
        return all_grams
    def processRow(self,pn,ttl,ab,clm,desc):
        if desc and desc.strip()=="":
            print "data missing",pn
            return
        keywords=self.getKeywords(ttl+". "+ab+". "+clm)
        sentences=desc.splitlines()
        # print keywords
        dicta={}
        sentdict={}

        for s in sentences:
            # print type(s),s
            sentdict[s]=set()
            keywordssent=self.getKeywords(s)
            for key in keywordssent:
                if key in keywords:
                    sentdict[s].add(key)
                    dicta[key]=dicta.get(key,0)+1
        sorted_x = sorted(dicta.items(), key=operator.itemgetter(1),reverse=True)
        ckeywords=set()
        for pi,i in enumerate(sorted_x):
            if pi>0.1*len(sorted_x) and pi>15:
                continue
            ckeywords.add(i[0])
        chosen_sentences=[]
        for s in sentences:
            keywordssent=sentdict[s]
            for k in keywordssent:
                if k in ckeywords:
                    chosen_sentences.append(s)
                    break
        #print "I'm here!!"

        return keywords,chosen_sentences
    def generateGrams(self,c1,n):
        c1=self.removeNonAscii(c1)
        all_grams=set()
        if "," in c1:
            c1=c1.replace(",","##")
        if ";" in c1:
                c1=c1.replace(";","##")
        #print c1
        versions=c1.split("##")
        for v in versions:
            v=self.test_trans(v)
            v_words=v.split()
            v_words1=[]
            for v_temp in v_words:
                if v_temp.lower() not in self.stopwords:
                    v_words1.append(v_temp)
            for i in range(len(v_words1)-n+1):
                gram=" ".join(v_words1[i:i+n]).lower()
                if len(gram)<4:
                    continue
                all_grams.add(gram)
        return all_grams
    def generateAllGrams(self,sen):
        all_grams=set()
        for i in range(1,5):
            grams=self.generateGrams(sen,i)
            grams=[g for g in grams if g not in self.newstopwords]
            all_grams.update(grams)
        return all_grams

import time
if __name__=="__main__":
       g=getSentences()
       print g.processRow('pn','title','abstract','claim','desc')