import lxml.html
import logging
import codecs
from lxml.html.clean import Cleaner
from unidecode import unidecode
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
utf8_parser =lxml.html.HTMLParser(encoding='utf-8')
def xmltotext(textxml,removetag=None,collapsetag=None,verbose=False):
    if removetag:
        xmdoc=remove_tags(lxml.html.fromstring(textxml,parser=utf8_parser),removetag,verbose)
    else:
        xmdoc=lxml.html.fromstring(textxml,parser=utf8_parser)

    if collapsetag:
        cleaned= collapsepara(xmdoc,collapsetag,verbose)
    else:
        cleaner=Cleaner()
        cleaner.kill_tags=['heading','figref','table']
        x=cleaner.clean_html(xmdoc)
        cleaned=[line for line in x.text_content().splitlines() if line.strip()]
    return cleaned
def test():
    desc='testdata/descriptionxml'
    f=codecs.open(desc,'r','utf-8')
    text=f.read()
    strip=xmltotext(text,'//p[figref]','//p')
    print unidecode(u'\n'.join(strip))

def remove_tags(xmdoc,xpathstr,verbose):

    for elem in xmdoc.xpath(xpathstr ) :
        elem.getparent().remove(elem)

    return xmdoc
def collapsepara(xmdoc,xpathstr='//p',verbose=False):
    text=[]
    for x in xmdoc.xpath(xpathstr):
        if verbose:
            print x.text_content()
        text.append(u' '.join(x.text_content().split()))

    return text
if __name__=='__main__':
    test()