# -*- coding: utf-8 -*-
import string
import unicodedata
import sys
from unidecode import unidecode
from pprint import pprint
class strpreproc:
    def __init__(self,neglist=u'-()_,.'):
        self.tbl = dict.fromkeys(i for i in xrange(sys.maxunicode)
                          if unicodedata.category(unichr(i)).startswith('P') and unichr(i) not in neglist )
    def removepunctstr(self,text,punctstr='!"#$%&\'*+/:;<=>?@[\]{}^`|~'):
        return text.translate(string.maketrans("",""),punctstr)
    def removepunctuni(self,text,neglist=None):
        tbl=self.tbl
        if neglist:
            tbl = dict.fromkeys(i for i in xrange(sys.maxunicode)
                          if unicodedata.category(unichr(i)).startswith('P') and unichr(i) not in neglist )
        return text.translate(tbl)
    def text_handler(self,text,operation='encode'):
        if type(text)==str:
            return text
        else:
            try:
                if operation=='unidecode':
                    strtext=unidecode(text)
                    # print 'unidecoded'
                else:
                    strtext=text.encode('utf-8')
                    # print 'encoded'
            except:
                strtext=unidecode(text)
                # print 'unidecoded'
            return strtext
def test():
    processor=strpreproc()
    unitext=u'0015 Encapsulation by “spray” '
    strtext=processor.text_handler(unitext,'encode')
    print strtext
    print processor.removepunctstr(strtext)

    print unitext
    print processor.removepunctuni(unitext)

if __name__=='__main__':
    test()