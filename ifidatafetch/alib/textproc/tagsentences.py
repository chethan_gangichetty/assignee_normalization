import requests
import time
import sys
import codecs
s=requests.session()


from multiprocessing import Queue,Process

class taggeriter:
    def __init__(self,itr,host='localhost',port=2907):
        self.iterator=itr
        self.host=host
        self.port=port
    def __iter__(self):

        for text in self.iterator:
            yield tokenizeff(text,self.host,self.port)


import psycopg2
import requests,csv,unicodedata
csv.field_size_limit(sys.maxsize)


def tokenizeff(partext,hostname='localhost',port=2907):

#    partext=unicodedata.normalize('NFKD', partext.decode('UTF-8')).encode('ascii','ignore')
#     print type(partext)
#     print partext.encode('utf-8')
    r=s.post('http://'+hostname+':'+str(port)+'/solr/tag?overlaps=LONGEST_DOMINANT_RIGHT&tagsLimit=5000&wt=python&matchText=true&fl=id,term_stem,final_form',partext.encode('utf-8'))
    respdict=eval(r.text)
    # print respdict
    taglist=respdict['tags']
    docs=respdict['matchingDocs']['docs']
    iddict={}
    for doc in docs:
        tag='_'.join(doc['final_form'].split())
        iddict[doc['id']]=tag if type(tag)==str else tag.encode('utf-8')
    lastpos=0
    replacedtxt=[]
    for tags in taglist:
        start=tags[1]
        end=tags[3]

        reptext=iddict[tags[7][0]]

        # print text[lastpos:start]
        # print start
        # print end
        # print type(partext[lastpos:start]),partext[lastpos:start]
        # print type(reptext),reptext
        replacedtxt.append(partext[lastpos:start])
        replacedtxt.append(reptext)
        lastpos=end

    replacedtxt.append(partext[lastpos:])
    # print replacedtxt
    return ' '.join(replacedtxt)

def worker(id,num):

#    condest=psycopg2.connect("dbname='analytics' user='postgres' host='srch1'")
 #   curinst=condest.cursor()
    s = requests.Session()
    count=0
    with open('tagged'+str(id)+'.csv','wb')as outf:
        ow=csv.writer(outf)
        while True:
            rec=num.get()
            if  rec is None:
  #              curinst.close()
   #             condest.commit()
    #            condest.close()
                return
	    if not rec.strip():
		continue
            doctotag=rec
            tagged=tokenizeff(doctotag,hostname='unicorn',port=2906)
            print type(tagged)
            print tagged
            ow.writerow([tagged.encode('utf-8')])
            count+=1
            if count%50000==0:
                print'thread '+str(id)+': done insert records:'+str(count)
     #           condest.commit()
if __name__ == '__main__':
    start = 0
    size = 10000
    startt = time.time()
    countglb=0
    NUMBER_OF_PROCESSES = 1
    filename='fullcorpustest.txt'
    jobs = Queue(10000)
    counter=0
    p=[Process(target=worker, args=(i, jobs))
                for i in xrange(NUMBER_OF_PROCESSES)]
    for proc in p:
        proc.start()
    with codecs.open(filename,'r','utf-8')as inf:
        #cr=csv.reader(inf)
        for x in inf:
            doctotag=''
         #   doctotag=' dolceraseperator '.join(t if t else '' for t in x[1:])
            jobs.put(x)
            # print doctotag[450:470]
            #print 'put row in jobs done'
            #print x
            counter+=1
            countglb+=1
            while True:
                if jobs.empty():
                    counter=0
                    break
                if counter<10000:
                    break





    for w in xrange(NUMBER_OF_PROCESSES):
            jobs.put(None)


    endt = time.time()
    for w in xrange(NUMBER_OF_PROCESSES):
            jobs.put(None)
    for proc in p:
        proc.join()
    print (endt-startt)

