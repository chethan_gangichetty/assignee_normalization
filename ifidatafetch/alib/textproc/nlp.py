import csv,sys
from spacy.en import English
csv.field_size_limit(sys.maxsize)

'''
grammar = r"""
            NP: {<DT|PP\$>?<JJ|VBN|VBD>*<NN|NNS>*(<VBG><NN|NNS>)*}   # chunk determiner/possessive, adjectives and nouns
                {<NNP|NNPS>+}               # chunk sequences of proper nouns"""


grammar = r"""
                NP: {<NN|NNS>*}   # chunk determiner/possessive, adjectives and nouns
                    {<NNP|NNPS>+}
               """
'''
from collections import Counter

class pos:
    def __init__(self,depparse=False,entityrec=False):
        self.parser = English(parser=depparse,entity=entityrec)

    def getnounphrase(self,taggedtext):
        phraseset=set()
        phrase=[]
        phrasecount=Counter()
        for tok,tag in taggedtext:
            if tag[:2]=='NN' and tok.lower().strip() not in ['a','an','the']:
                phrase.append(tok.lower().strip())
            else:
                if len(phrase)>=2:
                    phraseset.add(' '.join(phrase))
                    phrasecount[' '.join(phrase)]+=1
                    phrase=[]
        return phraseset,phrasecount

    def tagpos(self,text):
        if type(text)==str:
            text=text.decode('utf-8')
        en_doc = self.parser(text)
        op= [(tok.text,self.parser.vocab.strings[tok.tag]) for tok in en_doc]
        print ' '.join([tok.text+'_'+self.parser.vocab.strings[tok.tag] for tok in en_doc])
        return op
if __name__=='__main__':
    text='''
    2	Approval of Agenda
R1-110610
Draft Agenda for RAN1#64 meeting	RAN1 Chairman
Matthew Baker (Chairman) proposed the agenda for the meeting and the plan (tentative schedule) of the week.
Decision: The agenda is approved.


        '''
    proc=pos()
    print proc.getnounphrase(proc.tagpos(text))