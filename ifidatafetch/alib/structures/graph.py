from __future__ import division
import psycopg2
from collections import defaultdict
import cPickle as pickle
import operator
import itertools
import re
import Levenshtein
from unidecode import unidecode

from pprint import pprint


class vertex:
    def __init__(self, node):
        self.id = node
        self.adjacent = {}

    def __str__(self):
        return str(self.id) + ' adjacent: ' + str([x.id for x in self.adjacent])

    def add_neighbor(self, neighbor, weight=0):
        self.adjacent[neighbor] = weight

    def get_connections(self):
        return self.adjacent.keys()

    def get_id(self):
        return self.id

    def get_weight(self, neighbor):
        return self.adjacent[neighbor]


class Graph(object):
    def __init__(self, graph_dict=None):
        """ initializes a graph object
            If no dictionary or None is given,
            an empty dictionary will be used
        """
        if graph_dict == None:
            graph_dict = {}
        self.__graph_dict = graph_dict
        self.__vertex_dict = {}
        self.__name_lookup = {}
        self.prune_proportion = 0.7

    def vertices(self):
        """ returns the vertices of a graph """
        return list(self.__graph_dict.keys())

    def edges(self):
        """ returns the edges of a graph """
        return self.__generate_edges()

    def add_vertex(self, vertex):
        """ If the vertex "vertex" is not in
            self.__graph_dict, a key "vertex" with an empty
            list as a value is added to the dictionary.
            Otherwise nothing has to be done.
        """
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = {}

    def add_edge(self, edge):
        """ assumes that edge is of type set, tuple or list;
            between two vertices can be multiple edges!
        """
        edge = set(edge)
        # print edge
        try:
            (vertex1, vertex2) = tuple(edge)
        except:
            print 'error adding edge'
            return

        self.__vertex_dict[vertex1] = self.__vertex_dict.get(vertex1, 0) + 1
        self.__vertex_dict[vertex2] = self.__vertex_dict.get(vertex2, 0) + 1
        if vertex1 in self.__graph_dict:
            self.__graph_dict[vertex1][vertex2] = self.__graph_dict[vertex1].get(vertex2, 0) + 1
        else:
            self.__graph_dict[vertex1] = {vertex2: 1}
        if vertex2 in self.__graph_dict:
            self.__graph_dict[vertex2][vertex1] = self.__graph_dict[vertex2].get(vertex1, 0) + 1
        else:
            self.__graph_dict[vertex2] = {vertex1: 1}

    def __generate_edges(self):
        """ A static method generating the edges of the
            graph "graph". Edges are represented as sets
            with one (a loop back to the vertex) or two
            vertices
        """
        edges = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if {neighbour, vertex} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def prune_edges(self):
        print "Pruning Edges now"
        for vertex in self.__graph_dict:
            vertex_overall_count = self.__vertex_dict[vertex]
            vertex_prune_count = self.prune_proportion * vertex_overall_count
            neighbours = []
            flag = 0
            for neighbour in self.__graph_dict[vertex]:
                if self.__graph_dict[vertex][neighbour] >= vertex_prune_count:
                    flag = 1
                    continue
                neighbours.append(neighbour)
            if flag == 1:
                for n in neighbours:
                    self.__graph_dict[vertex].pop(n, None)

    def __str__(self):
        res = "vertices: "
        for k in self.__graph_dict:
            res += str(k) + " "
        res += "\nedges: "
        for edge in self.__generate_edges():
            res += str(edge) + " "
        return res

    def getconnected(self, node):
        nexts = set()
        nexts.add(node)
        explored = set()
        results = set()
        counter = 0
        while True:
            counter = counter + 1
            newnext = set()
            if not nexts:
                break
            for next in nexts:
                if next in self.__graph_dict:

                    for n in self.__graph_dict[next]:
                        if n not in explored:
                            results.add(n)
                            newnext.add(n)
                    explored.add(next)

            nexts = newnext
        return results


import itertools

if __name__ == "__main__":

    graph = Graph()
    transactions = [(1, 2, 3),
                    (1, 2, 3),
                    (4, 2, 5),
                    (5, 6, 7),
                    ]
    for transaction in transactions:
        for a, c in itertools.combinations(transaction, 2):
            graph.add_edge({a, c})
    print graph
    print graph.getconnected(7)
