import psycopg2
from partiesparser import partiesparser
import networkx as nx
import csv, string

normdict = {}
tbl = {ord(c): u' ' for c in u'.-,;'}
from fuzzywuzzy import fuzz

with open('data/assigneenormnxp.csv', 'rU')as normfile:
    nr = csv.reader(normfile)
    for name, sug, parent in nr:
        normdict[name] = (sug, parent)


def getcurowners(id, reassignments, origassignee, normdict):
    # print reasgxml
    # print origassignee
    owners = []
    normowners = []
    assignors = []
    assgevents = []
    ownerconvpair = []
    # owner and conveyance information
    for asgor in origassignee:
        ownerconvpair.append([asgor, None, 'original assignee'])
        # appending original assignee into initial assignee set  with no conveyance information
        # this mapping will help in getting initial assignor as current assignee if all other reassignment are security agreement
    for date, reel, conv, asgnees, asgnors in reassignments:
        # sorting of reassignments primarily on execution date then secondary sort on reel number
        # print date,reel,conv,ET.tostring(assgevent)
        asgnors = [x['name'] for x in asgnors]
        asgnees = [x['name'] for x in asgnees]
        curassgnors = []
        curowners = []
        for assignor in asgnors:
            curassgnors.append(assignor)
            assignors.append(assignor)

            # append present assigors to assignors set
        print curassgnors, owners
        print assignors
        print 'Owners'
        print owners
        print 'assignors'
        print assignors
        print date
        print reel
        print conv
        for owner in owners:
            normowner = normdict.get(owner, [owner, owner])[1]
            # from list of previous owners remove owners which are member of assignors after current event.
            # remove matched assignors from current assignors.
            # for every reassigment entry one pair so after removing 1 entry we will break the removal process.
            for assignor in curassgnors:
                normassignor = normdict.get(assignor, [assignees, assignor])[1]
                try:
                    nassignor = ''.join(assignor.translate(tbl).split())
                except:
                    nassignor = ''.join(assignor.translate(string.maketrans('.,;-', '    ')).split())
                try:
                    nowner = ''.join(owner.translate(tbl).split())
                except:
                    nowner = ''.join(owner.translate(string.maketrans('.,;-', '    ')).split())
                if normowner == normassignor or assignor.lower().split(',')[0].find(
                        owner.lower().split(',')[0]) <> -1 or fuzz.token_set_ratio(owner,
                                                                                   assignor) >= 90 or nassignor.find(
                    nowner) <> -1:
                    print 'matched: ' + str(assignor.lower().split(',')[0]) + '  with : ' + str(
                        owner.lower().split(',')[0]) + ' score: ' + str(fuzz.partial_ratio(owner, assignor))
                    print 'removed: ' + str(owner) + ' from: owners ' + str(owners)
                    owners.remove(owner)

                    print 'removed: ' + str(assignor) + ' from: assignors ' + str(assignors)
                    curassgnors.remove(assignor)
                    break
        # append new owners to owner list
        for newowner in asgnees:
            print 'new owner', newowner
            curowners.append(newowner)
            if newowner in normowners or newowner.lower().split(',')[0] not in [own.lower().split(',')[0] for own in
                                                                                owners]:
                print newowner.lower().split(',')[0], ' not in ', [own.lower().split(',')[0] for own in owners]
                owners.append(newowner)
                normowners.append(normdict.get(newowner, [newowner, newowner])[1])
        for curowner in curowners:
            for curassgnor in curassgnors:
                ownerconvpair.append([curowner, curassgnor, conv])
                # append convayance information for backtracking
                # print 'new owner'
                # print owners
                # print '---'
    print ownerconvpair
    bowners = []
    print owners
    for owner in owners:
        # print owner
        replacement = getassignorifsecurity(owner, ownerconvpair, 0)

        if replacement:
            bowners.append(replacement)

    return id, bowners


def getassignorifsecurity(owner, ownerconvpair, callnum):
    pos = 0
    callnum += 1
    print 'called for name: ' + str(owner)
    # recursively get assignor if previous assigment if current assignee comes from security agreement
    for comp, prevcomp, convtext in reversed(ownerconvpair):
        pos += 1
        print 'assignee company: ' + str(comp)
        print 'assignor : ' + str(prevcomp)
        if owner == comp:
            if convtext:
                if convtext.lower().find('security') <> -1:
                    if ownerconvpair:
                        return getassignorifsecurity(prevcomp, ownerconvpair[:-pos], callnum)
                else:
                    return owner
            else:
                return owner
        else:
            return owner
    return []


if __name__ == '__main__':

    con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    cur = con.cursor()
    cur.execute(
        "select b.publication_id,content  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid = 'US-8560741-B2'")
    for id, partyxmltext in cur:
        assignees = []
        assignors = []
        normparties = {}
        parser = partiesparser(partyxmltext)
        parser.getpartiesdata(['inv', 'asg', 'appl', 'agent', 'examiner'])
        parser.getreasgdata()
        reassignments = parser.data['reassignments']
        origasg = [x['name'] for x in parser.data['asg']]
        print getcurowners(id, reassignments, origasg, normdict)
    cur.close()
    con.close()
