# -*- coding: utf-8 -*-
import psycopg2
import xml.etree.ElementTree as ET
import logging
import string
from fuzzywuzzy import fuzz
from pprint import pprint

from partiesparser import partiesparser

logging.basicConfig(level=logging.INFO)
from collections import defaultdict, OrderedDict, Counter
# import jellyfish
import sys

sys.path.append('/home/analytics/Developer/an')
from assignee_normalization import assigneeNormalization


class graph(object):
    def __init__(self):
        self.graph = {}
        self.nodes = set()

    def addedge(self, start, end):
        self.graph[start] = end
        self.nodes.add(start)
        self.nodes.add(end)

    def traverse(self):
        start = self.nodes.difference(set(self.graph.values()))
        print start, type(start)


class assigneecalc:
    def __init__(self):
        self.tbl = {ord(c): u' ' for c in u'.-,;'}
        self.conn = psycopg2.connect(
            "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
        self.cur = self.conn.cursor()
        self.parryparser = None
        self.namepref = ['epo', 'intermediate', 'original']
        self.asnorm = assigneeNormalization()

    def finalize(self):
        self.cur.close()
        self.conn.close()

    def replacenamesreasg(self,data):
        names = defaultdict(set)
        recs = []
        nameid = {}
        invapplasgsets = defaultdict(set)
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                recs += data[key]

            if key == 'reassignments':
                for rec in data[key]:
                    recs += rec[3] + rec[4]

        for rec in recs:
            found = False
            for name in names:
                normname = rec['name']
                try:
                    normname = self.asnorm.get_normalized_assignee(rec['name'])[1]
                except Exception, e:
                    print e
                isecho = False
                if normname == rec['name']:
                    isecho = True
                if (not isecho and name == normname) or (isecho and (fuzz.token_sort_ratio(name, rec[
                    'name']) >= 90 or ''.join(name.lower().split(' ')[0:2]) in ''.join(rec['name']).lower().split(' '))):  # and fuzz.partial_ratio(name,rec['name'])==90:
                    names[name].add(rec['name'])
                    found = True
                    break
            if not found:
                normname = rec['name']
                try:
                    normname = self.asnorm.get_normalized_assignee(rec['name'])[1]
                except Exception, e:
                    print e
                names[normname].add(rec['name'])
        for key in names:
            for variation in names[key]:
                nameid[variation] = key
        ## get normname here



        # for name in sorted(names):
        #     print name,names[name]
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                for rec in data[key]:
                    invapplasgsets[key].add(nameid[rec['name']])
        curowners = invapplasgsets['inv']
        # print curowners
        # calculate current asg
        reassignmentnew = {}
        n = 1
        prev=[]
        for date, reel, conv, asg, asn in reversed(data['reassignments']):
            asgnames = set([nameid[x['name']] for x in asg])
            asnnames = set([nameid[x['name']] for x in asn])
            if prev:
                pdate,pasg,pasn=prev
                prev = [date, asgnames, asnnames]
                if pdate==date and pasg==asgnames and pasn==asnnames:


                    continue

            reassignmentnew[n] = [date, reel, conv, asgnames,
                                  asnnames]
            prev = [date, asgnames, asnnames]
            n += 1
        return reassignmentnew


    def getcurowners(self, data):
        reassignmentnew=self.replacenamesreasg(data)

        chain = OrderedDict()

        for x in xrange(0, len(reassignmentnew)):
            todel = -1
            for k in sorted(reassignmentnew):
                date, reel, conv, asg, asn = reassignmentnew[k]
                # print date, reel, conv, asg, asn
                # print 'owners current',curowners
                common = set(asn).intersection(curowners)
                if not conv:
                    conv = 'dummy conveyance data'

                if len(common) > 0 and (
                        'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower())):
                    chain[k] = (date, reel, conv, asg, asn)
                    # print 'chain',date,reel,conv,asg,asn
                    todel = k
                    for kname in common:
                        curowners.remove(kname)

                    for kname in asg:
                        curowners.add(kname)

                    break
            if todel <> -1:
                del reassignmentnew[todel]

                # for key in  chain:
                # print key, chain[key]
        #by date for other transaction
        brokenowners = Counter()
        brokenassigners = Counter()
        for key in sorted(reassignmentnew):
            date, reel, conv, asg, asn = reassignmentnew[key]
            # print reassignmentnew[key]
            for asgname in asn:
                if asgname in brokenowners and brokenowners[asgname] > 0:
                    brokenowners[asgname] -= 1
            if 'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower()):
                # print 'added'
                brokenowners.update(asg)
                # else:
                #     # print 'skipped'
        ownersfinal = curowners.difference(invapplasgsets['inv'])
        return '|'.join(brokenowners), '|'.join(ownersfinal)

    def getreasgfrompartiesxml(self, id, partiesxml):
        print partiesxml
        parties = ET.fromstring(partiesxml)
        pubid = id
        reassignments = []
        # pprint(parties)
        assgevents = []
        for reassignment in parties.findall('./assignee-history/reassignments/'):
            dates = []
            assignors = []
            assignees = []
            for x in reassignment.findall('./assignors/assignor'):
                if 'execution-date' in x.attrib:
                    dates.append(x.attrib['execution-date'])
            assignors = [name.text for name in reassignment.findall('./assignors/assignor//last-name')]
            assignees = [name.text for name in reassignment.findall('./assignees//last-name')]

            # in one reassignment many assignor can be present but execution date of that event is effective from earliest date mentioned with assignors
            # H"ere we are taking min(date) present with assignor as date of execution
            conv = ''
            try:
                conv = (reassignment.findall('.//conveyance'))[0].text
            except:
                conv = ''
            reel = reassignment.attrib['reel-frame'].strip()
            if dates:
                assgevents.append((id, min(dates), reel,
                                   conv, assignors, assignees, ET.tostring(reassignment)))
            else:
                assgevents.append(
                    (id, None, reel, conv, assignors, assignees, ET.tostring(reassignment)))
                # execution date is none if not dates are mentioned
        return assgevents

    def parsereasgxml(self, id, reasgxml, origassignee):
        # print reasgxml
        # print origassignee
        reassignments = ET.fromstring(reasgxml)
        owners = []
        assignors = []
        assgevents = []
        ownerconvpair = []
        # owner and conveyance information
        for asgor in origassignee:
            ownerconvpair.append([asgor, None, 'original assignee'])
            # appending original assignee into initial assignee set  with no conveyance information
            # this mapping will help in getting initial assignor as current assignee if all other reassignment are security agreement
        for reassignment in reassignments:
            dates = []
            for x in reassignment.findall('./assignors/assignor'):
                if 'execution-date' in x.attrib:
                    dates.append(int(x.attrib['execution-date']))
            # in one reassignment many assignor can be present but execution date of that event is effective from earliest date mentioned with assignors
            # H"ere we are taking min(date) present with assignor as date of execution
            if dates:
                assgevents.append((min(dates), reassignment.attrib['reel-frame'].strip(),
                                   reassignment.findall('.//conveyance'), reassignment))
            else:
                assgevents.append(
                    (None, reassignment.attrib['reel-frame'], reassignment.findall('.//conveyance'), reassignment))
                # execution date is none if not dates are mentioned

                # print 'xxxx'
        for date, reel, conv, assgevent in sorted(assgevents, key=lambda x: (x[0], x[1])):
            # sorting of reassignments primarily on execution date then secondary sort on reel number
            curassgnors = []
            curowners = []
            for assignor in assgevent.findall('./assignors//last-name'):
                curassgnors.append(assignor.text)
                assignors.append(assignor.text)

                # append present assigors to assignors set

            # print 'Owners'
            # print owners
            # print 'assignors'
            # print assignors
            # print date
            # print reel
            # print conv[0].text
            for owner in owners:
                # from list of previous owners remove owners which are member of assignors after current event.
                # remove matched assignors from current assignors.
                # for every reassigment entry one pair so after removing 1 entry we will break the removal process.
                for assignor in assignors:
                    try:
                        nassignor = ''.join(assignor.translate(self.self.tbl).split())
                    except:
                        try:
                            nassignor = ''.join(assignor.translate(string.maketrans('.,;-', '    ')).split())
                        except:
                            nassignor = ' '.join(assignor.replace(',', ' ').split())
                    try:
                        nowner = ''.join(owner.translate(self.self.tbl).split())
                    except:
                        nowner = ''.join(owner.translate(string.maketrans('.,;-', '    ')).split())
                    if assignor.lower().split(',')[0].find(owner.lower().split(',')[0]) <> -1 or fuzz.token_set_ratio(
                            owner,
                            assignor) >= 90 or nassignor.find(
                        nowner) <> -1:
                        # print 'matched: '+str(assignor.lower().split(',')[0])+'  with : '+str(owner.lower().split(',')[0])+' score: '+str(fuzz.partial_ratio(owner,assignor))
                        # print 'removed: '+str(owner)+' from: owners '+str(owners)
                        owners.remove(owner)

                        # print 'removed: '+str(assignor)+' from: assignors '+str(assignors)
                        assignors.remove(assignor)
                        break
            # append new owners to owner list
            for newowner in assgevent.findall('./assignees//last-name'):
                curowners.append(newowner.text)
                if newowner.text.lower().split(',')[0] not in [own.lower().split(',')[0] for own in owners]:
                    owners.append(newowner.text)
            for curowner in curowners:
                for curassgnor in curassgnors:
                    ownerconvpair.append([curowner, curassgnor, conv[0].text])
                    # append convayance information for backtracking
                    # print 'new owner'
                    # print owners
                    # print '---'
        bowners = []
        for owner in owners:
            # print owner
            replacement = self.getassignorifsecurity(owner, ownerconvpair, 0)

            if replacement:
                bowners.append(replacement)

        return bowners

    def getassignorifsecurity(self, owner, ownerconvpair, callnum):
        pos = 0
        callnum += 1
        # print 'called for name: '+str(owner)
        # recursively get assignor if previous assigment if current assignee comes from security agreement
        for comp, prevcomp, convtext in reversed(ownerconvpair):
            pos += 1
            # print 'assignee company: '+str(comp)
            # print 'assignor : '+str(prevcomp)
            if owner == comp:
                if convtext:
                    if convtext.lower().find('secur') <> -1:
                        if ownerconvpair:
                            return self.getassignorifsecurity(prevcomp, ownerconvpair[:-pos], callnum)
                    else:
                        return owner
                else:
                    return owner
        return []

    def workercurasg_reasg(self, id, num):
        condest = psycopg2.connect(
            "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
        curinst = condest.cursor()
        count = 0

        while True:
            rec = num.get()
            if rec is None:
                curinst.close()
                condest.commit()
                condest.close()
                return
            publication_id, reasgxml, origasg = rec
            if reasgxml:
                curasg = self.parsereasgxml(publication_id, reasgxml, origasg)
                curinst.execute('insert into public.reassignment_current_assignees values(%s,%s)',
                                (publication_id, curasg))
                count += 1
            if count % 10000 == 0:
                logging.info('thread ' + str(id) + ': done insert records:' + str(count))
                condest.commit()

    def getcurrentasg(self, pnlist):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()
                owners, extraowners = self.getcurowners(self.parryparser.data)
                res = [rec[0], owners, extraowners]
                recs.append(res)
                # print res
        # print res
        return recs

    def getnormalizedlist(self, pnlist):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        namenorm = {}
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                namerec = []
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()

                data = self.parryparser.data
                for key in data:

                    if key in ['inv', 'appl', 'asg']:
                        namerec += data[key]

                    if key == 'reassignments':
                        for recd in data[key]:
                            namerec += recd[3] + recd[4]
                for nrec in namerec:
                    # print nrec
                    name, sug, parent = self.asnorm.get_normalized_assignee(nrec['name'])
                    recs.append([rec[0], name, sug, parent])

                    # print res
        # print res
        return recs


import time
from multiprocessing import Queue, Process
#
# if __name__ == '__main__':
#     start = 0
#     size = 10000
#     startt = time.time()
#     countglb = 0
#     conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
#
#     cur = conn.cursor('iteratornew4')
#     cur.itersize = 10000
#     NUMBER_OF_PROCESSES = 20
#
#     jobs = Queue(10000)
#     p = [Process(target=workercurasg_reasg, args=(i, jobs))
#          for i in xrange(NUMBER_OF_PROCESSES)]
#     for proc in p:
#         proc.start()
#
#     sth = (
#     "select publication_id,(xpath(\'/parties/assignee-history/reassignments\',content))[1] reasgxml,CAST(xpath(\'/parties/assignees/assignee/addressbook/name/text()\',content) AS TEXT) :: TEXT[] orgiassignee from xml.t_parties")
#     cur.execute(sth)
#
#     counter = 0
#     for x in cur:
#         if x[1]:
#             jobs.put(x)
#             counter += 1
#         countglb += 1
#         while True:
#             if jobs.empty():
#                 counter = 0
#                 break
#             if counter < 10000:
#                 break
#     logging.info('total input:' + str(countglb))
#
#     for w in xrange(NUMBER_OF_PROCESSES):
#         jobs.put(None)
#
#     endt = time.time()
#     cur.close()
#     conn.commit()
#     conn.close()
#     for proc in p:
#         proc.join()
#     logging.info(endt - startt)
import unicodecsv as  csv

if __name__ == '__main__':
    # g=graph()
    # g.addedge(1,2)
    # g.addedge(2,3)
    # g.traverse()
    ca = assigneecalc()
    with open('1237.csv')as inf:
        with open('1237normasg.csv', 'wb')as outf:
            cr = csv.reader(inf)
            cw = csv.writer(outf)
            pnlist = []
            incount = 0
            for pn, in cr:
                incount += 1
                pnlist.append(pn)
            # pnlist=['US-7842936-B2', 'US-RE45552-E1', 'US-8648318-B2']
            # print pnlist

            outcount = 0
            for x in xrange(0, len(pnlist), 100):
                r = ca.getnormalizedlist(pnlist[x:x + 100])
                for pn, asg, sug, norm in r:
                    # print pn

                    cw.writerow([pn, asg, sug, norm])
                    outcount += 1
                    # for pn,assignee,extraasg in r:
                    #     print pn
                    #
                    #     cw.writerow([pn,assignee,extraasg])
                    #     outcount+=1
    # break
    # ca.cur.execute("select b.publication_id,content  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid = 'US-6715055-B1'")
    # for id,partiesxml in ca.cur:
    #     print ca.getcurrentasg(id,partiesxml)
    # print ca.getcurrentasg(['US-6715055-B1'])
    # print ca.getcurrentasg(['US-6972255-B2'])
    # print ca.getcurrentasg(['US-8560741-B2'])
    # print ca.getcurrentasg(['US-8468247-B1'])
    # print ca.getcurrentasg(['US-20020077984-A1'])
    #
    ca.finalize()
