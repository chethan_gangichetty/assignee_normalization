import requests
import lxml.html
import time
class getcurrentasgoogle(object):
    def __init__(self,session=None):
        if not session:
            self.session=requests.session()
        else:
            self.session=session

        self.results={}
    def getgooglepatentpage(self,pn):
        url='https://patents.google.com/patent/'+pn.replace('-','')+'/en'
        resp=self.session.get(url)
        tree=lxml.html.fromstring(resp.text)
        return tree
    def getstatus(self,tree):
        status=tree.xpath('//*[@itemprop="status"]/text()')
        return status
    def getcurowners(self,tree):
        curowners=[x.strip() for x in tree.xpath('//*[@itemprop="assigneeCurrent"]/text()')]
        return curowners
    def process(self,pniter):

        for pn in pniter:

            if pn not in self.results:
                res={}
                tree=self.getgooglepatentpage(pn)
                res['status']=self.getstatus(tree)
                res['curent owners']=self.getcurowners(tree)
                res['pn']=pn

                self.results[pn]=res
                time.sleep(2)
                yield res
            else:
                print 'cached version',pn
                yield self.results[pn]
        return
import csv
def csvgenerator(reciter,col=0):
    for rec in reciter:
        yield rec[col]

if __name__=='__main__':
    sess=requests.session()
    fetcher=getcurrentasgoogle(sess)
    with open('1237.csv')as inf:
        with open('1237googlepatents.csv', 'wb')as outf:
            cr = csv.reader(inf)
            cw = csv.writer(outf)
            pnlist = []
            incount = 0
            for res in fetcher.process(csvgenerator(cr)):
                print res['pn']
                cw.writerow([res['pn'],res['curent owners'],res['status']])
