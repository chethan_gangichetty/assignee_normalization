import re
from collections import OrderedDict


class titoifi:
    regexlib = None

    def auruleA1(self, match):
        yyyy = match.group(1)
        NNNNN = match.group(2)
        kind = match.group(3)
        return ['AU' + '-' + str(int(NNNNN)) + str(yyyy[2:]) + '-' + kind]

    def auruleA2(self, match):
        yyyy = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)
        return ['AU-' + yyyy + nnnnnn + '-' + kind]

    def auruleB1(self, match):  # modified
        nnnnnn = match.group(1)
        kind = match.group(2)
        if kind == 'B':
            return ['AU-' + nnnnnn + '-' + kind, 'AU-' + nnnnnn + '-B2', 'AU-' + nnnnnn + '-B1']
        else:
            return ['AU-' + nnnnnn + '-' + kind]

    def auruleB2(self, match):
        yyyy = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)

        return ['AU-' + yyyy + nnnnnn + '-' + kind]

    def jpruleA1(self, match):
        n = match.group(1)
        if int(n[0:4]) > 2011:
            return ['JP-' + match.group(1) + '-' + match.group(2), 'JP-WO' + match.group(1) + '-' + match.group(2)]
        else:
            return ['JP-' + match.group(1) + '-' + match.group(2)]

    def jpruleA(self, match):  # modified
        XX = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)
        ##        print XX
        ##        print nnnnnn[0:2]
        ##        print kind
        ##
        if XX == '1' and nnnnnn[0:2] == '00' and kind == 'A':
            return ['JP-S64' + str(int(nnnnnn)) + '-' + kind]
        if XX == '1' and kind == 'A':
            return ['JP-S64' + str(int(nnnnnn)) + '-' + kind,
                    'JP-H' + ('0' + str(XX))[-2:] + str(int(nnnnnn)) + '-' + kind]
        if len(XX) == 1:
            return ['JP-H' + ('0' + str(XX))[-2:] + str(int(nnnnnn)) + '-' + kind]
        if len(XX) == 2:
            if int(XX) in [10, 11]:
                return ['JP-H' + XX + str(int(nnnnnn)) + '-' + kind]
            else:
                return ['JP-S' + XX + str(int(nnnnnn)) + '-' + kind]

    def jpruleB1(self, match):  # modified
        yy = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)
        ey = None

        if int(yy) > 10:
            if kind == 'B1':
                return ['JP-' + yy + nnnnnn + '-B']
            else:
                if int(yy) >= 89:
                    ey = 'H' + ('00' + str(int(yy) - 88))[-2:]
                    return ['JP-' + ey + str(int(nnnnnn)) + '-' + kind]
                elif int(yy) < 88:
                    if kind == 'B2':
                        return ['JP-S' + yy + str(int(nnnnnn)) + '-' + kind]
                    else:
                        ey = 'S' + str(int(yy) - 25)
                        return ['JP-' + ey + str(int(nnnnnn)) + '-' + kind]
        else:
            return ['JP-' + str(int(yy)) + nnnnnn + '-' + kind]

    def jpruleK1(self, match):  # modified
        yy = match.group(1)
        n = match.group(2)
        kind = match.group(3)
        if int(yy) < 12:
            return ['JP-H' + yy + n + '-' + kind]
        else:
            return ['JP-S' + yy + n + '-' + kind]

    def jpruleB12(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        # print kind
        # print nnnnnn
        if kind == 'B2' or kind == 'B1' and nnnnnn[1] == '0':
            return ['JP-H0' + nnnnnn[0] + str(int(nnnnnn[1:])) + '-' + kind]
        else:
            return ['JP-' + nnnnnn + '-' + kind]

    def jpruleU12(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        # print kind
        # print nnnnnn
        return ['JP-' + nnnnnn + '-' + kind]

    def jprule0(self, match):  # added
        pn = match.group(1)
        kind = match.group(2)
        return ['JP-' + pn + '-' + kind]

    def jpruleU1(self, match):
        nnnnnnn = match.group(1)
        kind = match.group(2)

    def jprulepct1(self, match):
        xx = match.group(1)
        nnnnnn = match.group(2)

        kind = match.group(3)
        kind = kind.replace('X', 'A')
        if kind[0] == 'Y':
            return ['JP-S' + xx + str(int(nnnnnn)) + '-' + kind]
        else:
            return ['JP-S' + xx + nnnnnn + '-' + kind]

    def usrule1(self, match):
        cc = match.group(1)
        n = match.group(2)
        kind = match.group(3)
        if cc == 'US' and kind == 'E1':
            return [cc + '-' + n + '-' + kind, cc + '-' + n + '-E']
        elif cc == 'CA' and kind == 'A1':
            return [cc + '-' + n + '-' + kind, cc + '-' + n + '-A']
        elif cc == 'US' and kind == 'S1':
            return [cc + '-' + n + '-' + kind, cc + '-' + n + '-S']
        else:
            if cc == 'JP':
                kind = kind.replace('X', 'A')
            return [cc + '-' + n + '-' + kind]

    def deruleU2(self, match):
        yyyy = match.group(1)
        n = match.group(2)
        kind = match.group(3)
        return ['DE-' + yyyy[-2:] + n + '-' + kind]

    def derulet(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['DE-0' + n + '-' + kind, 'DE-' + n + '-' + kind]

    def derulet2(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['DE-00' + n + '-' + kind, 'DE-' + n + '-' + kind]

    def deTtoD2(self, match):
        n = match.group(1)
        kind = match.group(2)
        if kind == 'T':
            return ['DE-' + n + '-D2']
        elif kind == 'T1' and len(n) == 6:
            return ['DE-00' + n + '-T1', 'DE-' + n + '-' + kind]
        else:
            return ['DE-' + n + '-' + kind]

    def eprule(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['EP-' + ('000000' + n)[-7:] + '-' + kind]

    def worule1(self, match):
        yy = match.group(1)
        n = match.group(2)
        kind = match.group(3)

        return ['WO-' + yy + n + '-' + kind]

    def krruleA1(self, match):
        yyyy = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 1998:
            return ['KR-' + yyyy[-2:] + ('0000000' + nnnnnn)[-7:] + '-' + kind]
        # if int(yyyy)>2003:
        #     if kind.find('A')<>-1:
        #         return ['KR-'+'10'+yyyy+('0000000'+nnnnnn)[-7:]+'-'+kind]
        #     if kind.find('U')<>-1:
        #         return ['KR-'+'20'+yyyy+('0000000'+nnnnnn)[-7:]+'-'+kind]
        if int(yyyy) > 1998:
            return ['KR-' + yyyy + ('0000000' + nnnnnn)[-7:] + '-' + kind]
        if int(yyyy) == 1998:
            return ['KR-' + yyyy[-2:] + ('0000000' + nnnnnn)[-7:] + '-' + kind,
                    'KR-' + yyyy + ('0000000' + nnnnnn)[-7:] + '-' + kind]

    def krruleB1(self, match):
        yyyy = match.group(1)
        nnnnn = match.group(2)
        kind = match.group(3)
        if yyyy[0:2] == '10':
            return ['KR-' + yyyy + nnnnn + '-' + kind]
        else:
            return ['KR-' + yyyy[-2:] + ('0000000' + nnnnn)[-7:] + '-' + kind]

    def krruleB2(self, match):
        n = match.group(1)
        kind = match.group(2)
        cc = ''
        if kind.find('B') <> -1:
            cc = '10'
        if kind.find('Y') <> -1:
            cc = '20'
        if kind == 'B':
            return ['KR-' + ('0000000' + n)[-7:] + '-' + kind, 'KR-' + cc + ('0000000' + n)[-7:] + '-' + kind,
                    'KR-' + n + '-' + kind, 'KR-' + cc + ('0000000' + n)[-7:] + '-B1']
        else:
            return ['KR-' + ('0000000' + n)[-7:] + '-' + kind, 'KR-' + cc + ('0000000' + n)[-7:] + '-' + kind,
                    'KR-' + n + '-' + kind]

    def ESruleABC(self, match):
        yyyy = match.group(1)
        NNNNN = match.group(2)
        kind = match.group(3)
        return ['ES' + '-' + str(yyyy[2:]) + str(NNNNN) + '-' + kind]

    def ESruleABC2(self, match):
        nnnnnnn = match.group(1)
        kind = match.group(2)
        return ['ES-' + nnnnnnn + '-' + kind]

    def TWruleA(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['TW-' + nnnnnn + '-' + kind]

    def TWruleA2(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['TW-' + nnnnnn + '-' + kind]

    def TWruleB(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['TW-I' + nnnnnn + '-' + kind]

    def TWruleU(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['TW-M' + nnnnnn + '-' + kind]

    def SUruleABC(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['SU-' + nnnnnn + '-' + kind]

    def ATruleA(self, match):
        yyyy = match.group(1)
        NNNNN = match.group(2)
        kind = match.group(3)
        if int(yyyy) >= 2000:
            return ['AT-A' + str(int(NNNNN)) + yyyy + '-' + kind]
        else:
            return ['AT-A' + str(int(NNNNN)) + str(yyyy[2:]) + '-' + kind]

    def ATruleA2(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['AT-' + nnnnnn + '-' + kind]

    def ATruleU(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['AT-' + nnnnnn + '-' + kind]

    def SEruleA(self, match):
        yyyy = match.group(1)
        NNNNN = match.group(2)
        kind = match.group(3)
        return ['SE' + '-' + str(yyyy[2:]) + str(NNNNN) + '-' + kind]

    def SEruleBC(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['SE-' + nnnnnn + '-' + kind]

    def RUruleA(self, match):
        yyyy = match.group(1)
        nnnnnn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2000:
            return ['RU-' + yyyy[-2:] + nnnnnn + '-' + kind]
        else:
            return ['RU-' + yyyy + nnnnnn + '-' + kind]

    def RUruleC(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['RU-' + nnnnnn + '-' + kind]

    def RUruleU(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['RU-' + nnnnnn + '-' + kind]

    def ITruleB(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        if kind == 'B':
            return ['IT-' + nnnnnn + '-' + kind, 'IT-' + nnnnnn + '-B1']
        else:
            return ['IT-' + nnnnnn + '-' + kind]

    def CHrule(self, match):
        nnnnnn = match.group(1)
        kind = match.group(2)
        return ['CH-' + nnnnnn + '-' + kind]

    def cnrule(self, match):
        yyyy = match.group(1)
        xn = match.group(2)
        kind = match.group(3)

        return ['CN-' + yyyy[-2:] + xn + '-' + kind]

    def cnrule2(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['CN-' + pn + '-' + kind]

    def auruled0(self, match):  # modified
        yyyy = match.group(1)
        cc = match.group(2)
        kind = match.group(4)
        n = match.group(3)
        ##        y=''
        ##        if int(yyyy)<2000:
        ##            y=yyyy[-2:]
        ##        else:
        ##            y=yyyy
        return ['AU-' + cc + n + yyyy[-2:] + '-' + kind]

    def arrule(self, match):
        pn = match.group(1)
        kind = match.group(2)
        if len(pn) < 6:
            return ['AR-' + ('000000' + pn)[-6:] + '-' + kind]
        return ['AR-' + pn + '-' + kind]

    def brruleA(self, match):  # modified
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2004:
            return ['BR-' + yyyy[-2:] + pn + '-' + kind]
        else:
            return ['BR-PI' + yyyy[-2:] + pn + '-' + kind]

    def brruleA2(self, match):
        typecode = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        return ['BR-' + typecode + yyyy + pn + '-' + kind]

    def brruleA1(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        return ['BR-PI' + yyyy + pn + '-' + kind]

    def czruleA(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2000:
            return ['CZ-' + yyyy[-2:] + pn + '-' + kind]

        else:
            return ['CZ-' + yyyy + pn[-4:] + '-' + kind]  # modified

    def czruleB(self, match):

        pn = match.group(1)
        kind = match.group(2)
        return ['CZ-' + pn + '-' + kind]

    def csruleA(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        return ['CS-' + yyyy[-2:] + pn + '-' + kind]

    def csruleB(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['CS-' + pn + '-' + kind]

    def dkruleA(self, match):  # modified
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 1900:
            return ['DK-' + yyyy + str(int(pn)) + '-' + kind]
        elif int(yyyy) < 2000:
            return ['DK-' + str(int(pn)) + yyyy[-2:] + '-' + kind]
        else:
            return ['DK-' + yyyy + pn + '-' + kind]

    def dkruleB(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['DK-' + pn + '-' + kind]

    def dkrulet(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['DK-0' + pn + '-' + kind]

    def firuleA(self, match):
        y = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        return ['FI-' + y + pn[-4:] + '-' + kind]

    def firuleA2(self, match):
        y = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(y) < 2000:
            return ['FI-' + y[-2:] + pn[-4:] + '-' + kind]
        else:
            return ['FI-' + y + pn[-4:] + '-' + kind]

    def firuleB(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['FI-' + pn + '-' + kind]

    def itruleAmid(self, match):  # modified
        y = match.group(1)
        code = match.group(2)
        num = match.group(3)
        kind = match.group(4)
        if int(y) < 2000:
            return ['IT-' + code + y[-2:] + num[-4:] + '-' + kind]
        else:
            return ['IT-' + code + y + num[-4:] + '-' + kind]

    def NOruleD(self, match):
        cc = match.group(1)
        y = match.group(2)
        num = match.group(3)
        kind = match.group(4)
        if int(y) < 2000:
            return [cc + '-' + y[-2:] + num[-4:] + '-' + kind]
        else:
            return [cc + '-' + y + num[-4:] + '-' + kind]

    def brrulemid(self, match):  # modified
        code = match.group(1)
        num = match.group(2)
        kind = match.group(3)
        return ['BR-' + code + num + '-' + kind, 'BR-' + num + '-' + kind]

    def gbruled(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2010:
            return ['GB-' + yyyy[-2:] + pn + '-' + kind]
        else:
            return ['GB-' + yyyy + pn + '-' + kind]

    def NLrulec6(self, match):
        pn = match.group(1)
        return ['NL-' + pn + '-C1']

    def twodigityear(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        if cc == 'MX' and int(yyyy) < 2000 and int(yyyy) > 1990:
            return [cc + '-PA' + yyyy[-2:] + '0' + pn + '-' + kind, cc + '-' + yyyy[-2:] + pn + '-' + kind]
        return [cc + '-' + yyyy[-2:] + pn + '-' + kind]

    def yybefore2k(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        if int(yyyy) < 2000:
            return [cc + '-' + yyyy[-2:] + pn + '-' + kind]
        else:
            return [cc + '-' + yyyy + pn + '-' + kind]

    def HUruleD(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        if int(yyyy) < 1991:
            return [cc + '-' + yyyy[-2:] + pn[-4:] + '-' + kind]
        elif int(yyyy) == 1991:
            return [cc + '-' + yyyy[-2:] + pn[-4:] + '-' + kind, cc + '-' + yyyy[-2:] + pn + '-' + kind]
        else:
            return [cc + '-' + yyyy[-2:] + pn + '-' + kind]

    def MXruleA(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2007:
            return ['MX-PA' + yyyy[-2:] + pn + '-' + kind]
        else:
            return ['MX-' + yyyy + pn + '-' + kind]

    def HUruleT(self, match):
        pn = match.group(1)
        return ['HU-T' + pn + '-A']

    def yymidrule(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        mid = match.group(3)
        pn = match.group(4)
        kind = match.group(5)
        return [cc + '-' + mid + yyyy[-2:] + pn + '-' + kind]

    def PTruleT(self, match):
        pn = match.group(1)
        return ['PT-' + pn + '-E']

    def INrule(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        kind = match.group(3)
        if kind == "I1":
            return ['IN-' + str(int(pn)) + "DE" + yyyy + '-A']
        elif kind == "I2":
            if int(yyyy) < 2003:
                return ['IN-' + str(int(pn)) + "CA" + yyyy + '-A']
            else:
                return ['IN-' + str(int(pn)) + "KO" + yyyy + '-A']
        elif kind == "I3":
            return ['IN-' + str(int(pn)) + "MU" + yyyy + '-A']
        elif kind == "I4":
            ##            if len(str(int(pn)))==3:
            ##                return ['IN-'+str(int(pn))+"CA"+yyyy+'-A']
            ##            else:
            return ['IN-' + str(int(pn)) + "CH" + yyyy + '-A']
        elif kind == "P1":
            return ['IN-' + str(int(pn)) + "DEN" + yyyy + '-A']
        elif kind == "P2":
            return ['IN-' + str(int(pn)) + "KON" + yyyy + '-A']
        elif kind == "P3":
            return ['IN-' + str(int(pn)) + "MUN" + yyyy + '-A']
        elif kind == "P4":
            if yyyy == "2010":
                return ['IN-' + str(int(pn)) + "CHN" + yyyy + '-A', 'IN-' + yyyy + "CN" + pn + '-A']
            else:
                return ['IN-' + str(int(pn)) + "CHN" + yyyy + '-A']

    def INmidrule(self, match):
        yyyy = match.group(3)
        mid = match.group(2)
        pn = match.group(1)
        kind = match.group(4)
        return ['IN-' + pn + mid + yyyy + '-' + kind]

    def iesrule(self, match):
        pn = match.group(1)
        kind = match.group(2)
        return ['IE-S' + pn + '-' + kind]

    def yyatend(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        return [cc + '-' + pn + yyyy[-2:] + '-' + kind]

    def yyatend2(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        return [cc + '-' + str(int(pn)) + yyyy[-2:] + '-' + kind]

    def yearatend3(self, match):
        cc = match.group(1)
        yyyy = match.group(2)
        pn = match.group(3)
        kind = match.group(4)
        if int(yyyy) < 2000:
            return [cc + '-' + str(int(pn)) + yyyy[-2:] + '-' + kind]
        else:
            return [cc + '-' + pn + yyyy + '-' + kind]

    def yuA2U(self, match):
        y = match.group(1)
        pn = match.group(2)
        return ['YU-' + pn + y[-2:] + '-U']

    def SGkindA2G(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        return ['SG-' + str(int(pn)) + yyyy[-2:] + '-G']

    def SGA1trimmed(self, match):
        yyyy = match.group(1)
        pn = match.group(2)
        return ['SG-' + yyyy + pn + '1-A', 'SG-' + yyyy + pn + '2-A', 'SG-' + yyyy + pn + '3-A',
                'SG-' + yyyy + pn + '4-A', 'SG-' + yyyy + pn + '5-A', 'SG-' + yyyy + pn + '6-A',
                'SG-' + yyyy + pn + '7-A', 'SG-' + yyyy + pn + '8-A', 'SG-' + yyyy + pn + '9-A',
                'SG-' + yyyy + pn + '0-A', ]

    def SGkindA1toA(self, match):
        pn = match.group(1)
        return ['SG-' + pn + '-A', 'SG-' + pn + 'Q-A', 'SG-' + pn + 'T-A', 'SG-' + pn + 'X-A', 'SG-' + pn + 'Y-A',
                'SG-' + pn + 'S-A', 'SG-' + pn + 'V-A', 'SG-' + pn + 'W-A', 'SG-' + pn + 'P-A', 'SG-' + pn + 'U-A',
                'SG-' + pn + 'R-A']

    def skruleA(self, match):
        y = match.group(1)
        n = match.group(2)
        if n[-2:] == '20' and n[0:2] == '00':
            return ['SK-' + str(int(n)) + y + '-A3']
        if n[-2:] == '20' and n[0] == '0':
            return ['SK-' + str(int(n)) + y + '-A3', 'SK-' + str(int(n)) + y[-2:] + '-A3']
        if int(y) < 2000 or n[-2:] == '20':
            return ['SK-' + str(int(n)) + y[-2:] + '-A3']
        else:
            return ['SK-' + str(int(n)) + y + '-A3']

    def skruleA2(self, match):
        y = match.group(1)
        n = match.group(2)
        return ['SK-' + str(int(n)) + y[-2:] + '-A3']

    def EAruleB(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['EA-' + ('000000' + n)[-6:] + '-' + kind]

    def EAruleA(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['EA-' + n + '-' + kind]

    def CLruleA(self, match):

        yyyy = match.group(1)
        n = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2000:
            return ['CL-' + n + yyyy + '-' + kind]
        else:
            return ['CL-' + yyyy + n + '-' + kind]

    def hrprule(self, match):
        yyyy = match.group(1)
        n = match.group(2)
        kind = match.group(3)
        if int(yyyy) < 2000:
            return ['HR-P' + yyyy[-2:] + n + '-' + kind]
        else:
            return ['HR-P' + yyyy + n + '-' + kind]

    def hrmidrule(self, match):
        mid = match.group(1)
        yyyy = match.group(2)
        n = match.group(3)
        kind = match.group(4)
        if int(yyyy) < 2000:
            return ['HR-' + mid + yyyy[-2:] + n + '-' + kind]
        else:
            return ['HR-' + mid + yyyy + n + '-' + kind]

    def SIruleT(self, match):
        n = match.group(1)
        kind = match.group(2)
        return ['SI-' + n + '-' + kind, 'SI-0' + n + '-' + kind]

    def usruleh1(self, match):
        n = match.group(1)
        return ['US-H' + n + '-H1', 'US-H' + n + '-H']

    def __init__(self):
        self.regexlib = OrderedDict()
        regexlist = [['AR([0-9]+)([A-Z][0-9]{0,1})', self.arrule],
                     ['AU([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.auruleA1],
                     ['AU([0-9]{4})([0-9]{6})(A[0-9]{0,1})', self.auruleA2],
                     ['AU([0-9]{6})(B[0-9]{0,1}|C[0-9]{0,1})', self.auruleB1],
                     ['AU([0-9]{4})([0-9]{6})(B[0-9]{0,1}|C[0-9]{0,1})', self.auruleB2],
                     ['AU([0-9]{4})([A-Z]{2})([0-9]{4})(D[0-9]{0,1})', self.auruled0],  # modified
                     ['AT([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.ATruleA],
                     ['AT([0-9]{6})(A[0-9]{0,1}|B[0-9]{0,1}|T[0-9]{0,1})', self.ATruleA2],
                     ['AT([0-9]{4,5})(U[0-9]{0,1})', self.ATruleU],
                     ['BR([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.brruleA],
                     ##        ['BRPI([0-9]{2})([0-9]{5})(A[0-9]{0,1}|B[0-9]{0,1})',self. brruleA1],
                     ['BR([0-9]{2})([0-9]{4})([0-9]{6})(A[0-9]{0,1})', self.brruleA2],
                     ['CN([0-9]{4})([0-9]{6})([A-Z][0-9]{0,1})', self.cnrule],
                     ['CN([0-9]{7,9})([A-Z][0-9]{0,1})', self.cnrule2],
                     ['CZ([0-9]{4})([0-9]+)(A[0-9]{0,1})', self.czruleA],
                     ['CZ([0-9]+)(B[0-9]{0,1})', self.czruleB],
                     ['CS([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.csruleA],
                     ['CS([0-9]{6})(B[0-9]{0,1})', self.csruleB],
                     ['DK([0-9]{4})([0-9]{5})(A[0-9]{0,1}|D[0-9]{0,1})', self.dkruleA],  # modified
                     ['DK([0-9]{6})(T[0-9]{0,1})', self.dkrulet],  # added
                     ['DK([0-9]+)(B[0-9]{0,1}|T[0-9]{0,1})', self.dkruleB],  # modified
                     ['(DK)([0-9]{4})([0-9]{5})(U[0-9]{0,1})', self.yybefore2k],
                     ['FI([0-9]{2})([0-9]{5})(A[0-9]{0,1})', self.firuleA],  # modified
                     ['FI([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.firuleA2],  # added
                     ['FI([0-9]+)(B[0-9]{0,1})', self.firuleB],  # added
                     ['(FI)([0-9]{8})(A[0-9]{0,1})', self.usrule1],
                     ['JP([0-9]{1,2})([0-9]{6})(A[0-9]{0,1}|K[0-9]{0,1})', self.jpruleA],
                     ['JP0([0-9]{7})(U[0-9]{0,1}|Y[0-9]{0,1})', self.jprule0],  # added
                     ['JP([0-9]{10})(A[0-9]{0,1})', self.jpruleA1],
                     ['JP([0-9]{2})([0-9]{6})(B[0-9]{0,1})', self.jpruleB1],
                     ['JP([0-9]{2})([0-9]{6})(K[0-9]{0,1})', self.jpruleK1],
                     ['JP([0-9]{7})(B[0-9]{0,1})', self.jpruleB12],
                     ['JP([0-9]{7})(U[0-9]{0,1})', self.jpruleU12],
                     ['JP([0-9]{2})([0-9]{6})(W[0-9]{0,1}|X[0-9]{0,1}|Y[0-9]{0,1}|Z[0-9{0,1}])', self.jprulepct1],
                     ['DE([0-9]{4})([0-9]{5})(U[0-9]{0,1})', self.deruleU2],
                     ['DE([0-9]{7})(T[0-9]{0,1})', self.derulet],  # added
                     ['DE([0-9]+)(T[0-9]{0,1})', self.deTtoD2],  # added
                     ['DE([0-9]{6})(T[0-9]{0,1})', self.derulet2],  # added
                     ['GB([0-9]{4})([0-9]{5})(D[0-9]{0,1})', self.gbruled],  # added
                     ['USH([0-9]{4})H1', self.usruleh1],
                     ['(US|JP|DE|WO|GB|FR|CA|BE)([A-Z]{0,2}[0-9]+)([A-Z][0-9]{0,1})', self.usrule1],
                     ['EP([0-9]+)([A-Z][0-9]{0,1})', self.eprule],
                     ['WO19([0-9]{2})([0-9]{6})(A[0-9]{0,1}|B[0-9]{0,1})', self.worule1],
                     ['KR([0-9]{4})([0-9]{5,6})(A[0-9]{0,1}|U[0-9]{0,1})', self.krruleA1],
                     ['KR([0-9]{4})([0-9]{5})(B[0-9]{0,1}|Y[0-9]{0,1})', self.krruleB1],
                     ['KR([0-9]{6,7})(B[0-9]{0,1}|Y[0-9]{0,1})', self.krruleB2],
                     ['ES([0-9]{4})([0-9]{5})(A[0-9]{0,1}|B[0-9]{0,1}|C[0-9]{0,1})', self.ESruleABC],
                     ['ES([0-9]{7})(A[0-9]{0,1}|B[0-9]{0,1}|C[0-9]{0,1}|T[0-9]{0,1}|U[0-9]{0,1})', self.ESruleABC2],
                     ['TW([0-9]{6})(A[0-9]{0,1}|U[0-9]{0,1})', self.TWruleA],
                     ['TW([0-9]+)(A[0-9]{0,1})', self.TWruleA2],
                     ['TWI([0-9]{6})(B[0-9]{0,1})', self.TWruleB],
                     ['TWM([0-9]{6})(U[0-9]{0,1})', self.TWruleU],
                     ['(TW)([0-9]){9}(A)', self.usrule1],
                     ['(TW)([0-9]{4,6})(B)', self.usrule1],
                     ['(ES|DK)([0-9]){6,7}(T[0-9])', self.usrule1],
                     ['(ES)([0-9]{2,7})([A-Z][0-9]{0,1})', self.usrule1],
                     ['SU([0-9]{6,7})(A[0-9]{0,1}|B[0-9]{0,1}|C[0-9]{0,1})', self.SUruleABC],
                     ['SE([0-9]{4})([0-9]{5})(A[0-9]{0,1}|D[0-9]{0,1}|L[0-9]{0,1})', self.SEruleA],
                     ['(SE)([0-9]{7})(A[0-9]{0,1})', self.usrule1],  # added
                     ['SE([0-9]{6})(B[0-9]{0,1}|C[0-9]{0,1})', self.SEruleBC],
                     ['RU([0-9]{4})([0-9]{6})(A[0-9]{0,1})', self.RUruleA],
                     ['RU([0-9]{7})(C[0-9]{0,1})', self.RUruleC],
                     ['RU([0-9]{6})(U[0-9]{0,1})', self.RUruleU],
                     ['IT([0-9]{7})(B[0-9]{0,1})', self.ITruleB],
                     ['(IT)([0-9]{4})([0-9]{5})(D[0-9]{0,1}|A[0-9]{0,1}|V[0-9]{0,1}|U[0-9]{0,1})', self.twodigityear],
                     ['(IT)([0-9]{6})(Y[0-9]{0,1})', self.usrule1],
                     ['CH([0-9]{6})([A-Z][0-9]{0,1})', self.CHrule],
                     ['(FI)([0-9]{6})(A[0-9]{0,1}|B[0-9]{0,1})', self.usrule1],
                     ['(BR)([0-9]{12})([A-Z][0-9]{0,1}|B[0-9]{0,1})', self.usrule1],
                     ['(BR)([0-9]{4})([0-9]{5})(D0)', self.twodigityear],
                     ['(AU)([0-9]{10})([A-D][0-9]{0,1}|B[0-9]{0,1})', self.usrule1],
                     ['IT([0-9]{4})([A-Z]{2})([0-9]{4,5})([A-Z][0-9]{0,1})', self.itruleAmid],  # modified
                     ['BR(MU|PI|DI)([0-9]{7})([A-Z][0-9]{0,1}|B[0-9]{0,1})', self.brrulemid],
                     ['NL([0-9]+)C6', self.NLrulec6],
                     ['(NL)([0-9]{4})([0-9]{4,5})(A[0-9]{0,1}|I[0-9]{0,1})', self.twodigityear],
                     ['(NL)([0-9]+)([A-Z][0-9]{0,1})', self.usrule1],
                     ['(NO)([0-9]{4})([0-9]{5})(D[0-9]{0,1}|A[0-9]{0,1})', self.NOruleD],
                     ['(NO)([0-9]+)([A-Z][0-9]{0,1})', self.usrule1],
                     ['MX([0-9]{4})([0-9]{6})(A)', self.MXruleA],
                     ['(MX)([0-9]{4})([A-Z]{2})([0-9]+)([A-Z][0-9]{0,1})', self.yymidrule],
                     ['(HU|MX)([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.twodigityear],
                     ['(HU)([0-9]{4})([0-9]{5})(D[0-9]{0,1})', self.HUruleD],
                     ['(HU)([0-9]{4})([0-9]{5})(V[0-9]{0,1})', self.twodigityear],
                     ['HU([0-9]{5})T', self.HUruleT],
                     ['(ZA)([0-9]{4})([0-9]{5})(A[0-9]{0,1}|D[0-9]{0,1})', self.yybefore2k],
                     ['(GR)([0-9]{4})([0-9]{7})(T[0-9]{0,1}|A[0-9]{0,1}|U[0-9]{0,1})', self.yybefore2k],
                     ['PT([0-9]{6,7})T', self.PTruleT],
                     ['IN([0-9]{4})([0-9]{5})([A-Z][0-9]{0,1})', self.INrule],
                     ['IN([0-9]{4,5})([A-Z]{2,3})([0-9]{4})([A-Z][0-9]{0,1})', self.INmidrule],
                     ['IES([0-9]+)([A-Z][0-9]{0,1})', self.iesrule],
                     ['(HK)([0-9]+)(A[0-9])', self.usrule1],
                     ['(HK)([0-9]{4})([0-9]+)(A)', self.yyatend],
                     ['(IE)([0-9]{4})([0-9]{4})(L[0-9]{0,1}|A[0-9]{0,1})', self.yybefore2k],
                     ['(SG)([0-9]{4})([0-9]{5})(A2)', self.twodigityear],
                     ['SG([0-9]{4})([0-9]{5})A1', self.SGA1trimmed],
                     ['SG([0-9]{4})([0-9]{5})(A)', self.SGkindA2G],
                     ['SG([0-9]{11})A1', self.SGkindA1toA],
                     ['YU([0-9]{4})000([0-9])A', self.yuA2U],
                     ['(YU)([0-9]{4})([0-9]{4})(A)', self.yyatend2],
                     ['(PE)([0-9]{4})([0-9]{4})(A[0-9]{0,1}|Z[0-9]{0,1})', self.yearatend3],
                     ['(TR)([0-9]{4})([0-9]{5})(T[0-9]{0,1}|A[0-9]{0,1})', self.yybefore2k],
                     ['SK([0-9]{4})([0-9]{5})A3', self.skruleA],
                     ['SK([0-9]{4})([0-9]{6})A3', self.skruleA2],
                     ['(SK)([0-9]{4})([0-9]{4})(U[0-9]{0,1})', self.yearatend3],
                     ['EA([0-9]+)(B[0-9]{0,1})', self.EAruleB],
                     ['CL([0-9]{4})([0-9]{6})(A[0-9]{0,1})', self.CLruleA],
                     ['EA20([0-9]{9})(A[0-9]{0,1})', self.EAruleA],
                     ['SI([0-9]{6})(T[0-9])', self.SIruleT],
                     ['(SI|MY)([0-9]{4})([0-9]{5})(A[0-9]{0,1})', self.yybefore2k],
                     ['(AP)([0-9]{4})([0-9]{5})(D[0-9]{0,1})', self.yybefore2k],
                     ['HRP([0-9]{4})([0-9]{4})([A-Z][0-9]{0,1})', self.hrprule],
                     ['HR([A-Z]{2})([0-9]{4})([0-9]{4})([A-Z][0-9]{0,1})', self.hrmidrule],
                     ['([A-Z]{2})([A-Z]{0,2}[0-9]+)([A-Z][0-9]{0,1})', self.usrule1],
                     ]
        for exp, funcname in regexlist:
            self.regexlib[exp] = funcname

    def getIFInumber(self, pn):
        matched = False
        npn = pn
        for reg in self.regexlib:
            # print reg
            matchobj = re.match(reg, pn)

            if matchobj:
                matched = True
                ##                print self.regexlib[reg]
                ##                print reg
                npn = self.regexlib[reg](matchobj)
                break
        if not matched:
            # pass
            print pn + ' :not matching with any regex'
            return None
        else:
            # pass
            return npn



#
# def checkpninifi(pnset):
#     found = set()
#     cursor = condb.cursor()
#     count = 0
#     pnsub = []
#     for pn in pnset:
#         count += 1
#         pnsub.append(pn)
#
#         if count % 1000 == 0:
#             query = '\',\''.join(pnsub)
#             cursor.execute("select ucid from xml.t_patent_document_values where ucid in ('" + query + "')")
#             # print 'getting date for  patents : '+str(count)+' done'
#             for ucid, in cursor.fetchall():
#                 found.add(ucid.strip())
#             pnsub = []
#     if pnsub:
#         query = '\',\''.join(pnsub)
#         # print  query
#         cursor.execute("select ucid from xml.t_patent_document_values where ucid in ('" + query + "')")
#         for ucid, in cursor.fetchall():
#             found.add(ucid.strip())
#             # print 'getting date for patents : '+str(count)+' done'
#     cursor.close()
#     return found


##    return found,pnset.difference(found)
if __name__=='__main__':
    numconvertor=titoifi()
    pnlist='''KR-1174786-B1'''

    for pn in pnlist.splitlines():
       print  numconvertor.getIFInumber(pn)
