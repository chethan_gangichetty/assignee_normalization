from fuzzywuzzy import fuzz
from requests import get
from collections import defaultdict
import urllib

def matchnames(name1, name2):
    procname1 = name1.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()

    procname2 = name2.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()
    name1tw = ' '.join(procname1[:2])
    name2tw = ' '.join(procname2[:2])
    name1full = ' '.join(procname1)
    name2full = ' '.join(procname2)

    if name1full.find(name2tw) <> -1 or name2full.find(name1tw) <> -1 or fuzz.token_set_ratio(name1full,
                                                                                              name2full) >= 90:
        return True
    else:
        return False




def getnameid(namelist):
    names = set(namelist)
    nameid = {}

    for name1 in sorted(names, key=lambda x: len(x)):
        matched = None

        for name2 in nameid:

            if matchnames(name1, name2):
                matched = name2
                break
        if matched:
            nameid[name1] = nameid[name2]
        else:
            nameid[name1] = name1

    idname = defaultdict(set)
    for key in nameid:
        idname[nameid[key]].add(key)
    return idname, nameid