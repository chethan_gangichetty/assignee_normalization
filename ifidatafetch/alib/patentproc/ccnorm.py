import csv
import os

class ccnormalizer:
    def __init__(self):
        dir = os.path.dirname(__file__)
        self.mappingfile=os.path.join(dir,'data/CPC Lookup.csv')
        self.ccmap={}
        with open(self.mappingfile)as inf:
            cr=csv.reader(inf)
            for rec in cr:
                self.ccmap[rec[0]]=rec[1]

    def preproc(self,cc):
        try:
            if cc:
                ts=cc[:18].replace(' ','')
                return self.ccmap.get(ts,self.ccmap.get(ts[:-1],self.ccmap.get(ts[:-2],ts)))
            else:
                return cc
        except:
            cc

if __name__=='__main__':
    with open('data/uniqcc.csv')as inf:
        inr=csv.reader(inf)
        norm=ccnormalizer()
        for rec, in inr:
            print norm.preproc(rec)