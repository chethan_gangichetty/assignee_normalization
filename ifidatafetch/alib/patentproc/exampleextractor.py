from lxml import etree
class extractor:
    def __init__(self):
        self.name='exampleextractor'
    def extractexample(self,desc):
        try:
            descxml=etree.fromstring(desc)
            # print descxml.tag
            root=etree.fromstring('<ddes></ddes>')
            toadd=False
            added=False
            for child in descxml:

                if 'description' in  child.attrib:
                    if child.attrib['description']=='Detailed Description' and child.attrib['end']=='lead':
                        toadd=True
                        continue
                    if child.attrib['description']=='Detailed Description' and child.attrib['end']=='tail':
                        toadd=False
                        continue
                if toadd:
                    root.append(child)
                    added=True
            if added:
                return etree.tostring(root)
            else:
                return None
        except:
            return None