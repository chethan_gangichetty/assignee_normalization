import csv
from json import dumps
filename='finalsuggestions.csv'
with open(filename) as inf:
    with open('jsoned'+filename,'wb') as outf:
        ow=csv.writer(outf)
        fr=csv.reader(inf)

        for term,sug in fr:
            sugs=eval(sug)
            sugs = []

            for s,score in sugs:
                sd = {}
                sd['synonym']=s
                sd['score']=score
                sugs.append(sd)
        col2=dumps({'synonyms':sugs})
        ow.writerow([term,col2])