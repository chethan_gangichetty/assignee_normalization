import psycopg2
import csv
import urllib, json
import re
import datetime
from dateutil.relativedelta import relativedelta
import sys
from lxml import etree
from StringIO import StringIO
from pprint import pprint
from fuzzywuzzy import fuzz
import networkx as nx
from collections import OrderedDict


def normalize(comp):
    comp = comp.replace(",", " ").replace(".", " ").upper()
    comp = re.sub(' +', ' ', comp)
    comp = comp.strip()
    return comp


class USexpiry(object):
    def __init__(self, array):
        if array:
            self.array = array
            self.conn = psycopg2.connect(database="alexandria", user="alexandria", password="en-beg-or-um-e",
                                         host="66.211.108.74", port="5432")
            self.cursor = self.conn.cursor()
            self.data = {}

    def query(self):
        numbers = []
        datain = open(self.array, 'rU')
        datai = csv.reader(datain)
        dataout = csv.writer(open("Latest_assignee_Reassignment_output_14_feb.csv", "wb"))
        dataout.writerow(
            ["Patent", "Inventors", "Conveyance", "Current Assigee", "Chain of break of Title based on Inventor",
             "Chain of Break based on Assignee", "Security Agreement-Assignee", "Security Agreement-Assignor",
             "Assignee Graph"])
        for index, data in enumerate(datai):
            if index == 0:
                continue
            else:
                if "-" in data[0]:
                    numbers.append(data[0])
                else:
                    country_code = data[0][0:2]
                    remaining_no = data[0][2:]
                    match = re.findall(r"[^\W\d_]+|\d+", remaining_no)
                    if len(match) == 4:
                        patent_data = country_code + "-" + match[0] + match[1] + "-" + match[2] + match[3]
                        numbers.append(patent_data)
                    elif len(match) == 3 and match[0].isalpha():
                        patent_data = country_code + "-" + match[0] + match[1] + "-" + match[2]
                        numbers.append(patent_data)
                    elif len(match) == 3:
                        patent_data = country_code + "-" + match[0] + "-" + match[1] + match[2]
                        numbers.append(patent_data)
                    else:
                        patent_data = country_code + "-" + match[0] + "-" + match[1]
                        numbers.append(patent_data)
                        ##            sql="SELECT pdv.ucid,array_to_string(CAST(xpath('/parties/assignee-history/reassignments/reassignment/assignees/assignee/addressbook/last-name/text()' , tg.content) AS text)::text[],'|'), \
                        ##                    array_to_string(CAST(xpath('/parties/assignee-history/reassignments/reassignment/assignors/assignor/addressbook/last-name/text()' , tg.content) AS text)::text[],'|'), \
                        ##                    array_to_string(CAST(xpath('/parties/assignee-history/reassignments/reassignment/conveyance/text()' , tg.content) AS text)::text[],'|') \
                        ##                   as termofextension FROM xml.t_patent_document_values pdv, xml.t_parties tg WHERE pdv.publication_id = tg.publication_id \
                        ##                   AND pdv.country = 'US' AND pdv.ucid IN (%s)"
            sql = "SELECT pdv.ucid,tg.content \
                   as termofextension FROM xml.t_patent_document_values pdv, xml.t_parties tg WHERE pdv.publication_id = tg.publication_id \
                   AND pdv.country = 'US' AND pdv.ucid IN (%s)"
        in_p = ', '.join(map(lambda x: '%s', numbers))
        sql = sql % in_p
        self.cursor.execute(sql, numbers)
        rows = self.cursor.fetchall()
        # print rows
        for each in rows:
            # G=nx.Graph()
            # pprint(each)
            assignor_f = []
            assigner_f = []
            execution_f = []
            xml = each[1]
            tree = etree.parse(StringIO(xml))
            # ---------Inventors--------
            inventors = l = tree.xpath('//parties/inventors/inventor[@format="epo"]/addressbook/last-name//text()')
            for i in xrange(len(inventors)):
                inventors[i] = normalize(inventors[i])
            # inventors2=l=tree.xpath('//parties/inventors/inventor[@load-source="patent-office"]/addressbook/last-name//text()')
            # inventors=[]
            # for i in xrange(len(inventors1)):
            #     inventors.append(inventors2[i].lower()+" "+inventors1[i].lower())

            # ---------Assignors--------
            assignors = l = tree.xpath('//parties/assignee-history/reassignments/reassignment/assignors')
            for assignor in assignors:
                assigno = assignor.xpath('./assignor/addressbook/last-name/text()')
                if len(assigno) > 1:
                    assignor_data = ";".join(assigno)
                    assignor_f.append(assignor_data)
                else:
                    assignor_f.append(assigno[0])
                # ------Execution_dates-----------
                execution_date = assignor.xpath('./assignor/@execution-date')

                execution_f.append(execution_date[0])

            # ---------Assignees--------
            assignees = l = tree.xpath('//parties/assignee-history/reassignments/reassignment/assignees')
            for assignee in assignees:
                assigne = assignee.xpath('./assignee/addressbook/last-name/text()')

                if len(assigne) > 1:
                    assigner_data = ";".join(assigne)
                    assigner_f.append(assigner_data)
                else:
                    assigner_f.append(assigne[0])

            # ---------Conveyance--------
            conveyance = tree.xpath('//parties/assignee-history/reassignments/reassignment/conveyance/text()')

            # -----SOrting based on Execution_dates----------
            # sorted(assgevents, key=lambda x: (x[0], x[1]))
            final_data = zip(conveyance, assigner_f, assignor_f, execution_f)
            # print final_data
            final_data = sorted(final_data, key=lambda x: (x[3]))
            # print final_data
            # execution_len=len(execution_f)
            # for m in range(execution_len):
            #     for n in range(1,execution_len-m):
            #         if execution_f[n-1] > execution_f[n]:
            #             try:
            #                 (execution_f[n-1], execution_f[n]) = (execution_f[n], execution_f[n-1])
            #                 (assignor_f[n-1], assignor_f[n]) = (assignor_f[n], assignor_f[n-1])
            #                 (assigner_f[n-1], assigner_f[n]) = (assigner_f[n], assigner_f[n-1])
            #                 (conveyance[n-1], conveyance[n]) = (conveyance[n], conveyance[n-1])
            #             except:
            #                 pass

            # -----Final-output-------------

            # Made a set of all assignors to track all nodes of reassignment graph, multiple assignees seperated by ';' are put as different entities
            # in assignor set
            assignor_set = []
            assignee_set = []
            security_assignee = []
            security_assignor = []
            for i in xrange(len(final_data)):
                if ('SECURITY AGREEMENT' in final_data[i][0] or 'SECURITY INTEREST' in final_data[i][
                    0]) and "release".upper() not in final_data[i][0]:
                    security_assignor.append(final_data[i][2])
                    security_assignee.append(final_data[i][1])
                    continue
                elif ';' in final_data[i][2]:
                    assignee_set.append(normalize(final_data[i][1]))
                    xx = final_data[i][2].split(';')
                    for splited in xx:
                        # print "splitted",splited
                        assignor_set.append(normalize(splited))
                else:
                    # print "",final_data[i][0],final_data[i][2]
                    assignor_set.append(normalize(final_data[i][2]))
                    assignee_set.append(normalize(final_data[i][1]))
            assignor_set = list(set(assignor_set))
            security_assignee = list(set(security_assignee))
            security_assignor = list(set(security_assignor))
            # print "assignor set:", assignor_set
            # print "security assignee", security_assignee
            # print "security assignor", security_assignor
            # break
            assignee_graph = OrderedDict()

            ## Check for chain of break based on inventors, if atleast one inventor is present then no chain of break
            chain_of_break_inventor = 'No'
            flag = 0
            for i in assignor_set:
                for j in inventors:
                    if i == j or fuzz.ratio(i, j) > 90:
                        flag = 1
                        break
                if flag:
                    break
            if flag == 0:
                chain_of_break_inventor = 'Yes, based on Inventor Name'
            # print chain_of_break_inventor

            # print final_data


            for i in final_data:
                if ';' in i[2]:
                    xx = i[2].split(';')
                    for splited in xx:
                        try:
                            assignee_graph[normalize(splited)].append(normalize(i[1]))
                        except:
                            assignee_graph[normalize(splited)] = []
                            assignee_graph[normalize(splited)].append(normalize(i[1]))
                else:
                    try:
                        assignee_graph[normalize(i[2])].append(normalize(i[1]))
                    except:
                        assignee_graph[normalize(i[2])] = []
                        assignee_graph[normalize(i[2])].append(normalize(i[1]))

            # print "Inventors : ",inventors
            print "Assignment graph : ", assignee_graph
            ggg = str(assignee_graph)
            current_assignee = []
            visited_assignors = []
            keys_to_remove = []

            for inv in inventors:
                for key in assignee_graph:
                    if inv == key or fuzz.ratio(inv, key) > 90 or fuzz.partial_ratio(inv, key) > 90:
                        xx = assignee_graph[key]
                        for item in xx:
                            current_assignee.append(item)
                        keys_to_remove.append(key)
            current_assignee = list(set(current_assignee))

            for key in keys_to_remove:
                assignee_graph.pop(key, 'None')

            # print assignee_graph
            # print current_assignee

            # break

            ### Final logic to iterate in assignee graph and find current assignees
            flag_break = 0
            chain_of_break_assignment = 'No'
            for key in assignee_graph:
                # print key, assignee_graph[key]
                if key in current_assignee:
                    new_assignment = list(set(assignee_graph[key]))
                    for item in new_assignment:
                        current_assignee.append(item)
                    current_assignee.remove(key)
                else:
                    for i in assignee_set:
                        if i == key or fuzz.ratio(i, key) > 90:
                            flag_break = 0
                            break
                    if flag_break == 0:
                        chain_of_break_assignment = 'Yes, based on assignment :' + key
                    new_assignment = list(set(assignee_graph[key]))
                    for item in new_assignment:
                        print item
                        current_assignee.append(item)
            print current_assignee

            ### Removing assignees which are in security assignee
            to_remove = []
            for i in current_assignee:
                for j in security_assignee:
                    if i == j or fuzz.ratio(i, j) > 90:
                        to_remove.append(i)
            for i in to_remove:
                try:
                    current_assignee.remove(i)
                except:
                    pass

            #####considering F/K/A cases#######
            for i in xrange(len(current_assignee)):
                if 'F/K/A' in current_assignee[i]:
                    former_name = current_assignee[i].split(' F/K/A ')[1]
                    new_name = current_assignee[i].split(' F/K/A ')[0]
                    current_assignee[i] = new_name
                    try:
                        current_assignee[current_assignee.index(former_name)] = new_name
                    except:
                        pass
            current_assignee = list(set(current_assignee))
            if len(current_assignee) == 0:
                for item in final_data:
                    if "ASSIGNMENT OF ASSIGNORS INTEREST" in item[0]:
                        current_assignee.append(normalize(item[1]))

            sec_assignee = ''
            sec_assignor = ''
            try:
                if ('SECURITY AGREEMENT' in final_data[-1][0] or 'SECURITY INTEREST' in final_data[-1][
                    0]) and "release".upper() not in final_data[-1][0]:
                    sec_assignee = final_data[-1][1]
                    sec_assignor = final_data[-1][2]
            except:
                pass

            print current_assignee

            # break
            print each[
                0]  # ,inventors, str(final_data).encode('utf-8'), "|".join(current_assignee).encode('utf-8'), chain_of_break_inventor, chain_of_break_assignment
            dataout.writerow([each[0], "|".join(inventors).encode('utf-8'), str(final_data).encode('utf-8'),
                              "|".join(current_assignee).encode('utf-8'), chain_of_break_inventor,
                              chain_of_break_assignment, sec_assignee, sec_assignor, str(ggg).encode('utf-8')])
            # break






            # assigner_f=assigner_f[::-1]
            # assignor_f=assignor_f[::-1]
            # conveyance=conveyance[::-1]
            # execution_f=execution_f[::-1]
            # chain_of_break='No'
            # security_assignee='#N/A'
            # security_assignor='#N/A'
            # latest_assignee=[]
            # inventor_check=1
            # for i in xrange(len(conveyance)):
            #     #print i
            #     if ";" in assignor_f[i]:
            #         norm_inv=assignor_f[i].split(";")
            #         for tt in xrange(len(norm_inv)):
            #             norm_inv[tt]=norm_inv[tt].replace(",","").replace(".","").upper()
            #     else:
            #         norm_inv=[assignor_f[i]]

            #     if inventor_check:
            #         for inv in inventors:
            #             for kk in norm_inv:
            #                 if (inv in kk) or fuzz.ratio(inv, kk)>90:
            #                     chain_of_break='No'
            #                     inventor_check=0
            #                     break
            #                 else:
            #                     chain_of_break='Yes, Based on inventor name'

            #     if 'SECURITY AGREEMENT' in conveyance[i].upper() or 'SECURITY INTEREST' in conveyance[i].upper():
            #         continue
            #     # elif 'CHANGE OF NAME' in conveyance[i].upper():
            #     #     try:
            #     #         latest_assignee[latest_assignee.index(assignor_f[i].replace(",","").replace(".",""))]=assigner_f[i].replace(",","").replace(".","")
            #     #     except:
            #     #         pass


            #     elif i==0 and "ASSIGNMENT OF ASSIGNORS INTEREST" in conveyance[i].upper():
            #         assignor_initial=assignor_f[i].replace(";"," ").replace(","," ").replace("."," ").upper()
            #         assignor_initial= re.sub(' +',' ',assignor_initial)
            #         assignor_initial=assignor_initial.strip()

            #         xx=assigner_f[0].replace(","," ").replace("."," ")
            #         xx= re.sub(' +', ' ', xx)
            #         xx=xx.strip()
            #         latest_assignee.append(xx)
            #         latest_assignee=list(set(latest_assignee))
            #     elif i!=0 and "ASSIGNMENT OF ASSIGNORS INTEREST" in conveyance[i].upper():
            #         xx = assignor_f[i].replace(","," ").replace("."," ")
            #         xx= re.sub(' +', ' ',xx)
            #         xx=xx.strip()
            #         yy=assigner_f[i].replace(","," ").replace("."," ")
            #         yy= re.sub(' +', ' ',yy)
            #         yy=yy.strip()
            #         if xx in latest_assignee:
            #             latest_assignee.append(yy)
            #             latest_assignee.remove(xx)
            #         else:
            #             latest_assignee.append(yy)
            #         latest_assignee=list(set(latest_assignee))
            #     else:
            #         xx=assigner_f[i].replace(","," ").replace("."," ")
            #         xx= re.sub(' +', ' ',xx)
            #         xx=xx.strip()
            #         yy=assignor_f[i].replace(","," ").replace("."," ")
            #         yy= re.sub(' +', ' ',yy)
            #         yy=yy.strip()
            #         latest_assignee.append(xx)
            #         try:
            #             latest_assignee.remove(yy)
            #         except:
            #             chain_of_break='Yes, based on assignment'


            #         latest_assignee=list(set(latest_assignee))


            # if len(conveyance)>0 and 'SECURITY AGREEMENT' in conveyance[-1]:
            #     security_assignor=assignor_f[-1]
            #     security_assignee=assigner_f[-1]
            # latest_assignee=list(set(latest_assignee))




            # assignee_len=len(assigner_f)
            # k=[]
            # try:
            #     for i in range(assignee_len-1):

            #         if assigner_f[i].replace(" ","") in assignor_f[i+1].replace(" ","") or assignor_f[i+1].replace(" ","") in assigner_f[i].replace(" ",""):
            #             continue
            #         elif "SECURITY AGREEMENT" in conveyance[i]:
            #             continue
            #         else:
            #             k.append(assigner_f[i])
            #     if "SECURITY AGREEMENT" in conveyance[-1] and assignee_len>1:
            #         k.append(assigner_f[-2])
            #         pass
            #     else:
            #         k.append(assigner_f[-1])
            # except:
            #     pass
            # k="|".join(list(set(k)))
            # print each[0],inventors,assigner_f,assignor_f,execution_f,conveyance, latest_assignee, security_assignee, security_assignor
            # dataout.writerow([each[0],"|".join(inventors).encode('utf-8'),"|".join(assigner_f).encode('utf-8'),"|".join(assignor_f).encode('utf-8'),"|".join(execution_f).encode('utf-8'),"|".join(conveyance).encode('utf-8'),"|".join(latest_assignee).encode('utf-8'),str(zip(conveyance,assigner_f,assignor_f,execution_f)).encode('utf-8'),security_assignee,security_assignor,chain_of_break])


#
##
##        datain.close()
##        return pta_dict



if __name__ == "__main__":
    obj = USexpiry('1237.csv')
    assignee = obj.query()
