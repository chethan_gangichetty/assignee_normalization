# -*- coding: utf-8 -*-
import psycopg2
import xml.etree.ElementTree as ET
import logging
import string
from fuzzywuzzy import fuzz
from pprint import pprint
import sys
from partiesparser import partiesparser

logging.basicConfig(level=logging.INFO)
from collections import defaultdict, OrderedDict, Counter
# import jellyfish
import sys

sys.path.append('/home/analytics/Developer/an')
from assignee_normalization import assigneeNormalization


class graph(object):
    def __init__(self):
        self.start='startnode'
        self.graph = {}
        self.nodes = set()

    def addedge(self, start, end):
        self.graph[start] = end
        self.nodes.add(start)
        self.nodes.add(end)

    def traverse(self):
        start = self.nodes.difference(set(self.graph.values()))
        print start, type(start)
    def addnode(self,nodelist):
        for node in nodelist:
            self.nodes.add(node)


class assigneecalc:
    def __init__(self):
        self.tbl = {ord(c): u' ' for c in u'.-,;'}
        self.conn = psycopg2.connect(
            "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
        self.cur = self.conn.cursor()
        self.parryparser = None
        self.namepref = ['epo', 'intermediate', 'original']
        self.asnorm = assigneeNormalization(True)

    def finalize(self):
        self.cur.close()
        self.conn.close()


    def getconvtype(self,convtext):
        if not convtext:
            return 'dummy'
        convtext=convtext.lower()
        if convtext.find('secur')<>-1:
            if convtext.find('releas')<>-1:
                return 'secrel'
            else:
                return 'secagr'
        if convtext.find('patent')<>-1 and convtext.find('releas')<>-1:
            return 'secrel'
        return 'asg'
    def matchnames(self,name1,name2):
        procname1=name1.lower().replace('.','').replace(',',' ').replace('"','').replace('\\','').split()

        procname2=name2.lower().replace('.','').replace(',',' ').replace('"','').replace('\\','').split()
        name1tw=' '.join(procname1[:2])
        name2tw=' '.join(procname2[:2])
        name1full=' '.join(procname1)
        name2full=' '.join(procname2)

        if name1full.find(name2tw)<>-1 or name2full.find(name1tw)<>-1 or fuzz.token_set_ratio(name1full, name2full) >= 90:
            return True
        else:
            return False
    def getnameidnew(self,data):
        names = set()
        recs = []
        nameid = {}
        sugtoultimate = {}
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                names.update([x['name'] for x in data[key]])

            if key == 'reassignments':
               names.update([r['name'] for x in data[key] for r in x[3] + x[4]])
        # print names
        for name1 in sorted(names, key=lambda x: len(x)):
            matched = None


            for name2 in nameid:

                if self.matchnames(name1, name2):
                    matched = name2
                    break
            if matched:
                nameid[name1] = nameid[name2]
            else:
                nameid[name1] = name1

        idname = defaultdict(set)
        for key in nameid:
            idname[nameid[key]].add(key)
        # print idname
        idnormname=defaultdict(set)
        idparent={}
        for id in idname:
            norm,isnorm=self.normalizename(id)
            if isnorm:
                idparent[norm['suggested']]=norm['parent']
                idnormname[norm['suggested']].update(idname[id])
            else:
                for n in idname[id]:
                    norm, isnorm = self.normalizename(n)
                    if isnorm:
                        idparent[norm['suggested']] = norm['parent']
                        idnormname[norm['suggested']].update(idname[id])
                        break

        nameidnorm={}

        for x in idnormname:
            for val in idnormname[x]:
                nameidnorm[val]=x

        return nameidnorm,idnormname,idparent


    def normalizename(self, name):
        norm = []
        normname = {}
        isnorm = False
        try:
            norm = self.asnorm.get_normalized_assignee(name)
            isnorm = True
        except Exception, e:
            print e
        normname['name'] = name
        normname['suggested'] = norm[1].replace('"','').replace('\\','').strip() if norm else None
        normname['parent'] = norm[2].replace('"','').replace('\\','').strip() if norm else None
        return normname, isnorm


    def getgraph(self,data):
        # pprint(data)
        nameid, idname, idparent = self.getnameidnew(data)
        invapplasgsets = defaultdict(set)
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                for rec in data[key]:
                    invapplasgsets[key].add(nameid[rec['name']])
        curowners = set()
        inv=set()
        inv.update(invapplasgsets['inv'])
        curowners.update(invapplasgsets['inv'])
        # print 'start',invapplasgsets['inv']
        # calculate current asg
        reassignmentnew = {}
        n = 1
        # print nameid
        for date, reel, conv, asg, asn in reversed(data['reassignments']):
            reassignmentnew[n] = [date, reel, conv, set([nameid[x['name']] for x in asg]),
                                  set([nameid[x['name']] for x in asn])]
            n += 1
        lastid = None
        assignors = set()
        while True:
            next=[]
            nextids=[]
            prevlen=len(reassignmentnew)
            for k in sorted(reassignmentnew):
                date, reel, conv, asg, asn = reassignmentnew[k]
                if self.getconvtype(conv) in ['secrel','secagr']:
                    continue

                if curowners.intersection(asn):
                    nextids.append(k)
                    next.append(reassignmentnew[k])
                    lastid=k
            # print 'inv-----'
            # print curowners
            # print next
            # print '----'
            for date, reel, conv, asg, asn in next:
                curowners=curowners.difference(asn)
                curowners.update(asg)
                assignors.update(asn)
            for key in nextids:
                del reassignmentnew[key]

            if len(reassignmentnew)==prevlen:
                # print reassignmentnew
                break

        curowners=curowners.difference(inv)
        orignotassignor=invapplasgsets['asg'].intersection(assignors)
        # print 'orignotassignor',orignotassignor
        if not curowners or not invapplasgsets['asg'].intersection(assignors):
            inv = set()
            inv.update(invapplasgsets['asg'])
            # print 'origasg',data['asg']
            curowners.update(invapplasgsets['asg'])
            while True:
                next = []
                nextids = []
                prevlen = len(reassignmentnew)
                for k in sorted(reassignmentnew):
                    date, reel, conv, asg, asn = reassignmentnew[k]
                    if self.getconvtype(conv) in ['secrel', 'secagr']:
                        continue
                    if curowners.intersection(asn):
                        lastid=k
                        nextids.append(k)
                        next.append(reassignmentnew[k])
                # print 'asg-----'
                # print curowners
                # print next
                # print '----'
                for date, reel, conv, asg, asn in next:
                    curowners = curowners.difference(asn)
                    curowners.update(asg)
                for key in nextids:
                    del reassignmentnew[key]

                if len(reassignmentnew) == prevlen:
                    # print reassignmentnew
                    break

        ## broken and sec owners
        brokenowners=set()
        secowners=set()
        secstart=set()
        num=0
        # print reassignmentnew
        secrelnames = set()
        for x in sorted(reassignmentnew):


            date, reel, conv, asg, asn = reassignmentnew[x]
            convtype=self.getconvtype(conv)

            if convtype in ['secrel','secagr']:

                if convtype=='secagr':
                    secstart.update(asn)
                if convtype=='secrel':
                    secrelnames.update(asg)

                secowners = secowners.difference(asn)

                secowners.update(asg)
                # print 'sec -----'
                # print secowners
                # print reassignmentnew[x]
                # print '----'
                del reassignmentnew[x]
        secowners=secowners.intersection(secrelnames).difference(secstart)
        brokenowners.update(secowners)
        for x in sorted(reassignmentnew):
            print 'broken',reassignmentnew[x]
            date, reel, conv, asg, asn = reassignmentnew[x]
            brokenowners = brokenowners.difference(asn)

            brokenowners.update(asg)



        return set([idparent[x] for x in curowners]),set([idparent[x] for x in brokenowners]),set([idparent[x] for x in secowners])






    def getcurowners(self, data,parsechain=1):
        # pprint(data)
        names,nameid,sugtoultimate=self.getnameid(data)
        invapplasgsets = defaultdict(set)
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                for rec in data[key]:
                    invapplasgsets[key].add(nameid[rec['name']])
        curowners = set()
        curowners.update(invapplasgsets['inv'])
        # print 'start',invapplasgsets['inv']
        # calculate current asg
        reassignmentnew = {}
        n = 1
        for date, reel, conv, asg, asn in reversed(data['reassignments']):
            reassignmentnew[n] = [date, reel, conv, set([nameid[x['name']] for x in asg]),
                                  set([nameid[x['name']] for x in asn])]
            n += 1
        # pprint(reassignmentnew)
        if parsechain==1:
            chain = OrderedDict()

            for x in xrange(0, len(reassignmentnew)):
                todel = -1
                for k in sorted(reassignmentnew):
                    date, reel, conv, asg, asn = reassignmentnew[k]
                    # print date, reel, conv, asg, asn
                    # print 'owners current',curowners
                    common = set(asn).intersection(curowners)
                    if not conv:
                        conv = 'dummy conveyance data'

                    if len(common) > 0 and (
                                    'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower())):
                        chain[k] = (date, reel, conv, asg, asn)
                        # print 'chain',date,reel,conv,asg,asn
                        todel = k
                        for kname in common:
                            curowners.remove(kname)

                        for kname in asg:
                            curowners.add(kname)

                        break
                if todel <> -1:
                    del reassignmentnew[todel]

            # for key in chain:
            #     print key, chain[key]
        brokenowners = Counter()
        secrelease=set()
        brokenassigners = Counter()
        for key in sorted(reassignmentnew):
            date, reel, conv, asg, asn = reassignmentnew[key]
            # print reassignmentnew[key]
            if not conv:
                conv = 'dummy conveyance data'
            for asgname in asn:
                # print asgname, 'in sorted reasg'
                if asgname in brokenowners and brokenowners[asgname] > 0:
                    brokenowners[asgname] = brokenowners[asgname] - 1
                    # print 'decrement', asgname, brokenowners
            if 'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower()):
                # print 'added', asg
                brokenowners.update(asg)
                if 'secur' in conv.lower() and 'releas' in conv.lower():
                    secrelease.update(asg)
                # else:
                #     # print 'skipped'
        # print brokenowners,curowners
        # print invapplasgsets['inv']
        ownersfinal = curowners.difference(invapplasgsets['inv'])
        # print ownersfinal
        # print brokenowners
        return set([sugtoultimate[x] for x in ownersfinal]),set([sugtoultimate[x] for x in brokenowners if brokenowners[x] > 0]),secrelease





    def getcurrentasg(self, pnlist,parsechain=1):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()
                # owners,brkowner,secowner = self.getcurowners(self.parryparser.data,parsechain)
                # res = [rec[0], owners, brkowner,secowner]
                owners,broken,sec=self.getgraph(self.parryparser.data)
                res=[rec[0],owners,broken,sec]
                recs.append(res)
                # print res
        # print res
        return recs

    def getnormalizedlist(self, pnlist):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        namenorm = {}
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                namerec = []
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()

                data = self.parryparser.data
                for key in data:

                    if key in ['inv', 'appl', 'asg']:
                        namerec += data[key]

                    if key == 'reassignments':
                        for recd in data[key]:
                            namerec += recd[3] + recd[4]
                for nrec in namerec:
                    # print nrec
                    name, sug, parent = self.asnorm.get_normalized_assignee(nrec['name'])
                    recs.append([rec[0], name, sug, parent])

                    # print res
        # print res
        return recs
import csv
if __name__ == '__main__':
    # g=graph()
    # g.addedge(1,2)
    # g.addedge(2,3)
    # g.traverse()
    ca = assigneecalc()
    googlecurasg={}
    customnorm={'NXP USA INC':'NXP Semiconductors NV','SAMBA HOLDCO NETHERLANDS BV':'Ampleon Netherlands B.V.'}
    with open('1237googlepatents.csv')as gpf:
        gr=csv.reader(gpf)
        for pn,curasglisttext,status in gr:
            curasglist=eval(curasglisttext)
            normlist=map(ca.asnorm.get_normalized_assignee,curasglist)
            asgdict={key: val for key, val in zip(curasglist,normlist)}

            googlecurasg[pn]=asgdict
            # break
    count=0
    pnlist=[]
    with open('pnlist.csv','rU')as pnf:
        pr=csv.reader(pnf)
        for pn, in pr:
            pnlist.append(pn)
    with open('algo10marfullout.csv','wb') as outf:
        ow=csv.writer(outf)
        ow.writerow(['pn','matched','googleasg','following chain','brokenchain','sec'])
        for pn in googlecurasg:
            asgdict=googlecurasg[pn]
            googleasg=set([customnorm.get(asgdict[x][2].replace('"','').replace('\\',''),asgdict[x][2].replace('"','').replace('\\','')) for x in asgdict])
            recs=ca.getcurrentasg([pn],0)
            pn,curasg,curbroken,cursec=recs[0]

            matched='notmatched'
            if googleasg==curasg:
                matched='matched'
            print pn,matched,googleasg,curasg,curbroken,cursec
            ow.writerow([pn,matched,googleasg,curasg,curbroken,cursec])
            count+=1


    # with open('1237.csv')as inf:
    #     with open('1237out.csv','wb')as outf:
    #         cr=csv.reader(inf)
    #         cw=csv.writer(outf)
    #         pnlist=[]
    #         incount = 0
    #         for pn, in cr:
    #             incount+=1
    #             pnlist.append(pn)
    #         # pnlist=['US-7842936-B2', 'US-RE45552-E1', 'US-8648318-B2']
    #         # print pnlist
    #
    #         outcount=0
    #         for x in xrange(0,len(pnlist),200):
    #             r=ca.getcurrentasg(pnlist[x:x+200])
    #             for pn,assignee,extraasg in r:
    #                 print pn
    #
    #                 cw.writerow([pn,assignee,extraasg])
    #                 outcount+=1
    #             # break
    # # ca.cur.execute("select b.publication_id,content  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid = 'US-6715055-B1'")
    # # for id,partiesxml in ca.cur:
    # #     print ca.getcurrentasg(id,partiesxml)
    # # print ca.getcurrentasg(['US-6715055-B1'])
    # # print ca.getcurrentasg(['US-6972255-B2'])
    # # print ca.getcurrentasg(['US-8560741-B2'])
    # # print ca.getcurrentasg(['US-8468247-B1'])
    # # print ca.getcurrentasg(['US-20140167130-A1'])
    # #
    ca.finalize()
