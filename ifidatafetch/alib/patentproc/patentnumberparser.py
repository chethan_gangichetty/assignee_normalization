import re


class pnparser:
    def __init__(self):
        self.pnregex = re.compile(r'([A-Z][A-Z])[-]{0,1}([A-Z]{0,2})([0-9]+)[-]{0,1}([A-Z][0-9]{0,1}){0,1}')

    def getstandardizedpn(self, pn, out='str'):
        m = self.pnregex.match(pn)
        npn = []
        npn.append(m.group(1))
        npn.append(m.group(2) + m.group(3))
        if m.group(4):
            npn.append(m.group(4))
        if out == 'full':
            return '-'.join(npn)
        elif out == 'number':
            return npn[1]
        elif out == 'groups':
            return npn


if __name__ == '__main__':
    numparser = pnparser()
    print numparser.getstandardizedpn('USD7199429B')
