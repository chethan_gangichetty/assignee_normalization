import traceback
from mysolr import Solr
import psycopg2
import time
from multiprocessing import Queue, Process
from pprint import pprint
import requests
import xml.etree.ElementTree as ET

goodlist = '''AT HC
AT PC
AT PD9K
AT TC9K
AU HB
AU HC1
AU PC
AU PC1
AU PCD
AU TC
BE CH
BE CN
BR B25A
BR B25B
BR B25C
BR B25D
BR B25E
BR B25F
BR B25G
BR B25H
BR B25I
BR B25J
BR B25K
BR B25L
BR B25M
BR PC
BR PCP
CH PFA
CH PFUS
CH PUE
CH PUEA
CN ASS
CN C56
DD ASS
DE 8127
DE 8327
DE R081
EE GB1A
EE HC1A
EP RAP1
EP RAP2
EP RAP3
EP RAP4
ES PC1A
ES PC1K
ES PC2A
FI GB
FI HC
FI PC
FI PCU
FI TC
FI TCU
FR TP
FR TQ
GB COOA
HK AS
HU DGB9
HU GB9A
HU HC9A
HU HPC4
IL HC
IL HP
LT PC9A
LT PD9A
MD GB9A
MD HC9A
MD PD4A
MD PD4K
MD TC4A
MX GB
MX HC
NL CD
NL CNR
NL DD
NL DNT
NL NL80
NL SD
NL SNR
NL TD
NL TNT
NO CHAD
NZ ASS
PT PC3A
PT PC3K
PT PC4A
PT PC4K
PT PD3A
PT PD3K
PT PD4A
PT PD4K
SI SP73
SK PC4A
SK SPCC
SK TC4A
US AS'''.splitlines()


def parselegalxml(legalxml):

    eventstag = ET.fromstring(legalxml)
    events = []
    for eventtag in eventstag:
        event = eventtag.attrib

        for eventbodytag in eventtag:
            for eventdata in eventbodytag:
                if eventdata.tag == 'event-title':
                    event['event title'] = eventdata.text
                else:
                    if eventdata.tag == 'event-attributes':
                        eventattriblist = []
                        for eattribute in eventdata:
                            eventattrib = {}
                            key = ''
                            value = ''
                            for eattribdata in eattribute:

                                if eattribdata.tag == 'event-attribute-label':
                                    key = eattribdata.text
                                if eattribdata.tag == 'event-attribute-value':
                                    value = eattribdata.text
                                if key and value:
                                    eventattrib[key] = value
                                    key = ''
                                    value = ''
                            eventattriblist.append(eventattrib)
                        event['attributes'] = eventattriblist
        try:

            if event['code'] == 'REG' and event['country'] == 'EP' and 'attributes' in event:
                event['effcode'] = event['attributes'][0]['Ref Legal Event Code ']
                event['effcountry'] = event['attributes'][0]['Ref Country Code ']
                if 'Effective Date ' in event['attributes'][0]:
                    event['effdate'] = event['attributes'][0]['Effective Date ']
                effattributes = {}
                for key in event['attributes'][0]:
                    if key not in ['Ref Legal Event Code ', 'Ref Country Code ', 'Effective Date ']:
                        effattributes[key] = event['attributes'][0][key]
            events.append(event)
        except:
            print ' ---------'
            traceback.print_exc()
            # print id,pn
            # pprint(legalxml)

    # pprint(events)~
    # print '=============================='
    return events
def getreasgtransactions(events):
    flag = True
    flagreg = True
    owner = None
    owners = set()
    extraowners = []
    codes = []
    pprint(events)
    assignors = []
    countrydone = []
    for event in sorted(events, key=lambda x: x['date'], reverse=True):
        fcode = event['country'] + ' ' + event['code']
        if fcode not in goodlist:
            # print fcode
            # print 'fcode not in goodlist'+fcode
            if fcode in ['EP REG'] and flagreg:
                # print 'EP REG'
                gotextraasg = False
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and 'Ref Country Code ' in attrib and 'Ref Legal Event Code ' in attrib:
                        # print 'Owner name  present'
                        if attrib['Ref Country Code '] + ' ' + attrib['Ref Legal Event Code '] in goodlist and attrib[
                            'Ref Country Code '] not in countrydone:
                            # print 'good list ref code'
                            extraowners.append(attrib['Owner name '])
                            gotextraasg = True
                            countrydone.append(attrib['Ref Country Code '])
                            # print  extraowners,countrydone
                        else:
                            if attrib['Ref Country Code '] in countrydone:
                                gotextraasg = True

                    else:
                        continue
                # if gotextraasg:
                #     continue
                continue
                # print 'owners:',owners
                # print 'assignors:',assignors

            elif fcode in ['EP BECH', 'EP BECN', 'EP ITPR', 'EP NLS', 'EP NLT1', 'EP ITF']:
                # print 'explicit codes'
                # print countrydone,fcode
                # print attrib
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and fcode not in countrydone:

                        # print 'added extraowners'+str(attrib['Owner name '])
                        extraowners.append(attrib['Owner name '])
                        countrydone.append(fcode)
                        continue

                    else:
                        continue
                continue
            else:
                continue
        # print 'setting reg flag'
        flagreg = False
        if 'attributes' in event:
            if pn.find('AU') <> -1:
                seqevents = event['attributes']
            else:
                seqevents = reversed(event['attributes'])
            for attrib in sorted(seqevents, key=lambda x: x['Effective Date '] if 'Effective Date ' else None,
                                 reverse=True):
                print 'owners:', owners
                print 'assignors:', assignors
                # pprint(attrib)
                if 'Owner name ' in attrib:
                    owner = attrib['Owner name ']
                    flag = False

                    if 'free format text ' in attrib:
                        conven, formerowner, reel, other = processfreeformtext1(attrib['free format text '])
                        if conven.find('SECURITY') == -1:
                            for assignor in assignors:
                                if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                    flag = True
                            if not flag:
                                codes.append(event['country'] + ' ' + event['code'])
                                owners.add(owner)
                            assignors.append([id, pn, event['country'], formerowner, event['date']])
                    else:
                        for assignor in assignors:
                            if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                flag = True
                        if not flag:
                            codes.append(event['country'] + ' ' + event['code'])
                            owners.add(owner)

        if pn.find('US') <> -1 or pn.find('CN') <> -1:
            continue
        elif owners:
            break
    if owners or extraowners:
        return id, pn, codes, owners, extraowners, countrydone


def getLatestOwnerNew(id, pn, events):
    flag = True
    flagreg=True
    owner = None
    owners = set()
    extraowners = []
    codes = []
    pprint(events)
    assignors = []
    countrydone=[]
    for event in sorted(events,key=lambda x:x['date'],reverse=True):
        fcode = event['country'] + ' ' + event['code']
        if fcode not in goodlist:
            # print fcode
            # print 'fcode not in goodlist'+fcode
            if fcode in ['EP REG'] and flagreg:
                # print 'EP REG'
                gotextraasg=False
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and 'Ref Country Code ' in attrib and 'Ref Legal Event Code ' in attrib:
                        # print 'Owner name  present'
                        if attrib['Ref Country Code ']+' '+attrib['Ref Legal Event Code '] in goodlist and attrib['Ref Country Code '] not in countrydone:
                            # print 'good list ref code'
                            extraowners.append(attrib['Owner name '])
                            gotextraasg=True
                            countrydone.append(attrib['Ref Country Code '])
                            # print  extraowners,countrydone
                        else:
                            if attrib['Ref Country Code '] in countrydone:
                                gotextraasg=True

                    else:
                        continue
                # if gotextraasg:
                #     continue
                continue
                # print 'owners:',owners
                # print 'assignors:',assignors

            elif fcode in ['EP BECH','EP BECN','EP ITPR','EP NLS','EP NLT1','EP ITF']:
                # print 'explicit codes'
                # print countrydone,fcode
                # print attrib
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and fcode not in countrydone:

                        # print 'added extraowners'+str(attrib['Owner name '])
                        extraowners.append(attrib['Owner name '])
                        countrydone.append(fcode)
                        continue

                    else:
                        continue
                continue
            else:
                continue
        # print 'setting reg flag'
        flagreg=False
        if 'attributes' in event:
            if pn.find('AU') <> -1:
                seqevents = event['attributes']
            else:
                seqevents = reversed(event['attributes'])
            for attrib in sorted(seqevents,key=lambda x:x['Effective Date '] if 'Effective Date ' else None,reverse=True):
                print 'owners:',owners
                print 'assignors:',assignors
                # pprint(attrib)
                if 'Owner name ' in attrib:
                    owner = attrib['Owner name ']
                    flag = False

                    if 'free format text ' in attrib:
                        conven, formerowner, reel, other = processfreeformtext1(attrib['free format text '])
                        if conven.find('SECURITY') == -1:
                            for assignor in assignors:
                                if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                    flag = True
                            if not flag:
                                codes.append(event['country'] + ' ' + event['code'])
                                owners.add(owner)
                            assignors.append([id, pn, event['country'], formerowner, event['date']])
                    else:
                        for assignor in assignors:
                            if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                flag = True
                        if not flag:
                            codes.append(event['country'] + ' ' + event['code'])
                            owners.add(owner)

        if pn.find('US') <> -1 or pn.find('CN')<>-1:
            continue
        elif owners:
            break
    if owners or extraowners:
        return id, pn, codes, owners,extraowners,countrydone



def getLatestOwnerNewbak(id, pn, events):
    flag = True
    flagreg=True
    owner = None
    owners = set()
    extraowners = []
    codes = []
    # pprint(events)
    assignors = []
    countrydone=[]
    for event in events:
        fcode = event['country'] + ' ' + event['code']
        if fcode not in goodlist:
            # print fcode
            # print 'fcode not in goodlist'+fcode
            if fcode in ['EP REG'] and flagreg:
                # print 'EP REG'
                gotextraasg=False
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and 'Ref Country Code ' in attrib and 'Ref Legal Event Code ' in attrib:
                        # print 'Owner name  present'
                        if attrib['Ref Country Code ']+' '+attrib['Ref Legal Event Code '] in goodlist and attrib['Ref Country Code '] not in countrydone:
                            # print 'good list ref code'
                            extraowners.append(attrib['Owner name '])
                            gotextraasg=True
                            countrydone.append(attrib['Ref Country Code '])
                            # print  extraowners,countrydone
                        else:
                            if attrib['Ref Country Code '] in countrydone:
                                gotextraasg=True

                    else:
                        continue
                # if gotextraasg:
                #     continue
                continue
                # print 'owners:',owners
                # print 'assignors:',assignors

            elif fcode in ['EP BECH','EP BECN','EP ITPR','EP NLS','EP NLT1','EP ITF']:
                # print 'explicit codes'
                # print countrydone,fcode
                # print attrib
                for attrib in event['attributes']:
                    if 'Owner name ' in attrib and fcode not in countrydone:

                        # print 'added extraowners'+str(attrib['Owner name '])
                        extraowners.append(attrib['Owner name '])
                        countrydone.append(fcode)
                        continue

                    else:
                        continue
                continue
            else:
                continue
        # print 'setting reg flag'
        flagreg=False
        if 'attributes' in event:
            if pn.find('AU') <> -1:
                seqevents = event['attributes']
            else:
                seqevents = reversed(event['attributes'])
            for attrib in seqevents:
                print 'owners:',owners
                print 'assignors:',assignors
                # pprint(attrib)
                if 'Owner name ' in attrib:
                    owner = attrib['Owner name ']
                    flag = False

                    if 'free format text ' in attrib:
                        conven, formerowner, reel, other = processfreeformtext1(attrib['free format text '])
                        if conven.find('SECURITY') == -1:
                            for assignor in assignors:
                                if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                    flag = True
                            if not flag:
                                codes.append(event['country'] + ' ' + event['code'])
                                owners.add(owner)
                            assignors.append([id, pn, event['country'], formerowner, event['date']])
                    else:
                        for assignor in assignors:
                            if ' '.join(assignor[3]).find(owner.split(',')[0]) <> -1:
                                flag = True
                        if not flag:
                            codes.append(event['country'] + ' ' + event['code'])
                            owners.add(owner)

        # if pn.find('US') == -1:
        #     continue
        # elif owners:
        #     break
    if owners or extraowners:
        return id, pn, codes, owners,extraowners,countrydone

def processfreeformtext1(texti):
    spltext = texti.split(';')
    conveyance = spltext[0]
    assigneeflag = False
    assigneelist = []
    signdate = None
    rf = None
    #free form text of legal event attribute has signing date, assignor, Reel frame or signing fields in random order

    for text in spltext:
        # print text
        if text.startswith('SIGNING DATES'):
            spltext.remove(text)
        if text.startswith('ASSIGNOR:') or text.startswith('ASSIGNORS:'):
            assigneeflag = True
            assigneelist.append(text.split(':')[1])
            continue
        if text.startswith('REEL/FRAME:'):
            rf = text.split(':')[1]
            assigneeflag = False
        if text.startswith('SIGNING'):
            signdate = text
            assigneeflag = False
        if assigneeflag:
            assigneelist.append(text)
        if conveyance == text:
            if len(text.split(':')) == 2:
                assigneelist.append(text.split(':')[1].strip())
    # print assigneelist
    return conveyance, assigneelist, rf, signdate


def getLatestOwner(id, pn, events):
    flag = True
    owner = None
    for event in events:
        if 'attributes' in event:
            for attrib in event['attributes']:
                if 'Owner name ' in attrib:
                    owner = attrib['Owner name ']
                    return id, pn, event['country'] + ' ' + event['code'], owner, event['date']


def worker(tid, legxml):
    condest = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    curinst = condest.cursor()
    count = 0
    pcount = 0
    while True:
        rec = legxml.get()
        try:
            if rec is None:
                print 'thread id' + str(tid) + 'got None'
                curinst.close()
                condest.commit()
                condest.close()
                return
            pn = rec[0]
            legalid = rec[1]
            pubid = rec[2]
            legxmldoc = rec[3]
            if legxmldoc:
                res = parselegalxml( legxmldoc)
                # print res
                if res:
                    events = res
                    '''id,effdate,legalcode,conv,assignors,assignees,raw'''
                    legasgrecs=[]
                    for event in events:
                        fcode = event['country'] + ' ' + event['code']

                        if fcode in goodlist:
                            conven=''
                            formerowner=''
                            reel=''
                            other=''
                            effdate=event['attributes']['Effective Date '] if 'Effective Date ' in events['attributes'] else None
                            if event['country']=='US':
                                conven, formerowner, reel, other = processfreeformtext1(event['attributes']['free format text '])
                    # res1 = getLatestOwnerNew(pn, events)
                    # print res1
                    # if res1:
                    #     nid, npn, code, owner,extraowner,extracountries = res1
                    #     count += 1
                    #     # print nid,npn,owner,extraowner,extracountries,'R' if owner else 'K'
                    #     # curinst.execute(
                    #         # 'insert into public.current_asg_legal(legal_status_id ,publication_id,codes,owners,extraowner,extracountries,keepreplace)values(%s,%s,%s,%s,%s,%s,%s)',
                    #         # (legalid, pubid, code, [x for x in owner],extraowner,extracountries,'R' if owner else 'K'))
                    #     print (legalid, pubid, code, [x for x in owner],extraowner,extracountries,'R' if owner else 'K')

            if count % 1000 == 0 and count - pcount != 0:
                print'thread ' + str(tid) + ': done insert records:' + str(count)
                pcount = count
                condest.commit()
        except:
            print 'errorin processing : ' + str(rec[0])
            traceback.print_exc()


if __name__ == '__main__':
    # start = 0
    # size = 100000
    # startt = time.time()
    # countglb = 0
    # conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    #
    # cur = conn.cursor()
    # NUMBER_OF_PROCESSES = 18
    #
    # jobs = Queue(100000)
    #
    # p = [Process(target=worker, args=(i, jobs))
    #      for i in xrange(NUMBER_OF_PROCESSES)]
    # for proc in p:
    #     proc.start()
    # for batches in xrange(1, 1701):
    #     print 'batch: ' + str(batches)
    #     rng = "b.publication_id >=" + str(start) + "AND b.publication_id<" + str(start + size)
    #     start = start + size
    #     sth = "select b.ucid, a.legal_status_id,a.publication_id,a.content from xml.t_legal_status  a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.country='BR' AND  " + rng
    #     # print sth
    #     cur.execute(sth)
    #
    #     counter = 0
    #     for x in cur.fetchmany(100000):
    #         jobs.put(x)
    #         # print 'put row in jobs done'
    #         # print x
    #         counter += 1
    #         countglb += 1
    #         while True:
    #             if jobs.empty():
    #                 counter = 0
    #                 break
    #             if counter < 100000:
    #                 break
    #
    # for w in xrange(NUMBER_OF_PROCESSES):
    #     print 'end of inputs putting none'
    #     jobs.put(None)
    #
    # endt = time.time()
    # cur.close()
    # conn.close()
    # for proc in p:
    #     proc.join()
    # print (endt - startt)
    start = 0
    size = 100000
    startt = time.time()
    countglb = 0
    conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")

    cur = conn.cursor()
    NUMBER_OF_PROCESSES = 1

    jobs = Queue(10000)
    pnlist='''EP-2506614-B1
EP-2673978-B1
EP-2506614-A1
EP-2635070-B1
EP-2219303-B1
EP-2219303-A1
EP-2429229-B1
EP-2673978-A1
EP-2429229-A1
EP-2635070-A1
EP-2219303-A4
EP-2702818-A1
EP-2702818-A4'''.splitlines()
    # pnlist=["EP-0010903-A2"]
    pnlist=["US-H2204-H1"]

    p = [Process(target=worker, args=(i, jobs))
         for i in xrange(NUMBER_OF_PROCESSES)]
    for proc in p:
        proc.start()
    for batches in xrange(1, 2):
        print 'batch: ' + str(batches)
        # rng = "b.ucid in " + str(start) + "AND b.publication_id<" + str(start + size)
        rng="b.ucid in ('"+"','".join(pnlist)+"')"
        start = start + size
        sth = "select b.ucid, a.legal_status_id,a.publication_id,a.content from xml.t_legal_status  a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND  " + rng
        # print sth
        cur.execute(sth)

        counter = 0
        for x in cur.fetchmany(10000):
            jobs.put(x)
            pprint(x)
            # print 'put row in jobs done'
            # print x
            counter += 1
            countglb += 1
            while True:
                if jobs.empty():
                    counter = 0
                    break
                if counter < 10000:
                    break

    for w in xrange(NUMBER_OF_PROCESSES):
        # print 'end of inputs putting none'
        jobs.put(None)

    endt = time.time()
    cur.close()
    conn.close()
    for proc in p:
        proc.join()
    print (endt - startt)
