# -*- coding: utf-8 -*-
import psycopg2
import xml.etree.ElementTree as ET
import logging
import string
from fuzzywuzzy import fuzz
from pprint import pprint

from partiesparser import partiesparser

logging.basicConfig(level=logging.INFO)
from collections import defaultdict, OrderedDict, Counter
# import jellyfish
import sys

sys.path.append('/home/analytics/Developer/an')
from assignee_normalization import assigneeNormalization


class graph(object):
    def __init__(self):
        self.graph = {}
        self.nodes = set()

    def addedge(self, start, end):
        self.graph[start] = end
        self.nodes.add(start)
        self.nodes.add(end)

    def traverse(self):
        start = self.nodes.difference(set(self.graph.values()))
        print start, type(start)


class assigneecalc:
    def __init__(self):
        self.tbl = {ord(c): u' ' for c in u'.-,;'}
        self.conn = psycopg2.connect(
            "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
        self.cur = self.conn.cursor()
        self.parryparser = None
        self.namepref = ['epo', 'intermediate', 'original']
        self.asnorm = assigneeNormalization()

    def finalize(self):
        self.cur.close()
        self.conn.close()
    def getnameid(self,data):
        names = defaultdict(set)
        recs = []
        nameid = {}
        sugtoultimate = {}
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                recs += data[key]

            if key == 'reassignments':
                for rec in data[key]:
                    recs += rec[3] + rec[4]

        for rec in recs:
            found = False
            for name in names:
                normname = rec['name']
                try:
                    norm = self.asnorm.get_normalized_assignee(rec['name'])
                    normname = norm[1]
                    sugtoultimate[norm[1]] = norm[2]

                except Exception, e:
                    print e
                isecho = False
                if normname == rec['name']:
                    isecho = True
                # if (not isecho and name == normname) or (fuzz.token_sort_ratio(name, rec['name']) >= 90 or ''.join(
                #         name.lower().split(' ')[0:1]).startswith(''.join(rec['name']).lower().replace('.','').split(
                #         ' '))):  # and fuzz.partial_ratio(name,rec['name'])==90:
                nameproc=' '.join(name.lower().replace('.', '').replace(',',' ').split(' ')[0:2])
                recnameproc=' '.join(rec['name'].lower().replace('.', '').replace(',',' ').split(' '))
                # print nameproc,recnameproc
                if (not isecho and name == normname) or (fuzz.token_sort_ratio(name, rec['name']) >= 80 or nameproc.startswith(recnameproc)):
                    names[name].add(rec['name'])
                    # print 'matched'
                    found = True
                    break
            if not found:
                normname = rec['name']
                try:
                    norm = self.asnorm.get_normalized_assignee(rec['name'])
                    normname = norm[1]
                    sugtoultimate[norm[1]] = norm[2]
                except Exception, e:
                    print e
                names[normname].add(rec['name'])
        # print names
        for key in names:
            for variation in names[key]:
                nameid[variation] = key
        return names,nameid,sugtoultimate

    def getconvtype(self,convtext):
        if not convtext:
            return 'dummy'
        convtext=convtext.lower()
        if convtext.find('secur')<>-1:
            if convtext.find('releas')<>-1:
                return 'secrel'
            else:
                return 'secagr'
        if convtext.find('patent')<>-1 and convtext.find('releas')<>-1:
            return 'secrel'
        return 'asg'

    def getgraph(self,data):
        names, nameid, sugtoultimate = self.getnameid(data)
        invapplasgsets = defaultdict(set)
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                for rec in data[key]:
                    invapplasgsets[key].add(nameid[rec['name']])
        curowners = set()
        curowners.update(invapplasgsets['inv'])
        # print 'start',invapplasgsets['inv']
        # calculate current asg
        reassignmentnew = {}
        n = 1
        for date, reel, conv, asg, asn in reversed(data['reassignments']):
            reassignmentnew[n] = [date, reel, conv, set([nameid[x['name']] for x in asg]),
                                  set([nameid[x['name']] for x in asn])]
            n += 1

        while True:
            next=[]
            nextids=[]
            prevlen=len(reassignmentnew)
            for k in sorted(reassignmentnew):
                date, reel, conv, asg, asn = reassignmentnew[k]
                if self.getconvtype(conv) in ['secrel','secagr']:
                    continue
                if curowners.intersection(asn):
                    nextids.append(k)
            #         next.append(reassignmentnew[k])
            # print '-----'
            # print curowners
            # print next
            # print '----'
            for date, reel, conv, asg, asn in next:
                curowners=curowners.difference(asn)
            for date, reel, conv, asg, asn in next:
                curowners.update(asg)
            for key in nextids:
                del reassignmentnew[key]

            if len(reassignmentnew)==prevlen:
                # print reassignmentnew
                break
        return curowners
    def getcurowners(self, data,parsechain=1):
        # pprint(data)
        names = defaultdict(set)
        recs = []
        nameid = {}
        sugtoultimate={}
        invapplasgsets = defaultdict(set)
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                recs += data[key]

            if key == 'reassignments':
                for rec in data[key]:
                    recs += rec[3] + rec[4]

        for rec in recs:
            found = False
            for name in names:
                normname = rec['name']
                try:
                    norm=self.asnorm.get_normalized_assignee(rec['name'])
                    normname = norm[1]
                    sugtoultimate[norm[1]]=norm[2]

                except Exception, e:
                    print e
                isecho = False
                if normname == rec['name']:
                    isecho = True
                # if (not isecho and name == normname) or (fuzz.token_sort_ratio(name, rec['name']) >= 90 or ''.join(
                #         name.lower().split(' ')[0:1]).startswith(''.join(rec['name']).lower().replace('.','').split(
                #         ' '))):  # and fuzz.partial_ratio(name,rec['name'])==90:
                if (not isecho and name == normname) or (fuzz.token_sort_ratio(name, rec['name']) >= 90 or (''.join(
                        name.lower().split(' ')[0:2])).startswith(
                    ''.join(rec['name'].lower().replace('.', '').split(
                        ' ')))):
                    names[name].add(rec['name'])
                    found = True
                    break
            if not found:
                normname = rec['name']
                try:
                    norm = self.asnorm.get_normalized_assignee(rec['name'])
                    normname = norm[1]
                    sugtoultimate[norm[1]] = norm[2]
                except Exception, e:
                    print e
                names[normname].add(rec['name'])
        # print names
        for key in names:
            for variation in names[key]:
                nameid[variation] = key
        ## get normname here



        # for name in sorted(names):
        #     print name,names[name]
        for key in data:

            if key in ['inv', 'appl', 'asg']:
                for rec in data[key]:
                    invapplasgsets[key].add(nameid[rec['name']])
        curowners = set()
        curowners.update(invapplasgsets['inv'])
        # print 'start',invapplasgsets['inv']
        # calculate current asg
        reassignmentnew = {}
        n = 1
        for date, reel, conv, asg, asn in reversed(data['reassignments']):
            reassignmentnew[n] = [date, reel, conv, set([nameid[x['name']] for x in asg]),
                                  set([nameid[x['name']] for x in asn])]
            n += 1
        # pprint(reassignmentnew)
        if parsechain==1:
            chain = OrderedDict()

            for x in xrange(0, len(reassignmentnew)):
                todel = -1
                for k in sorted(reassignmentnew):
                    date, reel, conv, asg, asn = reassignmentnew[k]
                    # print date, reel, conv, asg, asn
                    # print 'owners current',curowners
                    common = set(asn).intersection(curowners)
                    if not conv:
                        conv = 'dummy conveyance data'

                    if len(common) > 0 and (
                                    'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower())):
                        chain[k] = (date, reel, conv, asg, asn)
                        # print 'chain',date,reel,conv,asg,asn
                        todel = k
                        for kname in common:
                            curowners.remove(kname)

                        for kname in asg:
                            curowners.add(kname)

                        break
                if todel <> -1:
                    del reassignmentnew[todel]

            # for key in chain:
            #     print key, chain[key]
        brokenowners = Counter()
        secrelease=set()
        brokenassigners = Counter()
        for key in sorted(reassignmentnew):
            date, reel, conv, asg, asn = reassignmentnew[key]
            # print reassignmentnew[key]
            if not conv:
                conv = 'dummy conveyance data'
            for asgname in asn:
                # print asgname, 'in sorted reasg'
                if asgname in brokenowners and brokenowners[asgname] > 0:
                    brokenowners[asgname] = brokenowners[asgname] - 1
                    # print 'decrement', asgname, brokenowners
            if 'secur' not in conv.lower() or ('secur' in conv.lower() and 'releas' in conv.lower()):
                # print 'added', asg
                brokenowners.update(asg)
                if 'secur' in conv.lower() and 'releas' in conv.lower():
                    secrelease.update(asg)
                # else:
                #     # print 'skipped'
        # print brokenowners,curowners
        # print invapplasgsets['inv']
        ownersfinal = curowners.difference(invapplasgsets['inv'])
        # print ownersfinal
        # print brokenowners
        return set([sugtoultimate[x] for x in ownersfinal]),set([sugtoultimate[x] for x in brokenowners if brokenowners[x] > 0]),secrelease





    def getcurrentasg(self, pnlist,parsechain=1):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()
                owners,brkowner,secowner = self.getcurowners(self.parryparser.data,parsechain)
                res = [rec[0], owners, brkowner,secowner]
                recs.append(res)
                # print res
        # print res
        return recs

    def getnormalizedlist(self, pnlist):
        inq = '(\'' + '\',\''.join(pnlist) + '\')'
        cur = self.conn.cursor()
        sth = (
            "select b.ucid,content orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in " + inq + "")
        cur.execute(sth)
        recs = []
        namenorm = {}
        for rec in cur.fetchall():
            if rec[1]:
                # print rec[0]
                # print rec[1]
                namerec = []
                self.parryparser = partiesparser(pxmltext=rec[1])
                self.parryparser.getpartiesdata(['inv', 'appl', 'asg'])
                self.parryparser.getreasgdata()

                data = self.parryparser.data
                for key in data:

                    if key in ['inv', 'appl', 'asg']:
                        namerec += data[key]

                    if key == 'reassignments':
                        for recd in data[key]:
                            namerec += recd[3] + recd[4]
                for nrec in namerec:
                    # print nrec
                    name, sug, parent = self.asnorm.get_normalized_assignee(nrec['name'])
                    recs.append([rec[0], name, sug, parent])

                    # print res
        # print res
        return recs
import csv
if __name__ == '__main__':
    # g=graph()
    # g.addedge(1,2)
    # g.addedge(2,3)
    # g.traverse()
    ca = assigneecalc()
    googlecurasg={}
    with open('1237googlepatents.csv')as gpf:
        gr=csv.reader(gpf)
        for pn,curasglisttext,status in gr:
            curasglist=eval(curasglisttext)
            normlist=map(ca.asnorm.get_normalized_assignee,curasglist)
            asgdict={key: val for key, val in zip(curasglist,normlist)}

            googlecurasg[pn]=asgdict
            # break
    count=0
    with open('1237compare.csv','wb') as outf:
        ow=csv.writer(outf)
        ow.writerow(['pn','googleasg','nochainasg','nochainsec','chainasg','chainbroken','chainsecrelease','matched'])
        for pn in googlecurasg:
            asgdict=googlecurasg[pn]
            googleasg=set([asgdict[x][2] for x in asgdict])
            recs=ca.getcurrentasg([pn],0)
            pn,orig,nochainasg,nochainsec=recs[0]

            recs=ca.getcurrentasg([pn], 1)
            pn, chainasg, chainbroken, chainsec = recs[0]

            matched=[]
            if googleasg==nochainasg:
                matched.append('nochain')
            if googleasg==chainasg:
                matched.append('withchain')
            print pn,matched,googleasg,nochainasg,nochainsec,chainasg,chainbroken,chainsec
            ow.writerow([pn,googleasg,nochainasg,nochainsec,chainasg,chainbroken,chainsec,matched])
            count+=1


    # with open('1237.csv')as inf:
    #     with open('1237out.csv','wb')as outf:
    #         cr=csv.reader(inf)
    #         cw=csv.writer(outf)
    #         pnlist=[]
    #         incount = 0
    #         for pn, in cr:
    #             incount+=1
    #             pnlist.append(pn)
    #         # pnlist=['US-7842936-B2', 'US-RE45552-E1', 'US-8648318-B2']
    #         # print pnlist
    #
    #         outcount=0
    #         for x in xrange(0,len(pnlist),200):
    #             r=ca.getcurrentasg(pnlist[x:x+200])
    #             for pn,assignee,extraasg in r:
    #                 print pn
    #
    #                 cw.writerow([pn,assignee,extraasg])
    #                 outcount+=1
    #             # break
    # # ca.cur.execute("select b.publication_id,content  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid = 'US-6715055-B1'")
    # # for id,partiesxml in ca.cur:
    # #     print ca.getcurrentasg(id,partiesxml)
    # # print ca.getcurrentasg(['US-6715055-B1'])
    # # print ca.getcurrentasg(['US-6972255-B2'])
    # # print ca.getcurrentasg(['US-8560741-B2'])
    # # print ca.getcurrentasg(['US-8468247-B1'])
    # # print ca.getcurrentasg(['US-20140167130-A1'])
    # #
    ca.finalize()
