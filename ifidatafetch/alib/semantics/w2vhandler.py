from multiprocessing import Process,Pool
import os,time
from gensim.models import Word2Vec
import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s  %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')
def loadmodel(path):
        if os.path.exists(path):
            logging.info('loading model at '+path)
            time.sleep(10)
            return Word2Vec.load(path,mmap='r')

class w2vhandler:
    def __init__(self,paths):
        self.paths=paths
        self.models=[]
        self.done=0
        self.state=0
    def updatestatus(self):
        self.done+=1
    def load(self):
        self.state=1
        loadproc=[]
        for m in self.paths:
            loadproc.append(Process(target=loadmodel, args=(m,)))
        self.state=2
        self.models=[g.get() for g in gets]
        self.state=3
    def main(self):
        if self.models:
            return self.state
        else:
            if self.state==0:
                self.load()


            return self.state





if __name__=='__main__':
    wh=w2vhandler(['../../models/mymodel','../../models/mymodel'])
    print wh.main()
    print wh.main()