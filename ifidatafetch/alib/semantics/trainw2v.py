from nltk.tokenize import RegexpTokenizer
from gensim.models.word2vec import Word2Vec
tokenizer=RegexpTokenizer(r'\w+._\w+|\w+')
import logging
import csv
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
logging.root.setLevel(level=logging.INFO)

class taggeriter:
    def __init__(self,filename):
        self.iterator=csv.reader(open(filename))
    def __iter__(self):

        for rec in self.iterator:
            yield rec[0].split('|')

from collections import Counter
if __name__=='__main__':

    sentences=taggeriter('INVDATA.CSV')
    model = Word2Vec(sentences,size=30,window=10,iter=2,min_count=0)
    model.save('cpcvec')
    # new_model=Word2Vec.load('models/mymodel')