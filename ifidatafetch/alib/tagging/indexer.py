

from  multiprocessing import Queue,Process

import csv,json

def worker(id,inqueue,outqueue):
    count=0
    while True:
        rec= inqueue.get()

        if not rec:
            return
        fieldnames = ("term_stem","term_lc_stem","final_form")
        titlelc ,title ,redirect ,infobox ,categories,related,source=rec

        # print wf
        row={}
        row['id']=titlelc.lower().strip()
        row['source']=source
        row['term_lc_stem']=title
        row['final_form']=redirect
        row['infobox']=infobox
        row['categories']=categories.split('|;') if categories else []
        row['term_stem']=title
        row['title']=title
        outqueue.put([row])
        count+=1
        if count%100000==0:
            print 'done: '+str(count)+' by worker: '+str(id)

def jsonwriter(id,writqueue):
    termid=9000000
    count=0
    size=500000
    fc=0
    numfiles=-1
    filenameprefix='./outnpkb'
    jsonfile = open(filenameprefix+str(fc)+'.json', 'wb')
    jsonfile.write('[\n')
    while True:
        rows=writqueue.get()
        if not rows:
            if count!=0:
                jsonfile.write('\n')
                jsonfile.write(']')
                jsonfile.flush()
                jsonfile.close()
                return
        termid+=1
        # print rows
        for row in rows:
            try:

                row['termid']=termid
                count+=1
                if count <=size:


                    if count!=1:
                        jsonfile.write(',')
                    json.dump(row, jsonfile)
                    jsonfile.write('\n')
                else:
                    jsonfile.write(']')
                    jsonfile.flush()
                    jsonfile.close()

                    fc+=1


                    count=0
                    # idcount=0
                    if (fc>=numfiles) and (numfiles!=-1):
                        breaked=True
                        break

                    count+=1
                    jsonfile = open(filenameprefix+str(fc)+'.json', 'wb')
                    jsonfile.write('[\n')
                    row['termid']=termid
                    json.dump(row, jsonfile)
                    jsonfile.write('\n')
            except:
                print 'error writing rec to json'


if __name__=='__main__':


    taxfile='/Volumes/Partition2/pcsanalytics/outnounphrese_cleaned1.csv'
    procqueue=Queue(1000)
    outqueue=Queue(1000)
    NUMPROC=4
    procs=[]
    for num in range(NUMPROC):
        procs.append(Process(target=worker, args=(num, procqueue,outqueue)))

    for p in procs:
        p.start()
    wproc=Process(target=jsonwriter,args=(NUMPROC+1,outqueue))
    wproc.start()
    with open(taxfile)as inf:
        cur=csv.reader(inf,delimiter=';')
        for redirect,title in cur:
            titlelc=title
            infobox=''
            categories=''
            related=''
            source='posnoun'
            procqueue.put([titlelc ,title ,redirect ,infobox ,categories,related,source])

    for p in procs:
        procqueue.put(None)
    for p in procs:
        p.join()
    outqueue.put(None)
    wproc.join()
    print 'completed'

