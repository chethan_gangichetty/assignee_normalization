import sys,csv
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
ps=PorterStemmer()
wnl=WordNetLemmatizer()
#
filename=sys.argv[1]
synmap={}
with open(filename) as inf:
    inr=csv.reader(inf)
    for word,count in inr:
        synmap[' '.join(map(ps.stem,word.split))]=(word,count)

for key in synmap:
    print max(synmap[key],key=lambda x:x[1]),synmap[key]