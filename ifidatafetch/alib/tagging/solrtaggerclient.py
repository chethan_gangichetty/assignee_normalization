import requests,unicodedata



s=requests.session()
def tokenizeff(partext,hostname='unicorn',port=2907):

    partext=unicodedata.normalize('NFKD', partext.decode('UTF-8')).encode('ascii','ignore')
    # print type(partext)
    # print partext
    r=s.post('http://'+hostname+':'+str(port)+'/solr/tag?overlaps=LONGEST_DOMINANT_RIGHT&tagsLimit=5000&wt=python&matchText=true&fl=id,term_stem,final_form',partext)
    respdict=eval(r.text)
    # print respdict
    taglist=respdict['tags']
    docs=respdict['matchingDocs']['docs']
    iddict={}
    for doc in docs:
        tag='_'.join(doc['final_form'].split())
        iddict[doc['id']]=tag if type(tag)==str else tag.encode('utf-8')
    lastpos=0
    replacedtxt=[]
    for tags in taglist:
        start=tags[1]
        end=tags[3]

        reptext=iddict[tags[7][0]]

        # print text[lastpos:start]
        # print start
        # print end
        # print type(partext[lastpos:start]),partext[lastpos:start]
        # print type(reptext),reptext
        replacedtxt.append(partext[lastpos:start])
        replacedtxt.append(reptext)
        lastpos=end

    replacedtxt.append(partext[lastpos:])
    # print replacedtxt
    return ' '.join(replacedtxt)
class taggeriter:
    def __init__(self,itr,host='localhost',port=2907):
        self.iterator=itr
        self.host=host
        self.port=port
    def __iter__(self):

        for text in self.iterator:
            yield tokenizeff(text,self.host,self.port)




