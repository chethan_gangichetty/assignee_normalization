import sys
from collections import Counter
sys.path.insert(0,'/Volumes/Partition2/analyticslib')
from pickle import dump
from alib.patentproc.partiesparser import partiesparser
from lib.ifiinterface.connection import connections
c=connections('db1')


query='select content from xml.t_parties limit'
c.cursor.close()


cur=c.connection.cursor('iterparties')
cur.itersize=100
cur.execute(query)
namecounter=Counter()
fnamecounter=Counter()
lnamecounter=Counter()
for contect, in cur:
    p=partiesparser(contect)
    p.getpartiesdata(['inv'])
    invdata= p.data['inv']
    for x in  invdata:
        if 'name' in x:
            namecounter[x['name']]+=1
        if 'fname' in x:
            fnamecounter[x['fname']]+=1
        if 'lname' in x:
            lnamecounter[x['lname']] += 1

dump([fnamecounter,lnamecounter,namecounter],open('invnamecounters.pkl','wb'))

cur.close()
c.connection.close()