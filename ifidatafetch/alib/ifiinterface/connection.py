import psycopg2
class connections:
    def __init__(self,key):
        connectionstr = {}
        connectionstr['db1'] = "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e' port= 5432"
        connectionstr['work1'] = 'dbname="analytics" user="analytics" host="localhost" port=5432'
        self.constr=connectionstr.get(key,key)
        self.connection=psycopg2.connect(self.constr)
        self.cursor=self.connection.cursor()
        # self.namedcursor=self.connection.cursor('default')
    def close(self):
        self.cursor.close()
        # self.namedcursor.close()
        self.connection.close()

if __name__=='__main__':
    con=connections('db1')
    con.close()