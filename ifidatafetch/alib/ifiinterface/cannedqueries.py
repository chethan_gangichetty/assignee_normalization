queries = {}
fields={}
queries['id']="select xml.f_ucid2id('%s')"
queries['pdv'] = '''select ucid publication_no,
    publication_id id,
	lang pub_lang,
    country country,
    to_char(published, 'YYYYMMDD') pub_date,
    family_id famity,
    case when withdraw=true then 'yes' else 'No' end  withdrawn
    from xml.t_patent_document_values '''
queries['patenttitle'] = '''select cast(xpath('/ttl/invention-title/text()',xmlelement(name ttl,xml.f_invention_title(%s, ''))) as text[]) titles'''
queries['applref'] = '''select cast((xpath('/application-reference/@ucid',content))[1] AS text) as applno,
	(xpath('/application-reference//date/text()',content))[1] as date
    from xml.t_application_reference'''
queries['parties'] = '''select content  parties
	from xml.t_parties'''
queries['citations'] = '''select cast(xpath('//@ucid',xmlagg(content)) as text[]) citationspub
from xml.t_citations'''
queries['cpc'] = '''select cast(xpath('//text()',content)as text[]) cpccodes
	from xml.t_classifications_cpc'''
queries['ipcr'] = '''select cast(xpath('//text()',content)as text[]) ipcrcodes
	from xml.t_classifications_ipcr'''
fields['id']=['id']
fields['pdv']=['publication_no','uuid',]

'''{
    "uuid": String,
    "assignees": [
        {
            "organization": String,
        },
        {
            "nameFirst": String,
            "nameLast": String
        }
    ],
    "coInventors": [
        {
            "nameFirst": String,
            "nameLast": String,
        },
    ],
    "cpc": [
        {
            "sectionID": String,
            "subsectionID": String,
        },
    ],
    "ipcr": [
        {
            "classificationLevel": String,
            "ipcClass": String,
            "section": String,
        },
    ],
    "lawyers": [
        {
            "nameFirst": String,
            "nameLast": String,
        }
    ],

    "patent": {
        "title": String,
    },
    "self": {
        "inventorID": String,
        "location": {
            "city": String,
            "state": String,
            "country": String,
        },
        "nameFirst": String,
        "nameLast": String,
        "nameMiddles": List[String],
        "nameSuffixes": List[String],
        "patentID": String,
        "sequence": String,
        "uuid": String,
    },
    "time":{
    "year":number,
    "month":number,
    "day":day
    }
}'''