from cannedqueries import queries
from connection import connections
import os,sys
import urllib
from json import loads
# sys.path.insert(0,'/home/analytics/analyticslib')
sys.path.insert(0,'/Volumes/Partition2/analyticslib')
from alib.patentproc.partiesparser import partiesparser
from pprint import pprint
from collections import defaultdict
from fuzzywuzzy import fuzz
from requests import get
def matchnames(name1, name2):
    procname1 = name1.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()

    procname2 = name2.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()
    name1tw = ' '.join(procname1[:2])
    name2tw = ' '.join(procname2[:2])
    name1full = ' '.join(procname1)
    name2full = ' '.join(procname2)

    if name1full.find(name2tw) <> -1 or name2full.find(name1tw) <> -1 or fuzz.token_set_ratio(name1full,
                                                                                              name2full) >= 90:
        return True
    else:
        return False

def normalizename(name):
    #print name
    url='http://srch1/an/an/'+urllib.quote(urllib.quote(name,safe='~()*!.\''),safe='~()*!.\'')
    #print url
    resp=get(url)
    # print resp.url
    resdict=loads(resp.text)
    
    match,parent,ultimatepar,source=loads(resp.text)
    isnorm=True if source not in ['Echo','Echo2'] else False
    normname={}
    normname['name'] = resdict['cleaned']
    normname['suggested'] = resdict['suggested']
    normname['parent'] = resdict['ultimateparent']
    return normname, isnorm
def getnameid( data):
    names = set()
    nameid = {}
    for key in data:

        if key in ['inv','asg']:
            # for d in  data[key]:
            #     print key,d
            names.update([x['name'] for x in data[key]])

     # print names
    for name1 in sorted(names, key=lambda x: len(x)):
        matched = None

        for name2 in nameid:

            if matchnames(name1, name2):
                matched = name2
                break
        if matched:
            nameid[name1] = nameid[name2]
        else:
            nameid[name1] = name1

    idname = defaultdict(set)
    for key in nameid:
        idname[nameid[key]].add(key)
    return idname,nameid
class ifigetter:
    def __init__(self,db='db1'):
        self.con=connections(db)
    def getdata(self,query):
        # print query
        self.con.cursor.execute(query)
        res=[]

        for rec in self.con.cursor:
            res.append(rec)
        # print res
        return res

    def getinvmentions(self,ucid):
        #print queries['id'] %ucid
        docid = self.getdata(queries['id'] % ucid)[0][0]
        # print docid
        cond = ' where publication_id = ' + str(docid)
        ucid, id, lang, country, pub_date, fam, withdrawn = self.getdata(queries['pdv'] + cond)[0]
        titles = self.getdata(queries['patenttitle'] % docid)[0][0]
        # titles=g.getdata("select xml.f_invention_title(xml.f_ucid2id('US-20040063969-A1'), '')")

        applno, date = self.getdata(queries['applref'] + cond)[0]
        partiesxml = self.getdata(queries['parties'] + cond)[0][0]
        p = partiesparser(partiesxml)
        p.getpartiesdata(['inv', 'asg', 'appl', 'agent', 'examiner'])
        citaitons = self.getdata(queries['citations'] + cond)[0][0]
        cpc = None
        try:
            cpc=self.getdata(queries['cpc'] + cond)[0][0]
        except:
            pass
        ipc = None
        try:
            ipc=self.getdata(queries['ipcr'] + cond)[0][0]
        except:
            pass
        invdict = p.data['inv']
        data = {}
        data["uuid"] = id
        data["pn"] = ucid
        data["publication_date"] = pub_date
        data["family"] = fam
        data["title"] = titles
        data["application_no"] = applno
        data["application_date"] = date
        data["prim_cpc"] = cpc
        data["prim_ipcr"] = ipc
        # print partiesxml
        data["citations"] = citaitons
        # print p.data
        idnames, nameid = getnameid(p.data)
        inv = set([nameid[x['name']] for x in p.data['inv']])
        asg = set([nameid[x['name']] for x in p.data['asg']])
        invnotasg = asg.difference(inv)
        invrecords = defaultdict(dict)
        asgrecords = {}
        for key in p.data:
            # print key
            if key == 'inv':
                records = invrecords
            elif key == 'asg':
                records = asgrecords
            else:
                continue
            for rec in p.data[key]:
                reckey = nameid[rec['name']]
                if reckey not in invrecords:
                    records[reckey] = defaultdict(set)
                for k in rec:
                    records[reckey][k].add(rec[k])
        # print invrecords
        # print asgrecords
        invslots = ['city', 'state', 'country', 'fname', 'lname', 'sequence']
        asgslots = ['city', 'state', 'country']
        asgmentions = []
        for key in asgrecords:
            name = key
            rec = asgrecords[key]
            asgmention = {}
            asgmention['name'] = name
            asgaliases = [name]
            for alias in asgaliases:
                normname = normalizename(alias)
            normname, isnorm = normalizename(name)

            # print normname
            for slot in asgslots:
                if slot in rec:
                    asgmention[slot] = rec[slot].pop()
            asgmention.update(normname)
            asgmentions.append(asgmention)

        invmentions = []
        for key in invrecords:
            rec = invrecords[key]
            invmention = {'self': {}}
            invmention['rawdata']=p.data['inv']
            invmention['self']['name'] = key
            invmention['coinventors'] = []
            cpcl1 = None
            ipcrl1 = None
            if cpc:
                cpcl1 = cpc[0][:4]
                invmention['cpc'] = {'code': cpc}
            if ipc:
                ipcrl1 = ipc[0][:4]
                invmention['ipcr'] = {'code': ipc}
            code = ipcrl1 if ipcrl1 else cpcl1 if cpcl1 else None
            invmention['code'] = {'code': code}
            for s in invslots:
                if s in rec:
                    invmention['self'][s] = rec[s].pop()
            invmention['assignees'] = asgmentions

            # print invmention
            invmentions.append(invmention)

        for rec in invmentions:
            n = rec['self']['name']
            rec['mentionid']=str(id)+str(rec['self']['sequence'])
            rec['patent']=data
            for reccoinv in invmentions:
                nco = reccoinv['self']['name']
                if n <> nco:
                    rec['coinventors'].append(reccoinv['self'])
        return invmentions

if __name__=='__main__':
    print 'testing...'
    g=ifigetter()

    ucid='US-20040063969-A1'
    # ucid='EP-2168359-B1'
    for rec in  g.getinvmentions(ucid):
        pprint(rec)
        print '-----------'

    # record={
    #     "uuid": String,
    #     "assignees": [
    #         {
    #             "organization": String,
    #         },
    #         {
    #             "nameFirst": String,
    #             "nameLast": String
    #         }
    #     ],
    #     "coInventors": [
    #         {
    #             "nameFirst": String,
    #             "nameLast": String,
    #         },
    #     ],
    #     "cpc": [
    #         {
    #             "sectionID": String,
    #             "subsectionID": String,
    #         },
    #     ],
    #     "ipcr": [
    #         {
    #             "classificationLevel": String,
    #             "ipcClass": String,
    #             "section": String,
    #         },
    #     ],
    #     "lawyers": [
    #         {
    #             "nameFirst": String,
    #             "nameLast": String,
    #         }
    #     ],
    #
    #     "patent": {
    #         "title": String,
    #     },
    #     "self": {
    #         "inventorID": String,
    #         "location": {
    #             "city": String,
    #             "state": String,
    #             "country": String,
    #         },
    #         "nameFirst": String,
    #         "nameLast": String,
    #         "nameMiddles": List[String],
    #         "nameSuffixes": List[String],
    #         "patentID": String,
    #         "sequence": String,
    #         "uuid": String,
    #     },
    #     "time":{
    #     "year":number,
    #     "month":number,
    #     "day":day
    #     }
    # }
    #
    #
