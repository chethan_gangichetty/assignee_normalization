import logging
from multiprocessing import Process,Queue
import psycopg2
class echo:
    def __init__(self, id=0):
        self.id = id
        logging.info('echo initialized:' + str(self.id))

    def process(self, rec):
        return rec

    def finalize(self):
        logging.info('echo finalize' + str(self.id))

    def commit(self):
        logging.info('echo commit' + str(self.id))

    def out(self, outrec):
        logging.info(str(outrec) + ' from echo ' + str(self.id))


def setextractworker(id, inque, outque, processor=None, progressepoch=1000):
    count = 0
    if not processor:
        logging.info('not processor class specified exiting....')
        return

        # with codecs.open('cancercorpus_part_'+str(id)+'.txt','wb','utf-8') as outf:
        #   processor.outwriter=outf
    while True:
        patdata = inque.get()
        if patdata:
            result = processor.process(patdata)
            processor.out(result)
            count += 1
            if count % progressepoch == 0:
                processor.commit()
                logging.info('worker id ' + str(id) + ' done records: ' + str(count))
        else:
            processor.commit()
            processor.finalize()
            logging.info('completed worker id ' + str(id) + ', exiting , done records: ' + str(count))
            return

class handler:
    def __init__(self,inputiter,numworker,workerclass):

        self.inputiters=inputiter
        self.inputprocs=[]
        self.workers=[]
        self.outprocs=[]
        self.numworkers=numworker
        self.processor=numworker
        self.inque=Queue(1000)
        self.outque=Queue(1000)
    def basicinput(self,initer,inque):
        for rec in initer:
            inque.put(rec)

    def basicworker(id, inque, outque, processor=None, progressepoch=1000):
        count = 0
        if not processor:
            logging.info('not processor class specified exiting....')
            return
        if outque:
            processor.setmode()
            # with codecs.open('cancercorpus_part_'+str(id)+'.txt','wb','utf-8') as outf:
            #   processor.outwriter=outf
        while True:
            record = inque.get()
            if record:
                result = processor.process(record)
                if result:
                    processor.out(result)
                    count += 1
                    if count % progressepoch == 0:
                        processor.commit()
                        logging.info('worker id ' + str(id) + ' done records: ' + str(count))
            else:
                processor.commit()
                processor.finalize()
                logging.info('completed worker id ' + str(id) + ', exiting , done records: ' + str(count))
                return
    def setup(self):

        inpcount=0;
        for initer in self.inputiters:
            self.inputprocs.append(Process(target=self.basicinput,args=(initer,self.inque)))
            inpcount+=1
        for x in xrange(0,self.numworkers):
            self.workers.append(Process(target=self.basicworker,args=))


#
if __name__ == '__main__':
    con = psycopg2.connect("dbname='analytics' user='postgres'  password=''")
    print con.encoding
    cur = con.cursor()
    query = 'select ucid,title,abstract,claims,description,cast((xpath(\'//text()\',ccode))[1] as text) primc from moonshot.t_ustacd limit 100'
    # query='select pdv.ucid,tacd.title,tacd.abstract,tacd.claims,tacd.description,ex.example from (select * from moonshot.t_patent_document_values limit 10) pdv join moonshot.t_patent_tacd tacd on pdv.publication_id=tacd.publication_id left join moonshot.t_examples ex on pdv.publication_id=ex.publication_id'
    inq = 'insert into moonshot.corpusus values(%s,%s,%s,%s,%s,%s,%s)'

        # strpreproc.removepunctstr
    start = 0
    max = 180000000
    size = 10000
    batches = 0

    inque = Queue(30000)
    numprocs = 1
    processors = [Process(target=setextractworker,
                          args=(
                          x, inque, None, echo("dbname='analytics' user='postgres'  password=''", inq)))
                  for x in xrange(numprocs)]
    # init workers
    for p in processors:
        p.start()
    # input feeding
    counter = 0
    # while start <= max:
    #     batches += 1
    #     logging.info('batch started: ' + str(batches))
    #
    #     cur.execute(query, (start, start + size))
    #     start += size
    #     for rec in cur:
    #         counter += 1
    #         inque.put(rec)
    #         if counter % 10000 == 0:
    #             logging.info('main thread  input done:' + str(counter))
    cur.execute(query, (start, start + size))
    #     start += size
    for rec in cur.fetchall():
        counter += 1
        inque.put(rec)
        if counter % 10000 == 0:
            logging.info('main thread  input done:' + str(counter))

    logging.info('main thread input completed. total records : ' + str(counter))

    # indicate end of job
    for p in processors:
        inque.put(None)
    # wait for all processors
    for p in processors:
        p.join()
