



import psycopg2

import traceback

import time
from multiprocessing import Queue, Process


def worker(tid, legxml):
    condest = psycopg2.connect("dbname='analytics' user='postgres' host='localhost' ")
    curinst = condest.cursor()
    count = 0

    while True:
        rec = legxml.get()


        if rec is None:
            print 'thread id' + str(tid) + 'got None'
            curinst.close()
            condest.commit()
            condest.close()
            return
        ucid = rec[0]
        pubid=rec[1]
        fam=rec[2]

        curinst.execute('insert into analysis.pdv(ucid,publication_id,fam)values(%s,%s,%s)',
                        (ucid,pubid,fam))
        count+=1
                # print tup
        if count % 10000 == 0:
            print'thread ' + str(tid) + ': done insert records:' + str(count)
            condest.commit()



if __name__ == '__main__':
    start = 0
    size = 10000
    startt = time.time()
    countglb = 0


    #############
    # conn = psycopg2.connect("dbname='analytics' user='analytics' host='localhost'")
    conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    cur = conn.cursor()

    NUMBER_OF_PROCESSES = 1

    jobs = Queue(10000)

    p = [Process(target=worker, args=(i, jobs))
         for i in xrange(NUMBER_OF_PROCESSES)]
    for proc in p:
        proc.start()
    for batches in xrange(1, 19999):
        print 'batch: ' + str(batches)
        rng = "part.publication_id >=" + str(start) + "AND part.publication_id<" + str(start + size)
        # rng="pdv.ucid='US-20020116601-A1'"
        start = start + size
        sth ="select pdv.ucid,pdv.publication_id,pdv.family_id  from xml.t_patent_document_values pdv join xml.t_parties part on part.publication_id=pdv.publication_id and " + rng
        # print sth
        cur.execute(sth)

        counter = 0
        for x in cur.fetchmany(10000):
            jobs.put(x)
            # print 'put row in jobs done'
            # print x
            counter += 1
            countglb += 1
            while True:
                if jobs.empty():
                    counter = 0
                    break
                if counter < 10000:
                    break

    for w in xrange(NUMBER_OF_PROCESSES):
        print 'end of inputs putting none'
        jobs.put(None)

    endt = time.time()
    cur.close()
    conn.close()
    for proc in p:
        proc.join()
    print (endt - startt)



