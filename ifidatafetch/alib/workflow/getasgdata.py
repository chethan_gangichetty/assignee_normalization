import psycopg2

import traceback

import time
from multiprocessing import Queue, Process
import os, sys, inspect

# realpath() will make your script run, even if you symlink it :)
sys.path.append('../../')
from lib.patentproc.partiesparser import partiesparser



import csv
if __name__ == '__main__':
    start = 0
    size = 10000
    startt = time.time()
    countglb = 0


    #############
    # conn = psycopg2.connect("dbname='analytics' user='analytics' host='localhost'")
    conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    cur = conn.cursor()

    jobs = Queue(10000)
    batchsize=10
    # slots=['load-source','format','sequence','fname','lname','name','city','state','country','address-1','address-2','address-3','postcode']

    slots = ['name', 'city', 'state', 'country']

    with open('pnlist.csv')as inf:
        inr=csv.reader(inf)
        with open('pnlistasgout.csv','wb')as outf:
            ow=csv.writer(outf)
            ow.writerow(['pn','fam','field']+slots)
            count=0

            pnlist=[]
            for pn, in inr:
                # print pn
                count+=1
                pnlist.append(pn)
                if count%batchsize==0:
                    print 'batch--',count


                    rng="pdv.ucid in ('"+"','".join(pnlist)+"')"
                    print pnlist
                    sth = "select pdv.ucid,pdv.family_id,part.content  from xml.t_parties part join xml.t_patent_document_values pdv on part.publication_id=pdv.publication_id where " + rng
                    # print sth
                    cur.execute(sth)
                    count = 0
                    # slots=['name','city','state','country']

                    counter = 0
                    for x in cur.fetchall():
                        print x
                        pn,fam,partyxml=x
                        print fam,pn
                        pp = partiesparser(partyxml)
                        # pp.getpartiesdata(['inv', 'asg', 'appl', 'agent', 'examiner'])

                        pp.getpartiesdata(['inv', 'asg', 'appl'])
                        # print pp.data
                        for party in pp.data:
                            if not pp.data[party]:
                                continue
                            for record in pp.data[party]:
                                count += 1
                                field = party
                                tup = [pn, str(fam), field]
                                for slot in slots:
                                    if slot in record:
                                        tup.append(record[slot])
                                    else:
                                        tup.append(None)
                                # curinst.execute('insert into analysis.asgdata(ucid,fam,field,source,format,sequence,fname,lname,name,city,state,country,address1,address2,address3,postcode)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                                # tup)
                                print tup
                                ow.writerow(tup)


                    pnlist=[]
                    count=0

            print 'batchlast--'

            rng = "pdv.ucid in ('" + "','".join(pnlist) + "')"
            print pnlist
            sth = "select pdv.ucid,pdv.family_id,part.content  from xml.t_parties part join xml.t_patent_document_values pdv on part.publication_id=pdv.publication_id where " + rng
            # print sth
            cur.execute(sth)
            count = 0
            # slots=['load-source','format','sequence','fname','lname','name','city','state','country','address-1','address-2','address-3','postcode']
            slots = ['name', 'city', 'state', 'country']

            counter = 0
            for x in cur.fetchall():
                pn, fam, partyxml = x
                pp = partiesparser(partyxml)
                # pp.getpartiesdata(['inv', 'asg', 'appl', 'agent', 'examiner'])

                pp.getpartiesdata(['inv', 'asg', 'appl'])
                # print pp.data
                for party in pp.data:
                    if not pp.data[party]:
                        continue
                    for record in pp.data[party]:
                        count += 1
                        field = party
                        tup = [pn, str(fam), field]
                        for slot in slots:
                            if slot in record:
                                tup.append(record[slot])
                            else:
                                tup.append(None)
                        # curinst.execute('insert into analysis.asgdata(ucid,fam,field,source,format,sequence,fname,lname,name,city,state,country,address1,address2,address3,postcode)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        # tup)
                        print tup
                        ow.writerow(tup)
    conn.close()