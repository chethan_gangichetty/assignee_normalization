import traceback
from mysolr import Solr
import psycopg2
import time
from multiprocessing import Queue, Process
from pprint import pprint
import requests
import xml.etree.ElementTree as ET
import csv
# goodlist = '''AT HC
# AT PC
# AT PD9K
# AT TC9K
# AU HB
# AU HC1
# AU PC
# AU PC1
# AU PCD
# AU TC
# BE CH
# BE CN
# BR B25A
# BR B25B
# BR B25C
# BR B25D
# BR B25E
# BR B25F
# BR B25G
# BR B25H
# BR B25I
# BR B25J
# BR B25K
# BR B25L
# BR B25M
# BR PC
# BR PCP
# CH PFA
# CH PFUS
# CH PUE
# CH PUEA
# CN ASS
# CN C56
# DD ASS
# DE 8127
# DE 8327
# DE R081
# EE GB1A
# EE HC1A
# EP RAP1
# EP RAP2
# EP RAP3
# EP RAP4
# ES PC1A
# ES PC1K
# ES PC2A
# FI GB
# FI HC
# FI PC
# FI PCU
# FI TC
# FI TCU
# FR TP
# FR TQ
# GB COOA
# HK AS
# HU DGB9
# HU GB9A
# HU HC9A
# HU HPC4
# IL HC
# IL HP
# LT PC9A
# LT PD9A
# MD GB9A
# MD HC9A
# MD PD4A
# MD PD4K
# MD TC4A
# MX GB
# MX HC
# NL CD
# NL CNR
# NL DD
# NL DNT
# NL NL80
# NL SD
# NL SNR
# NL TD
# NL TNT
# NO CHAD
# NZ ASS
# PT PC3A
# PT PC3K
# PT PC4A
# PT PC4K
# PT PD3A
# PT PD3K
# PT PD4A
# PT PD4K
# SI SP73
# SK PC4A
# SK SPCC
# SK TC4A
# US AS'''.splitlines()

codemap = {}
def parselegalxml(id, pn, legalxml):
    # print 'parsing for pn:'+pn
    # print 'rawxml:'+legalxml
    # print '--------------------'
    eventstag = ET.fromstring(legalxml)
    events = []
    # if eventstag.tag=='legal-status':
    #     pass
    #     #print 'valid legal-status root tag'
    # else:
    #     print 'invalid root tag: '+eventstag.tag
    #     print '=============================='
    #     return

    for eventtag in eventstag:
        event = eventtag.attrib

        for eventbodytag in eventtag:
            for eventdata in eventbodytag:
                if eventdata.tag == 'event-title':
                    event['event title'] = eventdata.text
                else:
                    if eventdata.tag == 'event-attributes':
                        eventattriblist = []
                        for eattribute in eventdata:
                            eventattrib = {}
                            key = ''
                            value = ''
                            for eattribdata in eattribute:

                                if eattribdata.tag == 'event-attribute-label':
                                    key = eattribdata.text
                                if eattribdata.tag == 'event-attribute-value':
                                    value = eattribdata.text
                                if key and value:
                                    eventattrib[key] = value
                                    key = ''
                                    value = ''
                            eventattriblist.append(eventattrib)
                        event['attributes'] = eventattriblist
        try:

            if event['code'] == 'REG' and event['country'] == 'EP' and 'attributes' in event:
                event['effcode'] = event['attributes'][0]['Ref Legal Event Code ']
                event['effcountry'] = event['attributes'][0]['Ref Country Code ']
                if 'Effective Date ' in event['attributes'][0]:
                    event['effdate'] = event['attributes'][0]['Effective Date ']
                effattributes = {}
                for key in event['attributes'][0]:
                    if key not in ['Ref Legal Event Code ', 'Ref Country Code ', 'Effective Date ']:
                        effattributes[key] = event['attributes'][0][key]
            events.append(event)
        except:
            print ' ---------' + str(id) + ':' + str(pn)
            traceback.print_exc()
            # print id,pn
            # pprint(legalxml)

    # pprint(events)~
    # print '=============================='
    return id, pn, events






def worker(tid, legxml):
    # condest = psycopg2.connect("dbname='postgres' user='analytics' host='localhost'")
    # curinst = condest.cursor()
    count = 0
    pcount = 0
    with open('FIDAXOMICIN.csv','wb')as outf:

        ow=csv.writer(outf)
        ow.writerow(['pn','code','effective code','isspccode','def','date','effective date','spc','rawdata'])
        while True:
            rec = legxml.get()
            try:
                if rec is None:
                    print 'thread id' + str(tid) + 'got None'
                    # curinst.close()
                    # condest.commit()
                    # condest.close()
                    return
                pn = rec[0]
                legalid = rec[1]
                pubid = rec[2]
                legxmldoc = rec[3]
                if legxmldoc:
                    res = parselegalxml(pubid, pn, legxmldoc)
                    # print res
                    if res:
                        id, npn, events = res
                        # pprint(events)

                        for event in events:
                            effc = event['effcountry'] + '-' + event[
                                'effcode'] if 'effcode' in event and 'effcountry' in event else event['country']+'-'+event['code']
                            if 'attribute' in event and event['attributes']:
                                for attribute in event['attributes']:

                                    ow.writerow([npn,event['country']+'-'+event['code'],effc,effc in codemap,codemap.get(effc,None),event['date'],event['effdate'] if 'effdate' in event else None,[attribute[key] for key in attribute if 'spc' in key.lower() or 'protec' in key.lower()],event ])
                            else:
                                ow.writerow([npn, event['country'] + '-' + event['code'],
                                            effc,effc in codemap,codemap.get(effc,None),

                                             event['date'], event['effdate'] if 'effdate' in event else None,
                                             None, event])
                if count % 1000 == 0 and count - pcount != 0:
                    print'thread ' + str(tid) + ': done insert records:' + str(count)
                    pcount = count
                    # condest.commit()
            except:
                print 'errorin processing : ' + str(rec[0])
                traceback.print_exc()


if __name__ == '__main__':
    # start = 0
    # size = 100000
    # startt = time.time()
    # countglb = 0
    # conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    #
    # cur = conn.cursor()
    # NUMBER_OF_PROCESSES = 18
    #
    # jobs = Queue(100000)
    #
    # p = [Process(target=worker, args=(i, jobs))
    #      for i in xrange(NUMBER_OF_PROCESSES)]
    # for proc in p:
    #     proc.start()
    # for batches in xrange(1, 1701):
    #     print 'batch: ' + str(batches)
    #     rng = "b.publication_id >=" + str(start) + "AND b.publication_id<" + str(start + size)
    #     start = start + size
    #     sth = "select b.ucid, a.legal_status_id,a.publication_id,a.content from xml.t_legal_status  a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.country='BR' AND  " + rng
    #     # print sth
    #     cur.execute(sth)
    #
    #     counter = 0
    #     for x in cur.fetchmany(100000):
    #         jobs.put(x)
    #         # print 'put row in jobs done'
    #         # print x
    #         counter += 1
    #         countglb += 1
    #         while True:
    #             if jobs.empty():
    #                 counter = 0
    #                 break
    #             if counter < 100000:
    #                 break
    #
    # for w in xrange(NUMBER_OF_PROCESSES):
    #     print 'end of inputs putting none'
    #     jobs.put(None)
    #
    # endt = time.time()
    # cur.close()
    # conn.close()
    # for proc in p:
    #     proc.join()
    # print (endt - startt)

    with open('legalcodes.csv','rU') as codefile:
        cr=csv.reader(codefile)
        for code,desc in cr:
            codemap[code]=desc
    start = 0
    size = 100000
    startt = time.time()
    countglb = 0
    conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")

    cur = conn.cursor()
    NUMBER_OF_PROCESSES = 1

    jobs = Queue(10000)
    pnlist=[]
    with open('c12n0001.csv')as inf:
        for line in inf:
            pnlist.append(line.strip())
    print len(pnlist),pnlist[10]

    # pnlist=["EP-0010903-A2"]
    pnlist=["US-6954628-B2"]
    pnlist='''AU-2008209623-A1
AU-2008209623-A2
EP-1539977-A2
EP-1539977-B1
EP-1539977-A4'''.splitlines()

    p = [Process(target=worker, args=(i, jobs))
         for i in xrange(NUMBER_OF_PROCESSES)]
    for proc in p:
        proc.start()

    for batches in xrange(1, 2):
        print 'batch: ' + str(batches)
        # rng = "b.ucid in " + str(start) + "AND b.publication_id<" + str(start + size)
        for x in xrange(0, len(pnlist),1000):
            rng="b.ucid in ('"+"','".join(pnlist[x:x+1000])+"')"
            print x,x+1000
            start = start + size
            sth = "select b.ucid, a.legal_status_id,a.publication_id,a.content from xml.t_legal_status  a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND  " + rng
            # sth="select pdv.ucid,pdv.publication_id,ls.content from xml.t_legal_status ls join xml.t_patent_document_values pdv on ls.publication_id = pdv.publication_id and pdv.country='EP' and pdv.produced > '2017-01-01'::date"
            # print sth
            cur.execute(sth)

            counter = 0
            for x in cur.fetchmany(10000):
                jobs.put(x)
                # pprint(x)
                # print 'put row in jobs done'
                # print x
                counter += 1
                countglb += 1
                while True:
                    if jobs.empty():
                        counter = 0
                        break
                    if counter < 10000:
                        break

    for w in xrange(NUMBER_OF_PROCESSES):
        # print 'end of inputs putting none'
        jobs.put(None)

    endt = time.time()
    cur.close()
    conn.close()
    for proc in p:
        proc.join()
    print (endt - startt)
