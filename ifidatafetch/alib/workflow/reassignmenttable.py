import psycopg2
import traceback

import time
from multiprocessing import Queue, Process
import os, sys, inspect

# realpath() will make your script run, even if you symlink it :)
sys.path.append('../../')
from alib.patentproc.partiesparser import partiesparser


def worker(tid,queue):
    condest = psycopg2.connect("dbname='analytics' user='postgres' host='srch1' ")
    curdest=condest.cursor()

    count=0
    while True:
        rec=queue.get()
        if not rec:

            condest.commit()
            curdest.close()
            condest.close()
            print 'Completed thread '+str(tid)+' done records '+str(count)+' Ending'

            return
        id,ucid,content=rec
        pp=partiesparser(content)
        pp.getreasgdata()
        for date,reel,conv,assignee,assignor,rawxml,recorddate in pp.data['reassignments']:
            count+=1
            curdest.execute('insert into analysis.reassignments values(%s,%s,%s,%s,%s,%s,%s,%s,%s)',(id,date,reel,conv,[x['name'] for x in assignee],[x['name'] for x in assignor],rawxml,ucid,recorddate))
            if count%10000==0:
                print 'thread '+str(tid)+' done records '+str(count)
                condest.commit()
import csv
if __name__ == '__main__':
    start = 0
    size = 10000
    startt = time.time()
    countglb = 0
    numproc=30
    conn = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e'")
    cur = conn.cursor()
    jobs = Queue(10000)
    procs=[Process(target=worker,args=(x,jobs)) for x in range(numproc)]
    for p in procs:
        p.start()


    for batch in range(19900):
        stop=start+size
        print 'batch done'+str(batch)


        # slots=['load-source','format','sequence','fname','lname','name','city','state','country','address-1','address-2','address-3','postcode']

        slots = ['name', 'city', 'state', 'country']

        sth = "select b.publication_id,b.ucid,content " \
              "from (select * from xml.t_parties where publication_id >= "+str(start)+\
              " AND publication_id <"+str(stop)\
              +") a  join xml.t_patent_document_values b on a.publication_id=b.publication_id and " \
               "array_length(xpath('//reassignments',content),1) is not null"
        cur.execute(sth)
        start=stop
        for pid,ucid,content in cur:
            jobs.put([pid,ucid,content])

    for p in procs:
        jobs.put(None)

    for p in procs:
        p.join()
    print 'all done'


