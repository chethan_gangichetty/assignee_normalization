import requests
from fuzzywuzzy import fuzz
from alib.patentproc.namematcher import getnameid
from alib.patentproc.partiesparser import partiesparser
import string
from mysolr import Solr
session = requests.Session()
import psycopg2
from pprint import pprint
from collections import Counter
from collections import defaultdict
from fuzzywuzzy import process
class namecleaner:
    def __init__(self):
        self.namestopwords=set()
        with open('namestopwords.csv') as namef:
            for line in namef:
                self.namestopwords.add(line.strip().lower())
    def cleanname(self,name):
        tokens = [x.lower().strip() for x in removepunct(name).split()]

        cname=[]
        for t in tokens:
            if t not in self.namestopwords:
                cname.append(t)
        return ' '.join(cname)

cleaner=namecleaner()
def removepunct(name):
    try:
        cleaned=string.translate(name,string.maketrans(string.punctuation," "*len(string.punctuation)))
        return ' '.join(cleaned.split()).lower().strip()
    except:
        return name.lower().strip().replace(',',' ')

def resolveconflict(name,recs):
    #case1:
    tokens= name.split()
    good=False
    for rec in recs:
        for tok in tokens:
            if tok in  rec['name']:
                good=True
    if good==False:
        del(rec)

    return recs
def getaliases(names,records,trace=False,cond=False):
    aliases=set()
    for fam in records:
        newalias=Counter()
        addflag=True
        for ucid in records[fam]:
            matchedseq=[]
            tracestring = []
            for seq in records[fam][ucid]:
                formatpref=['epo','intermediate','original']
                sourceneg=['']
                if trace and cond:
                    tracestring.append([ucid,records[fam][ucid][seq]])
                try:
                    nameseq={(x['format'],x['load-source']):x['name'] for x in records[fam][ucid][seq]['data']}
                except KeyError:
                    print 'name not found'
                    continue

                for formatn in formatpref:
                    done=False
                    for key in nameseq:
                        kformat,ksource=key
                        if formatn == kformat:# and ksource<>'mxw-smt':
                            records[fam][ucid][seq]['cleanestname']=nameseq[(kformat,ksource)]
                            records[fam][ucid][seq]['clnamesource']=formatn
                            done=True

                        break
                    if done:
                        break
                for name in names:

                    score=namematcher(name,records[fam][ucid][seq]['cleanestname'])
                    if score>0:
                        matchedseq.append((seq,score))
            if len(matchedseq)==1:
                newalias.update([records[fam][ucid][seq]['cleanestname'] for seq,score in matchedseq])
                # aliases.update(newalias)
                # newaliasmatch=[x.lower() for x in newalias if 'jun' not in x.lower() or 'wang' not in x.lower()]
                # if newaliasmatch:
                #     pprint(tracestring)
            else:
                addflag=False
                print fam,[records[fam][ucid][seq]['cleanestname'] for seq,score in matchedseq]
                pass
                # print  ucid,records[fam][ucid]
        if addflag:
            aliases.update(newalias)
        # else:
        #     print fam,newalias
    aliasset=set([x  for x,c in aliases.most_common() if c >1 ])
    return aliases

def namematcher(name1,name2):
    tokscore=0
    # print name2
    name1=cleaner.cleanname(name1)
    name2=cleaner.cleanname(name2)
    if name1==name2:
        return len(name1)
    procname1=set()
    procname1.update([x for x in name1.lower().split()])
    procname2=set([x for x in name2.lower().split()])
    commonword=procname1.intersection(procname2)
    # print 'names',procname1,procname2
    # print 'commonword',commonword
    if commonword==procname1 or commonword==procname2:
        return len(''.join(commonword))
    if commonword:
        score=len(commonword)
        notcommon1=procname1.difference(commonword)
        notcommon2=procname2.difference(commonword)
        # print 'not common',notcommon1,notcommon2
        initials1=set([x[0] for x in notcommon1])
        initials2=set([x[0] for x in notcommon2])
        # print 'initials',initials2,initials1
        iscore=max(len(initials1.intersection(initials2)),len(initials2.intersection(initials1)))
        if iscore or len(commonword)>1:
            return iscore+score
        else:
            return 0

    else:
        score=0

    return score
# con=psycopg2.connect("dbname='analytics' user='postgres' host='srch1' ")
# print removepunct('Landa,.benzion')


def getinvrecords(famlist,recsall):
    # query = '\',\''.join([str(x) for x in famlist])
    # print query
    if not famlist:
        return None
    else:
        # cur.execute('select ucid,family_id,content from xml.t_parties p join xml.t_patent_document_values pdv on p.publication_id=pdv.publication_id and pdv.family_id in (\'%s\')' % query)
        recs = []
        for rec in recsall:

            if str(rec[1]) in famlist:
                recs.append(rec)
        # print recs
        return recs
def getfamucidseq(recs):
    famucidseq = {}
    for ucid, fam, content in recs:
        # print ucid,content
        pp = partiesparser(content)
        pp.getpartiesdata(['inv'])
        # print pp.data['inv']

        for dictr in pp.data['inv']:
            seq = dictr['sequence']

            # for word in name.lower().split():
            #     if namer.lower().find(word)<>-1:

            famucidseq.setdefault(fam, {})
            famucidseq[fam].setdefault(ucid, {})
            famucidseq[fam][ucid].setdefault(seq, {'data': []})['data'].append(dictr)

    # print famucidseq
    return famucidseq

def getconnected(node,matched):
    nexts = set()
    nexts.add(node)
    explored = set()
    results = []
    counter = 0
    while True:
        counter = counter + 1
        newnext = set()
        if not nexts:
            break
        for next in nexts:
            if next in matched:

                for n in matched[next]:
                    if n not in explored:
                        results.append(n)
                        newnext.add(n)
                explored.add(next)

        nexts = newnext
    return results,explored
def matchfamnames(fam,famrec):
    pntable=[]
    names=set()
    maxseq=0
    ucidnamesdict={}
    for ucid in famrec:
        localseqcount=0
        ucidname=[]
        for seq in famrec[ucid]:
            localseqcount+=1
            pntable.append([ucid ,seq, famrec[ucid][seq]['cleanestname']])
            names.add(famrec[ucid][seq]['cleanestname'])
            ucidname.append(famrec[ucid][seq]['cleanestname'])
        ucidnamesdict[ucid]=ucidname
        if localseqcount>maxseq:
            maxseq=localseqcount
    done=set()

    for name in names:
        if name not in done:
            done.add(name)

            for ucid in ucidnamesdict:
                matche,score=process.extractOne(name, ucidnamesdict[ucid])
                if score>60:
                    print name,ucid,matche
                    done.add(matche)
    print '---------'
    # print fam,names,pntable
def generatealiases(names,famlistmaster,recsall):
    solr = Solr('http://dev1.dolcera.net:11080/solr/alexandria-standard/',make_request=session,version=4)
    # constr= "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e' port= 5432"
    # con=psycopg2.connect(constr)
    # cur=con.cursor()
    aliases=set()
    famlist=set()
    for name in names:
        pnlist=set()
        tokens = cleaner.cleanname(name).split()
        gt2tok = [x for x in tokens if len(x) > 2]
        if len(gt2tok) < 2:
            continue
        #print 'stdinv:("%s")'%(name)
        response = solr.search(q='stdinv:("%s") '%name,fl='pn,fam',rows=50000)
        docs=response.documents
        for doc in docs:
            if doc:
                if int(doc['fam'])>0:
                    pnlist.add(doc['fam'])
                    famlist.add(doc['fam'])
        pnlist=pnlist.difference(famlistmaster)
        # print pnlist
        recs=getinvrecords(pnlist,recsall)

        if recs:
            print 'gotrecords recs'

            # recsall += recs
            famucidseq=getfamucidseq(recs)


            aliase=getaliases([name.lower().strip()],famucidseq,trace=True,cond=True)
            if aliase:
                aliases.update(aliase)


    return aliases,famlist,recsall
from pickle import load,dump
import unicodecsv as csv
if __name__ == '__main__':

    masternames=[]
    with open('invnames.txt')as inf:
        for line in inf:
            masternames.append(line.strip().upper())
    recsall=load(open('junwangrecs.pkl'))

    masternames=['WANG JUN']#'WANG JUN',
    with open('aliasirs12may.csv','wb') as aliasrep:
        aliaswriter=csv.writer(aliasrep)

        for name in masternames:
            names=set([name])
            newnames=set([name])
            seed=name
            famlistmaster=set()
            while True:

                newnames,famlist,recsall=generatealiases(newnames,famlistmaster,recsall)
                famlistmaster.update(famlist)
                diffnames=newnames.difference(names)
                if not diffnames:
                    break
                else:
                    # print newnames
                    names.update(diffnames)
                    newnames=diffnames
                    print newnames
                    raw_input('press any key for continue')
            aliaswriter.writerow([name,'|'.join(names),'|'.join(famlistmaster)])
    # dump(recsall,open('junwangrecs.pkl','wb'))