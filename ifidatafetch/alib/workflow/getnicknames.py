import requests
from fuzzywuzzy import fuzz
from alib.patentproc.namematcher import getnameid
from alib.patentproc.partiesparser import partiesparser
import string
from mysolr import Solr
session = requests.Session()
import psycopg2
def removepunct(name):
    try:
        cleaned=string.translate(name,string.maketrans(string.punctuation," "*len(string.punctuation)))
        return ' '.join(cleaned.split()).lower().strip()
    except:
        return name.lower().strip().replace(',',' ')

def resolveconflict(name,recs):
    #case1:
    tokens= name.split()
    good=False
    for rec in recs:
        for tok in tokens:
            if tok in  rec['name']:
                good=True
    if good==False:
        del(rec)

    return recs
def getaliases(names,records):
    aliases=set()
    for fam in records:

        for ucid in records[fam]:
            matchedseq=[]
            for seq in records[fam][ucid]:
                formatpref=['epo','intermediate','original']
                nameseq={x['format']:x['name'] for x in records[fam][ucid][seq]['data']}
                for formatn in formatpref:
                    if formatn in nameseq:
                        records[fam][ucid][seq]['cleanestname']=nameseq[formatn]
                        break
                for name in names:
                    score=namematcher(name,records[fam][ucid][seq]['cleanestname'])
                    if score>0:
                        matchedseq.append((seq,score))
            if len(matchedseq)==1:
                aliases.update([records[fam][ucid][seq]['cleanestname'] for seq,score in matchedseq])
            else:
                pass
                # print  ucid,records[fam][ucid]

    return aliases

def namematcher(name1,name2):
    score=0
    name1=removepunct(name1)
    name2=removepunct(name2)
    if name1==name2:
        return 10
    procname1=set()
    procname1.update([x for x in name1.lower().split() if len(x)>2])
    procname2=set([x for x in name2.lower().split() if len(x)>2])
    commonword=procname1.intersection(procname2)
    if commonword:
        score=len(commonword)
        notcommon1=procname1.difference(commonword)
        notcommon2=procname2.difference(commonword)
        initials1=set([x[0] for x in notcommon1])
        initials2=set([x[0] for x in notcommon2])
        iscore=len(initials1.intersection(initials2))
        if iscore:
            return iscore+score
        else:
            return 0

    else:
        score=0

    return score
# con=psycopg2.connect("dbname='analytics' user='postgres' host='srch1' ")
print removepunct('Landa,.benzion')
def generatealiases(names,famlistmaster):
    solr = Solr('http://dev1.dolcera.net:11080/solr/alexandria-standard/',make_request=session,version=4)
    constr= "dbname='alexandria' user='alexandria' host='db1.dolcera.net' password='en-beg-or-um-e' port= 5432"
    con=psycopg2.connect(constr)
    cur=con.cursor()
    aliases=set()
    famlist=set()
    for name in names:
        pnlist=set()
        print 'stdinv:("%s")'%(name)
        response = solr.search(q='stdinv:("%s") '%name,fl='pn,fam',rows=50000)
        docs=response.documents
        for doc in docs:
            if doc:
                if int(doc['fam'])>0:
                    pnlist.add(doc['fam'])
                    famlist.add(doc['fam'])
        pnlist=pnlist.difference(famlistmaster)
        query='\',\''.join(pnlist)
        print query
        if not query:
            continue
        cur.execute('select ucid,family_id,content from xml.t_parties p join xml.t_patent_document_values pdv on p.publication_id=pdv.publication_id and pdv.family_id in (\'%s\')'%query)
        recs=[]
        for rec in cur.fetchall():
            recs.append(rec)
        famucidseq={}
        for ucid,fam,content in recs:
            # print ucid,content
            pp=partiesparser(content)
            pp.getpartiesdata(['inv'])
            # print pp.data['inv']

            for dictr in pp.data['inv']:

                seq=dictr['sequence']
                # for word in name.lower().split():
                #     if namer.lower().find(word)<>-1:
                famucidseq.setdefault(fam,{})
                famucidseq[fam].setdefault(ucid,{})
                famucidseq[fam][ucid].setdefault(seq,{'data':[]})['data'].append(dictr)
        aliase=getaliases([name.lower().strip()],famucidseq)
        if aliase:
            aliases.update(aliase)
    return aliases,famlist
import csv
if __name__ == '__main__':
    masternames='''WANG JUN'''.upper().splitlines()

    with open('aliasreportjunwang.csv','wb') as aliasrep:
        aliaswriter=csv.writer(aliasrep)

        for name in masternames:
            names=set([name])
            famlistmaster=set()
            while True:
                newnames,famlist=generatealiases(names,famlistmaster)
                famlistmaster.update(famlist)

                if not newnames.difference(names):
                    break
                else:
                    # print newnames
                    names.update(newnames)
                    print names,newnames
            aliaswriter.writerow([name,'|'.join(names),'|'.join(famlistmaster)])

        #