from alib.patentproc.partiesparser import partiesparser
import psycopg2
import unicodecsv as csv
from collections import defaultdict
from fuzzywuzzy import fuzz
con = psycopg2.connect("dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' port=5434 password='pergola-uncross-linseed'")
cur = con.cursor()
from pprint import pprint
from unidecode import unidecode_expect_nonascii
import unicodedata

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin-1')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    
with open('br_takatacorp.csv','rU') as infile:
    cr=csv.reader(infile)
    pns=[]
    for ucid,fam in cr:
        pns.append(ucid.strip())
    invslots = ['name','city', 'state', 'country', 'address-1', 'address-2', 'address-2', 'postcode',
                'sequence']
    faminv = {}

    for x in xrange(0,len(pns),1000):
        sublist=pns[x:x+1000]
        inq = '(\'' + '\',\''.join(sublist) + '\')'
        cur.execute(
            "select b.publication_id,b.family_id,b.ucid,content,b.published  orgiassignee from xml.t_parties a  join xml.t_patent_document_values b on a.publication_id=b.publication_id AND b.ucid in" + inq)

        for id,family,ucid, partyxmltext,pd in cur:
            # print id,partyxmltext
            p=partiesparser(partyxmltext)
            p.getpartiesdata(['inv'])
            for inv in p.data['inv']:
                done=False
                if family not in faminv:
                    faminv[family]={}
                for name in faminv[family]:
                    name=unidecode_expect_nonascii(strip_accents(name))
                    inv['name']=unidecode_expect_nonascii(strip_accents(inv['name']))
                    if fuzz.ratio(inv['name'],name)>70 or fuzz.token_sort_ratio(inv['name'],name)>70:
                        print name,'|', inv['name'],'|', fuzz.ratio(inv['name'], name), fuzz.token_set_ratio(inv['name'], name)
                        done=True

                        for key in inv:

                            faminv[family][name][key].add(inv[key])
                        break
                if not done:
                    if inv['name'] not in faminv[family]:
                        faminv[family][inv['name']]=defaultdict(set)
                    for key in inv:
                        faminv[family][inv['name']][key].add(inv[key])
    invslots = ['name', 'city', 'state', 'country', 'address-1', 'address-2', 'address-2', 'postcode',
                'sequence']
    with open('takatainv.csv','wb') as outf:

        rr=csv.writer(outf)
        rr.writerow(['familyid', 'name'] + invslots)
        for fam in faminv:
            for name in faminv[fam]:
                rr.writerow([fam,name]+['|'.join(faminv[fam][name].get(x,[])) for x in invslots])


