import sys
import csv
import psycopg2

csv.field_size_limit(sys.maxsize)

def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor('iter')
cur2.itersize = 10000
query2 = "select * from dolcera.primary_cpc_bala"

cur2.execute(query2)
pubid = {}
primcpc = {}
counter = 0
for p,u,_,c,_ in cur2:
    counter += 1
    if counter%500000 == 0:
        print counter
    pubid[u] = p
    primcpc[u] = c

reader = readCSV("../INVDATA1.csv")
writer = writeCSV("../INVDATA1_New.csv")
writer2 = writeCSV("../missing_pubids.csv")
counter = 0
missed_ucids = set()
missed_ucids_rows = []
for row in reader:
    counter += 1
    if counter%100000 == 0:
        print counter
    try:
        array = []
        array.extend(row)
        u = row[0].split(" ")
        class_code = primcpc[u[0]]
        array.append(class_code)
        writer.writerow(array)
    except:
        try:
            missed_ucids.add(pubid[u[0]])
        except:
            pass
for miss in missed_ucids:
    writer2.writerow([miss])