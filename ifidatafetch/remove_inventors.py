import logging
import psycopg2
from multiprocessing import Queue, Process
import psycopg2.extensions

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
import time

import csv
# analyticslibpath='/Volumes/Partition2/analyticslib'
import sys
import psycopg2
from pprint import pprint

# sys.path.insert(0,analyticslibpath)
from multiprocessing import Process, Queue

def writeCSV(path, mode="w"):
    import unicodecsv as csv
    myfile = open(path, mode)
    fileOutput = csv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput, myfile

def outworker(id, writer, fileobj, outque):
    outcount = 0
    while True:

        outrec = outque.get()
        # print outrec
        if not outrec:
            print 'output thread ', id, 'completed. wrote records', outcount
            fileobj.close()
            fileobj.flush()
            return
        writer.writerow(outrec.values())
        if outcount % 10000 == 0:
            fileobj.flush()

def matchnames( name1, name2):
    procname1 = name1.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()

    procname2 = name2.lower().replace('.', '').replace(',', ' ').replace('"', '').replace('\\', '').split()
    name1tw = ' '.join(procname1[:2])
    name2tw = ' '.join(procname2[:2])
    name1full = ' '.join(procname1)
    name2full = ' '.join(procname2)
    name1str = capiq.getStrippedName(name1)
    name2str = capiq.getStrippedName(name2)
    if name1full.find(name2tw) <> -1 or name2full.find(name1tw) <> -1 or fuzz.token_set_ratio(name1full,
                                                                                              name2full) >= 90 or \
            name1str.find(name2str) <> -1 or name2str.find(name1str) <> -1:
        return True
    else:
        return False

def getdata(id, inque, outque):
    con = psycopg2.connect(
        "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port=5434")
    cur = con.cursor()
    count = 0
    while True:
        fampub = inque.get()
        if not fampub:
            print 'thread id ', id, 'completed total records done :', count
            cur.close()
            con.close()
            return

        slots = ['Application_ID', 'Country', 'famid', 'Assignee', 'Sequence', 'Format', 'IFI', 'City', 'UCID', 'Party']
        logging.info(slots)
        famrec = []
        allinv = set()
        for rec in fampub:
            record = dict(zip(slots, rec))
            famrec.append(record)

        for rec in famrec:
            if rec['Party'] == 'inventors':
                allinv.add(rec['Assignee'])

        final_famrec = []
        for record in famrec:
            flag = 0
            for name in allinv:
                if matchnames(name,record['Assignee']):
                    flag = 1
                    break
            if flag == 0:
                final_famrec.append(record)
        outque.put(final_famrec)

if __name__ == "__main__":

    con = psycopg2.connect(
        "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' port='5434'  password='pergola-uncross-linseed'")
    cursor = con.cursor('iter')
    cursor.itersize = 10000
    query = """select Application_ID,Country,Family_ID,Assignee,Sequence,Format,IFI,City,UCID,Party from dolceradata.ifidata where family_id = '3243269'"""
    fampub = []
    lastfam = None

    numproc = 1
    processes = []
    inque = Queue(10000)
    outque = Queue(10000)
    for x in xrange(0, numproc):
        processes.append(Process(target=getdata, args=(x, inque, outque)))
    for p in processes:
        p.start()
    zzz, fileobj = writeCSV("../asgTable_removeinventors.csv")

    outproc = Process(target=outworker, args=(1, zzz, fileobj, outque))
    outproc.start()
    start_time = time.time()

    cursor.execute(query)
    count = 0
    for Application_ID,Country,famid,Assignee,Sequence,Format,IFI,City,UCID,Party in cursor:
        if lastfam != famid and fampub:
            count += 1
            if count % 100000 == 0:
                print "input" + str(count)
            inque.put(fampub)
            fampub = []
        fampub.append([Application_ID,Country,famid,Assignee,Sequence,Format,IFI,City,UCID,Party])
        lastfam = famid
    if fampub:
        inque.put(fampub)
    for p in processes:
        inque.put(None)
    for p in processes:
        p.join()
    outque.put(None)
    outproc.join()

    print time.time() - start_time, 'finished'

