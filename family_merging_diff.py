import psycopg2
from datetime import datetime
import csv
from collections import defaultdict

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select ucid, normasg from dolceradata.normalizedcompany"

cur.execute(query)
writer = writeCSV("family_merging_diff.csv")
family_merg_diff = []

counter = 0
for idx,rec in enumerate(cur):
    ucid, normasg = rec
    counter = counter + 1
    if counter%100000 == 0:
        print counter
    try:
        if u'ori_currentassigneenorm' in normasg:
            family_merg_diff.append([ucid, normasg, normasg[u'currentassigneesnorm'], normasg[u'ori_currentassigneenorm']])
            writer.writerow([ucid,normasg, normasg[u'currentassigneesnorm'], normasg[u'ori_currentassigneenorm']])
    except:
        continue
