import unicodedata
from collections import defaultdict
def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(c for c in unicodedata.normalize('NFD', unicode(s,'latin')) if unicodedata.category(c) != 'Mn')  
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

assgn_std = {}
std_assgn = defaultdict(list)
reader = readCSV("assgn_freq_dist.csv")
for row in reader:
    assgn_std[row[0]] = row[1]
    std_assgn[row[1]].append(row[0])

assgstds = set()
reader = readCSV("CLEANMATCH.csv")
for row in reader:
    if len(row)>2:
        assgstds.add(row[1])
        
business_entities = set()
reader = readCSV("Bussiness.csv")
for row in reader:
    business_entities.add(str(strip_accents(row[0])))
    
assignees = set()
multiplestd_assgns = []
for i in assgstds:
    if len(std_assgn[i]) ==1:
        assg = std_assgn[i][0]
        assignees.add(assg)
    else:
        multiplestd_assgns.append(std_assgn[i])
        
probable_inventors = set()
writer = writeCSV("probable_inventors.csv")
for i in assignees:
    flag = 1
    for j in business_entities:
        if j.lower() in i.lower():
            flag = 0
    if flag == 1:
        writer.writerrow([i])
