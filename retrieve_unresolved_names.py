import psycopg2
from datetime import datetime
import csv

def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor()
query = "select company_name,immparent_name from dolceradata.full_corporate_tree"
names = set()
cur.execute(query)
names = set()

for i,j in cur:
    names.add(i)
    names.add(j)

conn = psycopg2.connect(
    "dbname='alexandria' user='alexandria' host='db1-newnew.dolcera.net' password='pergola-uncross-linseed' port='5434'")
cur = conn.cursor('iter-1')
cur.itersize = 10000
query = "select normasg from dolceradata.normalizedcompany"

cur.execute(query)
writer = writeCSV("normasg.csv")

asgnames = set()

counter = 0
for i in cur:
    counter = counter + 1
    if counter%100000 == 0:
        print counter
    try:
        if i[0].get(u'currentassigneesnormdata',[])!= []:
            if i[0].get(u'currentassigneesnormdata',[])[0].get(u"isnorm","") == u'Lookup':
                continue
            else:
                try:
                    cleaned = str(i[0].get(u'currentassigneesnormdata',["",""])[0].get(u'cleaned',""))
                except:
                    cleaned = i[0].get(u'currentassigneesnormdata',["",""])[0].get(u'cleaned',"")
                try:
                    parent = str(i[0].get(u'currentassigneesnormdata',["",""])[0].get(u'parent',""))
                except:
                    parent = i[0].get(u'currentassigneesnormdata',["",""])[0].get(u'parent',"")
                if cleaned not in names:
                    asgnames.add(cleaned)
                if parent not in names:
                    asgnames.add(parent)
    except:
        continue

for i in asgnames:
    writer.writerow(tuple([i]))
