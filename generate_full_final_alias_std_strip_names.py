import requests
import unicodedata
import json
# !/usr/bin/env python
from cleanco import cleanco
from unidecode import unidecode_expect_nonascii
from collections import defaultdict
import re
# CAPIQ_CSV_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/FoundationCompanyReformatted.txt/"
# CAPIQ_FILEPATH="/Users/dolcera/wd/rc/CIQ/FC/"
# asn_path="/Users/dolcera/wd/Dolceraprojects/distributed_asn 2/lib/"
# ADDRESS="work1.sm.dolcera.net"
import csv
import sys
import tldextract
# from addressmatcherelastic import driver
import ast
import solr
from cleanco import cleanco
from unidecode import unidecode
import operator
import psycopg2


def removeAllPunctuations(g):
    g = g.replace(".", "")
    g = g.replace(",", "")
    g = g.replace("'", "")
    g = g.replace("-", " ")
    g = g.replace("/", "")
    g = g.replace(":", "")
    g = g.replace(";", "")
    g = g.replace("+", " ")
    g = g.replace('"', "")
    g = g.replace("*", "")
    g = g.replace("?", " ")
    g = g.replace("[", " ")
    g = g.replace("]", " ")
    g = g.replace("(", " ")
    g = g.replace(")", " ")
    g = g.replace("<", " ")
    g = g.replace(">", " ")
    g = g.replace("=", " ")
    g = g.replace(",", " ")
    g = g.replace("&", "AND")
    g = re.sub('\s+', ' ', g).strip()
    return g


ASN_PATH = ""
CAPIQ_FILEPATH = ""


def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'latin-1')) if unicodedata.category(c) != 'Mn')
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def removeSpaces(name):
    while "  " in name:
        name = name.replace("  ", " ")
    return name.strip()


class indexCapIq(object):
    def __init__(self):
        self.processVariations()
        self.processVariations1()
        self.articles = [" THE ", " AND "]

    def getCountry(self, country):
        return self.countryCode.get(country, "")

    def processVariations(self):
        reader = readCSVori(CAPIQ_FILEPATH + "AN_variations_modified.csv")
        self.AN_variations = {}
        for row in reader:
            self.AN_variations[row[0].strip()] = row[1].strip()

    def processVariations1(self):
        reader = readCSVori(CAPIQ_FILEPATH + "Bussiness.csv")
        self.replacers = []
        for row in reader:
            self.replacers.append(" " + row[0].upper() + " ")
        self.replacers.append(" " + "CO" + " ")

    def is_ascii(self, s):
        return all(ord(c) < 128 for c in s)

    def strip_accents(self, s):
        if type(s) == str:
            try:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
            except:
                return ''.join(
                    c for c in unicodedata.normalize('NFD', unicode(s, 'latin')) if unicodedata.category(c) != 'Mn')
        else:
            return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

    def getStandardizedName(self, name):
        if name == "":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name = self.strip_accents(name)
        name = unidecode_expect_nonascii(name.replace(".", "").replace(",", " "))
        name = removeSpaces(name)
        name = removeAllPunctuations(name)
        name = " " + name.upper() + " "
        for v in self.AN_variations:
            newv = " " + v + " "
            name = name.replace(newv, " " + self.AN_variations[v] + " ")
        name = removeSpaces(name)
        return name.strip()

    def getStrippedName(self, name):
        if name == "":
            return ""
        if not self.is_ascii(name[0]):
            return name
        name = unidecode_expect_nonascii(self.strip_accents(name))
        stripped_name = cleanco(name).clean_name()
        stripped_name = " " + stripped_name + " "
        for r in self.replacers:
            stripped_name = stripped_name.replace(r, " ")
        for r in self.articles:
            stripped_name = stripped_name.replace(r, " ")
        stripped_name = removeSpaces(stripped_name)
        return stripped_name.strip()


def readCSV(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def readCSVori(path):
    import csv
    reader = csv.reader(open(path, 'rU'), delimiter=',', quotechar='"')
    return reader


def writeCSV(path, mode="w"):
    import unicodecsv
    myfile = open(path, mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',', quotechar='"', lineterminator='\n')
    return fileOutput


session = requests.session()
icq = indexCapIq()


def capiqMapping(website, search, cities):
    cities = ['"' + unidecode_expect_nonascii(ct.replace(" AND ", " ")).replace("&", "") + '"' for ct in cities]
    cities = "(" + " OR ".join(cities) + ")"
    search = unidecode_expect_nonascii(search.split("(")[0])
    search1 = icq.getStandardizedName(search)
    search2 = icq.getStrippedName(search)
    search1 = search1.replace('"', "")
    search2 = search2.replace('"', "")

    url_search1 = "http://work1:8983/solr/companycore/select?indent=on&q=(standardized_name_keywords:%22{search1}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search1 = url_search1.replace("{search1}", search1.replace("&", "%26"))
    url_search1 = url_search1.replace(" ", "%20")
    try:
        data_search1 = session.get(url_search1).json()
    except IndexError, e:
        data_search1 = ""

    url_search2 = "http://work1:8983/solr/companycore/select?indent=on&q=(stripped_name_keywords:%22{search2}%22) &wt=json&sort=subsidiary_count%20desc&rows=40&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url_search2 = url_search2.replace("{search2}", search2.replace("&", "%26"))
    url_search2 = url_search2.replace(" ", "%20")
    try:
        data_search2 = session.get(url_search2).json()
    except IndexError, e:
        data_search2 = ""

    return data_search1, data_search2

def strip_accents(s):
    if type(s) == str:
        try:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'utf-8')) if unicodedata.category(c) != 'Mn')
        except:
            return ''.join(
                c for c in unicodedata.normalize('NFD', unicode(s, 'latin')) if unicodedata.category(c) != 'Mn')
    else:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def capiqMap_SubCnt(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}", id)
    url = url.replace(" ", "%20")
    data = session.get(url).text
    try:
        data = session.get(url).text.replace("\n", "||")
    except IndexError, e:
        return "Error!!"
    data = data.replace(
        "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||",
        "")
    data = unidecode_expect_nonascii(strip_accents(data))
    if data == "":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit) > 0:
        return msplit[0]


def capiqMap_geography(id):
    url = "http://work1:8983/solr/companycore/select?indent=on&q=(id:%22{id}%22) &wt=csv&sort=subsidiary_count%20desc&rows=1&fl=id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country"
    url = url.replace("{id}", id)
    url = url.replace(" ", "%20")
    data = session.get(url).text
    try:
        data = session.get(url).text.replace("\n", "||")
    except IndexError, e:
        return "Error!!"
    data = data.replace(
        "id,original_name,ultimateParentID,ultimateFlag,immParent,ultiParent,industry,subsidiary_count,city,website,country||",
        "")
    data = unidecode_expect_nonascii(strip_accents(data))
    if data == "":
        return ""
    msplit = list(csv.reader([data]))
    if len(msplit) > 0:
        return msplit[0]

iq= indexCapIq()
con2 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
con3 = psycopg2.connect(
"dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")
cur2 = con2.cursor()
cur3 = con3.cursor()
query2 = "select * from dolcera.rerun_final_alias_table_orig"
query3 = "insert into dolcera.rerun_final_full_info_alias_table values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

writer = writeCSV("final_full_alias_table.csv")

counter = 0
cur2.execute(query2)
for row in cur2:
    counter += 1
    if counter%10000 == 0:
        print counter
    array = []
    array.extend(row)
    std_name = iq.getStandardizedName(row[1])
    strip_name = iq.getStrippedName(row[1])
    clst_std_name = iq.getStandardizedName(row[0])
    clst_strip_name = iq.getStrippedName(row[0])
    array.extend([std_name,strip_name,clst_std_name,clst_strip_name])
    writer.writerow(array)
#     if counter%100 == 0:
#         con2.commit()
#         print counter
#         break
# con2.commit()
