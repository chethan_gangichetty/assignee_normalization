from __future__ import division
import regex
from collections import defaultdict
import operator
from unidecode import unidecode
import ast
import psycopg2

def readCSV(path):
    import csv
    reader = csv.reader(open(path,'rU'), delimiter=',', quotechar='"')
    return reader

def writeCSV(path,mode="w"):
    import unicodecsv
    myfile=open(path,mode)
    fileOutput = unicodecsv.writer(myfile, delimiter=',',quotechar='"',lineterminator='\n')
    return fileOutput

con = psycopg2.connect(
    "dbname='alexandria' user='alexandria'  password='snitch-betimes-froze' host='localhost' port=5432")

cur_final = con.cursor()
query_final = "select * from dolcera.an_final_alias_table order by frequency"
cur_final.execute(query_final)

zzz = writeCSV("Diff_FinalSampleAlias.csv")
yyy = writeCSV("Diff_FinalSampleAlias2.csv")

set_table = set()
set_file = set()

for ulti_name, orig_name, countries, cities, freq in cur_final:
    set_table.add((ulti_name.strip(),orig_name.strip()))

reader = readCSV('/home/analytics/rsynced_from_old_work1/Developer/Assignee_Normalization_Scripts/FinalSampleAlias.csv')
for row in reader:
    set_file.add((row[0].strip(),row[1].strip()))

set_diff1 = set_table - set_file
set_diff2 = set_file - set_table

for row in list(set_diff1):
    zzz.writerow(row)

for row in list(set_diff2):
    yyy.writerow(row)
